import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {useAppSelector} from "./app/hooks";

import Categories from "./components/Categories/Categories";
import ProtectedRoute from "./components/Auth/ProtectedRoute";
import Navbar from './components/Navbar/Navbar'
import ShoppingCart from './components/ShoppingCart/ShoppingCart'
import Products from './components/Products/Products';
import SearchPageNew from './components/SearchPage/SearchPage';
import ProductPage from './components/Products/ProductPage/ProductPage';
import Footer from './components/Footer/Footer';
import MenuItems from './components/Navbar/MenuItems';
import {IProduct} from './interfaces/Products/IProduct';
import {ICartItem} from './interfaces/Orders/ICartItem';
import {ISearchParams} from "./interfaces/Products/ISearchParams";
import {DateManipulation} from './components/Utils/DateManipulation';
import {ServiceSecurityType} from "./interfaces/Orders/ServiceSecurityType";
import {AccountType} from "./interfaces/Accounts/AccountType";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";

function App() {
    const [cartItems, setCartItems] = useState([] as ICartItem[]);
    const isLogged = useAppSelector(state => state.auth.isLogged);

    const [searchParams, setSearchParams] = useState({
        availability: true,
        categoryId: "",
        pageNum: 1,
        pageSize: 5,
        priceFrom: null,
        priceTo: null,
        productName: "",
        sortDescending: ""
    } as ISearchParams);

    const payPalUrl = process.env.REACT_APP_PAYPAL_URL;
    const payPalClientID = process.env.REACT_APP_PAYPAL_CLIENT_ID;
    const apiUrl = `${payPalUrl}?client-id=${payPalClientID}&disable-funding=card,blik,p24&currency=PLN`;

    useEffect(() => {
        const payPalScript = document.createElement("script");
        payPalScript.src = apiUrl;
        document.head.appendChild(payPalScript);
    }, [apiUrl]);

    useEffect(() => {
        const items = localStorage.getItem('cartItems');
        let parsedItems: any[] = [];
        if (items) {
            parsedItems = JSON.parse(items);
        }
        const lastCartItems: ICartItem[] = [];
        parsedItems.forEach(
            e => {
                lastCartItems.push({
                    categoryID: Number(e['categoryID']),
                    id: Number(e['id']),
                    name: String(e['name']),
                    imageUrl: String(e['imageUrl']),
                    description: String(e['description']),
                    beginDate: new Date(e['beginDate']),
                    endDate: new Date(e['endDate']),
                    amount: Number(e['amount']),
                    price: Number(e['price']),
                    deposit: Number(e['deposit']),
                    insurance: Number(e['insurance']),
                    serviceSecurityType: String(e['serviceSecurityType']),
                } as ICartItem)
            }
        );
        setCartItems(lastCartItems);
    }, [isLogged])

    React.useEffect(() => {
        localStorage.setItem('cartItems', JSON.stringify(cartItems));
    }, [cartItems]);


    const handleAddToCart = (clickedItem: IProduct, beginDate: Date, endDate: Date) => {
        setCartItems(prev => {
            const isItemInCart = prev.find(item => item.id === clickedItem.id);
            if (!isItemInCart || prev === []) {
                return [...prev, {
                    ...clickedItem,
                    beginDate: beginDate,
                    endDate: endDate,
                    amount: DateManipulation.getDiffInDays(beginDate, endDate),
                    serviceSecurityType: ServiceSecurityType.insurance
                }];
            }
            return [...prev]
        });
    };

    const handleRemoveFromCart = (id: number) => {
        setCartItems(prev =>
            prev.reduce((ack, item) => {
                if (item.id === id) {
                    return ack;
                } else {
                    return [...ack, item];
                }
            }, [] as ICartItem[])
        );
    };

    const handleClearCart = () => {
        setCartItems([] as ICartItem[]);
    };

    const handleIsInCart = (id: number) => {
        const foundItem = cartItems.find(item => item.id === id);
        return !!foundItem;
    };

    const handleChangeStartDate = (product: ICartItem, date: any) => {
        setCartItems(prev => {
            const isItemInCart = prev.find(item => item.id === product.id);
            if (isItemInCart) {
                return prev.map(item =>
                    item.id === product.id
                        ? {...item, amount: DateManipulation.getDiffInDays(date, item.endDate), beginDate: date}
                        : item
                );
            }
            return [...prev];
        });
    };

    const handleChangeEndDate = (product: ICartItem, date: any) => {
        setCartItems(prev => {
            const isItemInCart = prev.find(item => item.id === product.id);
            if (isItemInCart) {
                return prev.map(item =>
                    item.id === product.id
                        ? {...item, amount: DateManipulation.getDiffInDays(item.beginDate, date), endDate: date}
                        : item
                );
            }
            return [...prev];
        });
    };

    const handleChangeServiceSecurity = (product: ICartItem, type: ServiceSecurityType) => {
        setCartItems(prev => {
            const isItemInCart = prev.find(item => item.id === product.id);
            if (isItemInCart) {
                return prev.map(item =>
                    item.id === product.id
                        ? {...item, serviceSecurityType: type}
                        : item
                );
            }
            return [...prev];
        });
    };

    const handleChangeSearchInput = (newSearchInput: string) => {
        setSearchParams(prevState => ({
            ...prevState, productName: newSearchInput, pageNum: 1
        }));
    };

    return (
        <Router>
            <div className="d-flex flex-column min-vh-100">
                <Navbar setSearchInput={handleChangeSearchInput} cartItems={cartItems}/>
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/home"/>
                    </Route>
                    {MenuItems.map((item, index) => {
                        return (
                            <ProtectedRoute
                                roles={item.roles}
                                component={item.componentName}
                                path={item.url}
                                key={"menuItem_" + index}
                            >
                            </ProtectedRoute>
                        )
                    })},
                    <ProtectedRoute
                        component={ShoppingCart}
                        roles={[AccountType.none, AccountType.customer]}
                        path={'/shopping-cart'}
                        componentProps={{
                            cartItems: cartItems,
                            removeFromCart: handleRemoveFromCart,
                            clearCart: handleClearCart,
                            changeStartDate: handleChangeStartDate,
                            changeEndDate: handleChangeEndDate,
                            changeServiceSecurity: handleChangeServiceSecurity
                        }}
                    />,
                    <Route path='/products' render={() =>
                        <Products
                            cartItems={cartItems}
                            addToCart={handleAddToCart}
                            removeFromCart={handleRemoveFromCart}
                            isInCart={handleIsInCart}
                            categoryId={''}
                        />}/>,
                    <Route path='/search-page' render={() =>
                        <SearchPageNew
                            addToCart={handleAddToCart}
                            isInCart={handleIsInCart}
                            searchParams={searchParams}
                            setSearchParams={setSearchParams}
                        />}/>,
                    <Route exact path={`/product/details/:id/:name`}>
                        <ProductPage
                            cartItems={cartItems}
                            addToCart={handleAddToCart}
                            isInCart={handleIsInCart}
                        >
                        </ProductPage>
                    </Route>
                    <Route path='/categories' render={() =>
                        <Categories/>
                    }
                    >
                    </Route>
                    <Route path='/category/:id' render={(props) =>
                        <Products
                            cartItems={cartItems}
                            addToCart={handleAddToCart}
                            removeFromCart={handleRemoveFromCart}
                            isInCart={handleIsInCart}
                            categoryId={props.match.params.id}
                        />
                    }
                    />,
                </Switch>
                <Footer/>
            </div>
        </Router>
    );
}

export default App;
