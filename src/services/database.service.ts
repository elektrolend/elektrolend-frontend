import axios from "axios";

import {getAuthorizedHeaders, getUnauthorizedHeaders} from "../helpers/auth-header";
import {IPrice} from "../interfaces/Products/IPrice";
import {IProduct} from "../interfaces/Products/IProduct";
import {ICountry} from "../interfaces/Accounts/ICountry";
import {IState} from "../interfaces/Accounts/IState";
import {IUserProfile} from "../interfaces/Accounts/IUserProfile";
import {ICategory} from "../interfaces/Products/ICategory";
import {INewProduct} from "../interfaces/Products/INewProduct";
import {INewPrice} from "../interfaces/Products/INewPrice";
import {INewSpecification} from "../interfaces/Products/INewSpecification";
import {INewCategory} from "../interfaces/Products/INewCategory";
import {IProductOrderDates} from "../interfaces/Orders/IProductOrderDates";
import {IOrderProduct} from "../interfaces/Orders/IOrderProduct";
import {IUserOrdersInfo} from "../interfaces/Orders/IUserOrdersInfo";
import {OrderProductState} from "../interfaces/Orders/OrderProductState";
import {IOrdersParams} from "../interfaces/Orders/IOrdersParams";
import {IProductsResponse} from "../interfaces/Products/IProductsResponse";
import {IOrderSummary} from "../interfaces/Orders/IOrderSummary";
import {IOrderChangeStatus} from "../interfaces/Orders/IOrderChangeStatus";
import {IComplaint} from "../interfaces/Complaints/IComplaint";
import {IComplaintDTO} from "../interfaces/Complaints/IComplaintDTO";
import {IRefund} from "../interfaces/Refund/IRefund";
import {IPayment} from "../interfaces/Orders/IPayment";
import {IPaymentAccount} from "../interfaces/Orders/IPaymentAccount";
import {ICategoriesReport} from "../interfaces/Reports/ICategoriesReport";
import {IOrderProductsReport} from "../interfaces/Reports/IOrderProductsReport";
import {IOrderProductStatuses} from "../interfaces/Orders/IOrderProductStatuses";
import {IProductActivation} from "../interfaces/Products/IProductActivation";
import {IProductsParams} from "../interfaces/Products/IProductsParams";
import {IOfficeBoxDailyDetail} from "../interfaces/Orders/IOfficeBoxDailyDetail";
import {IEditProduct} from "../interfaces/Products/IEditProduct";
import {IUsersReport} from "../interfaces/Reports/IUsersReport";
import {ICustomerNewPassword} from "../interfaces/Accounts/ICustomerNewPassword";
import {ISearchParams} from "../interfaces/Products/ISearchParams";

const url = process.env.REACT_APP_API_URL;

export const findPrice = (productID: number, prices: IPrice[] | undefined) => {
    const price = prices?.find((p: IPrice) => p.productID === productID);
    if (price) {
        return price.pricePerDayBrutto;
    }
    return 200;
};

export const findCountryId = (countryName: string, countries: ICountry[] | undefined) => {
    const country = countries?.filter((c) => c.name === countryName)[0];
    if (country) {
        return country.id;
    }
    return 200;
};

export const getProducts = async (): Promise<IProductsResponse> => {
    return (await fetch(url + 'products', {headers: getUnauthorizedHeaders()})).json();
};

export const getFilteredProducts = async (productsParams: IProductsParams): Promise<IProductsResponse> => {
    const pageSizeParam = `pageSize=${productsParams.pageSize}`;
    const pageNumberParam = `&pageNum=${productsParams.pageNumber}`;
    let categoryParam = ''
    if (productsParams.categoryId !== '') {
        categoryParam = `&categoryId=${productsParams.categoryId}`;
    }
    const availabilityParam = `&availability=${productsParams.availability}`;
    return (await fetch(url + 'products?' + pageSizeParam + pageNumberParam + categoryParam
        + availabilityParam, {headers: getUnauthorizedHeaders()})).json();
};

export const getCategories = async (): Promise<ICategory[]> => {
    return (await fetch(url + 'categories', {headers: getUnauthorizedHeaders()})).json();
};

export const getPrices = async (): Promise<IPrice[]> => {
    return (await fetch(url + 'prices', {headers: getUnauthorizedHeaders()})).json();
};

export const getCountries = async (): Promise<ICountry[]> => {
    return (await fetch(url + 'countries', {headers: getUnauthorizedHeaders()})).json();
};

export const getStates = async (): Promise<IState[]> => {
    return (await fetch(url + 'states', {headers: getUnauthorizedHeaders()})).json();
};

export const getComplaints = async (): Promise<IComplaintDTO[]> => {
    return (await fetch(url + 'complaints', {headers: getAuthorizedHeaders()})).json();
};

export const getProduct = async (productID: string | number): Promise<IProduct> => {
    return (await fetch(url + `products/${productID}`, {headers: getUnauthorizedHeaders()})).json();
};

export const getComplaint = async (complaintID: number): Promise<IComplaintDTO> => {
    return (await fetch(url + `complaints/${complaintID}`, {headers: getAuthorizedHeaders()})).json();
};

export const getOrderProduct = async (orderProductID: number): Promise<IOrderProduct> => {
    return (await fetch(url + `order-products/${orderProductID}`, {headers: getUnauthorizedHeaders()})).json();
};

export const getImage = async (imageUrl: string): Promise<any> => {
    return (await fetch(imageUrl, {headers: getUnauthorizedHeaders()})).json();
};

export const getUser = async (userID: number): Promise<IUserProfile> => {
    return (await fetch(url + `users/${userID}`, {headers: getAuthorizedHeaders()})).json();
};

export const getPaymentAccount = async (orderProductID: number): Promise<IPaymentAccount> => {
    return (await fetch(url + `payments/order-product/${orderProductID}`, {headers: getAuthorizedHeaders()})).json();
};

export const addProduct = async (product: INewProduct) => {
    return axios.post(url + `products`, product, {headers: getAuthorizedHeaders()});
};

export const addCategory = async (category: INewCategory) => {
    return axios.post(url + `categories`, category, {headers: getAuthorizedHeaders()});
};

export const addPrice = async (price: INewPrice) => {
    return axios.post(url + `prices`, price, {headers: getAuthorizedHeaders()});
};

export const addPayment = async (payment: IPayment) => {
    return axios.post(url + `payments`, payment, {headers: getAuthorizedHeaders()});
};

export const addEmployee = async (employee: any) => {
    return axios.post(url + `employees`, employee, {headers: getAuthorizedHeaders()});
};

export const addComplaint = async (complaint: IComplaint) => {
    return axios.post(url + `complaints`, complaint, {headers: getAuthorizedHeaders()});
};

export const updateComplaint = async (complaintID: number, refund: IRefund) => {
    return axios.patch(url + `complaints/${complaintID}`, refund, {headers: getAuthorizedHeaders()});
};

export const editProduct = async (productID: number, product: IEditProduct) => {
    return axios.put(url + `products/${productID}`, product, {headers: getAuthorizedHeaders()});
};

export const updateProduct = async (productID: number, productActivation: IProductActivation) => {
    return axios.post(url + `products/availability/${productID}`, productActivation, {headers: getAuthorizedHeaders()});
};

export const addSpecification = async (specification: INewSpecification) => {
    return axios.post(url + `specifications`, specification, {headers: getAuthorizedHeaders()});
};

export const getDisabledDates = async (productID: number): Promise<IProductOrderDates> => {
    return (await fetch(url + `products/${productID}/availability`, {headers: getUnauthorizedHeaders()})).json();
};

export const addOrder = async (data: any) => {
    return fetch(url + 'orders/create-order', {
        headers: getAuthorizedHeaders(),
        method: 'POST',
        body: JSON.stringify(data)
    });
};

export const getOrderProducts = async (orderID: number): Promise<IOrderSummary> => {
    return (await fetch(url + `orders/${orderID}`, {headers: getAuthorizedHeaders()})).json();
};

export const getUserOrders = async (ordersParams: IOrdersParams): Promise<IUserOrdersInfo> => {
    const userIdParam = `users/${ordersParams.userID}/orders?`;
    const pageSizeParam = `pageSize=${ordersParams.pageSize}`;
    const pageNumberParam = `&pageNum=${ordersParams.pageNumber}`;
    const allStates = ordersParams.orderProductState !== OrderProductState.none;
    const orderProductStateParam = allStates ? `&orderProductState=${ordersParams.orderProductState}` : "";
    const requestUrl = url + userIdParam + pageSizeParam + pageNumberParam + orderProductStateParam;
    return (await fetch(requestUrl, {headers: getAuthorizedHeaders()})).json();
};

export const changeProfileData = async (data: any, userID: number) => {
    return fetch(url + 'users/' + userID, {
        headers: getAuthorizedHeaders(),
        method: 'PUT',
        body: JSON.stringify(data)
    });
};

export const changePassword = async (data: any, userID: number) => {
    delete data['repeatedPassword'];
    return axios.patch(url + 'users/login/' + userID, data, {headers: getAuthorizedHeaders()});
};

export const changeNick = async (data: any, userID: number) => {
    return axios.patch(url + 'users/login/' + userID, data, {headers: getAuthorizedHeaders()});
};

export const changeRefund = async (orderProductID: number) => {
    return axios.patch(url + `refunds/order-product/${orderProductID}`, {}, {headers: getAuthorizedHeaders()});
};

export const getAllOrderProducts = async (): Promise<IOrderProduct[]> => {
    return (await fetch(url + 'order-products', {headers: getUnauthorizedHeaders()})).json();
};

export const getCategory = async (categoryID: string | number): Promise<ICategory> => {
    return (await fetch(url + `categories/${categoryID}`, {headers: getUnauthorizedHeaders()})).json();
};

export const changeOrderProductsStates = async (orderChangeStatus: IOrderChangeStatus) => {
    return fetch(url + `order-products/status`, {
        headers: getAuthorizedHeaders(),
        method: 'POST',
        body: JSON.stringify(orderChangeStatus)
    });
};

export const updateOrderProductState = (orderProductID: number, state: OrderProductState) => {
    return changeOrderProductsStates({
        productList: [
            {
                orderProductID: orderProductID,
                orderProductState: state
            }
        ]
    });
};

export const getCategoriesReport = async (beginDate: string, endDate: string): Promise<ICategoriesReport> => {
    const fromParam = `from=${beginDate}`;
    const toParam = `&to=${endDate}`;
    return (await fetch(url + 'reports/categories?' + fromParam + toParam, {headers: getAuthorizedHeaders()})).json();
};

export const getUsersReport = async (beginDate: string, endDate: string): Promise<IUsersReport> => {
    const fromParam = `from=${beginDate}`;
    const toParam = `&to=${endDate}`;
    return (await fetch(url + 'reports/user-loans?' + fromParam + toParam, {headers: getAuthorizedHeaders()})).json();
};

export const getOrderProductsReport = async (beginDate: string, endDate: string): Promise<IOrderProductsReport> => {
    const fromParam = `from=${beginDate}`;
    const toParam = `&to=${endDate}`;
    return (await fetch(url + 'reports/order-products?' + fromParam + toParam, {headers: getAuthorizedHeaders()})).json();
};

export const getOrderProductStatuses = async (): Promise<IOrderProductStatuses> => {
    return (await fetch(url + 'order-products/statuses', {headers: getAuthorizedHeaders()})).json();
};

export const getOfficeBoxStatuses = async (date: string): Promise<IOfficeBoxDailyDetail[]> => {
    const dateParam = `date=${date}`;
    return (await fetch(url + 'office-boxes/statuses?' + dateParam, {headers: getAuthorizedHeaders()})).json();
};

export const getOrders = async (): Promise<IOrderSummary[]> => {
    return (await fetch(url + 'orders', {headers: getAuthorizedHeaders()})).json();
};

export const getSearchProducts = async (searchParams: ISearchParams): Promise<IProductsResponse> => {
    const defaultUrl = url + `products/search-page?`;
    const pageSizeParam = searchParams.pageSize ? `pageSize=${searchParams.pageSize}` : "";
    const pageNumberParam = searchParams.pageNum ? `&pageNum=${searchParams.pageNum}` : "";
    const availabilityParam = searchParams.availability ? `&availability=${searchParams.availability}` : "";
    const categoryIdParam = searchParams.categoryId ? `&categoryId=${searchParams.categoryId}` : "";
    const productNameParam = searchParams.productName ? `&productName=${searchParams.productName}` : "";
    const priceFromParam = searchParams.priceFrom ? `&priceFrom=${searchParams.priceFrom}` : "";
    const priceToParam = searchParams.priceTo ? `&priceTo=${searchParams.priceTo}` : "";
    const sortDescendingParam = searchParams.sortDescending ? `&sortDescending=${searchParams.sortDescending}` : "";
    const requestUrl = defaultUrl + pageSizeParam + pageNumberParam + availabilityParam + categoryIdParam + productNameParam + priceFromParam + priceToParam + sortDescendingParam;
    return (await fetch(requestUrl, {headers: getUnauthorizedHeaders()})).json();
};

export const restorePassword = async (email: string): Promise<any> => {
    return axios.post(url + `users/password-restoring-email`, {email: email}, {headers: getUnauthorizedHeaders()});
};

export const changeCustomerPassword = async (customerNewPassword: ICustomerNewPassword): Promise<any> => {
    return axios.post(url + `users/password-restoring`, customerNewPassword, {headers: getAuthorizedHeaders});
};
