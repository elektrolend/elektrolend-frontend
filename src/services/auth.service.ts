import axios from "axios";
import {Dispatch} from "redux";

import {
    loginFail,
    loginSuccess,
    logOut,
    registerFail,
    registerStart,
    registerSuccess
} from "../components/Auth/AuthSlice";
import {getUnauthorizedHeaders} from "../helpers/auth-header";
import {IUser} from "../interfaces/Accounts/IUser";
import {ICartItem} from "../interfaces/Orders/ICartItem";
import {ISignInData} from "../interfaces/Accounts/ISignInData";

const API_URL = `${process.env.REACT_APP_API_URL}users/`;

export const getAuthToken = (nickname: string, password: string) => {
    return Buffer.from(nickname + ":" + password).toString('base64');
};

export const signIn = (dispatch: Dispatch<any>, signInData: ISignInData) => {
    axios
        .post(API_URL + 'login', signInData, {headers: getUnauthorizedHeaders()})
        .then((response) => {
            if (response.status === 200) {
                logIn(dispatch, signInData, response.data);
            }
        })
        .catch((error) => {
            if (error.response) {
                dispatch(loginFail(
                    {
                        errorMessage: error.response.data,
                        status: error.response.status
                    }
                ));
            } else {
                dispatch(loginFail(
                    {
                        errorMessage: 'Brak połączenia z serwerem',
                        status: 500
                    }));
            }
        });
};

export const login = (username: string, password: string) => {
    return axios.post(API_URL + 'login', {
        nickname: username,
        password: password
    }, {headers: getUnauthorizedHeaders()});
};

export const register = (userRegisterData: IUser, dispatch: Dispatch<any>) => {
    dispatch(registerStart());
    axios
        .post(API_URL + "register", userRegisterData, {headers: getUnauthorizedHeaders()})
        .then((response) => {
            if (response.status === 201) {
                dispatch(registerSuccess(201));
            }
        })
        .catch(
            error => {
                if (error.response.status === 409) {
                    dispatch(registerFail("Takie konto już istnieje."))
                } else if (error.response.status === 400) {
                    dispatch(registerFail("Niepoprawne dane."))
                } else {
                    dispatch(registerFail("Brak połączenia z serwerem."))
                }
            }
        );
};

export const logIn = (dispatch: Dispatch<any>, signInData: ISignInData, responseData: any) => {
    const authCredentials = responseData;
    authCredentials['authToken'] = getAuthToken(signInData.nickname, signInData.password);
    dispatch(loginSuccess(authCredentials));
};

export const signOut = (dispatch: Dispatch<any>) => {
    localStorage.setItem("cartItems", JSON.stringify([] as ICartItem[]));
    dispatch(logOut());
};
