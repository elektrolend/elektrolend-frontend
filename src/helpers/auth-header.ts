import {store} from "../app/store";

const getAppState = () => {
    return store.getState();
};

export const getUnauthorizedHeaders = () => {
    return {
        'Content-Type': 'application/json; charset=utf-8',
    }
};

export const checkPermissions = (state: number, errorMessage: string) => {
    if (state === 401) {
        return "Twoje dane nie występują w bazie danych!";
    }

    if (state === 403) {
        return "Nie posiadasz odpowiednich uprawnień!";
    }

    return errorMessage;
};

export const getAuthorizedHeaders = () => {
    const authToken = getAppState().auth.authToken || "";
    return {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Basic ' + authToken
    }
};

store.subscribe(getAppState);
