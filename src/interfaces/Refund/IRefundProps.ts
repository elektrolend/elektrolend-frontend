import {IRefundMethod} from "./IRefundMethod";

export type IRefundProps = {
    refundMethod: IRefundMethod;
    paymentAccount: string;
    refundAmount: number;
    onSubmit: () => void;
}
