import {FC} from "react";
import {IRefundProps} from "./IRefundProps";

export interface IRefundMethod {
    component: FC<IRefundProps>;
    title: string;
    imageSrc: string;
}
