import {AccountType} from "./AccountType";

export interface IUserAuth {
    id: number;
    nickname: string;
    accountType: AccountType;
}
