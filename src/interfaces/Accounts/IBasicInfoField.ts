import {IconDefinition} from "@fortawesome/free-solid-svg-icons";

export interface IBasicInfoField {
    name: string;
    type: string;
    icon: IconDefinition;
}
