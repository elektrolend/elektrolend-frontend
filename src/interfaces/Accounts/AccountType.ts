export enum AccountType {
    none = "",
    customer = "CUSTOMER",
    regular = "REGULAR",
    admin = "ADMIN"
}
