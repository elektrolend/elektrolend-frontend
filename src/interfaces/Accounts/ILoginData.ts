import {IAuthData} from "./IAuthData";

export interface ILoginData extends IAuthData {
    nickname: string;
}
