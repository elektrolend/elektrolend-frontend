export interface IFieldInfo {
    label: string;
    name: string;
    type: string;
    cName: string;
}
