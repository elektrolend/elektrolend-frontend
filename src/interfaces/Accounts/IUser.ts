export interface IUser {
    email: string;
    nickname: string;
    password: string;
    repeatedPassword?: string;
    name: string;
    surname: string;
    phone: string;
    street: string;
    homeNumber: string;
    localNumber: string;
    zipCode: string;
    city: string;
    country: string;
    state: string;
}
