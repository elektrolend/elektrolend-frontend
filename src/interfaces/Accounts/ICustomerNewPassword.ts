export interface ICustomerNewPassword {
    code: string;
    email: string;
    nickname: string;
    password: string;
}
