import {AccountType} from "./AccountType";

export interface IEmployee {
    nickname: string;
    password: string;
    phone: string;
    email: string;
    name: string;
    surname: string;
    employeeType: AccountType.regular | AccountType.admin
}
