import {IUserAuth} from "./IUserAuth";

export interface IAuthCredentials extends IUserAuth {
    authToken: string;
}
