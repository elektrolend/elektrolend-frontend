import {IAuthData} from "./IAuthData";

export interface IPasswordData extends IAuthData {
    password: string;
    repeatedPassword: string;
}
