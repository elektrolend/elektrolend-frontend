export interface ICountry {
    id: number;
    name: string;
    acronym: string;
}
