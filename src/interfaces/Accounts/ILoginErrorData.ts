export interface ILoginErrorData {
    errorMessage: string;
    status: number;
}
