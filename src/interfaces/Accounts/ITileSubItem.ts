import {AccountType} from "./AccountType";

export interface ITileSubItem {
    title: string;
    url: string;
    component: any;
    roles: AccountType[];
}
