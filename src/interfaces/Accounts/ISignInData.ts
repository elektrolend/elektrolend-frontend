export interface ISignInData {
    nickname: string;
    password: string;
    registrationCode: string | null;
}
