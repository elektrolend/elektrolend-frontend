import {IUserAuth} from "./IUserAuth";

export interface IUserAuthData extends IUserAuth {
    password: string;
}
