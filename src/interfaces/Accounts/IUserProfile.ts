import {IUserAuth} from "./IUserAuth";

export interface IUserProfile extends IUserAuth {
    email: string;
    name: string;
    surname: string;
    phone: string;
    street: string;
    homeNumber: string;
    localNumber: string;
    zipCode: string;
    city: string;
    state: string;
    country: string;
}
