import {ITileSubItem} from "./ITileSubItem";

export interface IPanelItem {
    title: string;
    subItems: ITileSubItem[];
    icon: any;
}
