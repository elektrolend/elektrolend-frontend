export interface IProfileData {
    phone: string;
    email: string;
    street: string;
    homeNumber: string;
    localNumber: string;
    city: string;
    zipCode: string;
    state: string;
    country: string;
    name: string;
    surname: string;
}
