import {IProduct} from "./IProduct";
import {IPagination} from "../Orders/IPagination";

export interface IProductsResponse {
    productDTOList: IProduct[];
    paginationDTO: IPagination;
}
