export interface INewPrice {
    productID: number;
    pricePerDayNetto: number;
    pricePerDayBrutto: number,
    beginDate: string;
    endDate: string | null;
    deposit: number;
}
