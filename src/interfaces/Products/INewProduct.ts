export interface INewProduct {
    categoryID: number;
    name: string;
    imageUrl: string;
    description: string;
    deposit: number;
    insurance: number;
    isAvailable: boolean;
}
