export interface INewCategory {
    name: string;
    description: string;
    imageUrl: string;
}
