import {INewPrice} from "./INewPrice";

export interface IPrice extends INewPrice {
    id: number;
}
