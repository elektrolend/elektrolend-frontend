import {INewCategory} from "./INewCategory";

export interface ICategory extends INewCategory {
    id: number;
}
