export interface ISpecification {
    key: string;
    value: string;
}
