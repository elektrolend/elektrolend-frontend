import {ISpecification} from "./ISpecification";

export interface INewSpecification extends ISpecification {
    productID: number;
}
