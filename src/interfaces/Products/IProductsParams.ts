export interface IProductsParams {
    pageSize: number;
    pageNumber: number;
    categoryId: string;
    availability: boolean;
}
