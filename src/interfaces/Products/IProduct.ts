import {IEditProduct} from "./IEditProduct";

export interface IProduct extends IEditProduct {
    id: number;
    price: number;
}
