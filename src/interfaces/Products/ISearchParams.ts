export interface ISearchParams {
    availability: boolean;
    categoryId: string;
    pageNum: number;
    pageSize: number;
    priceFrom: number | null;
    priceTo: number | null;
    productName: string;
    sortDescending: string;
}
