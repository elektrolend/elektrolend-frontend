import {INewProduct} from "./INewProduct";

export interface IEditProduct extends INewProduct {
    specificationsMap: Map<string, string>;
    isAvailable: boolean;
}
