import {OrderProductState} from "./OrderProductState";
import {ServiceSecurityType} from "./ServiceSecurityType";

export interface IOrderProduct {
    id: number;
    officeBoxIDStart: number;
    officeBoxIDEnd: number;
    orderID?: number;
    productID: number;
    orderProductState: OrderProductState;
    orderBeginDate: string;
    orderEndDate: string;
    price: number;
    serviceSecurityType: ServiceSecurityType;
    deposit?: any;
    boxPassword: string;
}
