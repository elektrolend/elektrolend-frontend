import {ServiceSecurityType} from "./ServiceSecurityType";

export interface ICartItem {
    categoryID: number;
    id: number;
    name: string;
    imageUrl: string;
    description: string;
    beginDate: Date;
    endDate: Date;
    amount: number;
    price: number;
    deposit: number;
    insurance: number;
    serviceSecurityType: ServiceSecurityType;
}
