export enum OrderProductState {
    to_realization = "TO_REALIZATION",
    paid = "PAID",
    canceled = "CANCELED",
    in_office_box = "IN_OFFICE_BOX",
    received = "RECEIVED",
    damaged = "DAMAGED",
    not_damaged = "NOT_DAMAGED",
    in_office_box_complaint = "IN_OFFICE_BOX_COMPLAINT",
    complaint_for_consideration = "COMPLAINT_FOR_CONSIDERATION",
    complaint_accepted = "COMPLAINT_ACCEPTED",
    complaint_not_accepted = "COMPLAINT_NOT_ACCEPTED",
    back_to_office_box = "BACK_TO_OFFICE_BOX",
    back_to_office = "BACK_TO_OFFICE",
    refund = "REFUND",
    finished = "FINISHED",
    none = "NULL"
}
