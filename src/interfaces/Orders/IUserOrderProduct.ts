import {OrderProductState} from "./OrderProductState";
import {ServiceSecurityType} from "./ServiceSecurityType";
import {IOfficeBox} from "./IOfficeBox";

export interface IUserOrderProduct {
    productID: number;
    orderProductID: number;
    orderBeginDate: number[];
    orderEndDate: number[];
    serviceSecurityType: ServiceSecurityType;
    orderProductState: OrderProductState;
    orderProductPrice: number;
    boxPassword: string;
    officeBoxStart: IOfficeBox;
    officeBoxEnd: IOfficeBox;
}
