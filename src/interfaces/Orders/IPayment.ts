export interface IPayment {
    value: number;
    date: Date;
    orderID: number;
    paymentAccount: string;
}
