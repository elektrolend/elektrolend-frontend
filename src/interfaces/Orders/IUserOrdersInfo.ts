import {IPagination} from "./IPagination";
import {IUserOrder} from "./IUserOrder";

export interface IUserOrdersInfo {
    userOrderDTOList: IUserOrder[];
    paginationDTO: IPagination;
}
