import {ServiceSecurityType} from "./ServiceSecurityType";

export interface IOrderInfo {
    productID: number;
    orderBeginDate: string;
    orderEndDate: string;
    serviceSecurityType: ServiceSecurityType;
}
