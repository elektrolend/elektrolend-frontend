import {IUserOrderProduct} from "./IUserOrderProduct";

export interface IUserOrder {
    orderID: number;
    orderDate: number[];
    orderInfoList: IUserOrderProduct[];
}
