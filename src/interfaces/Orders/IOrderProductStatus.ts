import {OrderProductState} from "./OrderProductState";

export interface IOrderProductStatus {
    id: number;
    productName: string;
    customerName: string;
    orderProductState: OrderProductState
}
