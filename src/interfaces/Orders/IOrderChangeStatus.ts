import {IOrderProductState} from "./IOrderProductState";

export interface IOrderChangeStatus {
    productList: IOrderProductState[];
}
