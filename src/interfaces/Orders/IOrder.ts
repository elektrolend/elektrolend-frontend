export interface IOrder {
    id: number;
    customerID: number;
    orderDate: string;
}
