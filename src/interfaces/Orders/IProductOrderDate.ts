export interface IProductOrderDate {
    beginDate: number[];
    endDate: number[];
}
