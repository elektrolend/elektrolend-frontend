import {FC} from "react";
import {IPaymentProps} from "./IPaymentProps";

export interface IPaymentMethod {
    component: FC<IPaymentProps>;
    title: string;
    imageSrc: string;
}
