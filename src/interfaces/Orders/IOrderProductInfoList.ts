export interface IOrderProductInfoList {
    orderProductID: number;
    officeBoxIDStart: number;
    officeBoxIDEnd: number;
    officeBoxPassword: string;
}
