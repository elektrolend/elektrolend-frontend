export interface IOfficeBoxDailyDetail {
    officeBoxID: number;
    orderProductID: number | null;
    productID: number | null;
    productName: string | null;
    customerName: string | null;
}
