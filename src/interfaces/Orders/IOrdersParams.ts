import {OrderProductState} from "./OrderProductState";

export interface IOrdersParams {
    userID: number;
    pageSize: number;
    pageNumber: number;
    orderProductState: OrderProductState;
}
