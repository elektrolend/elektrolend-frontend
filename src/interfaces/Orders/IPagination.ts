export interface IPagination {
    pageNum: number;
    pageSize: number;
    totalResults: number;
}
