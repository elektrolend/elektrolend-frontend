import {IOrderProductInfoList} from "./IOrderProductInfoList";

export interface IOrderResponse {
    orderID: number;
    orderProductInfoList: IOrderProductInfoList[];
}
