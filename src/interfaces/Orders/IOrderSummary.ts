import {IOrder} from "./IOrder";
import {IOrderProduct} from "./IOrderProduct";

export interface IOrderSummary {
    order: IOrder;
    orderProductList: IOrderProduct[];
}
