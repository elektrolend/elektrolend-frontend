import {IPaymentMethod} from "./IPaymentMethod";

export type IPaymentProps = {
    paymentMethod: IPaymentMethod;
    orderID: number;
    orderAmount: number;
    handleChangeOrderStates: (paymentAccount: string) => void;
}
