import {IProductOrderDate} from "./IProductOrderDate";

export interface IProductOrderDates {
    dates: IProductOrderDate[];
}
