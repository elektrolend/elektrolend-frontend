export interface IOrderProductState {
    orderProductID: number;
    orderProductState: string;
}
