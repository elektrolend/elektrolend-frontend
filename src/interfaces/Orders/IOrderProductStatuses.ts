import {IOrderProductStatus} from "./IOrderProductStatus";

export interface IOrderProductStatuses {
    orderProducts: IOrderProductStatus[];
}
