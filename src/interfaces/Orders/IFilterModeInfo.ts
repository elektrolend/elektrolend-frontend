import {OrderProductState} from "./OrderProductState";
import {FilterState} from "./FilterState";

export interface IFilterModeInfo {
    title: string;
    state: FilterState;
    checkState: (orderProductState: OrderProductState) => boolean;
}
