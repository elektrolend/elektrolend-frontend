import {IComplaintDTO} from "./IComplaintDTO";

export interface IComplaintResponse extends IComplaintDTO {
    newBoxID: number;
    newBoxPassword: string;
}
