export interface IComplaintDTO {
    id: number;
    customerID: number;
    orderProductID: number;
    refund: number;
    date: [number, number, number];
    description: string;
    imageUrls: string[];
    isConsidered: boolean;
    customerName: string;
    productName: string;
}
