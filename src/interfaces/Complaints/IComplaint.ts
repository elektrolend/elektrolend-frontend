export interface IComplaint {
    orderProductID: number;
    description: string;
    imageUrls: string[];
}
