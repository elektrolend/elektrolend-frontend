import {IOrderProductReport} from "./IOrderProductReport";

export interface ICategoryReport {
    orderProductReportDTOList: IOrderProductReport[];
    categorySum: number;
    categoryName: string;
}
