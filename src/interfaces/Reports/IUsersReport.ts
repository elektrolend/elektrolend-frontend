import {IUserStat} from "./IUserStat";

export interface IUsersReport {
    userStats: IUserStat[]
}
