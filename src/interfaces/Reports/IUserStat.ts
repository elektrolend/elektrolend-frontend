import {IProductListForUserStats} from "./IProductListForUserStats";

export interface IUserStat {
    customerName: string;
    totalSum: number;
    productListForUserStatsDTO: IProductListForUserStats[];
}
