export interface IProductReport {
    productName: string;
    rentalNumbers: number;
}
