export interface IProductListForUserStats {
    productName: string;
    totalSum: number;
}
