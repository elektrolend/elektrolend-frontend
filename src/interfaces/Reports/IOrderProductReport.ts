export interface IOrderProductReport {
    productName: string;
    productSum: number;
}
