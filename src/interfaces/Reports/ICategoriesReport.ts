import {ICategoryReport} from "./ICategoryReport";

export interface ICategoriesReport {
    categoryReportDTOList: ICategoryReport[];
    categoriesSum: number;
}
