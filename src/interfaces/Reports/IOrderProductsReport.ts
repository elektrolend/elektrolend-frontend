import {IProductReport} from "./IProductReport";

export interface IOrderProductsReport {
    productReportDTOList: IProductReport[];
}
