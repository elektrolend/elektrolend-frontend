import React, {useState} from "react";
import {generatePath, Link} from "react-router-dom";

import PasswordBox from "../../../Orders/UpcomingOrders/OrderProductsTable/OrderProductItem/PasswordBox";
import {getProduct} from "../../../../services/database.service";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProductInfo: IOrderProduct;
    addToList: (orderProduct: IOrderProduct) => void;
    removeFromList: (id: number) => void;
    isInList: (orderProductID: number) => boolean;
};

const UnclaimedItem: React.FC<Props> = ({
                                            orderProductInfo,
                                            addToList,
                                            removeFromList,
                                            isInList,
                                        }) => {

    const [productName, setProductName] = useState(" ");
    const [productID, setProductID] = useState(-1);

    React.useEffect(() => {
        getProduct(orderProductInfo.productID)
            .then(p => {
                setProductName(p.name);
                setProductID(p.id);
            })
            .catch(() => {
                setProductName(" ");
                setProductID(-1);
            });
    }, [orderProductInfo.productID]);

    const handleChange = (e: any, orderProduct: IOrderProduct) => {
        if (!e.target.checked) {
            removeFromList(orderProduct.id);
        } else {
            addToList(orderProduct);
        }
    };
    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">
                <p>
                    <input
                        type="checkbox"
                        onChange={(e) => handleChange(e, orderProductInfo)}
                        checked={isInList(orderProductInfo.id)}>
                    </input>
                </p>
            </td>
            <td className="border-right">
                <p>{orderProductInfo.id}</p>
            </td>
            <td className="border-right">
                <Link
                    to={generatePath(`/product/details/:id/:name`, {id: productID, name: productName})}
                    target="_blank">
                    <p>{productName}</p>
                </Link>
            </td>
            <td className="border-right">
                <p>{orderProductInfo.officeBoxIDEnd}</p>
            </td>
            <td>
                <PasswordBox boxPassword={orderProductInfo.boxPassword}/>
            </td>
        </tr>
    );
};

export default UnclaimedItem;
