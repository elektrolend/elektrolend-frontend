import React from "react";
import {isEmptyArray} from "formik";

import UnclaimedItem from "./UnclaimedItem";
import {isBoxChecked, isChecked} from "../../../Utils/checkbox.service";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

import {EmojiSmileFill} from "react-bootstrap-icons";

type Props = {
    orderProducts: IOrderProduct[];
    addToList: (orderProduct: IOrderProduct) => void;
    removeFromList: (id: number) => void;
    isInList: (orderProductID: number) => boolean;
};

const UnclaimedComplaintsTable: React.FC<Props> = ({
                                                       orderProducts,
                                                       addToList,
                                                       removeFromList,
                                                       isInList,
                                                   }) => {
    const handleChangeCheckBox = (e: any) => {
        if (!isBoxChecked(e)) {
            orderProducts.forEach(o => removeFromList(o.id));
        } else {
            orderProducts.forEach(o => addToList(o));
        }
    };

    if (isEmptyArray(orderProducts)) {
        return (
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2">
                        <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                            <EmojiSmileFill
                                className="col-md-12 mb-2"
                                size={50}/>
                            <h3 className="col-md-12">Dobra robota!</h3>
                            <p className="col-md-12">
                                Wszystkie produkty zgłoszone do reklamacji zostały odebrane ze skrytek.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <thead
                                className="bg-primary text-center text-white">
                            <tr>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>
                                        <input
                                            type="checkbox"
                                            onChange={(e) => handleChangeCheckBox(e)}
                                            checked={isChecked(orderProducts, isInList)}
                                        >
                                        </input>
                                    </h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>ID</h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>Nazwa produktu</h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>Numer skrytki</h5>
                                </th>
                                <th scope="col">
                                    <h5>Hasło do skrytki</h5>
                                </th>
                            </tr>
                            </thead>
                            <tbody className="bg-white">
                            {orderProducts.map((o, index) => {
                                return (
                                    <UnclaimedItem
                                        orderProductInfo={o}
                                        addToList={addToList}
                                        removeFromList={removeFromList}
                                        isInList={isInList}
                                        key={"item_" + index}
                                    />
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UnclaimedComplaintsTable;
