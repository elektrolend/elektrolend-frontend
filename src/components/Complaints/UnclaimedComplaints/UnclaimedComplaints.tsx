import React, {useState} from "react";

import UnclaimedComplaintsTable from "./UnclaimedComplaintsTable/UnclaimedComplaintsTable";
import ConfirmationBar from "../../Orders/UpcomingOrders/ConfirmationBar/ConfirmationBar";
import Alert from "../../Orders/UpcomingOrders/Alert/Alert";
import {changeOrderProductsStates, getAllOrderProducts} from "../../../services/database.service";
import {IOrderProductState} from "../../../interfaces/Orders/IOrderProductState";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";
import {OrderProductState} from "../../../interfaces/Orders/OrderProductState";

const UnclaimedComplaints = () => {
    const [selectedOrderProducts, setSelectedOrderProducts] = useState([] as IOrderProductState[]);
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        getAllOrderProducts()
            .then(items => setOrderProducts(items))
            .catch(() => []);
    }, [state]);

    const handleAddToList = (orderProduct: IOrderProduct) => {
        setSelectedOrderProducts(prev => {
            const isItemInCart = prev.find(i => i.orderProductID === orderProduct.id);
            if (!isItemInCart) {
                return [...prev, {
                    orderProductID: orderProduct.id,
                    orderProductState: OrderProductState.complaint_for_consideration
                }];
            }
            return [...prev]
        })
    };

    const handleRemoveFromList = (id: number) => {
        setSelectedOrderProducts(prev =>
            prev.reduce((ack, item) => {
                if (item.orderProductID === id) {
                    return ack;
                } else {
                    return [...ack, item];
                }
            }, [] as IOrderProductState[])
        );
    };

    const handleIsInList = (orderProductID: number): boolean => {
        return selectedOrderProducts.filter(o => o.orderProductID === orderProductID).length > 0;
    };

    const handleClearList = () => {
        setSelectedOrderProducts([] as IOrderProductState[]);
    };

    const handleChangeStates = (states: IOrderProductState[]) => {
        setState(0);
        setErrorMessage("");
        changeOrderProductsStates({productList: states})
            .then((response) => {
                if (response.ok) {
                    setState(response.status);
                    handleClearList();
                } else {
                    setState(response.status);
                    setErrorMessage(response.statusText);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                }
            });
    };

    const isAvailable = (orderProduct: IOrderProduct) => {
        return orderProduct.orderProductState === OrderProductState.in_office_box_complaint;
    };

    const getFilteredOrderProducts = () => {
        return orderProducts.filter(orderProduct => isAvailable(orderProduct));
    };

    return (
        <>
            <Alert
                status={state}
                errorMessage={errorMessage}
                successMessage={"Poprawnie zmieniono statusy."}
            />
            <UnclaimedComplaintsTable
                orderProducts={getFilteredOrderProducts()}
                addToList={handleAddToList}
                removeFromList={handleRemoveFromList}
                isInList={handleIsInList}
            />
            <ConfirmationBar
                orderProductsStates={selectedOrderProducts}
                changeStates={handleChangeStates}
                clearList={handleClearList}
            />
        </>
    );
};

export default UnclaimedComplaints;
