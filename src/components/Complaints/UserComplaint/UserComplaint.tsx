import React, {useState} from "react";

import ComplaintForm from "./ComplaintForm/ComplaintForm";
import PasswordBox from "../../Orders/UpcomingOrders/OrderProductsTable/OrderProductItem/PasswordBox";
import {IComplaintResponse} from "../../../interfaces/Complaints/IComplaintResponse";
import {DateManipulation} from "../../Utils/DateManipulation";

type Props = {
    orderProductID: number;
};

const UserComplaint = ({...props}) => {
    const state: Props = props.history.location.state;
    const orderProductID: number = state.orderProductID;
    const [complaintState, setComplaintState] = useState(0);
    const [complaintResponse, setComplaintResponse] = useState(null as IComplaintResponse | null);

    if (complaintState === 200 && complaintResponse) {
        return (
            <div className="m-5 ">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading ">Sukces!</h4>
                    <p>Udało Ci się poprawnie złożyć reklamację.</p>
                    <hr></hr>
                    <p className="mb-0">
                        Zwróć uszkodzony przedmiot do skrytki numer <b>{complaintResponse?.newBoxID}</b> w
                        dniu <b>{DateManipulation.getNextDay(new Date()).toLocaleDateString()}</b>
                    </p>
                    <p>
                        Hasło do skrytki:
                        <div className="row">
                            <div className="col-md-3">
                                <PasswordBox boxPassword={complaintResponse?.newBoxPassword}/>
                            </div>
                            <div className="col-md-3">
                            </div>
                        </div>
                    </p>
                    <p>Teraz możesz wrócić do <a className="hover-move" href="/user-orders"> zamówień</a>.</p>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container mt-2 mb-2">
                <div className="border mb-3">
                    <div className="bg-light m-2">
                        <ComplaintForm
                            orderProductID={orderProductID}
                            complaintState={complaintState}
                            setState={setComplaintState}
                            setComplaintResponse={setComplaintResponse}
                        />
                    </div>
                </div>
            </div>
        );
    }
};

export default UserComplaint;
