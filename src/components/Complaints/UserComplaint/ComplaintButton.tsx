import React, {useState} from "react";
import {Redirect} from "react-router-dom";

import {PatchExclamationFill} from "react-bootstrap-icons";

type Props = {
    orderProductID: number;
};

const ComplaintButton: React.FC<Props> = ({orderProductID}) => {
    const [clicked, setClicked] = useState(false);

    if (clicked) {
        return (
            <Redirect to={{
                pathname: "/user-complaint",
                state: {
                    orderProductID: orderProductID
                }
            }}/>
        );
    }

    return (
        <button
            type="button"
            className="btn btn-outline-danger btn-group btn-block"
            onClick={() => setClicked(true)}>
            <h6 className="text-justify m-auto">REKLAMUJ</h6> <PatchExclamationFill className="ml-1"/>
        </button>
    );
};

export default ComplaintButton;
