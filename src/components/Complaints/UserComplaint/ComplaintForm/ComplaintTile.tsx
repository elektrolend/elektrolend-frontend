import React from "react";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFileImage} from '@fortawesome/free-solid-svg-icons'
import {XLg} from "react-bootstrap-icons";

type Props = {
    image: File;
    removeImage: (imageToRemove: File) => void;
};

const ComplaintTile: React.FC<Props> = ({image, removeImage}) => {
    const getImageSize = (sizeInBytes: number) => {
        return Math.floor(sizeInBytes / 1000);
    };

    const getImageName = (name: string) => {
        return name.length > 40 ? name.slice(0, 40).concat("...") : name;
    };

    return (
        <div className="col-md-6 mb-3">
            <div
                className="bg-white border"
                style={{height: "150px"}}
            >
                <div className="row align-items-center p-1 m-1">
                    <div className="col-2 text-left">
                        <FontAwesomeIcon
                            className="icon img-fluid"
                            size={"3x"}
                            icon={faFileImage}/>
                    </div>
                    <div
                        className="col-6 text-left mt-2"
                        style={{lineBreak: "anywhere"}}>
                        <h6>
                            <strong>{getImageName(image.name)}</strong>
                        </h6>
                        <p>
                            {getImageSize(image.size)} KB
                        </p>
                    </div>
                    <div className="col-4 text-right">
                        <button
                            type="button"
                            className="btn btn-lg btn-danger"
                            onClick={() => removeImage(image)}
                        >
                            <XLg/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ComplaintTile;
