import React, {useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";

import UploadPhotoList from "../../../UploadFiles/UploadPhotoList";
import ComplaintFormTiles from "./ComplaintFormTiles";
import SpinnerButton from "../../../Utils/SpinnerButton";
import UploadProgress from "./UploadProgress";
import {generateImageName, getActualProgress, getImageUrl, uploadPhoto} from "../../../UploadFiles/photos.service";
import {addComplaint} from "../../../../services/database.service";
import ComplaintValidationSchema from "./ComplaintValidationSchema";
import {IComplaint} from "../../../../interfaces/Complaints/IComplaint";
import {IComplaintResponse} from "../../../../interfaces/Complaints/IComplaintResponse";

import {ExclamationTriangleFill} from "react-bootstrap-icons";

type Props = {
    orderProductID: number;
    complaintState: number;
    setState: (state: number) => void;
    setComplaintResponse: (complaintResponse: IComplaintResponse | null) => void;
};

const ComplaintForm: React.FC<Props> = ({orderProductID, complaintState, setState, setComplaintResponse}) => {
    const [errorMessage, setErrorMessage] = useState('');
    const [images, setImages] = useState([] as File[]);
    const [imageUrls, setImageUrls] = useState([] as string[]);
    const maxImagesNumber = 6;
    const [progress, setProgress] = useState(0);

    const uploadAllPhotos = async () => {
        try {
            await Promise.all(
                images.map((img) => {
                    return new Promise((resolve) => {
                        const imageName = generateImageName(img);
                        const uploadTask = uploadPhoto(imageName, img, `complaints/${orderProductID}`);
                        uploadTask.on(
                            "state_changed",
                            (snapshot) => {
                                setProgress(getActualProgress(snapshot));
                            },
                            (error) => {
                                setState(500);
                                setErrorMessage(error.message);
                            },
                            () => {
                                getImageUrl(`images/complaints/${orderProductID}`, imageName)
                                    .then((downloadUrl) => {
                                        imageUrls.push(downloadUrl);
                                        resolve(imageUrls);
                                    });
                            }
                        );
                    });
                })
            );
        } catch (err) {
            setState(err.status);
            setErrorMessage(err.message);
        }
    };

    const createNewComplaint = (newComplaint: IComplaint) => {
        addComplaint(newComplaint)
            .then(response => {
                setComplaintResponse(response.data);
                setState(response.status);
            })
            .catch((error) => {
                    setComplaintResponse(null);
                    setState(error.status);
                    setErrorMessage(error.message);
                }
            );
    };

    const onSubmit = (complaint: any) => {
        setErrorMessage("");
        setState(100);
        uploadAllPhotos()
            .then(
                () => {
                    complaint.imageUrls = imageUrls;
                    createNewComplaint(complaint);
                })
            .catch(error => {
                setImageUrls([]);
                setState(error.state);
                setErrorMessage(error.message);
            });
    };

    const handleAddImages = (newImages: FileList) => {
        setErrorMessage("");
        for (let i = 0; i < newImages.length; i++) {
            const imageToAdd = newImages.item(i);
            if (imageToAdd && canBeAdded(i)) {
                setImages(prev => {
                    const isInImages = prev.find(img => img.name === imageToAdd.name);
                    if (!isInImages) {
                        return [...prev, imageToAdd];
                    }
                    return [...prev]
                });
            }
        }
    };

    const handleRemoveImage = (imageToRemove: File) => {
        setImages(prev =>
            prev.reduce((ack, image) => {
                if (image === imageToRemove) {
                    return ack;
                } else {
                    return [...ack, image];
                }
            }, [] as File[])
        );
    };

    const canBeAdded = (newImageIndex: number) => {
        return images.length + newImageIndex + 1 <= maxImagesNumber;
    };

    const isFull = () => {
        return images.length >= maxImagesNumber;
    };

    return (
        <Formik
            initialValues={{
                orderProductID: orderProductID,
                description: '',
            }}
            validationSchema={ComplaintValidationSchema}
            onSubmit={values => {
                onSubmit(values);
            }}>
            {() => (
                <Form>
                    <div className="bg-primary text-white p-2 m-2">
                        <h4>Nowa reklamacja</h4>
                    </div>
                    <div className="bg-light p-2 m-2">
                        <div className="row">
                            <div className="col-12 form-group">
                                <h5>Opis</h5>
                                <Field
                                    name="description"
                                    as="textarea"
                                    rows="4"
                                    className="form-control"
                                />
                                <ErrorMessage
                                    render={msg => <div className="alert alert-danger">{msg}</div>}
                                    name="description"
                                />
                            </div>
                            {!isFull() ?
                                <UploadPhotoList
                                    fieldName={"images"}
                                    addImages={handleAddImages}
                                /> : null}
                            <ComplaintFormTiles
                                images={images}
                                removeImage={handleRemoveImage}
                            />
                            <div className="col-12">
                                {isFull() ?
                                    <div
                                        className="border alert alert-warning d-flex align-items-center"
                                        role="alert">
                                        <ExclamationTriangleFill/>
                                        <div className="ml-1">
                                            Dodano maksymalną liczbę zdjęć!
                                        </div>
                                    </div>
                                    : null}
                                <UploadProgress progress={progress}/>
                            </div>
                            <div className="col-12 mt-1">
                                {errorMessage ?
                                    <div
                                        className="alert alert-danger"
                                        role="alert">
                                        {errorMessage}
                                    </div>
                                    : null}
                                <SpinnerButton
                                    state={complaintState}
                                    buttonText={"Zgłoś reklamację"}
                                />
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default ComplaintForm;
