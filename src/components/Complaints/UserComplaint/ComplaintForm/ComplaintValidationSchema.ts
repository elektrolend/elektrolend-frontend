import * as Yup from "yup";

const ComplaintValidationSchema = Yup.object().shape({
    orderProductID: Yup.number()
        .required('OrderProductID jest wymagany!'),
    description: Yup.string()
        .min(10, 'Opis musi mieć minimalnie 10 znaków!')
        .max(600, 'Opis może mieć maksymalnie 600 znaków!')
        .required('Opis jest wymagany!')
});

export default ComplaintValidationSchema;
