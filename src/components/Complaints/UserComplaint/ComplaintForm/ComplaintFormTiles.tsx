import React from "react";

import ComplaintTile from "./ComplaintTile";

type Props = {
    images: File[];
    removeImage: (imageToRemove: File) => void;
};

const ComplaintFormTiles: React.FC<Props> = ({images, removeImage}) => {
    if (images.length === 0) {
        return null;
    }
    return (
        <div className="col-12 form-group">
            <h5>Załączniki</h5>
            <div className="row p-2 m-2">
                {images.map((img, index) => {
                    return (
                        <ComplaintTile
                            image={img}
                            removeImage={removeImage}
                            key={"complaintImage_" + index}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default ComplaintFormTiles;
