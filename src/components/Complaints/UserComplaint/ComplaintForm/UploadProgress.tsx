import React from "react";

import {ProgressBar} from "react-bootstrap";

type Props = {
    progress: number;
};

const UploadProgress: React.FC<Props> = ({progress}) => {
    if (progress) {
        return (
            <div className="mb-2">
                <h6>Przetwarzanie reklamacji...</h6>
                <ProgressBar animated now={progress}/>
            </div>
        );
    }

    return null;
};

export default UploadProgress;
