interface IFilterModeInfo {
    title: string;
    isConsidered: boolean;
}

const filterModes: IFilterModeInfo[] = [
    {
        title: "nierozpatrzone",
        isConsidered: false
    },
    {
        title: "rozpatrzone",
        isConsidered: true
    }
]

export default filterModes;
