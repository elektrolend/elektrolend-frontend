import React from "react";

import filterModes from "./filterModes";

type Props = {
    currentFilterMode: boolean;
    setCurrentFilterMode: (isConsidered: boolean) => void;
};

export const FilterBar: React.FC<Props> = ({
                                               currentFilterMode,
                                               setCurrentFilterMode,
                                           }) => {
    const isActive = (filterModeToCheck: boolean) => {
        return filterModeToCheck === currentFilterMode ? "active" : "";
    };

    const handleChangeFilterMode = (newFilterMode: boolean) => {
        setCurrentFilterMode(newFilterMode);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-12">
                        <nav className="nav nav-pills nav-fill">
                            {filterModes.map((filterMode, index) => {
                                return (
                                    <div
                                        key={"filterMode_" + index}
                                        className={"nav-item nav-link " + (isActive(filterMode.isConsidered))}
                                        onClick={() => handleChangeFilterMode(filterMode.isConsidered)}>
                                        {filterMode.title}
                                    </div>
                                );
                            })}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FilterBar;
