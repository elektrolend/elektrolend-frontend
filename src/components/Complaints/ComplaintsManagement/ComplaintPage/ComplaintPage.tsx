import React, {useState} from "react";

import ComplaintDetails from "./ComplaintDetails/ComplaintDetails";
import ComplaintPhotos from "./ComplaintPhotos/ComplaintPhotos";
import ConsiderationBar from "./ConsiderationBar/ConsiderationBar";
import ComplaintState from "./ComplaintState/ComplaintState";
import SuccessAlert from "../../../Utils/SuccessAlert";
import RefundBar from "./RefundBar/RefundBar";
import RefundState from "./ComplaintState/RefundState";
import WarningAlert from "../../../Utils/WarningAlert";
import Refund from "../../../Refund/Refund";
import {getComplaint, getOrderProduct} from "../../../../services/database.service";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";
import {IComplaintDTO} from "../../../../interfaces/Complaints/IComplaintDTO";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

import {ArrowLeftCircleFill} from "react-bootstrap-icons";


type Props = {
    complaintID: number;
    setComplaintDetailsID: (complaintID: number) => void;
};

const ComplaintPage: React.FC<Props> = ({complaintID, setComplaintDetailsID}) => {
    const [state, setState] = useState(0);
    const [complaint, setComplaint] = useState(null as IComplaintDTO | null);
    const [orderProduct, setOrderProduct] = useState(null as IOrderProduct | null);
    const [refundMode, setRefundMode] = useState(false);
    const [successMessage, setSuccessMessage] = useState("");

    React.useEffect(() => {
        getComplaint(complaintID)
            .then(c => {
                setComplaint(c);
                getOrderProduct(c.orderProductID)
                    .then(o => setOrderProduct(o))
                    .catch(() => setOrderProduct(null))

            })
            .catch(() => setComplaint(null));
    }, [complaintID, state, refundMode]);

    const isToConsider = () => {
        return orderProduct?.orderProductState === OrderProductState.complaint_for_consideration;
    };

    const canBeRefund = () => {
        return orderProduct?.orderProductState === OrderProductState.complaint_accepted;
    };

    const isRefund = () => {
        return orderProduct?.orderProductState === OrderProductState.refund;
    };

    if (!complaint || !orderProduct) {
        return null;
    }

    if (refundMode) {
        return (
            <Refund
                orderProductID={orderProduct.id}
                refundAmount={complaint.refund}
                setRefundMode={setRefundMode}
                setState={setState}
                setSuccessMessage={setSuccessMessage}
            />
        );
    }

    return (
        <div className="container mt-2">
            <SuccessAlert
                state={state}
                message={successMessage}
            />
            <WarningAlert
                show={canBeRefund()}
                message={"Reklamacja wymaga zwrotu pieniędzy!"}
            />
            <div>
                <h2 className="text-primary">Reklamacja nr. {complaint.id}</h2>
                <h6 className="mb-10" onClick={() => setComplaintDetailsID(-1)}>
                    <ArrowLeftCircleFill className="mb-1 mr-1"/>
                    POWRÓT DO LISTY
                </h6>
            </div>
            <div className="row">
                <div className="col-7">
                    <ComplaintDetails
                        complaint={complaint}
                        orderProduct={orderProduct}
                    />
                </div>
                <div className="col-5">
                    <ComplaintPhotos
                        imageUrls={complaint.imageUrls}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    {canBeRefund() ?
                        <RefundBar
                            refundAmount={complaint.refund}
                            setRefundMode={setRefundMode}
                        /> : null}
                    {isRefund() ? <RefundState refundAmount={complaint.refund}/> : null}
                </div>
                <div className="col-12">
                    {isToConsider() ?
                        <ConsiderationBar
                            complaintID={complaintID}
                            state={state}
                            setState={setState}
                            setSuccessMessage={setSuccessMessage}
                        />
                        :
                        <ComplaintState orderProduct={orderProduct}/>
                    }
                </div>
            </div>
        </div>
    );
};

export default ComplaintPage;
