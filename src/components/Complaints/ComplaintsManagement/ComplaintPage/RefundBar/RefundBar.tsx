import React from "react";

import {CreditCard2BackFill} from "react-bootstrap-icons";

type Props = {
    refundAmount: number;
    setRefundMode: (show: boolean) => void;
};

export const RefundBar: React.FC<Props> = ({refundAmount, setRefundMode}) => {

    return (
        <div className="border mb-3">
            <div className="bg-light p-2 m-2">
                <div className="col-12">
                    <button
                        className="btn btn-lg bg-danger text-white"
                        style={{width: "100%"}}
                        onClick={() => setRefundMode(true)}
                    >
                        <CreditCard2BackFill className="mr-2"/>Dokonaj zwrotu {refundAmount} PLN
                    </button>
                </div>
            </div>
        </div>
    );
};

export default RefundBar;
