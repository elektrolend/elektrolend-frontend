import React from "react";

import ConsideredState from "./ConsideredState";
import OfficeBoxState from "./OfficeBoxState";
import {OrderProductState} from "../../../../../interfaces/Orders/OrderProductState";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProduct: IOrderProduct;
};

const ComplaintState: React.FC<Props> = ({orderProduct}) => {
    const isComplaintConsidered = () => {
        if (orderProduct.orderProductState === OrderProductState.complaint_accepted) {
            return true;
        }
        if (orderProduct.orderProductState === OrderProductState.complaint_not_accepted) {
            return true;
        }
        return false;
    };

    const isInOfficeBox = () => {
        return orderProduct.orderProductState === OrderProductState.in_office_box_complaint;
    };

    if (isComplaintConsidered()) {
        return (
            <ConsideredState orderProductState={orderProduct.orderProductState}/>
        );
    } else if (isInOfficeBox()) {
        return (
            <OfficeBoxState
                boxID={orderProduct.officeBoxIDEnd}
                boxPassword={orderProduct.boxPassword}
            />
        );
    } else {
        return null;
    }
};

export default ComplaintState;
