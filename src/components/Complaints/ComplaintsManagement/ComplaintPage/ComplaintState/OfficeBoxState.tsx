import React from "react";

import OfficeBoxStateTable from "./OfficeBoxStateTable";

import {InfoCircleFill} from "react-bootstrap-icons";

type Props = {
    boxID: number;
    boxPassword: string;
};

const OfficeBoxState: React.FC<Props> = ({boxID, boxPassword}) => {
    return (
        <div className="border mb-3">
            <div className="bg-light p-2 m-2">
                <div className="col-12">
                    <div className="alert alert-primary">
                        <InfoCircleFill className="mb-1"/> Produkt znajduje się jeszcze w skrytce
                    </div>
                </div>
                <OfficeBoxStateTable
                    boxID={boxID}
                    boxPassword={boxPassword}
                />
            </div>
        </div>
    );
};

export default OfficeBoxState;
