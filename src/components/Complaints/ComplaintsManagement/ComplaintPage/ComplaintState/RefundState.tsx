import React from "react";

import {CreditCard2BackFill} from "react-bootstrap-icons";

type Props = {
    refundAmount: number;
};

const RefundState: React.FC<Props> = ({refundAmount}) => {
    return (
        <div className="border mb-3">
            <div className="bg-light p-2 m-2">
                <div className="col-12 btn-group">
                    <div
                        className="bg-success btn-lg btn-labeled text-white text-center"
                        style={{width: "100%"}}
                    >
                        <CreditCard2BackFill className="mr-2"/>
                        Reklamacja zakończona zwrotem {refundAmount} PLN
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RefundState;
