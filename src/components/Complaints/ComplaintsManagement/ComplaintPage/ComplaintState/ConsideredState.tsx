import React from "react";
import {OrderProductState} from "../../../../../interfaces/Orders/OrderProductState";
import {HandThumbsDownFill, HandThumbsUpFill} from "react-bootstrap-icons";

type Props = {
    orderProductState: string;
};

const ConsideredState: React.FC<Props> = ({orderProductState}) => {
    const isComplaintAccepted = () => {
        return orderProductState === OrderProductState.complaint_accepted ? true : false;
    };

    const getBackGroundColor = () => {
        return isComplaintAccepted() ? "bg-success" : "bg-danger";
    };

    const getComplaintStateInString = () => {
        return isComplaintAccepted() ? "POZYTYWNIE" : "NEGATYWNIE";
    };

    const getComplaintIcon = () => {
        return isComplaintAccepted() ? <HandThumbsUpFill/> : <HandThumbsDownFill/>;
    };

    return (
        <div className="border mb-3">
            <div className="bg-light p-2 m-2">
                <div className="col-12 btn-group">
                    <div
                        className={"btn-lg btn-labeled text-white text-center " + getBackGroundColor()}
                        style={{width: "100%"}}
                    >
                        Reklamacja rozpatrzona {getComplaintStateInString()} {getComplaintIcon()}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ConsideredState;
