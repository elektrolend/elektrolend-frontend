import React from "react";

import PasswordBox from "../../../../Orders/UpcomingOrders/OrderProductsTable/OrderProductItem/PasswordBox";

type Props = {
    boxID: number;
    boxPassword: string;
};

const OfficeBoxStateTable: React.FC<Props> = ({boxID, boxPassword}) => {
    return (
        <div className="col-12">
            <table className="table bg-white">
                <thead className="bg-primary text-center text-white">
                <tr>
                    <th
                        scope="col"
                        className="border-right">Numer skrytki
                    </th>
                    <th
                        scope="col"
                        className="border-right"
                    >Hasło do skrytki
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr className="border text-center rounded-bottom">
                    <td
                        className="border-right">
                        {boxID}</td>
                    <td
                        className="border-right">
                        <PasswordBox boxPassword={boxPassword}/></td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default OfficeBoxStateTable;
