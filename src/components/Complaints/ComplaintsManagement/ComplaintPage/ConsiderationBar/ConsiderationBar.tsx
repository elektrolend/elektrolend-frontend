import React, {useState} from "react";

import ConsiderationModal from "./ConsiderationModal";
import {updateComplaint} from "../../../../../services/database.service";
import {IRefund} from "../../../../../interfaces/Refund/IRefund";

import {HandThumbsDownFill, HandThumbsUpFill} from "react-bootstrap-icons";

type Props = {
    complaintID: number;
    state: number;
    setState: (state: number) => void;
    setSuccessMessage: (message: string) => void;
};

const ConsiderationBar: React.FC<Props> = ({complaintID, state, setState, setSuccessMessage}) => {
    const [decision, setDecision] = useState(null as string | null);
    const [refund, setRefund] = useState(1);
    const [errorMessage, setErrorMessage] = useState("");
    const [isModalOpen, setIsModalOpen] = React.useState(false);

    const handleClick = (actualDecision: string) => {
        setDecision(actualDecision);
        setIsModalOpen(true);
    };

    const isPositive = () => {
        return decision === "positive";
    };

    const onSubmit = () => {
        const data: IRefund = isPositive() ? {refund: refund} : {refund: null};
        updateComplaint(complaintID, data)
            .then((response) => {
                if (response.status === 200) {
                    setState(response.status);
                    setSuccessMessage("Udało się poprawnie rozpatrzyć reklamację.");
                }
            })
            .catch((error) => {
                if (error.response) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                } else {
                    setState(500);
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
    };

    return (
        <div className="border mb-3">
            <div className="bg-primary text-white text-center p-2 m-2">
                <h4>Oceń reklamację</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="col-12 btn-group">
                    <button
                        className="btn btn-lg btn-labeled btn-danger"
                        data-toggle="modal"
                        data-target="#negativeModal"
                        style={{width: "50%"}}
                        onClick={() => handleClick("negative")}
                    >
                        NEGATYWNIE <HandThumbsDownFill/>
                    </button>
                    <button
                        className="btn btn-lg btn-labeled btn-success"
                        data-toggle="modal"
                        data-target="#positiveModal"
                        style={{width: "50%"}}
                        onClick={() => handleClick("positive")}
                    >
                        POZYTYWNIE <HandThumbsUpFill/>
                    </button>
                    {decision ?
                        <ConsiderationModal
                            refund={refund}
                            state={state}
                            errorMessage={errorMessage}
                            setRefund={setRefund}
                            onSubmit={onSubmit}
                            modalName={decision}
                            setDecision={setDecision}
                            isOpen={isModalOpen}
                            setIsOpen={setIsModalOpen}
                        /> : null}
                </div>
            </div>
        </div>
    );
};

export default ConsiderationBar;
