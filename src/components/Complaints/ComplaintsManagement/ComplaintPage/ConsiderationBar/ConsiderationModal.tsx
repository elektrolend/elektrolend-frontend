import React from "react";

import RefundInput from "./RefundInput";

import {Modal} from "react-bootstrap";

type Props = {
    errorMessage: string;
    state: number;
    modalName: string;
    setDecision: (decision: null | string) => void;
    onSubmit: () => void;
    refund: number;
    setRefund: (refund: number) => void;
    isOpen: boolean;
    setIsOpen: (isOpen: boolean) => void;
};

const ConsiderationModal: React.FC<Props> = ({
                                                 errorMessage,
                                                 state,
                                                 modalName,
                                                 setDecision,
                                                 onSubmit,
                                                 refund,
                                                 setRefund,
                                                 isOpen,
                                                 setIsOpen
                                             }) => {
    const hideModal = () => {
        setIsOpen(false);
    };

    const isPositive = () => {
        return modalName === "positive";
    };

    const handleCancel = () => {
        setDecision(null);
        hideModal();
    };

    const handleSubmit = () => {
        onSubmit();
        if (state === 200) {
            setDecision(null);
            hideModal();
        }
    };

    const getDecisionInString = () => {
        if (isPositive()) {
            return "pozytywna";
        }
        return "negatywna";
    };

    return (
        <Modal
            show={isOpen}
            onHide={hideModal}>
            <Modal.Header>
                <Modal.Title>Potwierdź decyzję</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Twoja opinia na temat reklamacji jest <b>{getDecisionInString()}</b>.
                {isPositive() ?
                    <RefundInput
                        refund={refund}
                        setRefund={setRefund}
                        errorMessage={errorMessage}
                    />
                    :
                    <p>Czy to twoja ostateczna decyzja?</p>}
            </Modal.Body>
            <Modal.Footer>
                <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                    onClick={handleCancel}>
                    Anuluj
                </button>
                <button
                    className="btn btn-primary"
                    type="submit"
                    onClick={handleSubmit}>
                    Potwierdź
                </button>
            </Modal.Footer>
        </Modal>

    );
};

export default ConsiderationModal;