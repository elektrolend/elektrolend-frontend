import React from "react";

type Props = {
    refund: number;
    setRefund: (event: any) => void;
    errorMessage: string;
};

const RefundInput: React.FC<Props> = ({refund, setRefund, errorMessage}) => {
    return (
        <div className="form-group">
            <label className="form-label">Podaj kwotę zwrotu</label>
            <input
                name="refund"
                type="number"
                min={1}
                value={refund}
                onChange={(event: any) => setRefund(event.target.value)}
                className="form-control"
            />
            {errorMessage ?
                <div
                    className="alert alert-danger"
                    role="alert">
                    {errorMessage}
                </div> : null}
        </div>
    );
};

export default RefundInput;
