import React, {useState} from "react";
import {generatePath, Link} from "react-router-dom";

import {getProduct} from "../../../../../services/database.service";
import {DateManipulation} from "../../../../Utils/DateManipulation";
import {IComplaintDTO} from "../../../../../interfaces/Complaints/IComplaintDTO";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";
import {IProduct} from "../../../../../interfaces/Products/IProduct";

import {Box, CalendarDay, CardText, PersonCircle, Wallet2} from "react-bootstrap-icons";

type Props = {
    complaint: IComplaintDTO;
    orderProduct: IOrderProduct;
};

const ComplaintDetails: React.FC<Props> = ({complaint, orderProduct}) => {
    const [product, setProduct] = useState(null as IProduct | null);

    React.useEffect(() => {
        getProduct(orderProduct.productID)
            .then(p => setProduct(p))
            .catch(() => setProduct(null));
    }, [orderProduct.productID]);

    const getDate = () => {
        return DateManipulation.makeDate(complaint.date).toLocaleDateString();
    };

    const getProductID = () => {
        return product ? product.id : -1;
    };

    return (
        <div className="border mb-3">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Szczegóły reklamacji</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>
                            <PersonCircle className="mr-1 mb-1"/>
                            <strong>Klient</strong>
                        </h5>
                        <p>
                            {complaint.customerName}
                        </p>
                    </div>
                    <div className="col-md-6">
                        <h5>
                            <CalendarDay className="mr-1 mb-1"/>
                            <strong>Data</strong>
                        </h5>
                        <p>{getDate()}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>
                            <Box className="mr-1 mb-1"/>
                            <strong>Produkt</strong>
                        </h5>
                        <p>
                            <Link
                                to={generatePath(`/product/details/:id/:name`,
                                    {id: getProductID(), name: complaint.productName})}
                                target="_blank"
                                className="text-dark"
                            >
                                {complaint.productName}
                            </Link>
                        </p>
                    </div>
                    <div className="col-md-6">
                        <h5>
                            <Wallet2 className="mr-1 mb-1"/>
                            <strong>Cena</strong>
                        </h5>
                        <p>{orderProduct.price} PLN</p>
                    </div>
                    <div className="col-md-12">
                        <h5>
                            <CardText className="mr-1 mb-1"/>
                            <strong>Opis</strong>
                        </h5>
                        <p>{complaint.description}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ComplaintDetails;
