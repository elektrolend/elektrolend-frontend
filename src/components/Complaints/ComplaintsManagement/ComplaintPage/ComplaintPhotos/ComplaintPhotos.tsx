import React from "react";
import {SRLWrapper} from "simple-react-lightbox";

import PhotoItem from "./PhotoItem";

type Props = {
    imageUrls: string[];
};

const ComplaintPhotos: React.FC<Props> = ({imageUrls}) => {
    return (
        <div className="border mb-3">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Załączone zdjęcia</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <SRLWrapper>
                    <div className="row">
                        {imageUrls.map((imageUrl, index) => {
                            return (
                                <PhotoItem
                                    imageUrl={imageUrl}
                                    key={index}
                                />
                            );
                        })}
                    </div>
                </SRLWrapper>
            </div>
        </div>
    );
};

export default ComplaintPhotos;
