import React, {useState} from "react";

type Props = {
    imageUrl: string;
};

const PhotoItem: React.FC<Props> = ({imageUrl}) => {
    const [loading, setLoading] = useState(true);

    const imageLoaded = () => {
        setLoading(false);
    };

    return (
        <div className="col-md-4 mb-2">
            <div className="d-flex justify-content-center">
                <div
                    className="spinner-border"
                    role="status"
                    style={{display: loading ? "block" : "none"}}>
                </div>
            </div>
            <img
                src={imageUrl}
                alt="complaintPhoto"
                className="img-thumbnail"
                style={{display: loading ? "none" : "block"}}
                onLoad={() => imageLoaded()}
            />
        </div>
    );
};

export default PhotoItem;
