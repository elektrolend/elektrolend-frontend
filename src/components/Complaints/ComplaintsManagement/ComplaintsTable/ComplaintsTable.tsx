import React from "react";
import {isEmptyArray} from "formik";

import ComplaintItem from "./ComplaintItem";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import {IComplaintDTO} from "../../../../interfaces/Complaints/IComplaintDTO";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    complaints: IComplaintDTO[];
    setComplaintDetailsID: (complaintDetailsID: number) => void;
};

const ComplaintsTable: React.FC<Props> = ({complaints, setComplaintDetailsID}) => {
    if (isEmptyArray(complaints)) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych wyników!"}
                description={"Zmień parametry filtrowania lub wróć później."}
                icon={
                    <EmojiFrownFill
                        className="col-md-12 mb-2"
                        size={50}
                    />
                }
            />
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <thead className="bg-primary text-center text-white">
                            <tr>
                                <th
                                    scope="col"
                                    className="border-right">
                                    ID
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    Klient
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    Produkt
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    Data
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    Opcje
                                </th>
                            </tr>
                            </thead>
                            <tbody className="bg-white">
                            {complaints.map((complaint, index) => {
                                return (
                                    <ComplaintItem
                                        complaint={complaint}
                                        setComplaintDetailsID={setComplaintDetailsID}
                                        key={"complaint_" + index}
                                    />
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ComplaintsTable;
