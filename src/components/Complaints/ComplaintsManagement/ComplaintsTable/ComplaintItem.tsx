import React from "react";

import ComplaintDetailsButton from "./ComplaintDetailsButton";
import {IComplaintDTO} from "../../../../interfaces/Complaints/IComplaintDTO";
import {DateManipulation} from "../../../Utils/DateManipulation";

type Props = {
    complaint: IComplaintDTO;
    setComplaintDetailsID: (complaintDetailsID: number) => void;
};

const ComplaintItem: React.FC<Props> = ({complaint, setComplaintDetailsID}) => {
    const getDate = () => {
        return DateManipulation.makeDate(complaint.date).toLocaleDateString();
    };

    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">{complaint.id}</td>
            <td className="border-right">{complaint.customerName}</td>
            <td className="border-right">
                {complaint.productName}
            </td>
            <td className="border-right">{getDate()}</td>
            <td>
                <ComplaintDetailsButton
                    complaintID={complaint.id}
                    setComplaintDetailsID={setComplaintDetailsID}
                />
            </td>
        </tr>
    );
};

export default ComplaintItem;
