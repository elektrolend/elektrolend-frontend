import React from "react";

import {BinocularsFill} from "react-bootstrap-icons";

type Props = {
    complaintID: number;
    setComplaintDetailsID: (complaintDetails: number) => void;
};

const ComplaintDetailsButton: React.FC<Props> = ({complaintID, setComplaintDetailsID}) => {
    return (
        <button
            type="button"
            className="btn btn-secondary"
            data-content="Zgłoś reklamację"
            onClick={() => setComplaintDetailsID(complaintID)}>
            <BinocularsFill className="mb-1"/> <strong>PODGLĄD</strong>
        </button>
    );
};

export default ComplaintDetailsButton;
