import React, {useState} from "react";

import ComplaintsTable from "./ComplaintsTable/ComplaintsTable";
import ComplaintPage from "./ComplaintPage/ComplaintPage";
import FilterBar from "./FilterBar/FilterBar";
import {getComplaints} from "../../../services/database.service";
import {IComplaintDTO} from "../../../interfaces/Complaints/IComplaintDTO";

const ComplaintsManagement = () => {
    const [complaints, setComplaints] = useState([] as IComplaintDTO[]);
    const [complaintDetailsID, setComplaintDetailsID] = useState(-1);
    const [currentFilterMode, setCurrentFilterMode] = useState(false);

    React.useEffect(() => {
        getComplaints()
            .then(complaintsList => setComplaints(complaintsList))
            .catch(() => setComplaints([]));
    }, [complaintDetailsID]);

    const getFilterComplaints = () => {
        return complaints.filter(complaint => complaint.isConsidered === currentFilterMode);
    };

    if (complaintDetailsID !== -1) {
        return (
            <ComplaintPage
                complaintID={complaintDetailsID}
                setComplaintDetailsID={setComplaintDetailsID}
            />
        );
    }

    return (
        <div className="container mt-2 mb-2">
            <FilterBar
                currentFilterMode={currentFilterMode}
                setCurrentFilterMode={setCurrentFilterMode}
            />
            <ComplaintsTable
                complaints={getFilterComplaints()}
                setComplaintDetailsID={setComplaintDetailsID}
            />
        </div>
    );
};

export default ComplaintsManagement;
