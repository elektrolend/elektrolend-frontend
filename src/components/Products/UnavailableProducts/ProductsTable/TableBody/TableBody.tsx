import React from "react";

import BodyItem from "./BodyItem/BodyItem";
import {IProduct} from "../../../../../interfaces/Products/IProduct";

type Props = {
    products: IProduct[];
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const TableBody: React.FC<Props> = ({products, setState, setErrorMessage}) => {
    return (
        <tbody className="bg-white">
        {products.map((product, index) => {
            return (
                <BodyItem
                    product={product}
                    setState={setState}
                    setErrorMessage={setErrorMessage}
                    key={"product_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default TableBody;
