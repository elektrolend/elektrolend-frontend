import React from "react";
import {generatePath, Link} from "react-router-dom";

import {BinocularsFill} from "react-bootstrap-icons";

type Props = {
    productID: number;
    productName: string;
};

const PreviewButton: React.FC<Props> = ({productID, productName}) => {
    return (
        <Link
            to={generatePath(`/product/details/:id/:name`,
                {id: productID || "", name: productName || ""})}
            className="text-dark"
            target="_blank"
        >
            <button
                type="button"
                className="btn btn-outline-secondary mr-1"
            >
                <BinocularsFill className="mb-1"/> <strong>PODGLĄD</strong>
            </button>
        </Link>
    );
};

export default PreviewButton;
