import React, {useState} from "react";

import {updateProduct} from "../../../../../../services/database.service";

import {Power} from "react-bootstrap-icons";
import {Spinner} from "react-bootstrap";

type Props = {
    productID: number;
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const ActivateButton: React.FC<Props> = ({productID, setState, setErrorMessage}) => {
    const [buttonState, setButtonState] = useState(0);

    const handleOnClick = () => {
        setButtonState(100);
        setState(100);
        setErrorMessage("");
        updateProduct(productID, {isAvailable: true})
            .then((response) => {
                if (response.status === 200) {
                    setState(response.status);
                }
            })
            .catch((error) => {
                if (error.response.status < 500) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                } else {
                    setState(500);
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
        setButtonState(0);
    };

    return (
        <button
            type="button"
            className="btn btn-outline-success"
            onClick={() => handleOnClick()}
        >
            {buttonState === 100 ?
                <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
                :
                <>
                    <Power className="mb-1"/> <strong>AKTYWUJ</strong>
                </>
            }
        </button>
    );
};

export default ActivateButton;
