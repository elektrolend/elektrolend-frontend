import React from "react";

import ActivateButton from "./ActivateButton";
import PreviewButton from "./PreviewButton";
import {IProduct} from "../../../../../../interfaces/Products/IProduct";

type Props = {
    product: IProduct;
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const BodyItem: React.FC<Props> = ({product, setState, setErrorMessage}) => {
    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">{product.id}</td>
            <td className="border-right">
                {product.name}
            </td>
            <td className="border-right">
                {product.price} PLN
            </td>
            <td className="border-right">
                <PreviewButton
                    productID={product.id}
                    productName={product.name}
                />
                <ActivateButton
                    productID={product.id}
                    setState={setState}
                    setErrorMessage={setErrorMessage}
                />
            </td>
        </tr>
    );
};

export default BodyItem;
