import React from "react";
import {isEmptyArray} from "formik";

import TableHeader from "./TableHeader/TableHeader";
import TableBody from "./TableBody/TableBody";
import {IProduct} from "../../../../interfaces/Products/IProduct";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    products: IProduct[];
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const ProductsTable: React.FC<Props> = ({products, setState, setErrorMessage}) => {
    if (isEmptyArray(products)) {
        return (
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2 justify-content-center">
                        <div className="p-4 bg-light row m-5 align-items-center text-center">
                            <EmojiFrownFill
                                className="col-md-12 mb-2"
                                size={50}
                            />
                            <h3 className="col-md-12">Brak wyników!</h3>
                            <p className="col-md-12">
                                Musisz zmienić parametry filtrowania.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <TableHeader/>
                            <TableBody
                                products={products}
                                setState={setState}
                                setErrorMessage={setErrorMessage}
                            />
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductsTable;
