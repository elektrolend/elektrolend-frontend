import React from "react";

const TableHeader = () => {
    return (
        <thead className="bg-primary text-center text-white">
        <tr>
            <th
                scope="col"
                className="border-right col-md-1">
                ID
            </th>
            <th
                scope="col"
                className="border-right col-md-6">
                NAZWA
            </th>
            <th
                scope="col"
                className="border-right col-md-1">
                CENA
            </th>
            <th
                scope="col"
                className="border-right col-md-4">
                OPCJE
            </th>
        </tr>
        </thead>
    );
};

export default TableHeader;
