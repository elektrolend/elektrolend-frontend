import React, {useEffect, useState} from "react";

import PageNumberBar from "../../Pagination/PageNumberBar";
import FilterBar from "./FilterBar/FilterBar";
import ProductsTable from "./ProductsTable/ProductsTable";
import Alert from "../../Orders/UpcomingOrders/Alert/Alert";
import {getFilteredProducts} from "../../../services/database.service";
import {IProduct} from "../../../interfaces/Products/IProduct";
import {IProductsParams} from "../../../interfaces/Products/IProductsParams";

const UnavailableProducts = () => {
    const [products, setProducts] = useState([] as IProduct[]);

    const [currentPageNumber, setCurrentPageNumber] = useState(1);
    const [middlePageNumber, setMiddlePageNumber] = useState(currentPageNumber + 1);
    const [currentPageSize, setCurrentPageSize] = useState(5);
    const [maxPageNumber, setMaxPageNumber] = useState(1);

    const [currentCategoryID, setCurrentCategoryID] = useState('');

    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    useEffect(() => {
        const productsParams: IProductsParams = {
            pageSize: currentPageSize,
            pageNumber: currentPageNumber,
            categoryId: currentCategoryID,
            availability: false
        };
        getFilteredProducts(productsParams)
            .then(productsResponse => {
                setProducts(productsResponse.productDTOList);
                setMaxPageNumber(Math.ceil(productsResponse.paginationDTO.totalResults / currentPageSize));
            }).catch(() => setProducts([]));
    }, [currentPageSize, currentPageNumber, currentCategoryID, state]);

    return (
        <>
            <Alert
                status={state}
                errorMessage={errorMessage}
                successMessage={"Udało się poprawnie aktywować produkt!"}
            />
            <FilterBar
                currentPageSize={currentPageSize}
                setPageSize={setCurrentPageSize}
                setCurrentPageNumber={setCurrentPageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                setCurrentCategoryID={setCurrentCategoryID}
            />
            <ProductsTable
                products={products}
                setState={setState}
                setErrorMessage={setErrorMessage}
            />
            <PageNumberBar
                currentPageNumber={currentPageNumber}
                setCurrentPageNumber={setCurrentPageNumber}
                middlePageNumber={middlePageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                maxPageNumber={maxPageNumber}
            />
        </>
    );
};

export default UnavailableProducts;
