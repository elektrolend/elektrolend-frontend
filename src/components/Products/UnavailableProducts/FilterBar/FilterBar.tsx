import React from "react";

import PageSizeBar from "../../../Pagination/PageSizeBar";
import CategorySelect from "./CategorySelect";

type Props = {
    currentPageSize: number;
    setPageSize: (pageSize: number) => void;
    setCurrentPageNumber: (pageNumber: number) => void;
    setMiddlePageNumber: (pageNumber: number) => void;
    setCurrentCategoryID: (categoryID: string) => void;
}

export const FilterBar: React.FC<Props> = ({
                                               currentPageSize,
                                               setPageSize,
                                               setCurrentPageNumber,
                                               setMiddlePageNumber,
                                               setCurrentCategoryID
                                           }) => {
    const resetPagination = () => {
        setCurrentPageNumber(1);
        setMiddlePageNumber(2);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-3">
                        <h6>Kategoria: </h6>
                        <CategorySelect
                            setCurrentCategoryID={setCurrentCategoryID}
                            resetPagination={resetPagination}
                        />
                    </div>
                    <div className="col-md-6">
                    </div>
                    <div className="col-md-3 justify-content-end allign-items-center text-right">
                        <h6 className="text-left">Liczba pozycji: </h6>
                        <PageSizeBar
                            currentPageSize={currentPageSize}
                            setPageSize={setPageSize}
                            resetPagination={resetPagination}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FilterBar;
