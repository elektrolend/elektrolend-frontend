import React, {useEffect, useState} from "react";

import {getCategories} from "../../../../services/database.service";
import {ICategory} from "../../../../interfaces/Products/ICategory";
import {isEmptyArray} from "formik";

type Props = {
    setCurrentCategoryID: (categoryID: string) => void;
    resetPagination: () => void;
};

const CategorySelect: React.FC<Props> = ({setCurrentCategoryID, resetPagination}) => {
    const [categories, setCategories] = useState([] as ICategory[]);

    useEffect(() => {
        getCategories()
            .then((c) => setCategories(c))
            .catch(() => setCategories([]));
    }, []);

    const handleChangeCategoryID = (event: any) => {
        const newCategoryID = event.target.value || "";
        resetPagination();
        setCurrentCategoryID(newCategoryID);
    };

    if (isEmptyArray(categories)) {
        return null;
    }

    return (
        <select
            className="form-control"
            onChange={handleChangeCategoryID}
        >
            <option value="">
                Wszystkie
            </option>
            {categories?.map((category, index) => {
                return (
                    <option
                        key={"category_" + index}
                        value={category.id}>
                        {category.name}
                    </option>
                );
            })}
        </select>
    );
};

export default CategorySelect;
