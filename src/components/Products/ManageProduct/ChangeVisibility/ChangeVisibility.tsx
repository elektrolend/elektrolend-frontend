import React, {useState} from "react";

import {updateProduct} from "../../../../services/database.service";

import {Spinner} from "react-bootstrap";

type Props = {
    productID: number;
    isAvailable: boolean;
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const ChangeVisibility: React.FC<Props> = ({productID, isAvailable, setState, setErrorMessage}) => {
    const [buttonState, setButtonState] = useState(0);

    const getButtonText = () => {
        if (buttonState) {
            return (
                <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
            );
        }
        if (isAvailable) {
            return "Zdezaktywuj produkt ";
        }
        return "Aktywuj produkt";
    };

    const handleOnClick = () => {
        setButtonState(100);
        setState(100);
        setErrorMessage("");
        updateProduct(productID, {isAvailable: !isAvailable})
            .then((response) => {
                if (response.status === 200) {
                    setState(response.status);
                }
            })
            .catch((error) => {
                if (error.response.status < 500) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                } else {
                    setState(500);
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
        setButtonState(0);
    };


    return (
        <div className="col-md-12">
            <button type="button"
                    className="btn btn-primary"
                    style={{width: "80%"}}
                    onClick={() => handleOnClick()}
            >
                {getButtonText()}
            </button>
        </div>
    );
};

export default ChangeVisibility;
