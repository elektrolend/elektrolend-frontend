import * as Yup from "yup";

const EditProductValidationSchema = Yup.object().shape({
    categoryID: Yup.number()
        .required('Kategoria jest wymagana!'),
    name: Yup.string()
        .min(5, 'Nazwa produktu musi mieć minimalnie 5 znaków!')
        .max(55, 'Nazwa produktu może mieć maksymalnie 55 znaków!')
        .required('Nazwa produktu jest wymagana!'),
    description: Yup.string()
        .min(10, 'Opis musi mieć minimalnie 10 znaków!')
        .max(600, 'Opis może mieć maksymalnie 600 znaków!')
        .required('Opis jest wymagany!'),
    deposit: Yup.number()
        .min(1, 'Kaucja musi być liczbą dodatnią!')
        .required('Kaucja jest wymagana!'),
    insurance: Yup.number()
        .min(1, 'Kwota za ubezpieczenie musi być liczbą dodatnią!')
        .required('Kwota za ubezpieczenie jest wymagana!'),
});

export default EditProductValidationSchema;
