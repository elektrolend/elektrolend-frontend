import React from "react";

import PhotoItem from "../../../Complaints/ComplaintsManagement/ComplaintPage/ComplaintPhotos/PhotoItem";

type Props = {
    imageUrl: string;
};

const CurrentPhoto: React.FC<Props> = ({imageUrl}) => {
    return (
        <div className="col-12 form-group">
            <label className="form-label">Aktualne zdjęcie</label>
            <div className="input-group">
                <PhotoItem imageUrl={imageUrl}/>
            </div>
        </div>
    );
};

export default CurrentPhoto;
