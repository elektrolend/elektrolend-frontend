import React, {useState} from "react";
import {Redirect} from "react-router-dom";

type Props = {
    productID: number;
};

const EditProductButton: React.FC<Props> = ({productID}) => {
    const [editable, setEditable] = useState(false);

    if (editable) {
        return (
            <Redirect to={{
                pathname: "/edit-product",
                state: {
                    productID: productID
                }
            }}/>
        );
    }

    return (
        <div className="col-md-12">
            <button
                type="button"
                className="btn btn-primary"
                style={{width: "80%"}}
                onClick={() => setEditable(true)}
            >
                Edytuj produkt
            </button>
        </div>
    );
};

export default EditProductButton;
