import React, {useEffect, useState} from "react";
import {useQuery} from "react-query";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {generatePath, Link, Redirect} from "react-router-dom";

import UploadPhoto from "../../../UploadFiles/UploadPhoto";
import SpinnerButton from "../../../Utils/SpinnerButton";
import CurrentPhoto from "./CurrentPhoto";
import {editProduct, getCategories, getProduct} from "../../../../services/database.service";
import {generateImageName, getActualProgress, getImageUrl, uploadPhoto} from "../../../UploadFiles/photos.service";
import AddProductFields from "../../AddProduct/AddProductFields";
import EditProductValidationSchema from "./EditProductValidationSchema";
import {ICategory} from "../../../../interfaces/Products/ICategory";
import {IProduct} from "../../../../interfaces/Products/IProduct";

import {ArrowLeftCircleFill, CheckCircleFill} from "react-bootstrap-icons";

type Props = {
    productID: number;
};

const EditProduct = ({...props}) => {
    const state: Props = props.history.location.state;
    const [product, setProduct] = useState(null as IProduct | null);

    const [status, setStatus] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");
    const [progress, setProgress] = useState(0);
    const [backToPrevPage, setBackToPrevPage] = useState(false);

    const {data: categories, isLoading: categoriesLoading, error: categoriesError} = useQuery<ICategory[]>(
        'states',
        getCategories
    );

    useEffect(() => {
        if (state.productID) {
            getProduct(state.productID)
                .then((p) => setProduct(p))
                .catch(() => setProduct(null));
        }
    }, [state.productID]);

    if (!state.productID) {
        return null;
    }

    if (categoriesLoading || categoriesError) {
        return <div> Ładowanie ...</div>;
    }

    if (!product) {
        return null;
    }

    if (status === 200) {
        return (
            <div className="container mt-2 mb-2">
                <div className="border mb-3">
                    <div className="bg-light m-2">
                        <div className="row p-2 m-2 justify-content-center">
                            <div className="p-4 bg-light row m-5 text-center">
                                <CheckCircleFill
                                    className="col-md-12 mb-2 text-success"
                                    size={50}
                                />
                                <h2 className="col-md-12 text-success">Sukces!</h2>
                                <p className="col-md-12 h6">
                                    Udało się poprawnie dokonać edycji produktu.
                                </p>
                                <p className="col-md-12">
                                    <Link
                                        className="hover-move"
                                        to={generatePath(`/product/details/:id/:name`,
                                            {id: product.id, name: product.name})}
                                    >
                                        Powrót do produktu
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    if (backToPrevPage) {
        return (
            <Redirect
                to={generatePath(`/product/details/:id/:name`,
                    {id: product.id, name: product.name})}
            />
        );
    }

    const handleEditProduct = (productID: number, productToEdit: any) => {
        delete productToEdit['image'];
        editProduct(productID, productToEdit)
            .then((response) => {
                setStatus(response.status);
                setBackToPrevPage(true);
            })
            .catch((reason) => setErrorMessage(reason));
    };

    const onSubmit = (productToEdit: any) => {
        setStatus(100);
        productToEdit.isAvailable = product.isAvailable;
        if (productToEdit.image) {
            const imageName = generateImageName(productToEdit.image);
            uploadPhoto(imageName, productToEdit.image, "products").on(
                "state_changed",
                snapshot => {
                    setProgress(getActualProgress(snapshot));
                },
                (error) => {
                    setStatus(500);
                    setErrorMessage(error.message);
                },
                () => {
                    getImageUrl("images/products", imageName).then(url => {
                        productToEdit.imageUrl = url;
                        handleEditProduct(product.id, productToEdit);
                    });
                }
            );
        } else {
            productToEdit.imageUrl = product.imageUrl;
            handleEditProduct(product.id, productToEdit);
        }
    };

    return (
        <div className="container mt-2 mb-2">
            <div>
                <h2 className="text-primary">{product.name}</h2>
                <h6 className="mb-10" onClick={() => setBackToPrevPage(true)}>
                    <ArrowLeftCircleFill className="mb-1 mr-1"/>
                    POWRÓT DO PRODUKTU
                </h6>
            </div>
            <div className="border">
                <Formik
                    initialValues={{
                        categoryID: product.categoryID,
                        name: product.name,
                        image: '',
                        description: product.description,
                        deposit: product.deposit,
                        insurance: product.insurance
                    }}
                    validationSchema={EditProductValidationSchema}
                    onSubmit={values => {
                        onSubmit(values);
                    }}>
                    {({setFieldValue}) => (
                        <Form>
                            <div className="bg-primary text-white p-2 m-2">
                                <h4>Edytuj produkt</h4>
                            </div>
                            <div className="bg-light p-2 m-2">
                                <div className="row">
                                    <div className="col-12">
                                        <label className="form-label mr-1">Kategoria</label>
                                        <Field name="categoryID" as="select" className="form-control">
                                            {categories?.map((category, index) => {
                                                return (
                                                    <option key={index} value={category.id}>
                                                        {category.name}
                                                    </option>
                                                );
                                            })}
                                        </Field>
                                        <ErrorMessage
                                            render={msg =>
                                                <div className="alert alert-danger">{msg}</div>}
                                            name="categoryID"/>
                                    </div>
                                    {AddProductFields.map((field, index) => {
                                        return (
                                            <div className="col-12 form-group" key={index}>
                                                <label className="form-label">{field.label}</label>
                                                <Field name={field.name}
                                                       as={field.type}
                                                       className={field.cName}
                                                       rows={field.rows}
                                                />
                                                <ErrorMessage render={msg => <div
                                                    className="alert alert-danger">{msg}</div>}
                                                              name={field.name}/>
                                            </div>
                                        )
                                    })}
                                    <div className="col-12 form-group">
                                        <label className="form-label">Kaucja</label>
                                        <div className="input-group">
                                            <Field
                                                type="number"
                                                className="form-control"
                                                name="deposit"
                                                min={1}>
                                            </Field>
                                            <div className="input-group-append">
                                                <span className="input-group-text">PLN</span>
                                            </div>
                                        </div>
                                        <ErrorMessage render={msg => <div
                                            className="alert alert-danger">{msg}</div>}
                                                      name="deposit"/>
                                    </div>
                                    <div className="col-12 form-group">
                                        <label className="form-label">Ubezpieczenie</label>
                                        <div className="input-group mb-3">
                                            <Field
                                                type="number"
                                                className="form-control"
                                                name="insurance"
                                                min={1}>
                                            </Field>
                                            <div className="input-group-append">
                                                <span className="input-group-text">PLN</span>
                                            </div>
                                        </div>
                                        <ErrorMessage render={msg => <div
                                            className="alert alert-danger">{msg}</div>}
                                                      name="insurance"/>
                                    </div>
                                    <CurrentPhoto imageUrl={product.imageUrl}/>
                                    <UploadPhoto
                                        setFieldValue={setFieldValue}
                                        progress={progress}
                                        fieldName={"image"}
                                    />
                                    {errorMessage ?
                                        <div className="alert alert-danger" role="alert">
                                            {errorMessage}
                                        </div>
                                        : null}
                                    <div className="col-12">
                                        <SpinnerButton
                                            state={status}
                                            buttonText={"Zatwierdź zmiany"}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default EditProduct;
