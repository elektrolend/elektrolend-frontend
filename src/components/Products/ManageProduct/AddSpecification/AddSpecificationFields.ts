interface AddSpecificationField {
    label: string,
    name: string,
    type: string,
    cName: string,
}

const AddSpecificationFields: AddSpecificationField[] = [
    {
        label: "Nazwa specyfikacji",
        name: "key",
        type: "text",
        cName: "form-control"
    },
    {
        label: "Wartość specyfikacji",
        name: "value",
        type: "text",
        cName: "form-control"
    }
]

export default AddSpecificationFields;
