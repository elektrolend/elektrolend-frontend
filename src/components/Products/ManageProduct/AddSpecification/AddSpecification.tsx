import React, {useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";

import {addSpecification} from "../../../../services/database.service";
import {INewSpecification} from "../../../../interfaces/Products/INewSpecification";
import AddSpecificationFields from "./AddSpecificationFields";
import SpecificationValidationSchema from "./SpecificationValidationSchema";

type Props = {
    id: number;
    productChanged: boolean;
    setProductChanged: (productChanged: boolean) => void;
};

const AddSpecification: React.FC<Props> = ({id, productChanged, setProductChanged}) => {
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');

    const onSubmit = (specification: INewSpecification) => {
        specification["productID"] = id;
        addSpecification(specification)
            .then(response => setState(response.status))
            .catch(reason => setErrorMessage(reason));
    };

    const specificationAdded = () => {
        return state === 201
    };

    return (
        <div className="col-md-12">
            <button type="button"
                    className={"btn " + (specificationAdded() ? "btn-secondary" : "btn-primary")}
                    style={{width: "80%"}}
                    disabled={specificationAdded()}
                    data-toggle="modal"
                    data-target="#specificationModal">

                {!specificationAdded() ? "Dodaj specyfikację" : "Dodano specyfikację"}
            </button>
            <Formik
                initialValues={{
                    productID: -1,
                    key: '',
                    value: ''
                }}
                validationSchema={SpecificationValidationSchema}
                onSubmit={values => {
                    onSubmit(values);
                }}>
                {() => (
                    <div className="modal fade"
                         id="specificationModal"
                         role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Dodaj specyfikację</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <Form>
                                        <div className="bg-light p-2 m-2">
                                            <div className="row">
                                                {AddSpecificationFields.map((field, index) => {
                                                    return (
                                                        <div className="col-12 form-group" key={index}>
                                                            <label className="form-label">{field.label}</label>
                                                            <Field name={field.name}
                                                                   type={field.type}
                                                                   className={field.cName}
                                                                   disabled={specificationAdded()}
                                                            />
                                                            <ErrorMessage render={msg =>
                                                                <div className="alert alert-danger">
                                                                    {msg}
                                                                </div>}
                                                                          name={field.name}/>
                                                        </div>
                                                    )
                                                })}
                                                {errorMessage ?
                                                    <div className="alert alert-danger" role="alert">
                                                        {errorMessage}
                                                    </div> : null}
                                                <div className="col-12 mt-4">
                                                    {specificationAdded() ?
                                                        <div>
                                                            <div className="alert alert-success">
                                                                Udało się poprawnie dodać specyfikację!
                                                            </div>
                                                            <button className="btn btn-lg btn-danger"
                                                                    data-dismiss="modal"
                                                                    type="button"
                                                                    style={{width: "100%"}}
                                                                    onClick={() =>
                                                                        setProductChanged(!productChanged)}>
                                                                Zamknij
                                                            </button>
                                                        </div> :
                                                        <button className="btn btn-lg btn-primary"
                                                                type="submit"
                                                                style={{width: "100%"}}>
                                                            Zatwierdź
                                                        </button>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </Formik>
        </div>
    );
};

export default AddSpecification;
