import * as Yup from "yup";

const SpecificationValidationSchema = Yup.object().shape({
    productID: Yup.number()
        .required('ID produktu jest wymagane!'),
    key: Yup.string()
        .required('Nazwa specyfikacji jest wymagana!')
        .min(2, 'Nazwa specyfikacji musi mieć minimalnie 2 litery!')
        .max(15, 'Nazwa specyfikacji może mieć maksymalnie 15 liter!')
        .matches(/^[a-zA-Z]+$/, 'Nazwa specyfikacji może składać się tylko z liter!'),
    value: Yup.string()
        .required('Wartość specyfikacji jest wymagana!')
        .min(2, 'Wartość specyfikacji musi mieć minimalnie 2 znaki!')
        .max(15, 'Wartość specyfikacji może mieć maksymalnie 15 znaków!'),
});

export default SpecificationValidationSchema;
