import React from "react";

import AddPrice from "./AddPrice/AddPrice";
import AddSpecification from "./AddSpecification/AddSpecification";
import ChangeVisibility from "./ChangeVisibility/ChangeVisibility";
import {IProduct} from "../../../interfaces/Products/IProduct";

import {PencilSquare} from 'react-bootstrap-icons';
import EditProductButton from "./EditProduct/EditProductButton";

type Props = {
    product: IProduct;
    productChanged: boolean;
    setProductChanged: (productChanged: boolean) => void;
    setState: (state: number) => void;
    setErrorMessage: (message: string) => void;
};

const ManageProduct: React.FC<Props> = ({product, productChanged, setProductChanged, setState, setErrorMessage}) => {
    if (!product) {
        return null;
    }

    return (
        <div className="border bg-light text-center">
            <div className="row mt-2">
                <div className="col-md-12">
                    <h6>
                        <PencilSquare/>
                        ZARZĄDZAJ PRODUKTEM
                    </h6>
                </div>
            </div>
            <div className="row">
                <EditProductButton productID={product.id} />
                <AddPrice
                    id={product.id}
                    productChanged={productChanged}
                    setProductChanged={setProductChanged}
                />
                <AddSpecification
                    id={product.id}
                    productChanged={productChanged}
                    setProductChanged={setProductChanged}
                />
                <ChangeVisibility
                    productID={product.id}
                    isAvailable={product.isAvailable}
                    setState={setState}
                    setErrorMessage={setErrorMessage}
                />
            </div>
        </div>
    );

}

export default ManageProduct;
