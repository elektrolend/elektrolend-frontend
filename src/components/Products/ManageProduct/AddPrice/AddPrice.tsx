import React, {useState} from "react";
import {ErrorMessage, Form, Formik} from "formik";
import {startOfDay} from "date-fns";

import DateRangePicker from "./DateRangePicker";
import PriceRangePicker from "./PriceRangePicker";
import {addPrice} from "../../../../services/database.service";
import {INewPrice} from "../../../../interfaces/Products/INewPrice";
import {DateManipulation} from "../../../Utils/DateManipulation";


type Props = {
    id: number;
    productChanged: boolean;
    setProductChanged: (productChanged: boolean) => void;
};

const AddPrice: React.FC<Props> = ({id, productChanged, setProductChanged}) => {
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');

    const [startDate, setStartDate] = useState(startOfDay(new Date()));
    const [finishDate, setFinishDate] = useState(DateManipulation.getNextDay(startOfDay(new Date())));
    const [endDateMarked, setEndDateMarked] = useState(true);

    const [priceNetto, setPriceNetto] = useState(1);
    const [priceBrutto, setPriceBrutto] = useState(1);
    const [deposit, setDeposit] = useState(0);

    const onSubmit = (price: INewPrice) => {
        price["productID"] = id;
        price["beginDate"] = DateManipulation.getDateInString(startDate);
        price["endDate"] = endDateMarked ? DateManipulation.getDateInString(finishDate) : null;
        price["pricePerDayNetto"] = priceNetto;
        price["pricePerDayBrutto"] = priceBrutto;
        addPrice(price)
            .then(response => setState(response.status))
            .catch(reason => setErrorMessage(reason));
    };

    const priceAdded = () => {
        return state === 201
    };


    return (
        <div className="col-md-12">
            <button type="button"
                    className={"btn " + (priceAdded() ? "btn-secondary" : "btn-primary")}
                    style={{width: "80%"}}
                    disabled={priceAdded()}
                    data-toggle="modal"
                    data-target="#priceModal">
                {!priceAdded() ? "Dodaj cenę" : "Dodano cenę"}
            </button>
            <Formik
                initialValues={{
                    productID: -1,
                    pricePerDayNetto: 0,
                    pricePerDayBrutto: 0,
                    beginDate: '',
                    endDate: '',
                    deposit: 0
                }}
                onSubmit={values => {
                    onSubmit(values);
                }}>
                {() => (
                    <div className="modal fade"
                         id="priceModal"
                         role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Dodaj cenę</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <Form>
                                        <div className="bg-light p-2 m-2">
                                            <div className="row">
                                                <DateRangePicker
                                                    startDate={startDate}
                                                    setStartDate={setStartDate}
                                                    finishDate={finishDate}
                                                    setFinishDate={setFinishDate}
                                                    priceAdded={priceAdded()}
                                                    endDateMarked={endDateMarked}
                                                    setEndDateMarked={setEndDateMarked}/>
                                                <PriceRangePicker
                                                    priceNetto={priceNetto}
                                                    setPriceNetto={setPriceNetto}
                                                    priceBrutto={priceBrutto}
                                                    setPriceBrutto={setPriceBrutto}
                                                    priceAdded={priceAdded()}
                                                    deposit={deposit}
                                                    setDeposit={setDeposit}/>
                                                {errorMessage ?
                                                    <div className="alert alert-danger" role="alert">
                                                        {errorMessage}
                                                    </div> : null}
                                                <ErrorMessage render={msg =>
                                                    <div className="alert alert-danger">
                                                        {msg}
                                                    </div>}
                                                              name="productID"/>
                                                <div className="col-12 mt-4">
                                                    {priceAdded() ?
                                                        <div>
                                                            <div className="alert alert-success">
                                                                Udało się poprawnie dodać cenę!
                                                            </div>
                                                            <button className="btn btn-lg btn-danger"
                                                                    data-dismiss="modal"
                                                                    type="button"
                                                                    style={{width: "100%"}}
                                                                    onClick={() =>
                                                                        setProductChanged(!productChanged)}>
                                                                Zamknij
                                                            </button>
                                                        </div> :
                                                        <button className="btn btn-lg btn-primary"
                                                                type="submit"
                                                                style={{width: "100%"}}>
                                                            Zatwierdź
                                                        </button>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </Formik>
        </div>
    );
};

export default AddPrice;
