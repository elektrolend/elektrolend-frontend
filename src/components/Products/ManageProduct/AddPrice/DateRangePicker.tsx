import React from "react";
import DatePicker from "react-datepicker";

import {DateManipulation} from "../../../Utils/DateManipulation";

type Props = {
    startDate: Date;
    setStartDate: (sDate: Date) => void;
    finishDate: Date;
    setFinishDate: (fDate: Date) => void;
    priceAdded: boolean;
    endDateMarked: boolean;
    setEndDateMarked: (dateMarked: boolean) => void;
};

const DateRangePicker: React.FC<Props> = ({startDate,
                                              setStartDate,
                                              finishDate,
                                              setFinishDate,
                                              priceAdded,
                                              endDateMarked,
                                              setEndDateMarked
                                          }) => {
    return (
        <>
            <div className="col-md-6 form-group">
                <label className="form-label">Od</label>
                <DatePicker minDate={new Date()}
                            maxDate={DateManipulation.getDayBefore(finishDate)}
                            selected={startDate}
                            className="form-control"
                            disabled={priceAdded}
                            onChange={(date: any) => setStartDate(date)}/>
            </div>
            <div className="col-md-6 form-group">
                <label className="form-label">Do</label>
                <DatePicker minDate={DateManipulation.getNextDay(startDate)}
                            selected={finishDate}
                            className="form-control"
                            disabled={priceAdded || !endDateMarked}
                            onChange={(date: any) => setFinishDate(date)}/>
                <div className="col-sm-12">
                <input type="checkbox"
                       disabled={priceAdded}
                       className="form-check-inline"
                       onClick={() => setEndDateMarked(!endDateMarked)}>
                </input>
                <label className="form-check-label">Bezterminowo</label>
                </div>
            </div>
        </>
    )

}
export default DateRangePicker;
