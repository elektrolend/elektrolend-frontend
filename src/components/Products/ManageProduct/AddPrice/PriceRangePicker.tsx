import React from "react";

type Props = {
    priceNetto: number;
    setPriceNetto: (price: number) => void;
    priceBrutto: number;
    setPriceBrutto: (price: number) => void;
    priceAdded: boolean;
    deposit: number;
    setDeposit: (price: number) => void;
};

const PriceRangePicker: React.FC<Props> = ({
                                               priceNetto,
                                               setPriceNetto,
                                               priceBrutto,
                                               setPriceBrutto,
                                               priceAdded,
                                               deposit,
                                               setDeposit
                                           }) => {

    return (
        <>
            <div className="col-md-6 form-group">
                <label className="form-label">Cena za dzień netto</label>
                <input name="pricePerDayNetto"
                       type="number"
                       min={1}
                       max={priceBrutto}
                       value={priceNetto}
                       onChange={(event: any) => setPriceNetto(Number(event.target.value))}
                       className="form-control"
                       disabled={priceAdded}
                />
            </div>
            <div className="col-md-6 form-group">
                <label className="form-label">Cena za dzień brutto</label>
                <input name="pricePerDayBrutto"
                       type="number"
                       min={priceNetto}
                       value={priceBrutto}
                       onChange={(event: any) => setPriceBrutto(Number(event.target.value))}
                       className="form-control"
                       disabled={priceAdded}
                />
            </div>
            <div className="col-md-6 form-group">
                <label className="form-label">Kaucja</label>
                <input name="deposit"
                       type="number"
                       min={0}
                       value={deposit}
                       onChange={(event: any) => setDeposit(Number(event.target.value))}
                       className="form-control"
                       disabled={priceAdded}
                />
            </div>
        </>
    )

}
export default PriceRangePicker;
