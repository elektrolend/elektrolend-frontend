import React, {useState} from "react";
import {isEmptyArray} from "formik";

import Product from "../Products/Product";
import PageNumberBar from "../Pagination/PageNumberBar";
import FilterBar from "./FilterBar/FilterBar";
import {getFilteredProducts} from "../../services/database.service";
import {IProduct} from "../../interfaces/Products/IProduct";
import {ICartItem} from "../../interfaces/Orders/ICartItem";
import {IProductsParams} from "../../interfaces/Products/IProductsParams";

type Props = {
    cartItems: ICartItem[];
    addToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    removeFromCart: (id: number) => void;
    isInCart: (id: number) => boolean;
    categoryId: string;
};

const Products: React.FC<Props> = ({addToCart, isInCart, categoryId}) => {
    const [products, setProducts] = useState([] as IProduct[]);
    const [currentPageNumber, setCurrentPageNumber] = useState(1);
    const [middlePageNumber, setMiddlePageNumber] = useState(currentPageNumber + 1);
    const [currentPageSize, setCurrentPageSize] = useState(5);
    const [maxPageNumber, setMaxPageNumber] = useState(1);

    React.useEffect(() => {
        const productsParams: IProductsParams = {
            pageSize: currentPageSize,
            pageNumber: currentPageNumber,
            categoryId: categoryId,
            availability: true
        };
        getFilteredProducts(productsParams).then(productsResponse => {
            setProducts(productsResponse.productDTOList);
            setMaxPageNumber(Math.ceil(productsResponse.paginationDTO.totalResults / currentPageSize));
        }).catch(() => setProducts([]));
    }, [currentPageSize, currentPageNumber, categoryId]);

    if (isEmptyArray(products)) {
        return (
            <div className="container mt-2">
                <div className="h2 text-primary"> Produkty</div>
                <div>Loading ...</div>
                ;
            </div>
        )
    }

    return (
        <div className="container mt-2">
            <div className="h2 text-primary"> Produkty</div>
            <FilterBar
                currentPageSize={currentPageSize}
                setPageSize={setCurrentPageSize}
                setCurrentPageNumber={setCurrentPageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
            />
            <div className="border mb-3">
                {products.map((product, index) => (
                    <Product
                        key={index}
                        product={product}
                        handleAddToCart={addToCart}
                        isInCart={isInCart}>
                    </Product>
                ))}
            </div>
            <PageNumberBar
                currentPageNumber={currentPageNumber}
                setCurrentPageNumber={setCurrentPageNumber}
                middlePageNumber={middlePageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                maxPageNumber={maxPageNumber}/>
        </div>
    )
}

export default Products;
