import React, {useEffect, useState} from "react";
import {generatePath, Link} from "react-router-dom";
import pl from 'date-fns/locale/pl';

import ProtectedComponent from "../Auth/ProtectedComponent";
import {DateManipulation} from "../Utils/DateManipulation";
import {IProduct} from "../../interfaces/Products/IProduct";
import {DateRange} from "react-date-range";
import {IProductOrderDate} from "../../interfaces/Orders/IProductOrderDate";
import {getDisabledDates} from "../../services/database.service";
import {getImageSrc} from "../UploadFiles/photos.service";
import {AccountType} from "../../interfaces/Accounts/AccountType";

import {CheckLg, XLg} from "react-bootstrap-icons";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import "./Products.css";

type Props = {
    product: IProduct;
    handleAddToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    isInCart: (id: number) => boolean;
}

const Product: React.FC<Props> = ({product, handleAddToCart, isInCart}) => {
    const [chooseDate, setChooseDate] = useState(false);
    const [disabledDates, setDisabledDates] = useState([] as IProductOrderDate[]);
    const id = product.id;
    const name = product.name.replace(/\s/g, '-');

    const [state, setState] = useState([
        {
            startDate: new Date(),
            endDate: new Date(),
            key: 'selection',
        }
    ]);

    useEffect(() => {
        getDisabledDates(product.id).then(d => setDisabledDates(d.dates));
    }, [product.id]);

    useEffect(() => {
        const initDate = DateManipulation.getInitDate(disabledDates);
        setState([{startDate: initDate, endDate: initDate, key: "selection"}]);
    }, [disabledDates]);

    const checkAvailability = (date: Date) => {
        for (const d of disabledDates) {
            if (DateManipulation.isBetween(date, d.beginDate, d.endDate)) {
                return true;
            }
        }

        return false;
    };

    const canBeSelected = (start: Date, end: Date) => {
        const daysDiff = DateManipulation.getDatesDiffInDays(start, end);

        for (let i = 0; i < daysDiff; i++) {
            const checkDate = DateManipulation.getDateAfterDays(start, i);
            for (const d of disabledDates) {
                if (DateManipulation.isBetween(checkDate, d.beginDate, d.endDate)) {
                    return false;
                }
            }
        }
        return true;
    }

    const handleSetState = (item: any) => {
        const [{startDate, endDate}] = [item.selection];
        if (canBeSelected(startDate, endDate)) {
            setState([item.selection]);
        }
    };

    const handleSelect = () => {
        const [dates] = state;
        handleAddToCart(product, dates.startDate, dates.endDate);
        setChooseDate(false);
    };

    if (!product.isAvailable) {
        return null;
    }

    return (
        <div key={product.id} className="border row m-2 w-20 mt-4 p-5">
            <div className="col-md">
                <img
                    src={getImageSrc(product.imageUrl)}
                    className="img-fluid"
                    alt={product.name}>
                </img>
            </div>
            <div className="col-md-6">
                <div className="flex-column">
                    <div className="col-md">
                        <h3>
                            <Link to={generatePath(`/product/details/:id/:name`, {id: id, name: name})}>
                                <strong>
                                    {product.name}
                                </strong>
                            </Link>
                        </h3>
                    </div>
                    <div className="col-md">
                        {product.description}
                    </div>
                </div>
            </div>
            <div className="col-md">
                <div className="flex-column">
                    <h3>
                        {product.price} zł / DZIEŃ
                    </h3>
                    <ProtectedComponent
                        component={
                            <div className="col-md">
                                {isInCart(product.id) ? (
                                    <button disabled={isInCart(product.id)} className="btn btn-secondary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor" className="bi bi-cart mr-2" viewBox="0 0 16 16">
                                            <path
                                                d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                        </svg>
                                        Dodano do koszyka</button>) : (
                                    <button onClick={() => setChooseDate(!chooseDate)} className="btn btn-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor" className="bi bi-cart mr-2" viewBox="0 0 16 16">
                                            <path
                                                d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                        </svg>
                                        Dodaj do koszyka</button>)
                                }
                                {
                                    chooseDate ? (
                                        <div className="border p-1">
                                            <DateRange
                                                editableDateInputs={false}
                                                ranges={state}
                                                onChange={(item: any) => handleSetState(item)}
                                                moveRangeOnFirstSelection={false}
                                                minDate={new Date()}
                                                disabledDay={date => {
                                                    return checkAvailability(date)
                                                }}
                                                dragSelectionEnabled={false}
                                                showSelectionPreview={false}
                                                locale={pl}
                                            >
                                            </DateRange>
                                            <div className="row">
                                                <div className="col-12">
                                                    <button type="button" onClick={() => handleSelect()}
                                                            className="btn btn-labeled btn-success float-right">
                                                        <span className="btn-label"><CheckLg/></span>
                                                        Zatwierdź
                                                    </button>
                                                    <button type="button" onClick={() => setChooseDate(false)}
                                                            className="btn btn-labeled btn-danger float-left">
                                                        <span className="btn-label"><XLg/></span>Anuluj
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null
                                }
                            </div>
                        }
                        roles={[AccountType.none, AccountType.customer]}
                    />
                </div>

            </div>
        </div>

    )
}

export default Product;
