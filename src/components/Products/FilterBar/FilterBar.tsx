import React from "react";

import PageSizeBar from "../../Pagination/PageSizeBar";

type Props = {
    currentPageSize: number;
    setPageSize: (pageSize: number) => void;
    setCurrentPageNumber: (pageNumber: number) => void;
    setMiddlePageNumber: (pageNumber: number) => void;
}

export const FilterBar: React.FC<Props> = ({
                                               currentPageSize,
                                               setPageSize,
                                               setCurrentPageNumber,
                                               setMiddlePageNumber
                                           }) => {
    const resetPagination = () => {
        setCurrentPageNumber(1);
        setMiddlePageNumber(2);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-9">
                    </div>
                    <div className="col-md-3 justify-content-end allign-items-center text-right">
                       <PageSizeBar
                           currentPageSize={currentPageSize}
                           setPageSize={setPageSize}
                           resetPagination={resetPagination}
                       />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FilterBar;
