import * as Yup from "yup";

const CategoryValidationSchema = Yup.object().shape({
    name: Yup.string()
        .min(5, 'Nazwa kategorii musi mieć minimalnie 5 znaków!')
        .max(55, 'Nazwa kategorii może mieć maksymalnie 55 znaków!')
        .required('Nazwa kategorii jest wymagana!'),
    description: Yup.string()
        .min(10, 'Opis musi mieć minimalnie 10 znaków!')
        .max(600, 'Opis może mieć maksymalnie 600 znaków!')
        .required('Opis jest wymagany!'),
    image: Yup.string()
        .required("Zdjęcie jest wymagane!")
});

export default CategoryValidationSchema;
