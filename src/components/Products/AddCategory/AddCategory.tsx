import React, {useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";

import UploadPhoto from "../../UploadFiles/UploadPhoto";
import SpinnerButton from "../../Utils/SpinnerButton";
import {generateImageName, getActualProgress, getImageUrl, uploadPhoto} from "../../UploadFiles/photos.service";
import {addCategory} from "../../../services/database.service";
import CategoryValidationSchema from "./CategoryValidationSchema";
import AddCategoryFields from "./AddCategoryFields";

const AddCategory = () => {
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');
    const [progress, setProgress] = useState(0);

    const onSubmit = (category: any) => {
        setState(100);
        const imageName = generateImageName(category.image);
        uploadPhoto(imageName, category.image, "categories").on(
            "state_changed",
            snapshot => {
                setProgress(getActualProgress(snapshot));
            },
            (error) => {
                setState(500);
                setErrorMessage(error.message);
            },
            () => {
                getImageUrl("images/categories", imageName).then(url => {
                    category.imageUrl = url;
                    addCategory(category)
                        .then(response => setState(response.status))
                        .catch(reason => setErrorMessage(reason));
                });
            }
        );
    };

    if (state === 201) {
        return (
            <div className="m-5">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading">Sukces!</h4>
                    <p>Udało Ci się poprawnie dodać nową kategorię.</p>
                    <hr></hr>
                    <p className="mb-0">
                        Teraz możesz <a className="hover-move" href="/add-product"> dodać</a> przedmiot.
                    </p>
                </div>
            </div>
        )
    } else {
        return (
            <div className="container mt-2 mb-2">
                <div className="border">
                    <Formik
                        initialValues={{
                            name: '',
                            description: '',
                            image: '',
                        }}
                        validationSchema={CategoryValidationSchema}
                        onSubmit={values => {
                            onSubmit(values);
                        }}>
                        {({setFieldValue}) => (
                            <Form>
                                <div className="bg-primary text-white p-2 m-2">
                                    <h4>Nowa kategoria</h4>
                                </div>
                                <div className="bg-light p-2 m-2">
                                    <div className="row">
                                        {AddCategoryFields.map((field, index) => {
                                            return (
                                                <div className="col-12 form-group" key={index}>
                                                    <label className="form-label">{field.label}</label>
                                                    <Field name={field.name}
                                                           as={field.type}
                                                           className={field.cName}
                                                           rows={field.rows}
                                                    />
                                                    <ErrorMessage render={msg => <div
                                                        className="alert alert-danger">{msg}</div>}
                                                                  name={field.name}/>
                                                </div>
                                            )
                                        })}
                                        <UploadPhoto
                                            setFieldValue={setFieldValue}
                                            progress={progress}
                                            fieldName={"image"}
                                        />
                                        {errorMessage ?
                                            <div className="alert alert-danger" role="alert">
                                                {errorMessage}
                                            </div>
                                            : null}
                                        <div className="col-12">
                                            <SpinnerButton
                                                state={state}
                                                buttonText={"Dodaj nową kategorię"}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        );
    }
};

export default AddCategory;
