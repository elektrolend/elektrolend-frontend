import React from "react";
import {SRLWrapper} from "simple-react-lightbox";

import {IProduct} from "../../../interfaces/Products/IProduct";
import {getImageSrc} from "../../UploadFiles/photos.service";

type Props = {
    product: IProduct
};

const ProductDetails: React.FC<Props> = ({product}) => {
    const specificationsMap = product.specificationsMap ? product.specificationsMap : [];

    return (
        <div className="border bg-light">
            <div className="row">
                <div className="col-md-4">
                    <div className="border" style={{height: "100%"}}>
                        <SRLWrapper>
                            <img
                                src={getImageSrc(product.imageUrl)}
                                className="img-fluid"
                                style={{height: "100%"}}
                                alt={product.name}>
                            </img>
                        </SRLWrapper>
                    </div>
                </div>
                <div className="col-md-8 mt-2">
                    <div className="col-12">
                        <h3 className="text-primary">
                            <strong>
                                {product.name}
                            </strong>
                        </h3>
                    </div>
                    {Object.entries(specificationsMap).map(([key, value], index) => {
                        return (
                            <div key={index}>
                                <div className="col-12">
                                    <h6>{key}</h6>
                                </div>
                                <div className="col-12">
                                    <button className="btn btn-primary btn-sm">
                                        {value.toString()}
                                    </button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );

}

export default ProductDetails;
