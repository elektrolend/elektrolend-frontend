import React from "react";
import {IProduct} from "../../../interfaces/Products/IProduct";

type Props = {
    product: IProduct;
};

const PricesBar: React.FC<Props> = ({product}) => {

    const calculateDiscount = (discount: number, days: number) => {
        return Math.round((1 - discount) * product.price * days);
    };

    return (
        <div>
            <div className="row p-3">
                <div className="col-md-4 border bg-light text-center">
                    <h4>Cena za dzień</h4>
                    <h3 className="text-primary">
                        <b>{product.price} PLN</b>
                    </h3>
                </div>
                <div className="col-md-4 border bg-light text-center">
                    <h4>Cena za tydzień</h4>
                    <h3 className="text-primary">
                        <b>{calculateDiscount(0.1, 7)} PLN</b>
                    </h3>
                    <p>
                        <b>zniżka 10%</b>
                    </p>
                </div>
                <div className="col-md-4 border bg-light text-center">
                    <h4 className="text-xl-center">Cena za miesiąc</h4>
                    <h3 className="text-primary">
                        <b>{calculateDiscount(0.2, 30)} PLN</b>
                    </h3>
                    <p>
                        <b>zniżka 20%</b>
                    </p>
                </div>
            </div>
        </div>
    );

}

export default PricesBar;
