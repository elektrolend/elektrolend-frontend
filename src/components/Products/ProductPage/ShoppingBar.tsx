import React, {useEffect, useState} from "react";
import DatePicker, {registerLocale} from "react-datepicker";
import pl from 'date-fns/locale/pl';
import {startOfDay} from "date-fns";

import {IProduct} from "../../../interfaces/Products/IProduct";
import {ICartItem} from "../../../interfaces/Orders/ICartItem";
import {IProductOrderDate} from "../../../interfaces/Orders/IProductOrderDate";
import {DateManipulation} from "../../Utils/DateManipulation";
import {getDisabledDates} from "../../../services/database.service";

import {BookmarkPlusFill, CartFill} from 'react-bootstrap-icons';

registerLocale('pl', pl);

type Props = {
    cartItems: ICartItem[];
    product: IProduct;
    addToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    inCart: (id: number) => boolean;
}

const ShoppingBar: React.FC<Props> = ({cartItems, product, addToCart, inCart}) => {
    const [startDate, setStartDate] = useState(startOfDay(new Date()));
    const [endDate, setEndDate] = useState(DateManipulation.getNextDay(startOfDay(new Date())));
    const [cartItem, setCartItem] = useState(cartItems.filter(p => p.id === product.id)[0]);
    const [disabledDates, setDisabledDates] = useState([] as IProductOrderDate[]);

    useEffect(() => {
        setCartItem(cartItems.filter(p => p.id === product.id)[0]);
    }, [cartItems, product.id]);

    useEffect(() => {
        if (product.id) {
            getDisabledDates(product.id).then(d => setDisabledDates(d.dates));
        }
    }, [product.id]);

    useEffect(() => {
        const initDate = DateManipulation.getInitDate(disabledDates);
        setStartDate(initDate);
        setEndDate(initDate);
        setMaxEndDate(DateManipulation.getMaxDate(initDate, disabledDates));
    }, [disabledDates]);

    const checkStartDate = (date: Date) => {
        for (const d of disabledDates) {
            if (DateManipulation.isBetween(date, d.beginDate, d.endDate)) {
                return false;
            }
        }
        return true;
    };

    const [maxEndDate, setMaxEndDate] = useState(DateManipulation.getMaxDate(startDate, disabledDates) as Date | null);

    const handleSelect = () => {
        addToCart(product, startDate, endDate);
    };

    const handleChangeStartDate = (date: any) => {
        if (date) {
            setStartDate(date);
            setEndDate(date);
            setMaxEndDate(DateManipulation.getMaxDate(date, disabledDates));
        }
    };

    const handleChangeEndDate = (date: any) => {
        if (date) {
            setEndDate(date);
        }
    };

    const checkEndDate = (date: Date) => {
        if (maxEndDate === null) {
            return true;
        }
        if (DateManipulation.inDates(date, startDate, maxEndDate)) {
            return true;
        }
        return false;
    };

    const calculatePrice = (productPrice: number, beginDate: Date, finishDate: Date) => {
        return productPrice * DateManipulation.getDatesDiffInDays(beginDate, finishDate);
    };

    return (
        <div className="border bg-light text-center">
            <div className="row mt-2">
                <div className="col-md-12">
                    <h6>
                        <BookmarkPlusFill/>
                        WYPOŻYCZ PRZEDMIOT
                    </h6>
                </div>
            </div>
            {!inCart(product.id) ? (
                    <div className="col-12">
                        <div className="col-md-4">
                            <b>Od</b>
                        </div>
                        <DatePicker minDate={new Date()}
                                    selected={startDate}
                                    filterDate={checkStartDate}
                                    onChange={(date) => handleChangeStartDate(date)}
                                    className="col-md-8"
                                    dateFormat="dd/MM/yyyy"
                                    locale="pl"
                                    required={true}/>
                        <div className="col-md-4">
                            <b>Do</b>
                        </div>
                        <DatePicker minDate={startDate}
                                    selected={endDate}
                                    filterDate={checkEndDate}
                                    onChange={(date: any) => handleChangeEndDate(date)}
                                    className="col-md-8"
                                    dateFormat="dd/MM/yyyy"
                                    locale="pl"
                                    required={true}/>
                    </div>) :
                <div className="col-12">
                    <div className="col-md-4">
                        <b>Od</b>
                    </div>
                    <DatePicker selected={cartItem?.beginDate}
                                onChange={(date: any) => setStartDate(date)}
                                disabled={true}
                                className="col-md-8"
                                dateFormat="dd/MM/yyyy"
                                locale="pl"/>
                    <div className="col-md-4">
                        <b>Do</b>
                    </div>
                    <DatePicker selected={cartItem?.endDate}
                                onChange={(date: any) => setEndDate(date)}
                                disabled={true}
                                className="col-md-8"
                                dateFormat="dd/MM/yyyy"
                                locale="pl"/>
                </div>}
            <div className="row">
                <div className="col-md-6">
                    <p>Do zapłaty:</p>
                </div>
                <div className="col-md-6">
                    <p className="text-primary">
                        {!inCart(product.id) ? (
                            <b>{calculatePrice(product.price, startDate, endDate)} PLN</b>) : (
                            <b>{calculatePrice(product.price, cartItem?.beginDate, cartItem?.endDate)} PLN</b>
                        )}
                    </p>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    {inCart(product.id) ? (
                        <button disabled={inCart(product.id)}
                                className="btn btn-secondary">
                            <CartFill/>
                            Dodano do koszyka
                        </button>) : (
                        <button
                            onClick={() => handleSelect()}
                            className="btn btn-primary">
                            <CartFill/>
                            Dodaj do koszyka
                        </button>
                    )
                    }
                </div>
            </div>
        </div>
    );

}

export default ShoppingBar;
