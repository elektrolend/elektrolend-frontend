import React, {useState} from "react";
import {useParams} from 'react-router';

import ProductDetails from "./ProductDetails";
import ShoppingBar from "./ShoppingBar";
import PricesBar from "./PricesBar";
import ManageProduct from "../ManageProduct/ManageProduct";
import WarningAlert from "../../Utils/WarningAlert";
import Alert from "../../Orders/UpcomingOrders/Alert/Alert";
import ProtectedComponent from "../../Auth/ProtectedComponent";
import {getProduct} from "../../../services/database.service";
import {IProduct} from "../../../interfaces/Products/IProduct";
import {ICartItem} from "../../../interfaces/Orders/ICartItem";
import {AccountType} from "../../../interfaces/Accounts/AccountType";

type Props = {
    cartItems: ICartItem[];
    addToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    isInCart: (id: number) => boolean;
};

const ProductPage: React.FC<Props> = ({cartItems, addToCart, isInCart}) => {
    const {id} = useParams<{ id: string }>();

    const [product, setProduct] = useState({} as IProduct);
    const [productChanged, setProductChanged] = useState(false);
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        getProduct(id).then(p => setProduct(p));
    }, [id, productChanged, state]);

    return (
        <div className="container mt-2">
            <WarningAlert
                show={!product.isAvailable}
                message={"Produkt nie jest dostępny publicznie!"}
            />
            <Alert
                status={state}
                errorMessage={errorMessage}
                successMessage={"Udało się poprawnie zmienić widoczność produktu!"}
            />
            <div className="h2 text-primary"> Produkt</div>
            <div className="row mt-2">
                <div className="col-md-9">
                    <ProductDetails product={product}/>
                </div>
                <div className="col-md-3">
                    <ProtectedComponent
                        component={
                            <ShoppingBar
                                cartItems={cartItems}
                                product={product}
                                addToCart={addToCart}
                                inCart={isInCart}
                            />
                        }
                        roles={[AccountType.none, AccountType.customer]}
                    />
                    <ProtectedComponent
                        component={
                            <ManageProduct
                                product={product}
                                productChanged={productChanged}
                                setProductChanged={setProductChanged}
                                setState={setState}
                                setErrorMessage={setErrorMessage}
                            />
                        }
                        roles={[AccountType.admin, AccountType.regular]}/>

                </div>
            </div>
            <div className="row mt-2 mb-2">
                <div className="col-md-9">
                    <PricesBar product={product}/>
                </div>
                <div className="col-md-3"></div>
            </div>
            <div className="row mt-2 mb-4">
                <div className="col-md-9">
                    <div className="border bg-light">
                        <div className="h2 text-primary ml-2 mt-2"> Opis produktu</div>
                        <hr></hr>
                        <p className="p-2">
                            {product.description}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default ProductPage;
