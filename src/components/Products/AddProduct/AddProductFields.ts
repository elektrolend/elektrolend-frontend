interface AddProductField {
    label: string,
    name: string,
    type: string,
    cName: string,
    rows: string
}

const AddProductFields: AddProductField[] = [
    {
        label: "Nazwa produktu",
        name: "name",
        type: "textarea",
        cName: "form-control",
        rows: "1"
    },
    {
        label: "Opis",
        name: "description",
        type: "textarea",
        cName: "form-control",
        rows: "5"

    }
]

export default AddProductFields;
