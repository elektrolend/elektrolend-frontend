import React, {useState} from "react";
import {useQuery} from "react-query";
import {Link} from "react-router-dom";
import {ErrorMessage, Field, Form, Formik} from "formik";

import UploadPhoto from "../../UploadFiles/UploadPhoto";
import SpinnerButton from "../../Utils/SpinnerButton";
import {addProduct, getCategories} from "../../../services/database.service";
import {
    generateImageName,
    getActualProgress,
    getImageUrl,
    uploadPhoto
} from "../../UploadFiles/photos.service";
import {ICategory} from "../../../interfaces/Products/ICategory";
import ProductValidationSchema from "./ProductValidationSchema";
import AddProductFields from "./AddProductFields";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons";

const AddProduct = () => {
    const {data: categories, isLoading: categoriesLoading, error: categoriesError} = useQuery<ICategory[]>(
        'states',
        getCategories
    );
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');
    const [progress, setProgress] = useState(0);

    const onSubmit = (product: any) => {
        setState(100);
        const imageName = generateImageName(product.image);
        uploadPhoto(imageName, product.image, "products").on(
            "state_changed",
            snapshot => {
                setProgress(getActualProgress(snapshot));
            },
            (error) => {
                setState(500);
                setErrorMessage(error.message);
            },
            () => {
                getImageUrl("images/products", imageName).then(url => {
                    product.imageUrl = url;
                    product['isAvailable'] = true;
                    addProduct(product)
                        .then(response => setState(response.status))
                        .catch(reason => setErrorMessage(reason));
                });
            }
        );
    };

    if (categoriesLoading || categoriesError) {
        return <div> Ładowanie ...</div>;
    }

    if (state === 201) {
        return (
            <div className="m-5">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading">Sukces!</h4>
                    <p>Udało Ci się poprawnie dodać nowy przedmiot.</p>
                    <hr></hr>
                    <p className="mb-0">
                        Teraz możesz <a className="hover-move" href="/add-product"> dodać</a> kolejny przedmiot.
                        Możesz także <a className="hover-move" href="/products">przejść</a> do wszystkich przedmiotów.
                    </p>
                </div>
            </div>
        )
    } else {
        return (
            <div className="container mt-2 mb-2">
                <div className="border">
                    <Formik
                        initialValues={{
                            categoryID: 1,
                            name: '',
                            image: '',
                            description: '',
                            deposit: 1,
                            insurance: 1
                        }}
                        validationSchema={ProductValidationSchema}
                        onSubmit={values => {
                            onSubmit(values);
                        }}>
                        {({setFieldValue}) => (
                            <Form>
                                <div className="bg-primary text-white p-2 m-2">
                                    <h4>Nowy produkt</h4>
                                </div>
                                <div className="bg-light p-2 m-2">
                                    <div className="row">
                                        <div className="col-12">
                                            <label className="form-label mr-1">Kategoria</label>
                                            <Link to='/add-category'>
                                                <FontAwesomeIcon
                                                    className="icon text-success"
                                                    icon={faPlusCircle}/>
                                            </Link>
                                            <Field name="categoryID" as="select" className="form-control">
                                                {categories?.map((category, index) => {
                                                    return (
                                                        <option key={index} value={category.id}>
                                                            {category.name}
                                                        </option>
                                                    );
                                                })}
                                            </Field>
                                            <ErrorMessage
                                                render={msg =>
                                                    <div className="alert alert-danger">{msg}</div>}
                                                name="categoryID"/>
                                        </div>
                                        {AddProductFields.map((field, index) => {
                                            return (
                                                <div className="col-12 form-group" key={index}>
                                                    <label className="form-label">{field.label}</label>
                                                    <Field name={field.name}
                                                           as={field.type}
                                                           className={field.cName}
                                                           rows={field.rows}
                                                    />
                                                    <ErrorMessage render={msg => <div
                                                        className="alert alert-danger">{msg}</div>}
                                                                  name={field.name}/>
                                                </div>
                                            )
                                        })}
                                        <div className="col-12 form-group">
                                            <label className="form-label">Kaucja</label>
                                            <div className="input-group">
                                                <Field
                                                    type="number"
                                                    className="form-control"
                                                    name="deposit"
                                                    min={1}>
                                                </Field>
                                                <div className="input-group-append">
                                                    <span className="input-group-text">PLN</span>
                                                </div>
                                            </div>
                                            <ErrorMessage render={msg => <div
                                                className="alert alert-danger">{msg}</div>}
                                                          name="deposit"/>
                                        </div>
                                        <div className="col-12 form-group">
                                            <label className="form-label">Ubezpieczenie</label>
                                            <div className="input-group mb-3">
                                                <Field
                                                    type="number"
                                                    className="form-control"
                                                    name="insurance"
                                                    min={1}>
                                                </Field>
                                                <div className="input-group-append">
                                                    <span className="input-group-text">PLN</span>
                                                </div>
                                            </div>
                                            <ErrorMessage render={msg => <div
                                                className="alert alert-danger">{msg}</div>}
                                                          name="insurance"/>
                                        </div>
                                        <UploadPhoto
                                            setFieldValue={setFieldValue}
                                            progress={progress}
                                            fieldName={"image"}
                                        />
                                        {errorMessage ?
                                            <div className="alert alert-danger" role="alert">
                                                {errorMessage}
                                            </div>
                                            : null}
                                        <div className="col-12">
                                            <SpinnerButton
                                                state={state}
                                                buttonText={"Dodaj nowy produkt"}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        );
    }
};

export default AddProduct;
