import * as Yup from "yup";

import EditProductValidationSchema from "../ManageProduct/EditProduct/EditProductValidationSchema";

const ProductValidationSchema = Yup.object().shape({
    image: Yup.string()
        .required('Zdjęcie jest wymagane!')
}).concat(EditProductValidationSchema);

export default ProductValidationSchema;
