import React from "react";
import {XLg} from "react-bootstrap-icons";

type Props = {
    errorMessage: string;
};

const DangerAlert: React.FC<Props> = ({errorMessage}) => {
    if (errorMessage) {
        return (
            <div
                className="border row m-2 mt-4 alert alert-danger d-flex align-items-center"
                role="alert">
                <XLg />
                <div className="ml-1">
                    <b>Coś poszło nie tak... </b> {errorMessage}
                </div>
            </div>
        );
    }
    return null;

};

export default DangerAlert;
