import React from "react";

import {Spinner} from "react-bootstrap";

type Props = {
    state: number;
    buttonText: string;
};

const SpinnerButton: React.FC<Props> = ({state, buttonText}) => {
    return (
        <button
            className="btn btn-lg btn-primary"
            style={{width: "100%"}}
            type="submit">
            {state === 100 ?
                <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
                : buttonText}
        </button>
    );
};

export default SpinnerButton;
