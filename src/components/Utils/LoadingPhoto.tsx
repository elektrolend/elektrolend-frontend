import React, {useState} from "react";

type Props = {
    photoUrl: string;
    photoAlt: string;
};

const LoadingPhoto: React.FC<Props> = ({photoUrl, photoAlt}) => {
    const [isLoading, setIsLoading] = useState(true);

    const photoLoaded = () => {
        setIsLoading(false);
    };

    return (
        <>
            <div
                className="spinner-border align-self-center mt-5 mb-3 text-primary"
                role="status"
                style={{display: isLoading ? "block" : "none"}}>
            </div>
            <img
                alt={photoAlt}
                className="card-img-top"
                style={{display: isLoading ? "none" : "block"}}
                src={photoUrl}
                onLoad={() => photoLoaded()}
            />
        </>
    );
};

export default LoadingPhoto;
