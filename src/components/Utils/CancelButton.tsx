import React from "react";

type Props = {
    text: string;
    newState: number;
    setState: (state: number) => void;
};

const CancelButton: React.FC<Props> = ({text, newState, setState}) => {
    return (
        <button
            className="btn btn-lg btn-outline-danger"
            style={{width: "100%"}}
            onClick={() => setState(newState)}
        >
            {text}
        </button>
    );
};

export default CancelButton;
