import {IOrderProduct} from "../../interfaces/Orders/IOrderProduct";

export const isBoxChecked = (e: any) => {
    return e.target.checked;
};

export const isChecked = (orderProducts: IOrderProduct[], isInList: (orderProductID: number) => boolean) => {
    for (const orderProduct of orderProducts) {
        if (!isInList(orderProduct.id)) {
            return false;
        }
    }
    return true;
};
