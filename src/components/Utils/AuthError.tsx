import * as React from "react";

import {XCircleFill} from "react-bootstrap-icons";

const AuthError = () => {
    return (
        <div className="container mt-2 mb-2">
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2 justify-content-center">
                        <div className="p-4 bg-light row m-5 text-center">
                            <XCircleFill
                                className="col-md-12 mb-2 text-danger"
                                size={50}
                            />
                            <h2 className="col-md-12 text-danger">Odmowa dostępu!</h2>
                            <p className="col-md-12 h6">
                                Nie posiadasz uprawnień do korzystania z tej strony.
                            </p>
                            <p className="col-md-12">
                                <a className="hover-move" href="/home">Powrót do strony głównej</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AuthError;
