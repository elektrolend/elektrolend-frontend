import React from "react";

import {CheckLg} from "react-bootstrap-icons";

type Props = {
    state: number;
    message: string;
};

const SuccessAlert: React.FC<Props> = ({state, message}) => {
    if (state === 200) {
        return (
            <div
                className="border row m-2 mt-4 alert alert-success d-flex align-items-center"
                role="alert">
                <CheckLg/>
                <div className="ml-1">
                    <b>Sukces!</b> {message}
                </div>
            </div>
        );
    }

    return null;
};

export default SuccessAlert;
