import moment from "moment";
import {IProductOrderDate} from "../../interfaces/Orders/IProductOrderDate";
import {isEmptyArray} from "formik";

export namespace DateManipulation {

    export const getNextDay = (date: Date) => {
        return new Date(moment(date, 'YYYY/MM/DD').add(1, 'days').toString());
    };

    export const getDayBefore = (date: Date) => {
        return new Date(moment(date, 'YYYY/MM/DD').subtract(1, 'days').toString());
    };

    export const getDateAfterDays = (date: Date, diff: number) => {
        return new Date(moment(date).add(diff, 'd').format('YYYY/MM/DD'));
    };

    export const getDatesDiffInDays = (beginDate: Date, endDate: Date) => {
        return moment(endDate, 'YYYY/MM/DD').diff(beginDate, 'days') + 1;
    };

    export const getDateInString = (date: Date) => {
        return getNextDay(date).toISOString().split('T')[0].toString();
    };

    export const getDiffInDays = (beginDate: Date, endDate: Date) => {
        const beginDateToCheck = new Date(DateManipulation.getDateInString(beginDate));
        const endDateToCheck = new Date(DateManipulation.getDateInString(endDate));

        return moment(endDateToCheck, 'YYYY/MM/DD').diff(beginDateToCheck, 'days');
    };

    export const isBeforeTomorrow = (date: string) => {
        return DateManipulation.getDatesDiffInDays(new Date(), new Date(date)) === 1;
    };

    export const makeDate = (tab: number[]) => {
        return new Date(tab[0], tab[1] - 1, tab[2]);
    };

    export const isBetween = (date: Date, min: number[], max: number[]) => {
        return date.getTime() >= makeDate(min).getTime() && date.getTime() <= makeDate(max).getTime();
    };

    export const inDates = (date: Date, min: Date, max: Date) => {
        return date.getTime() >= getDayBefore(min).getTime() && date.getTime() <= max.getTime();
    };

    export const isAfter = (firstDate: Date, secondDate: Date) => {
        return firstDate.getTime() >= secondDate.getTime();
    };

    export const compareDates = (firstDate: number[], secondDate: number[]) => {
        return makeDate(firstDate).getTime() - makeDate(secondDate).getTime();
    };

    export const sortDates = (dates: IProductOrderDate[]) => {
        return dates.sort((p, c) => compareDates(p.beginDate, c.beginDate));
    };

    export const getInitDate = (dates: IProductOrderDate[]) => {
        if (isEmptyArray(dates)) {
            return new Date();
        }
        const sortedDates = sortDates(dates);
        const daysDiff = getDatesDiffInDays(new Date(), makeDate(sortedDates[sortedDates.length - 1].endDate));
        let prevDate = getDayBefore(getDateAfterDays(new Date(), 0));

        for (let i = 0; i < daysDiff; i++) {
            const checkDate = getDateAfterDays(new Date(), i);
            for (const d of sortedDates) {
                if (inDates(checkDate, getNextDay(prevDate), getDayBefore(makeDate(d.beginDate)))) {
                    return checkDate;
                }
                prevDate = getNextDay(makeDate(d.endDate));
            }
        }
        return getNextDay(makeDate(sortedDates[sortedDates.length - 1].endDate));
    };

    export const getMaxDate = (date: Date, dates: IProductOrderDate[]) => {
        let mEndDate = null;

        const sortedDates = sortDates(dates);
        let checkDate = new Date();

        sortedDates.forEach(d => {
            if (inDates(date, checkDate, makeDate(d.beginDate))) {
                mEndDate = getDayBefore(makeDate(d.beginDate));
            }
            checkDate = makeDate(d.endDate);
        });

        return mEndDate;
    };
}
