import React from "react";

import {CheckLg} from "react-bootstrap-icons";

type Props = {
    message: string;
};

const ErrorAlert: React.FC<Props> = ({message}) => {
    if (message) {
        return (
            <div
                className="border row m-2 mt-4 alert alert-danger d-flex align-items-center"
                role="alert">
                <CheckLg/>
                <div className="ml-1">
                    <b>Coś poszło nie tak...</b> {message}
                </div>
            </div>
        );
    }

    return null;
};

export default ErrorAlert;
