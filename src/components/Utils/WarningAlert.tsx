import React from "react";

import {ExclamationTriangleFill} from "react-bootstrap-icons";

type Props = {
    show: boolean;
    message: string;
};

const WarningAlert: React.FC<Props> = ({show, message}) => {
    if (show) {
        return (
            <div
                className="border row m-2 mt-4 alert alert-warning d-flex align-items-center"
                role="alert">
                <ExclamationTriangleFill className="mr-2"/>
                <div className="ml-1">
                    {message}
                </div>
            </div>
        );
    }

    return null;
};

export default WarningAlert;
