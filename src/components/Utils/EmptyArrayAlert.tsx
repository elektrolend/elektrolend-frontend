import React from "react";

type Props = {
    title: string;
    description: string;
    icon: JSX.Element;
};

const EmptyArrayAlert: React.FC<Props> = ({title, description, icon}) => {
    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                        {icon}
                        <h3 className="col-md-12">{title}</h3>
                        <p className="col-md-12">
                            {description}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EmptyArrayAlert;
