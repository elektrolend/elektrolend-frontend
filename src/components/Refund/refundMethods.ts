import PayPal from "./Methods/PayPal/PayPal";
import FakeRefund from "./Methods/FakeRefund/FakeRefund";
import {IRefundMethod} from "../../interfaces/Refund/IRefundMethod";

const refundMethods: IRefundMethod[] = [
    {
        component: PayPal,
        title: "PayPal",
        imageSrc: "paypal.png"
    },
    {
        component: FakeRefund,
        title: "PayU",
        imageSrc: "payu.png"
    },
    {
        component: FakeRefund,
        title: "BLIK",
        imageSrc: "blik.png"
    },
    {
        component: FakeRefund,
        title: "Przelewy24",
        imageSrc: "przelewy24.png"
    },
    {
        component: FakeRefund,
        title: "Skycash",
        imageSrc: "skycash.png"
    },
    {
        component: FakeRefund,
        title: "PaylinkIdea",
        imageSrc: "paylinkidea.png"
    }
]

export default refundMethods;
