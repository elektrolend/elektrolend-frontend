import React from "react";

import {IRefundProps} from "../../../interfaces/Refund/IRefundProps";

const RefundTile: React.FC<IRefundProps> = ({refundMethod, paymentAccount, refundAmount, onSubmit}) => {
    return (
        <div className="col-md-4 mb-3">
            <refundMethod.component
                refundMethod={refundMethod}
                paymentAccount={paymentAccount}
                refundAmount={refundAmount}
                onSubmit={onSubmit}
            />
        </div>
    );
}

export default RefundTile;
