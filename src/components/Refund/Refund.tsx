import React, {useState} from "react";

import ReturnButton from "./ReturnButton";
import RefundTile from "./RefundTile/RefundTile";
import ErrorAlert from "../Utils/ErrorAlert";
import EmptyArrayAlert from "../Utils/EmptyArrayAlert";
import {changeRefund, getPaymentAccount} from "../../services/database.service";
import refundMethods from "./refundMethods";

import {XLg} from "react-bootstrap-icons";

type Props = {
    orderProductID: number;
    refundAmount: number;
    setRefundMode: (show: boolean) => void;
    setSuccessMessage: (message: string) => void;
    setState: (state: number) => void;
};

const Refund: React.FC<Props> = ({orderProductID, refundAmount, setRefundMode, setSuccessMessage, setState}) => {
    const [paymentAccount, setPaymentAccount] = useState(null as string | null);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        getPaymentAccount(orderProductID)
            .then(p => setPaymentAccount(p.paymentAccount))
            .catch(() => setPaymentAccount(null));
    }, [orderProductID]);

    const backToPrevPage = () => {
        setRefundMode(false);
        setPaymentAccount(null);
    };

    const onSubmit = () => {
        changeRefund(orderProductID)
            .then((response) => {
                if (response.status === 200) {
                    setSuccessMessage("Udało się poprawnie zwrócić pieniądze.");
                    setState(response.status);
                    backToPrevPage();
                }
            })
            .catch((error) => {
                if (error.response) {
                    setErrorMessage(error.response.data);
                } else {
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
    };

    if (!paymentAccount) {
        return (
            <EmptyArrayAlert
                title={"Coś poszło nie tak...!"}
                description={"Nie znaleziono danych niezbędnych do wykonania zwrotu."}
                icon={
                    <XLg
                        className="col-md-12 mb-2"
                        size={50}
                    />
                }
            />
        );
    }

    return (
        <div className="container mt-2 mb-2">
            <ErrorAlert
                message={errorMessage}
            />
            <div className="h2 text-primary">Wybierz metodę zwrotu pieniędzy</div>
            <div className="border">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2">
                        {refundMethods.map((refundMethod, index) => {
                            return (
                                <RefundTile
                                    key={'refund_' + index}
                                    refundMethod={refundMethod}
                                    paymentAccount={paymentAccount}
                                    refundAmount={refundAmount}
                                    onSubmit={onSubmit}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
           <ReturnButton backToPrevPage={backToPrevPage}/>
        </div>
    );
};

export default Refund;
