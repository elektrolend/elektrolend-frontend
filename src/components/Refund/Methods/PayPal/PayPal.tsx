import React, {useState} from "react";
import {Modal} from "react-bootstrap";

import SubmitButton from "./SubmitButton";
import CancelButton from "./CancelButton";
import Tile from "./Tile";
import {getAccessToken, makeRefund} from "./paypal.service";
import {IRefundProps} from "../../../../interfaces/Refund/IRefundProps";

import {ExclamationTriangleFill} from "react-bootstrap-icons";


const PayPal: React.FC<IRefundProps> = ({refundMethod, paymentAccount, refundAmount, onSubmit}) => {
    const [isOpen, setIsOpen] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [loading, setLoading] = useState(false);

    const hideModal = () => {
        setIsOpen(false);
    };

    const handleCancel = () => {
        hideModal();
    };

    const handleError = (message: string) => {
        setErrorMessage(message);
        setLoading(false);
    };

    const handleMakeRefund = (accessToken: string) => {
        makeRefund(accessToken, paymentAccount, refundAmount)
            .then(() => {
                onSubmit();
            })
            .catch(error => {
                handleError(error.message);
            })
    };

    const handleSubmit = () => {
        setLoading(true);
        setErrorMessage("");
            getAccessToken()
                .then(response => {
                    handleMakeRefund(response.data.access_token);
                })
                .catch(error => {
                    handleError(error.message);
                })
    };


    return (
        <>
            <Tile
                refundMethod={refundMethod}
                setIsOpen={setIsOpen}
            />
            <Modal
                show={isOpen}
                onHide={hideModal}>
                <Modal.Header>
                    <Modal.Title>Potwierdź decyzję</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Wybrano zwrot <b>{refundAmount} PLN</b> za pomocą <b>{refundMethod.title}</b>.
                    <p>Czy potwierdzasz decyzję?</p>
                    {errorMessage ?
                        <div className="border row m-2 mt-4 alert alert-danger align-items-center" role="alert">
                            <ExclamationTriangleFill/>
                            <div className="ml-1">
                                {errorMessage}
                            </div>
                        </div> : null}
                </Modal.Body>
                <Modal.Footer>
                    <CancelButton
                        loading={loading}
                        onCancel={handleCancel}
                    />
                   <SubmitButton
                       loading={loading}
                       onSubmit={handleSubmit}
                   />
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default PayPal;
