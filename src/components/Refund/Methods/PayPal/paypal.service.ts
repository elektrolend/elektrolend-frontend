import axios from "axios";

const accessTokenUrl = "https://api-m.sandbox.paypal.com/v1/oauth2/token";
const captureUrl = "https://api.sandbox.paypal.com/v2/payments/captures/";
const accessTokenData = "grant_type=client_credentials";
const accessTokenAuth = {
    username: process.env.REACT_APP_PAYPAL_CLIENT_ID || "",
    password: process.env.REACT_APP_PAYPAL_SECRET || ""
};
const accessTokenHeaders = {
    'Content-Type': 'text/plain'
};

export const getAccessToken = (): Promise<any> => {
    return axios.post(accessTokenUrl, accessTokenData, {
        auth: accessTokenAuth,
        headers: accessTokenHeaders
    });
};

export const makeRefund = (accessToken: string, paymentAccount: string, refundAmount: number) => {
    return axios.post(
        captureUrl + `${paymentAccount}/refund`,
        JSON.stringify({
            "amount": {
                "currency_code": "PLN",
                "value": refundAmount.toFixed(2).toString()
            }
        }),
        {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        }
    );
};
