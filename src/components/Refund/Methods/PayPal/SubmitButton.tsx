import React from "react";

type Props = {
    loading: boolean;
    onSubmit: () => void;
};

const SubmitButton: React.FC<Props> = ({loading, onSubmit}) => {
    return (
        <button
            className="btn btn-primary"
            type="submit"
            disabled={loading}
            onClick={onSubmit}>
            {loading ?
                <>
                    <span className="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                    Przetwarzanie...
                </>
                : "Potwierdź"}
        </button>
    );
};

export default SubmitButton;
