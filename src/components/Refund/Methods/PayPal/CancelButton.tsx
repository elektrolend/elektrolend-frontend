import React from "react";

type Props = {
    loading: boolean;
    onCancel: () => void;
};

const CancelButton: React.FC<Props> = ({loading, onCancel}) => {
    return (
        <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
            disabled={loading}
            onClick={onCancel}>
            Anuluj
        </button>
    );
};

export default CancelButton;
