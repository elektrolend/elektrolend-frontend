import React from "react";

import {IRefundMethod} from "../../../../interfaces/Refund/IRefundMethod";

type Props = {
    refundMethod: IRefundMethod;
    setIsOpen: (isOpen: boolean) => void;
};

const Tile: React.FC<Props> = ({refundMethod, setIsOpen}) => {
    return (
        <div
            className="btn btn-lg btn-light card border-dark align-items-center"
            style={{height: "47px", textAlign: "center"}}
            onClick={() => setIsOpen(true)}
        >
            <img
                src={refundMethod.imageSrc}
                alt={refundMethod.title}
                style={{position: "absolute", maxWidth: '80%', maxHeight: '80%'}}
            />
        </div>
    );
};

export default Tile;
