import React from "react";

import FakePayment from "../../../ShoppingCart/Payment/Methods/FakePayment";
import {IRefundProps} from "../../../../interfaces/Refund/IRefundProps";

const FakeRefund: React.FC<IRefundProps> = ({refundMethod}) => {
    return (
        <FakePayment
            src={refundMethod.imageSrc}
            alt={refundMethod.title}
        />
    );
};

export default FakeRefund;
