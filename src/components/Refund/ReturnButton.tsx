import React from "react";
import {ArrowLeftCircleFill} from "react-bootstrap-icons";

type Props = {
    backToPrevPage: () => void;
};

const ReturnButton: React.FC<Props> = ({backToPrevPage}) => {
    return (
        <div className="border mt-2 mb-2">
            <div className="bg-light m-2">
                <div className="col-12 text-right">
                    <button
                        className="btn btn-lg btn-labeled btn-primary"
                        type="submit"
                        style={{width: "100%"}}
                        onClick={() => backToPrevPage()}
                    >
                        <ArrowLeftCircleFill className="mb-1 mr-1"/> POWRÓT DO REKLAMACJI
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ReturnButton;
