import Register from "../Auth/Register/Register";
import Login from "../Auth/Login/Login";
import {AccountType} from "../../interfaces/Accounts/AccountType";

interface NavbarItem {
    title: string,
    url: string,
    cName: string,
    componentName: any,
    roles: AccountType[]
}

const NavbarItems: NavbarItem[] = [
    {
        title: "Logowanie",
        url: "/login",
        cName: "btn",
        componentName: Login,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Rejestracja",
        url: "/register",
        cName: "btn btn-primary",
        componentName: Register,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    }
]

export default NavbarItems;
