import React from 'react';
import {Link} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {useAppSelector} from '../../app/hooks';

import ProtectedComponent from "../Auth/ProtectedComponent";
import {signOut} from '../../services/auth.service'
import {ICartItem} from '../../interfaces/Orders/ICartItem';
import {AccountType} from "../../interfaces/Accounts/AccountType";
import NavbarItems from './Navbaritems';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSearch, faShoppingCart, faUser} from '@fortawesome/free-solid-svg-icons'
import './Navbar.css'
import ProfileItems from "./ProfileItems";

type Props = {
    cartItems: ICartItem[];
    setSearchInput: (input: string) => void;
};

const Navbar: React.FC<Props> = ({cartItems, setSearchInput}) => {
    const isLogged = useAppSelector(state => state.auth.isLogged);
    const userName = useAppSelector(state => state.auth.username);
    const dispatch = useDispatch();

    const getTotalItems = (items: ICartItem[]) => items.length;

    const handleSearchInput = (event: any) => {
        const newInput: string = event.target.value || "";
        setSearchInput(newInput);
    };

    return (
        <nav className="navbar navbar-light navbar-expand-lg bg-light sticky-top border-bottom">
            <Link
                to='/'
                className="navbar-brand">
                <img
                    style={{height: "50px", width: "261px"}}
                    src="logo.png"
                    alt="logo"/>
            </Link>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup"
                aria-expanded="false"
                aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div
                className="collapse navbar-collapse"
                id="navbarNavAltMarkup">
                <form className="form-inline justify-content-center w-100">
                    <div className="input-group">
                        <input
                            className="form-control"
                            onChange={handleSearchInput}
                            type="search"
                            placeholder="Czego szukasz?">
                        </input>
                        <Link to='/search-page'>
                            <button className="btn btn-primary">
                                <FontAwesomeIcon
                                    className="icon"
                                    icon={faSearch}/>
                            </button>
                        </Link>
                    </div>
                </form>
                <ul className="navbar-nav w-100 mr-auto justify-content-end align-items-center">
                    <li className="nav-item">
                        {isLogged ?
                            <div className="dropdown">
                                <button
                                    className="btn btn-light mr-2 dropdown-toggle"
                                    type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                    <FontAwesomeIcon
                                        className="icon mr-1"
                                        icon={faUser}/>
                                    <b>{userName}</b>
                                </button>
                                <div
                                    className="dropdown-menu"
                                    aria-labelledby="dropdownMenuButton">
                                    {ProfileItems.map((profileItem, index) => {
                                        return (
                                            <ProtectedComponent
                                                component={
                                                    <Link
                                                        to={profileItem.url}
                                                        className="dropdown-item"
                                                        key={"profileItem_" + index}
                                                    >
                                                        {profileItem.title}
                                                    </Link>
                                                }
                                                roles={profileItem.roles}
                                                key={"profileItem_" + index}
                                            />
                                        )
                                    })}
                                </div>
                            </div>
                            : null}
                    </li>
                    <ProtectedComponent
                        component={
                            <li className="nav-item mr-2 mb-2">
                                <Link to='/shopping-cart'>
                                    <FontAwesomeIcon
                                        className="icon"
                                        icon={faShoppingCart}/>
                                    {getTotalItems(cartItems) > 0 ?
                                        <i className="fa fa-bell">
                                    <span className='badge badge-pill'>
                                        {getTotalItems(cartItems)}
                                    </span>
                                        </i> : null
                                    }
                                </Link>
                            </li>
                        }
                        roles={[AccountType.none, AccountType.customer]}
                    />
                    <li className="nav-item">
                        <Link to='/products'>
                            <button
                                className="btn"
                                type="button">
                                Produkty
                            </button>
                        </Link>
                    </li>
                    {isLogged ?
                        <li className="nav-item">
                            <button
                                className="btn btn-danger"
                                type="button"
                                onClick={() => signOut(dispatch)}>
                                Wyloguj
                            </button>
                        </li> : null}
                    {NavbarItems.map((item, index) => {
                        if (!isLogged) {
                            return (
                                <ProtectedComponent
                                    component={
                                        <li
                                            className="nav-item">
                                            <Link to={item.url}>
                                                <button
                                                    className={item.cName}
                                                    type="button"
                                                    disabled={isLogged}>
                                                    {item.title}
                                                </button>
                                            </Link>
                                        </li>
                                    }
                                    roles={item.roles}
                                    key={"navbarItem_" + index}
                                />
                            )
                        } else {
                            return null;
                        }
                    })}
                </ul>
            </div>
        </nav>
    );
}

export default Navbar;
