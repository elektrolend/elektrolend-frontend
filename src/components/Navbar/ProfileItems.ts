import {AccountType} from "../../interfaces/Accounts/AccountType";

interface ProfileItem {
    title: string;
    url: string;
    roles: AccountType[];
}

const ProfileItems: ProfileItem[] = [
    {
        title: "Profil",
        url: "/profile",
        roles: [AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Zamówienia",
        url: "/user-orders",
        roles: [AccountType.customer]
    },
    {
        title: "Panel Pracownika",
        url: "/employee-panel",
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default ProfileItems;
