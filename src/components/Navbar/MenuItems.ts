import Register from "../../components/Auth/Register/Register";
import Login from "../Auth/Login/Login";
import ProfilePage from "../Profile/ProfilePage";
import Contact from "../Contact/Contact";
import AboutUs from "../AboutUs/AboutUs";
import PrivacyPolicy from "../PrivacyPolicy/PrivacyPolicy";
import Regulations from "../Regulations/Regulations";
import UserOrders from "../Orders/UserOrders/UserOrders";
import HomePage from "../HomePage/HomePage";
import EmployeePanel from "../EmployeePanel/EmployeePanel";
import OrderPayment from "../Orders/UserOrders/UserOrdersTable/OrderPayment/OrderPayment";
import UserComplaint from "../Complaints/UserComplaint/UserComplaint";
import Refund from "../Refund/Refund";
import RefundView from "../Orders/DepositRefund/RefundView/RefundView";
import AddProduct from "../Products/AddProduct/AddProduct";
import AddCategory from "../Products/AddCategory/AddCategory";
import {AccountType} from "../../interfaces/Accounts/AccountType";
import EditProduct from "../Products/ManageProduct/EditProduct/EditProduct";
import RestorePassword from "../Auth/RestorePassword/RestorePassword";

interface MenuItem {
    title: string,
    url: string,
    cName: string,
    componentName: any,
    roles: AccountType[];
}

const MenuItems: MenuItem[] = [
    {
        title: "Strona Główna",
        url: "/home",
        cName: "btn",
        componentName: HomePage,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Logowanie",
        url: "/login",
        cName: "btn",
        componentName: Login,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Rejestracja",
        url: "/register",
        cName: "btn btn-info",
        componentName: Register,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Profil",
        url: "/profile",
        cName: "btn btn-info",
        componentName: ProfilePage,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Zamówienia",
        url: "/user-orders",
        cName: "btn btn-info",
        componentName: UserOrders,
        roles: [AccountType.customer]
    },
    {
        title: "Kontakt",
        url: "/kontakt",
        cName: "btn btn-info",
        componentName: Contact,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "O nas",
        url: "/o-nas",
        cName: "btn btn-info",
        componentName: AboutUs,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Polityka prywatności",
        url: "/polityka-prywatnosci",
        cName: "btn btn-info",
        componentName: PrivacyPolicy,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Regulamnin",
        url: "/regulamin",
        cName: "btn btn-info",
        componentName: Regulations,
        roles: [AccountType.none, AccountType.customer, AccountType.regular, AccountType.admin]
    },
    {
        title: "Panel pracownika",
        url: "/employee-panel",
        cName: "btn btn-info",
        componentName: EmployeePanel,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Opłać zamówienie",
        url: "/order-payment",
        cName: "btn btn-info",
        componentName: OrderPayment,
        roles: [AccountType.customer]
    },
    {
        title: "Zgłoś reklamację",
        url: "/user-complaint",
        cName: "btn btn-info",
        componentName: UserComplaint,
        roles: [AccountType.customer]
    },
    {
        title: "Zwróć pieniądze",
        url: "/refund",
        cName: "btn btn-info",
        componentName: Refund,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Zwróć kaucję",
        url: "/deposit-refund",
        cName: "btn btn-info",
        componentName: RefundView,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Nowy produkt",
        url: "/add-product",
        cName: "btn btn-info",
        componentName: AddProduct,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Nowa kategoria",
        url: "/add-category",
        cName: "btn btn-info",
        componentName: AddCategory,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Edytuj produkt",
        url: "/edit-product",
        cName: "btn btn-info",
        componentName: EditProduct,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Odzyskiwanie hasła",
        url: "/restore-password",
        cName: "btn btn-primary",
        componentName: RestorePassword,
        roles: [AccountType.none]
    }
]

export default MenuItems;
