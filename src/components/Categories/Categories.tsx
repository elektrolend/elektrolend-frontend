import React from "react";
import {ICategory} from "../../interfaces/Products/ICategory";
import CategoryItem from "../HomePage/BestCategories/CategoryItem";

interface IProps {

}

interface IState {
    categories: Array<ICategory>;
}


class Categories extends React.Component <IProps, IState> {


    constructor(props: any) {
        super(props);
        this.state = {
            categories: [],
        }
    }

    componentDidMount() {
        this.update()
    }

    update() {
        fetch(`${process.env.REACT_APP_API_URL}categories`)
            .then(result => result.json())
            .then(json => {
                this.setState({categories: json})
            })
            .catch(console.log)
    }

    render() {
        return (
            <div className="bg-light mb-5 p-4">
                <div className="container">
                    <div className="row justify-content-center">
                        {this.state.categories.map((category, index) => {
                            return (
                                <CategoryItem
                                    categoryID={category.id.toString()}
                                    key={"category_" + index}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        )
    }

}

export default Categories;
