import Contact from '../Contact/Contact';
import AboutUs from '../AboutUs/AboutUs';
import PrivacyPolicy from '../PrivacyPolicy/PrivacyPolicy';
import Regulations from '../Regulations/Regulations';

interface FooterItem {
    title: string,
    url: string,
    componentName: any
}

const FooterItems: FooterItem[] = [

    {
        title: "Kontakt",
        url: "/kontakt",
        componentName: Contact
    },
    {
        title: "O nas",
        url: "/o-nas",
        componentName: AboutUs
    },
    {
        title: "Polityka prywatności",
        url: "/polityka-prywatnosci",
        componentName: PrivacyPolicy
    },
    {
        title: "Regulamnin",
        url: "/regulamin",
        componentName: Regulations
    }

]

export default FooterItems;
