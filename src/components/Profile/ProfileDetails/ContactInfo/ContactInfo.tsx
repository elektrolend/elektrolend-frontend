import React from "react";

import {IProfileData} from "../../../../interfaces/Accounts/IProfileData";

type Props = {
    profileData: IProfileData;
};

const ContactInfo: React.FC<Props> = ({profileData}) => {
    return (
        <div className="border">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Adres zamieszkania</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>Kraj</h5>
                        <p>{profileData.country}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>Województwo</h5>
                        <p>{profileData.state}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <h5>Kod pocztowy</h5>
                        <p>{profileData.zipCode}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>Miasto</h5>
                        <p>{profileData.city}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <h5>Ulica</h5>
                        <p>{profileData.street}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>Numer domu{Number(profileData.localNumber) ? '/lokalu' : null}</h5>
                        <p>
                            {profileData.homeNumber}
                            {Number(profileData.localNumber) ? '/' + profileData.localNumber : null}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactInfo;
