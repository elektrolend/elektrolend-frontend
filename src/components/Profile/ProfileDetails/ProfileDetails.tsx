import React from 'react';

import ContactInfo from "./ContactInfo/ContactInfo";
import BasicInfo from "./BasicInfo/BasicInfo";
import UserInfo from "./UserInfo/UserInfo";
import ProtectedComponent from "../../Auth/ProtectedComponent";
import {IUserProfile} from '../../../interfaces/Accounts/IUserProfile';
import {AccountType} from "../../../interfaces/Accounts/AccountType";

type Props = {
    profileData: IUserProfile
};

const ProfileDetails: React.FC<Props> = ({profileData}) => {
    return (
        <div className="row">
            <div className="col-md-3">
                <UserInfo
                    name={profileData.name}
                    surname={profileData.surname}
                    phone={profileData.phone}
                />
            </div>
            <div className="col-md-9">
                <BasicInfo
                    email={profileData.email}
                    nickname={profileData.nickname}
                />
                <div className="mt-2 mb-2">
                    <ProtectedComponent
                        component={<ContactInfo profileData={profileData}/>}
                        roles={[AccountType.customer]}
                    />
                </div>
            </div>
        </div>
    );
};

export default ProfileDetails;
