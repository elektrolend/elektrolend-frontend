import React from "react";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAddressCard, faPhoneAlt} from "@fortawesome/free-solid-svg-icons";

type Props = {
    name: string;
    surname: string;
    phone: string;
};

const UserInfo: React.FC<Props> = ({name, surname, phone}) => {
    const capitalizeFirstLetter = (word: string) => {
        return word ? word.charAt(0).toUpperCase() + word.slice(1) : "";
    };

    const getFullName = () => {
        return capitalizeFirstLetter(name)
            .concat(' ')
            .concat(capitalizeFirstLetter(surname));
    };

    return (
        <div className="border">
            <div className="bg-light p-2 m-2">
                <div className="col-md-12">
                    <img
                        src="avatar.png"
                        className="img-fluid"
                        alt="profile"/>
                </div>
                <div className="col-md-12 text-center">
                    <p>
                        <FontAwesomeIcon className="icon mr-1" icon={faAddressCard}/>
                        <b>
                            {getFullName()}
                        </b>
                    </p>
                </div>
                <div className="col-md-12 text-center">
                    <p>
                        <FontAwesomeIcon className="icon mr-1" icon={faPhoneAlt}/>
                        <b>
                            {phone}
                        </b>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default UserInfo;
