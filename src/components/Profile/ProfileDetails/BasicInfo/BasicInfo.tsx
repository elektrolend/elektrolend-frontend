import React from "react";

type Props = {
    email: string;
    nickname: string;
};

const BasicInfo: React.FC<Props> = ({email, nickname}) => {
    return (
        <div className="border">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Podstawowe informacje</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>E-mail</h5>
                        <p>{email}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>Nazwa użytkownika</h5>
                        <p>{nickname}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BasicInfo;
