import {IFieldInfo} from "../../../../interfaces/Accounts/IFieldInfo";

const ContactInfoFields: [IFieldInfo, IFieldInfo][] = [
    [
        {
            label: "Kod pocztowy",
            name: "zipCode",
            type: "text",
            cName: "col-md-6"
        },
        {
            label: "Miasto",
            name: "city",
            type: "text",
            cName: "col-md-6"
        }
    ]
]

export default ContactInfoFields;
