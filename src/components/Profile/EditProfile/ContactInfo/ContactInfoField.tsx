import React from "react";
import {ErrorMessage, Field} from "formik";

import {IFieldInfo} from "../../../../interfaces/Accounts/IFieldInfo";

type Props = {
    contactInfoField: IFieldInfo
};

const ContactInfoField: React.FC<Props> = ({contactInfoField}) => {

    return (
        <div className={contactInfoField.cName}>
            <h5>{contactInfoField.label}</h5>
            <Field
                name={contactInfoField.name}
                type={contactInfoField.type}
                className="form-control"
            />
            <ErrorMessage render={msg => <div
                className="alert alert-danger">{msg}</div>}
                          name={contactInfoField.name}/>
        </div>
    );
}

export default ContactInfoField;
