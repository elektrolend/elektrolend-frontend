import React, {useState} from 'react';
import {ErrorMessage, Field} from "formik";
import {useQuery} from "react-query";

import {ICountry} from "../../../../interfaces/Accounts/ICountry";
import {IState} from "../../../../interfaces/Accounts/IState";
import {IProfileData} from "../../../../interfaces/Accounts/IProfileData";
import {findCountryId, getCountries, getStates} from "../../../../services/database.service";
import ContactInfoFields from "./ContactInfoFields";
import ContactInfoField from "./ContactInfoField";

type Props = {
    profileData: IProfileData;
    setFieldValue: (field: string, value: any, shouldValidate?: (boolean | undefined)) => void;
    setFieldTouched: (field: string, isTouched?: (boolean | undefined), shouldValidate?: (boolean | undefined)) => void;
};

const ContactInfo: React.FC<Props> = ({profileData, setFieldValue, setFieldTouched}) => {

    const {data: countries, isLoading: countriesLoading, error: countriesError} = useQuery<ICountry[]>(
        'countries',
        getCountries
    );

    const {data: states, isLoading: statesLoading, error: statesError} = useQuery<IState[]>(
        'states',
        getStates
    );

    const getInitCountryId = () => {
        return states ? states.filter(p => p.name === profileData.state)[0].countryID : 0;
    };

    const [countryId, setCountryId] = useState(getInitCountryId());

    const handleChangeCountryId = (e: any) => {
        const foundCountryId = findCountryId(e.target.value, countries);
        if (foundCountryId !== 200) {
            setCountryId(foundCountryId);
        }
    };

    if (countriesLoading || statesLoading) {
        return <div> Ładowanie ...</div>;
    }


    if (countriesError || statesError) {
        return <div>Coś poszło nie tak ...</div>;
    }

    if (!countryId) {
        setCountryId(getInitCountryId());
    }

    return (
        <div className="border mt-2 mb-2">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Adres zamieszkania</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>Kraj</h5>
                        <Field
                            as="select"
                            className="form-control bg-light"
                            name="country"
                            onChange={(e: any) => {
                                handleChangeCountryId(e);
                                setFieldValue("country", e.target.value);
                                setFieldValue("state", '');
                                setFieldTouched("state", false);
                            }}>
                            <option hidden value="">Kraj</option>
                            {countries?.map((country) => {
                                return (
                                    <option
                                        key={country.id}
                                        value={country.name}>
                                        {country.name}
                                    </option>
                                )
                            })}
                        </Field>
                        <ErrorMessage
                            render={msg => <div className="alert alert-danger">{msg}</div>}
                            name="country"/>
                    </div>
                    <div className="col-md-6">
                        <h5>Województwo</h5>
                        <Field
                            as="select"
                            className="form-control bg-light"
                            name="state"
                        >
                            <option value="">Wybierz województwo</option>
                            {states?.filter((state) => state.countryID === countryId)
                                .map((state) => {
                                    return (
                                        <option
                                            key={state.id}
                                            value={state.name}>{state.name}
                                        </option>
                                    )
                                })}
                        </Field>
                        <ErrorMessage render={msg => <div
                            className="alert alert-danger">{msg}</div>}
                                      name="state"/>
                    </div>
                </div>
                {ContactInfoFields.map(contactInfoRow => {
                    return (
                        <div className="row">
                            {contactInfoRow.map(contactInfoField => {
                                return (
                                    <ContactInfoField contactInfoField={contactInfoField}/>
                                );
                            })}
                        </div>
                    );
                })}
                <div className="row">
                    <div className="col-md-6">
                        <h5>Ulica</h5>
                        <Field
                            name="street"
                            type="text"
                            className="form-control"
                        />
                        <ErrorMessage render={msg => <div
                            className="alert alert-danger">{msg}</div>}
                                      name="street"/>
                    </div>
                    <div className="col-md-6">
                        <h5>Numer domu{Number(profileData.localNumber) ? '/lokalu' : null}</h5>
                        <div className="row">
                            <div className="input-group">
                                <div className="col-5">
                                    <Field
                                        name="homeNumber"
                                        type="text"
                                        className="form-control"
                                    />
                                    <ErrorMessage render={msg => <div
                                        className="alert alert-danger">{msg}</div>}
                                                  name="homeNumber"/>
                                </div>
                                {profileData.localNumber ?
                                    <>
                                        <div className="col-2">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">/</span>
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <Field
                                                name="localNumber"
                                                type="text"
                                                className="form-control"
                                            />
                                            <ErrorMessage render={msg => <div
                                                className="alert alert-danger">{msg}</div>}
                                                          name="localNumber"/>
                                        </div>
                                    </> : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactInfo;
