import React, {useState} from "react";
import {Form, Formik} from "formik";
import {useAppSelector} from "../../../app/hooks";

import {IUserProfile} from "../../../interfaces/Accounts/IUserProfile";
import {IProfileData} from "../../../interfaces/Accounts/IProfileData";
import BasicInfo from "./BasicInfo/BasicInfo";
import UserInfo from "./UserInfo/UserInfo";
import ContactInfo from "./ContactInfo/ContactInfo";
import {changeProfileData} from "../../../services/database.service";
import ProfileValidationSchema from "./ProfileValidationSchema";

import {CheckLg, ExclamationTriangleFill, XLg} from "react-bootstrap-icons";

type Props = {
    profileData: IUserProfile,
    setEditable: (editable: boolean) => void
};

const EditProfile: React.FC<Props> = ({profileData, setEditable}) => {
    const userID = useAppSelector(state => state.auth.userID);
    const [errorMessage, setErrorMessage] = useState('');

    const onSubmit = (newProfileData: IProfileData) => {
        setErrorMessage('');
        changeProfileData(newProfileData, userID)
            .then((response) => {
                if (response.status === 200) {
                    setEditable(false);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setErrorMessage(error.response.data);
                } else {
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
    };

    return (
        <Formik
            initialValues={{
                phone: profileData.phone,
                email: profileData.email,
                street: profileData.street,
                homeNumber: profileData.homeNumber,
                localNumber: profileData.localNumber,
                city: profileData.city,
                zipCode: profileData.zipCode,
                state: profileData.state,
                country: profileData.country,
                name: profileData.name,
                surname: profileData.surname
            }}
            validationSchema={ProfileValidationSchema}
            validateOnChange={false}
            validateOnBlur={false}
            onSubmit={values => {
                onSubmit(values);
            }}>
            {({setFieldValue, setFieldTouched}) => (
                <Form>
                    <div className="row">
                        <div className="col-md-3">
                            <div className="border">
                                <BasicInfo/>
                            </div>
                        </div>
                        <div className="col-md-9">
                            <UserInfo
                                nickname={profileData.nickname}/>
                            <ContactInfo
                                profileData={profileData}
                                setFieldValue={setFieldValue}
                                setFieldTouched={setFieldTouched}/>
                        </div>
                    </div>
                    {errorMessage ?
                        <div className="border row m-2 mt-4 alert alert-danger align-items-center" role="alert">
                            <ExclamationTriangleFill/>
                            <div className="ml-1">
                                {errorMessage}
                            </div>
                        </div> : null}
                    <div className="border mt-2 mb-2">
                        <div className="bg-light m-2">
                            <div className="col-12 text-right">
                                <button
                                    className="btn btn-labeled btn-danger pt-0 pb-0 mb-10"
                                    onClick={() => setEditable(false)}
                                >
                                    <span className="button-label"><XLg/></span>Anuluj
                                </button>
                                <button
                                    className="btn btn-labeled btn-success pt-0 pb-0 mb-10 ml-4"
                                    type="submit"
                                >
                                    <span className="button-label"><CheckLg/></span>Zapisz
                                </button>
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default EditProfile;
