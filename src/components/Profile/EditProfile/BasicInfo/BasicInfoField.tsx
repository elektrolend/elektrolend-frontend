import React from "react";
import {ErrorMessage, Field} from "formik";

import {IBasicInfoField} from "../../../../interfaces/Accounts/IBasicInfoField";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type Props = {
    basicInfoField: IBasicInfoField
};

const BasicInfoField: React.FC<Props> = ({basicInfoField}) => {

    return (
        <div className="col-12 m-1">
            <div className="input-group">
                <div className="input-group-prepend">
                    <span className="input-group-text">
                        <FontAwesomeIcon className="icon" icon={basicInfoField.icon}/>
                    </span>
                </div>
                <Field
                    name={basicInfoField.name}
                    type={basicInfoField.type}
                    className="form-control"/>
                <ErrorMessage
                    render={msg => <div className="alert alert-danger">{msg}</div>}
                    name={basicInfoField.name}/>
            </div>
        </div>
    );
}

export default BasicInfoField;
