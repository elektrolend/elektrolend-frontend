import {IBasicInfoField} from "../../../../interfaces/Accounts/IBasicInfoField";

import {faAddressCard, faPhoneAlt} from "@fortawesome/free-solid-svg-icons";

const BasicInfoFields: IBasicInfoField[] = [
    {
        name: "name",
        type: "text",
        icon: faAddressCard
    },
    {
        name: "surname",
        type: "text",
        icon: faAddressCard
    },
    {
        name: "phone",
        type: "text",
        icon: faPhoneAlt
    }
]

export default BasicInfoFields;
