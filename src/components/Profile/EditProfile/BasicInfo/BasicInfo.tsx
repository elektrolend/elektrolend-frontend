import React from 'react';

import BasicInfoFields from "./BasicInfoFields";
import BasicInfoField from "./BasicInfoField";

const BasicInfo = () => {

    return (
        <div className="bg-light p-2 m-2">
            <div className="col-md-12">
                <img
                    src="avatar.png"
                    className="img-fluid"
                    alt="profile"/>
            </div>
            {BasicInfoFields.map((basicInfoField, index) => {
                return (
                    <BasicInfoField
                        basicInfoField={basicInfoField}
                        key={index}/>
                );
            })}
        </div>
    );
}

export default BasicInfo;
