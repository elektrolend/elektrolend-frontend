import * as Yup from "yup";
import BasicInfoValidationSchema from "../../Auth/BasicInfoValidationSchema";

const ProfileValidationSchema = Yup.object().shape({
    street: Yup.string()
        .required('Ulica jest wymagana!')
        .matches(/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*(\s[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*)*$/, 'Przykład: Władysława Łokietka'),
    homeNumber: Yup.string()
        .required('Numer domu jest wymagany!'),
    localNumber: Yup.string()
        .required('Numer lokalu jest wymagany!'),
    zipCode: Yup.string()
        .required('Kod pocztowy jest wymagany!')
        .min(6, 'Format kodu xx-xxx')
        .max(6, 'Format kodu xx-xxx')
        .matches(/^\d\d-\d\d\d$/, 'Format kodu xx-xxx'),
    city: Yup.string()
        .required('Miasto jest wymagane!')
        .matches(/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*(\s[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*)*$/, 'Przykład: Kraków'),
    country: Yup.string()
        .required('Kraj jest wymagany!'),
    state: Yup.string()
        .required('Województwo jest wymagane!')
}).concat(BasicInfoValidationSchema);

export default ProfileValidationSchema;
