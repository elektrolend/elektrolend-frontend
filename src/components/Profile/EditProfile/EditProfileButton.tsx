import React from "react";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons";

type Props = {
    editable: boolean;
    setEditable: (canBeEdited: boolean) => void;
};

const EditProfileButton: React.FC<Props> = ({editable, setEditable}) => {
    return (
        <button
            className="btn btn-labeled mb-10 pt-0 pb-0 mr-2 btn-primary"
            onClick={() => setEditable(!editable)}
            hidden={editable}
        >
            <span className="button-label">
                <FontAwesomeIcon
                    className="icon"
                    icon={faPencilAlt}
                />
            </span>
            Edytuj profil
        </button>
    );
};

export default EditProfileButton;
