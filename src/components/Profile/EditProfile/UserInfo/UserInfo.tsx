import {ErrorMessage, Field} from "formik";
import React from "react";

type Props = {
    nickname: string
}
const UserInfo: React.FC<Props> = ({nickname}) => {

    return (
        <div className="border">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Podstawowe informacje</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>E-mail</h5>
                        <Field
                            name="email"
                            type="text"
                            className="form-control"
                        />
                        <ErrorMessage render={msg => <div
                            className="alert alert-danger">{msg}</div>}
                                      name="email"/>
                    </div>
                    <div className="col-md-6">
                        <h5>Nazwa użytkownika</h5>
                        <p>{nickname}</p>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default UserInfo;
