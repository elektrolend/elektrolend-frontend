import {IFieldInfo} from "../../../../interfaces/Accounts/IFieldInfo";

const ChangeLoginFields: IFieldInfo[] = [
    {
        label: "Podaj obecne hasło",
        name: "currentPassword",
        type: "password",
        cName: "form-control"
    },
    {
        label: "Podaj nową nazwę użytkownika",
        name: "nickname",
        type: "text",
        cName: "form-control"
    }
]

export default ChangeLoginFields;
