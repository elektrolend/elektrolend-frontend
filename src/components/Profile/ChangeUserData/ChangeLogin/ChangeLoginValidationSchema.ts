import CurrentPasswordValidationSchema from "../CurrentPasswordValidationSchema";
import LoginValidationSchema from "../../../Auth/LoginValidationSchema";

const ChangeLoginValidationSchema = CurrentPasswordValidationSchema.concat(LoginValidationSchema);

export default ChangeLoginValidationSchema;
