import React, {useState} from "react";
import {Formik} from "formik";
import {useDispatch} from "react-redux";
import {useAppSelector} from "../../../../app/hooks";

import FormModal from "../FormModal";
import ChangeButton from "../ChangeButton";
import {changeNick} from "../../../../services/database.service";
import {login, signOut} from "../../../../services/auth.service";
import {ILoginData} from "../../../../interfaces/Accounts/ILoginData";
import NickValidationSchema from "./ChangeLoginValidationSchema";
import ChangeLoginFields from "./ChangeLoginFields";

import {faUser} from "@fortawesome/free-solid-svg-icons";

type Props = {
    setDataChanged: (dataChanged: boolean) => void;
};

const ChangeLogin: React.FC<Props> = ({setDataChanged}) => {
    const title: string = "Zmień login";
    const [loginState, setLoginState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");
    const userID = useAppSelector(state => state.auth.userID);
    const currentNick = useAppSelector(state => state.auth.username);
    const [isOpen, setIsOpen] = useState(false);
    const dispatch = useDispatch();

    const handleError = (error: any) => {
        if (error.response) {
            setLoginState(error.response.status);
            setErrorMessage(error.response.data);
        } else {
            setLoginState(500);
            setErrorMessage("Brak połączenia z serwerem.");
        }
    };

    const onSubmit = (loginData: ILoginData) => {
        setErrorMessage("");
        setLoginState(100);
        login(currentNick, loginData.currentPassword)
            .then((response) => {
                if (response.status === 200) {
                    handleChangeNick(loginData);
                }
            })
            .catch((error) => {
                handleError(error);
            });
    };

    const handleChangeNick = (loginData: ILoginData) => {
        changeNick({nickname: loginData.nickname}, userID)
            .then((response) => {
                if (response.status === 200) {
                    setLoginState(response.status);
                    setDataChanged(true);
                    signOut(dispatch);
                }
            })
            .catch((error) => {
                handleError(error);
            });
    };

    return (
        <>
            <ChangeButton
                title={title}
                icon={faUser}
                setIsOpen={setIsOpen}
            />
            <Formik
                initialValues={{
                    currentPassword: '',
                    nickname: ''
                }}
                validationSchema={NickValidationSchema}
                validateOnChange={false}
                validateOnBlur={false}
                onSubmit={values => {
                    onSubmit(values);
                }}>
                {() => (
                    <FormModal
                        title={title}
                        fields={ChangeLoginFields}
                        errorMessage={errorMessage}
                        state={loginState}
                        isOpen={isOpen}
                        setIsOpen={setIsOpen}
                    />
                )}
            </Formik>
        </>
    );
};

export default ChangeLogin;
