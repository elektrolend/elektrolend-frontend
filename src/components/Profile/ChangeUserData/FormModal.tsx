import React from "react";
import {ErrorMessage, Field, Form} from "formik";

import SpinnerButton from "../../Utils/SpinnerButton";
import {IFieldInfo} from "../../../interfaces/Accounts/IFieldInfo";

import {Modal} from "react-bootstrap";

type Props = {
    title: string;
    fields: IFieldInfo[];
    errorMessage: string;
    state: number;
    isOpen: boolean;
    setIsOpen: (isOpen: boolean) => void;
};

const FormModal: React.FC<Props> = ({title, fields, errorMessage, state, isOpen, setIsOpen}) => {
    const hideModal = () => {
        setIsOpen(false);
    };

    return (
        <Modal
            show={isOpen}
            onHide={hideModal}>
            <Modal.Header>
                <Modal.Title>{title}</Modal.Title>
                <button
                    type="button"
                    className="close"
                    onClick={() => hideModal()}
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <div className="bg-light p-2 m-2 text-left">
                        <div className="row">
                            {fields.map((field, key) => {
                                return (
                                    <div className="col-md-12 form-group">
                                        <label className="form-label">{field.label}</label>
                                        <Field
                                            name={field.name}
                                            type={field.type}
                                            className={field.cName}
                                        />
                                        <ErrorMessage render={msg => <div
                                            className="alert alert-danger">{msg}</div>}
                                                      name={field.name}/>
                                    </div>
                                );
                            })}
                            {errorMessage ?
                                <div className="col-md-12">
                                    <div className="alert alert-danger" role="alert">
                                        {errorMessage}
                                    </div>
                                </div> : null}
                            <div className="col-12 mt-4">
                                <SpinnerButton
                                    state={state}
                                    buttonText={"Zatwierdź"}
                                />
                            </div>
                        </div>
                    </div>
                </Form>
            </Modal.Body>
        </Modal>
    );
};

export default FormModal;
