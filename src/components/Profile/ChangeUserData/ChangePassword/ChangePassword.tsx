import React, {useState} from "react";
import {Formik} from "formik";
import {useAppSelector} from "../../../../app/hooks";
import {useDispatch} from "react-redux";

import FormModal from "../FormModal";
import ChangeButton from "../ChangeButton";
import {changePassword} from "../../../../services/database.service";
import {login, signOut} from "../../../../services/auth.service";
import {IPasswordData} from "../../../../interfaces/Accounts/IPasswordData";
import ChangePasswordValidationSchema from "./ChangePasswordValidationSchema";
import ChangePasswordFields from "./ChangePasswordFields";

import {faKey} from "@fortawesome/free-solid-svg-icons";

type Props = {
    setDataChanged: (dataChanged: boolean) => void;
};

const ChangePassword: React.FC<Props> = ({setDataChanged}) => {
    const title: string = "Zmień hasło";
    const [passwordState, setPasswordState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");
    const userID = useAppSelector(state => state.auth.userID);
    const userName = useAppSelector(state => state.auth.username);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const dispatch = useDispatch();

    const handleError = (error: any) => {
        if (error.response) {
            setPasswordState(error.response.status);
            setErrorMessage(error.response.data);
        } else {
            setPasswordState(500);
            setErrorMessage("Brak połączenia z serwerem.");
        }
    };

    const onSubmit = (passwordData: IPasswordData) => {
        setErrorMessage("");
        setPasswordState(100);
        login(userName, passwordData.currentPassword)
            .then((response) => {
                if (response.status === 200) {
                    handleChangePassword(passwordData);
                }
            })
            .catch((error) => {
                handleError(error);
            });
    };

    const handleChangePassword = (loginData: IPasswordData) => {
        changePassword({password: loginData.password}, userID)
            .then((response) => {
                if (response.status === 200) {
                    setPasswordState(response.status);
                    setDataChanged(true);
                    signOut(dispatch);
                }
            })
            .catch((error) => {
                handleError(error);
            });
    };

    return (
        <>
            <ChangeButton
                title={title}
                icon={faKey}
                setIsOpen={setIsModalOpen}
            />
            <Formik
                initialValues={{
                    currentPassword: '',
                    password: '',
                    repeatedPassword: ''
                }}
                validationSchema={ChangePasswordValidationSchema}
                validateOnChange={false}
                validateOnBlur={false}
                onSubmit={values => {
                    onSubmit(values);
                }}>
                {() => (
                    <FormModal
                        title={title}
                        fields={ChangePasswordFields}
                        errorMessage={errorMessage}
                        state={passwordState}
                        isOpen={isModalOpen}
                        setIsOpen={setIsModalOpen}
                    />
                )}
            </Formik>
        </>
    );
};

export default ChangePassword;
