import CurrentPasswordValidationSchema from "../CurrentPasswordValidationSchema";
import PasswordValidationSchema from "../../../Auth/PasswordValidationSchema";

const ChangePasswordValidationSchema = CurrentPasswordValidationSchema.concat(PasswordValidationSchema);

export default ChangePasswordValidationSchema;
