import * as Yup from "yup";

const CurrentPasswordValidationSchema = Yup.object().shape({
    currentPassword: Yup.string()
        .required('To pole jest wymagane!')
});

export default CurrentPasswordValidationSchema;
