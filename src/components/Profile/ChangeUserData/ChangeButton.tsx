import React from "react";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type Props = {
    title: string;
    icon: any;
    setIsOpen: (isOpen: boolean) => void;
};

const ChangeButton: React.FC<Props> = ({title, icon, setIsOpen}) => {
    return (
        <button
            type="button"
            className="btn btn-labeled btn-primary mr-2 mb-10 pt-0 pb-0"
            onClick={() => setIsOpen(true)}
        >
                <span className="button-label">
                    <FontAwesomeIcon className="icon" icon={icon}/>
                </span>
            {title}
        </button>
    );
};

export default ChangeButton;
