import React from "react";
import {CheckCircleFill} from "react-bootstrap-icons";

type Props = {
    dataChanged: boolean;
};

const DataChangedAlert: React.FC<Props> = ({dataChanged}) => {
    if (dataChanged) {
        return (
            <div
                className="container text-center"
                style={{width: "50%", maxWidth: "330px"}}
            >
                <div
                    className="alert alert-success mt-4 row"
                    role="alert"
                >
                    <div className="col-2 m-auto">
                        <CheckCircleFill size={30}/>
                    </div>
                    <div className="col-10 text-left">
                        <strong>Sukces!</strong> Dane zmienione. <div className="text-left"> Zaloguj się ponownie.</div>
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

export default DataChangedAlert;
