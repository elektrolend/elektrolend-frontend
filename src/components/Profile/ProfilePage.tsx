import React, {useEffect, useState} from 'react';
import {useAppSelector} from '../../app/hooks';

import ProtectedComponent from "../Auth/ProtectedComponent";
import ChangePassword from "./ChangeUserData/ChangePassword/ChangePassword";
import ChangeNick from "./ChangeUserData/ChangeLogin/ChangeLogin";
import EditProfile from "./EditProfile/EditProfile";
import Login from "../Auth/Login/Login";
import ProfileDetails from './ProfileDetails/ProfileDetails';
import DataChangedAlert from "./DataChangedAlert";
import EditProfileButton from "./EditProfile/EditProfileButton";
import {getUser} from '../../services/database.service';
import {AccountType} from "../../interfaces/Accounts/AccountType";
import {IUserProfile} from '../../interfaces/Accounts/IUserProfile';

import "./ProfilePage.css";

const ProfilePage = () => {
    const isLogged = useAppSelector(state => state.auth.isLogged);
    const userId = useAppSelector(state => state.auth.userID);
    const [userInfo, setUserInfo] = useState({} as IUserProfile);
    const [editable, setEditable] = useState(false);
    const [dataChanged, setDataChanged] = useState(false);

    useEffect(() => {
        getUser(userId).then(u => setUserInfo(u));
    }, [userId, editable]);

    if (isLogged) {
        return (
            <>
                <div className="container mt-2">
                    <div className="row">
                        <div className="col-6 text-left">
                            <h2 className="text-primary">
                                Profil
                            </h2>
                        </div>
                        <div className="col-6 text-right">
                            <ProtectedComponent
                                component={
                                    <EditProfileButton
                                        editable={editable}
                                        setEditable={setEditable}
                                    />
                                }
                                roles={[AccountType.customer]}
                            />
                            {!editable ?
                                <>
                                    <ChangeNick setDataChanged={setDataChanged}/>
                                    <ChangePassword setDataChanged={setDataChanged}/>
                                </> : null}
                        </div>
                    </div>
                </div>
                <div className="container mt-2">
                    {editable ?
                        <EditProfile
                            profileData={userInfo}
                            setEditable={setEditable}
                        /> :
                        <ProfileDetails
                            profileData={userInfo}
                        />
                    }
                </div>
            </>
        )
    } else {
        return (
            <>
                <DataChangedAlert dataChanged={dataChanged}/>
                <Login/>
            </>
        );
    }
};

export default ProfilePage;
