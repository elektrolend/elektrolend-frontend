import React from "react";
import {ErrorMessage} from "formik";

type Props = {
    fieldName: string;
    addImages: (images: FileList) => void;
};

const UploadPhotoList: React.FC<Props> = ({fieldName, addImages}) => {
    const handleAddImage = (event: any) => {
        const newImages = event.target.files;
        if (newImages) {
            addImages(newImages);
        }
        event.target.value = null;
    };

    return (
        <div className="col-12 form-group">
            <h5>Zdjęcia</h5>
            <input
                type="file"
                accept="image/*"
                name={fieldName}
                className="form-control-file"
                multiple
                onChange={(event) => handleAddImage(event)}>
            </input>
            <div className="mt-2">
                <ErrorMessage render={msg => <div
                    className="alert alert-danger">{msg}</div>}
                              name={fieldName}/>
            </div>
        </div>
    );
};

export default UploadPhotoList;
