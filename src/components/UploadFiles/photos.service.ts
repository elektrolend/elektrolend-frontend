import {storage} from "../../firebase";
import firebase from "firebase/compat";

export const uploadPhoto = (name: string, image: File, directory: string) => {
    return storage.ref(`images/${directory}/${name}`).put(image);
};

export const getImageSrc = (imageUrl: string) => {
    return imageUrl ? imageUrl : "defaultImage.png";
};

export const getActualProgress = (snapshot: firebase.storage.UploadTaskSnapshot) => {
    return Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
};

export const getImageUrl = (directory: string, path: string) => {
    return storage.ref(directory).child(path).getDownloadURL();
};

export const generateImageName = (image: File) => {
    const crypto = window.crypto || window.Crypto;
    const array = new Uint32Array(1);
    return image.name + crypto.getRandomValues(array).toString();

};

