import React from "react";
import {ProgressBar} from "react-bootstrap";
import {ErrorMessage} from "formik";

type Props = {
    setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
    progress: number;
    fieldName: string;
};

const UploadPhoto: React.FC<Props> = ({setFieldValue, progress, fieldName}) => {
    const handleChange = (event: any) => {
        const newImage = event.target.files[0];
        if (newImage) {
            setFieldValue("image", newImage);
        } else {
            setFieldValue("image", "");
        }
    };

    return (
        <div className="col-12 form-group">
            <label className="form-label">Wybierz zdjęcie</label>
            <input
                type="file"
                accept="image/*"
                name={fieldName}
                className="form-control-file"
                onChange={(event) => handleChange(event)}
            >
            </input>
            <div className="mt-2">
                {progress ?
                    <>
                        <h6>Ładowanie zdjęcia na hosting...</h6>
                        <ProgressBar animated now={progress}/>
                    </>
                    : null}

                <ErrorMessage render={msg => <div
                    className="alert alert-danger">{msg}</div>}
                              name={fieldName}/>
            </div>
        </div>
    );
};

export default UploadPhoto;
