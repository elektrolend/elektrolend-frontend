import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";

export const generatePDF = (from: string, to: string, tableName: string) => {
    const doc = new jsPDF("p", "pt", "a4");
    doc.setFont("FreeSansBold");
    doc.text(`Raport za okres od ${from} do ${to}`, doc.internal.pageSize.getWidth() / 5, 20);

    autoTable(doc, {
        html: `#${tableName}`, styles: {
            font: "FreeSansBold",
            fontStyle: "normal",
        }
    });
    doc.save(`${tableName}_${from}_${to}`);
};
