import React from "react";
import {CSVLink} from "react-csv";

type Props = {
    beginDate: string;
    endDate: string;
    generateCSV: () => (string | number)[][];
    exportable: boolean;
};

const CSVButton: React.FC<Props> = ({beginDate, endDate, generateCSV, exportable}) => {
    const csvData = generateCSV();

    return (
        <CSVLink
            data={csvData}
            filename={`CSV_Report_${beginDate}_${endDate}`}
        >
            <button
                className="btn btn-outline-primary"
                disabled={!exportable}
            >
                CSV
            </button>
        </CSVLink>
    );
};

export default CSVButton;
