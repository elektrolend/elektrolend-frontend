import React from "react";
import DatePicker from "react-datepicker";

type Props = {
    beginDate: string;
    setBeginDate: (date: string) => void;
    endDate: string;
    setEndDate: (date: string) => void;
};

const DateRangePicker: React.FC<Props> = ({beginDate, setBeginDate, endDate, setEndDate}) => {
    const handleChangeBeginDate = (date: any) => {
        if (date) {
            setBeginDate(date.toLocaleDateString('en-CA'));
        }
    };

    const handleChangeEndDate = (date: any) => {
        if (date) {
            setEndDate(date.toLocaleDateString('en-CA'));
        }
    };

    return (
        <>
            <div className="col-md-3">
                <h6>OD </h6>
                <DatePicker
                    selected={new Date(beginDate)}
                    onChange={(date) => handleChangeBeginDate(date)}
                    maxDate={new Date(endDate)}
                    dateFormat="dd/MM/yyyy"
                    locale="pl"
                    required={true}
                />
            </div>
            <div className="col-md-3">
                <h6>DO </h6>
                <DatePicker
                    selected={new Date(endDate)}
                    onChange={(date) => handleChangeEndDate(date)}
                    minDate={new Date(beginDate)}
                    maxDate={new Date()}
                    dateFormat="dd/MM/yyyy"
                    locale="pl"
                    required={true}
                />
            </div>
        </>
    );
};

export default DateRangePicker;
