import React from "react";

type Props = {
    generatePDF: () => void;
    exportable: boolean;
};

const PDFButton: React.FC<Props> = ({generatePDF, exportable}) => {
    return (
        <button
            className="btn btn-outline-primary mr-2"
            onClick={() => generatePDF()}
            disabled={!exportable}
        >
            PDF
        </button>
    );
};

export default PDFButton;
