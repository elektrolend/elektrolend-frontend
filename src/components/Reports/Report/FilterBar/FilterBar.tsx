import React from "react";

import DateRangePicker from "./DateRangePicker";
import PDFButton from "./PDFButton";
import CSVButton from "./CSVButton";

type Props = {
    beginDate: string;
    setBeginDate: (date: string) => void;
    endDate: string;
    setEndDate: (date: string) => void;
    generatePDF: () => void;
    generateCSV: () => (string | number)[][];
    exportable: boolean;
};

const FilterBar: React.FC<Props> = ({
                                        beginDate,
                                        setBeginDate,
                                        endDate,
                                        setEndDate,
                                        generatePDF,
                                        generateCSV,
                                        exportable
                                    }) => {

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <DateRangePicker
                        beginDate={beginDate}
                        setBeginDate={setBeginDate}
                        endDate={endDate}
                        setEndDate={setEndDate}
                    />
                    <div className="col-md-6">
                        <h6>EXPORT</h6>
                        <PDFButton
                            generatePDF={generatePDF}
                            exportable={exportable}
                        />
                        <CSVButton
                            beginDate={beginDate}
                            endDate={endDate}
                            generateCSV={generateCSV}
                            exportable={exportable}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FilterBar;
