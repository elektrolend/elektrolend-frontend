import React, {useState} from "react";
import {isEmptyArray} from "formik";

import CategoriesReportTable from "./CategoriesReportTable/CategoriesReportTable";
import FilterBar from "../Report/FilterBar/FilterBar";
import {getCategoriesReport} from "../../../services/database.service";
import {generatePDF} from "../Report/report.service";
import {ICategoriesReport} from "../../../interfaces/Reports/ICategoriesReport";
import "./CategoriesReportTable/FreeSansBold-normal";

const CategoriesReport = () => {
    const initDate = new Date().toLocaleDateString('en-CA');
    const [categoriesReport, setCategoriesReport] = useState(null as ICategoriesReport | null);
    const [beginDate, setBeginDate] = useState(new Date("2020-01-01").toLocaleDateString('en-CA'));
    const [endDate, setEndDate] = useState(initDate);
    const tableName = "categoriesReportTable";

    React.useEffect(() => {
        getCategoriesReport(beginDate, endDate)
            .then(report => setCategoriesReport(report))
            .catch(() => setCategoriesReport(null));
    }, [beginDate, endDate]);

    const handleGeneratePDF = () => {
        generatePDF(beginDate, endDate, tableName);
    };

    const handleGenerateCSV = () => {
        const csvHeaders = ["Nazwa kategorii", "Nazwa produktu", "Suma za wypożyczenia"];
        const csvData: (string | number)[][] = [];
        csvData.push(csvHeaders);
        categoriesReport?.categoryReportDTOList.forEach(categoryReport => {
            csvData.push([categoryReport.categoryName, "", ""]);
            categoryReport.orderProductReportDTOList.forEach(orderProduct =>
                csvData.push(
                    [
                        "",
                        orderProduct.productName,
                        `${orderProduct.productSum} PLN`
                    ]
                ));
            csvData.push(["", "SUMA:", `${categoryReport.categorySum} PLN`]);
        });

        const totalSum = categoriesReport ? categoriesReport.categoriesSum : "";
        csvData.push(["SUMA:", "", `${totalSum} PLN`]);

        return csvData;
    };

    const exportable = () => {
        return !isEmptyArray(categoriesReport?.categoryReportDTOList);
    };

    if (!categoriesReport) {
        return null;
    }

    return (
        <div className="container mt-2 mb-2">
            <FilterBar
                beginDate={beginDate}
                setBeginDate={setBeginDate}
                endDate={endDate}
                setEndDate={setEndDate}
                generatePDF={handleGeneratePDF}
                generateCSV={handleGenerateCSV}
                exportable={exportable()}
            />
            <CategoriesReportTable
                tableName={tableName}
                categoryReportList={categoriesReport.categoryReportDTOList}
                totalSum={categoriesReport.categoriesSum}
                display={exportable()}
            />
        </div>
    );
};

export default CategoriesReport;
