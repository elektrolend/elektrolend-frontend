import React from "react";

const TableHeader = () => {
    return (
        <thead className="bg-primary text-center text-white">
        <tr>
            <th
                scope="col"
                className="border-right">
                NAZWA KATEGORII
            </th>
            <th
                scope="col"
                className="border-right">
                NAZWA PRZEDMIOTU
            </th>
            <th
                scope="col"
                className="border-right">
                SUMA ZA WYPOŻYCZENIA
            </th>
        </tr>
        </thead>
    );
};

export default TableHeader;
