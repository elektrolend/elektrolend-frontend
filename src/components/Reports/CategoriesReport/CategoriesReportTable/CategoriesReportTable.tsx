import React from "react";

import TableHeader from "./TableHeader/TableHeader";
import TableFooter from "./TableFooter/TableFooter";
import TableBody from "./TableBody/TableBody";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import {ICategoryReport} from "../../../../interfaces/Reports/ICategoryReport";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    tableName: string;
    categoryReportList: ICategoryReport[];
    totalSum: number;
    display: boolean;
};

const CategoriesReportTable: React.FC<Props> = ({tableName, categoryReportList, totalSum, display}) => {

    if (!display) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych wyników!"}
                description={"Zmień parametry wyszukiwania."}
                icon={
                    <EmojiFrownFill
                        size={50}
                        className="col-md-12"
                    />
                }
            />
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table" id={tableName}>
                            <TableHeader/>
                            <TableBody categoryReportList={categoryReportList}/>
                            <TableFooter totalSum={totalSum}/>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CategoriesReportTable;
