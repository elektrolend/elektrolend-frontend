import React from "react";

import CategoryTable from "../CategoryTable/CategoryTable";
import {ICategoryReport} from "../../../../../interfaces/Reports/ICategoryReport";

type Props = {
    categoryReportList: ICategoryReport[];
};

const TableBody: React.FC<Props> = ({categoryReportList}) => {
    return (
        <tbody className="bg-white">
        {categoryReportList.map((categoryReport, index) => {
            return (
                <CategoryTable
                    categoryReport={categoryReport}
                    key={"categoryReport_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default TableBody;
