import React from "react";

type Props = {
    totalSum: number;
};

const TableFooter: React.FC<Props> = ({totalSum}) => {
    return (
        <tfoot
            className="text-center text-white"
            style={{backgroundColor: "#48ABFF"}}>
        <tr>
            <td><b>SUMA:</b></td>
            <td></td>
            <td><b>{totalSum} PLN</b></td>

        </tr>
        </tfoot>
    );
};

export default TableFooter;
