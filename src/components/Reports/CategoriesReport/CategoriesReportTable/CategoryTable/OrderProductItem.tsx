import React from "react";

import {IOrderProductReport} from "../../../../../interfaces/Reports/IOrderProductReport";

type Props = {
    orderProductReport: IOrderProductReport;
};

const OrderProductItem: React.FC<Props> = ({orderProductReport}) => {
    return (
        <tr className="border rounded-bottom">
            <td className="border-right">
                {orderProductReport.productName}
            </td>
            <td className="text-center">
                {orderProductReport.productSum} PLN
            </td>
        </tr>
    );
};

export default OrderProductItem;
