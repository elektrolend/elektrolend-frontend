import React from "react";

import OrderProductItem from "./OrderProductItem";
import {IOrderProductReport} from "../../../../../interfaces/Reports/IOrderProductReport";

type Props = {
    orderProductReportList: IOrderProductReport[];
};

const CategoryTableBody: React.FC<Props> = ({orderProductReportList}) => {
    return (
        <>
            {orderProductReportList.map((orderProductReport, index) => {
                return (
                    <OrderProductItem
                        orderProductReport={orderProductReport}
                        key={"orderProductReport_" + index}
                    />
                );
            })}
        </>
    );
};

export default CategoryTableBody;
