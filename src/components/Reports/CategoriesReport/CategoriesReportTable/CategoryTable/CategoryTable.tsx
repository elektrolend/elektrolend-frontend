import React from "react";

import CategoryTableHeader from "./CategoryTableHeader";
import CategoryTableBody from "./CategoryTableBody";
import CategoryTableFooter from "./CategoryTableFooter";
import {ICategoryReport} from "../../../../../interfaces/Reports/ICategoryReport";

type Props = {
    categoryReport: ICategoryReport;
};

const CategoryTable: React.FC<Props> = ({categoryReport}) => {
    return (
        <>
            <CategoryTableHeader
                categoryName={categoryReport.categoryName}
                rowSpanNumber={categoryReport.orderProductReportDTOList.length + 2}
            />

            <CategoryTableBody orderProductReportList={categoryReport.orderProductReportDTOList}/>
            <CategoryTableFooter categorySum={categoryReport.categorySum}/>
        </>
    );
};

export default CategoryTable;
