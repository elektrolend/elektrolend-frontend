import React from "react";

type Props = {
    categorySum: number;
};

const CategoryTableFooter: React.FC<Props> = ({categorySum}) => {
    return (
        <tr className="bg-light border rounded-bottom">
            <td className={""}><b>SUMA:</b></td>
            <td className="text-center"><b>{categorySum} PLN</b></td>
        </tr>
    );
};

export default CategoryTableFooter;
