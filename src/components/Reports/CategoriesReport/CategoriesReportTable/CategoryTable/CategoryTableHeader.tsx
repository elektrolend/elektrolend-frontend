import React from "react";

type Props = {
    categoryName: string;
    rowSpanNumber: number;
};

const CategoryTableHeader: React.FC<Props> = ({categoryName, rowSpanNumber}) => {
    return (
        <tr className="border text-center rounded-bottom">
            <td
                className="border-right"
                rowSpan={rowSpanNumber}>
                <b>{categoryName}</b>
            </td>
        </tr>
    );
};

export default CategoryTableHeader;
