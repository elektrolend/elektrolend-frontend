import React from "react";

type Props = {
    customerSum: number;
};

const Footer: React.FC<Props> = ({customerSum}) => {
    return (
        <tr className="bg-light border rounded-bottom">
            <td className={""}><b>SUMA:</b></td>
            <td className="text-center"><b>{customerSum} PLN</b></td>
        </tr>
    );
};

export default Footer;
