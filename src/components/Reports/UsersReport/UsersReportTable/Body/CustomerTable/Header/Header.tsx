import React from "react";

type Props = {
    customerName: string;
    rowSpanNumber: number;
};

const Header: React.FC<Props> = ({customerName, rowSpanNumber}) => {
    return (
        <tr className="border text-center rounded-bottom">
            <td
                className="border-right"
                rowSpan={rowSpanNumber}>
                <b>{customerName}</b>
            </td>
        </tr>
    );
};

export default Header;
