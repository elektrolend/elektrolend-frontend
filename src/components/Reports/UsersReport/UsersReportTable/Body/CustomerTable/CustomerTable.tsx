import React from "react";

import Header from "./Header/Header";
import Body from "./Body/Body";
import Footer from "./Footer/Footer";
import {IUserStat} from "../../../../../../interfaces/Reports/IUserStat";

type Props = {
    customerReport: IUserStat;
};

const CustomerTable: React.FC<Props> = ({customerReport}) => {
    return (
        <>
            <Header
                customerName={customerReport.customerName}
                rowSpanNumber={customerReport.productListForUserStatsDTO.length + 2}
            />
            <Body productListForUserStats={customerReport.productListForUserStatsDTO}/>
            <Footer customerSum={customerReport.totalSum}/>
        </>
    );
};

export default CustomerTable;
