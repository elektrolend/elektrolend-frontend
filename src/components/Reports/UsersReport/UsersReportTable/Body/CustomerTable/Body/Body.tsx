import React from "react";

import Item from "./Item";
import {IProductListForUserStats} from "../../../../../../../interfaces/Reports/IProductListForUserStats";

type Props = {
    productListForUserStats: IProductListForUserStats[];
};

const Body: React.FC<Props> = ({productListForUserStats}) => {
    return (
        <>
            {productListForUserStats.map((customerProduct, index) => {
                return (
                    <Item
                        customerProduct={customerProduct}
                        key={"customerProduct_" + index}
                    />
                );
            })}
        </>
    );
};

export default Body;
