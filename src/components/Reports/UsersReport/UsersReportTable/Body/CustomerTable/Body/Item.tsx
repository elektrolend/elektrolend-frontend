import React from "react";

import {IProductListForUserStats} from "../../../../../../../interfaces/Reports/IProductListForUserStats";

type Props = {
    customerProduct: IProductListForUserStats;
};

const Item: React.FC<Props> = ({customerProduct}) => {
    return (
        <tr className="border rounded-bottom">
            <td className="border-right">
                {customerProduct.productName}
            </td>
            <td className="text-center">
                {customerProduct.totalSum} PLN
            </td>
        </tr>
    );
};

export default Item;
