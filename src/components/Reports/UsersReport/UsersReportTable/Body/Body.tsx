import React from "react";

import CustomerTable from "./CustomerTable/CustomerTable";
import {IUserStat} from "../../../../../interfaces/Reports/IUserStat";

type Props = {
    userReportList: IUserStat[];
};

const Body: React.FC<Props> = ({userReportList}) => {
    return (
        <tbody className="bg-white">
        {userReportList.map((userStat, index) => {
            return (
                <CustomerTable
                    customerReport={userStat}
                    key={"customerReport_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default Body;
