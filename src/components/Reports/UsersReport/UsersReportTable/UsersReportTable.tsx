import React from "react";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";

import Header from "./Header/Header";
import Body from "./Body/Body";
import {IUserStat} from "../../../../interfaces/Reports/IUserStat";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    tableName: string;
    userReportList: IUserStat[];
    show: boolean;
};

const UsersReportTable: React.FC<Props> = ({tableName, userReportList, show}) => {
    if (!show) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych wyników!"}
                description={"Zmień parametry wyszukiwania."}
                icon={
                    <EmojiFrownFill
                        size={50}
                        className="col-md-12"
                    />
                }
            />
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table" id={tableName}>
                            <Header/>
                            <Body userReportList={userReportList}/>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UsersReportTable;
