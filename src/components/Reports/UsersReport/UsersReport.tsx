import React, {useEffect, useState} from "react";
import {isEmptyArray} from "formik";

import FilterBar from "../Report/FilterBar/FilterBar";
import UsersReportTable from "./UsersReportTable/UsersReportTable";
import {generatePDF} from "../Report/report.service";
import {getUsersReport} from "../../../services/database.service";
import {IUsersReport} from "../../../interfaces/Reports/IUsersReport";

const UsersReport = () => {
    const initDate = new Date().toLocaleDateString('en-CA');
    const [usersReport, setUsersReport] = useState(null as IUsersReport | null);
    const [beginDate, setBeginDate] = useState(new Date("2020-01-01").toLocaleDateString('en-CA'));
    const [endDate, setEndDate] = useState(initDate);
    const tableName = "usersReportTable";

    useEffect(() => {
        getUsersReport(beginDate, endDate)
            .then(report => setUsersReport(report))
            .catch(() => setUsersReport(null));
    }, [beginDate, endDate]);

    const handleGeneratePDF = () => {
        generatePDF(beginDate, endDate, tableName);
    };

    const handleGenerateCSV = () => {
        const csvHeaders = ["Nazwa klienta", "Nazwa produktu", "Suma za wypożyczenia"];
        const data: (string | number)[][] = [];

        data.push(csvHeaders);

        usersReport?.userStats.forEach(customerReport => {
            data.push([customerReport.customerName, "", ""]);
            customerReport.productListForUserStatsDTO.forEach(productReport =>
                data.push(
                    [
                        "",
                        productReport.productName,
                        `${productReport.totalSum} PLN`
                    ]
                ));
            data.push(["", "SUMA:", `${customerReport.totalSum} PLN`]);
        });

        return data;
    };

    const exportable = () => {
        return !isEmptyArray(usersReport?.userStats);
    };

    if (!usersReport) {
        return null;
    }

    return (
        <>
            <div className="container mt-2 mb-2">
                <FilterBar
                    beginDate={beginDate}
                    setBeginDate={setBeginDate}
                    endDate={endDate}
                    setEndDate={setEndDate}
                    generatePDF={handleGeneratePDF}
                    generateCSV={handleGenerateCSV}
                    exportable={exportable()}
                />
                <UsersReportTable
                    tableName={tableName}
                    userReportList={usersReport.userStats}
                    show={exportable()}
                />
            </div>
        </>
    );
};

export default UsersReport;
