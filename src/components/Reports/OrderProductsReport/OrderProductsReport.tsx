import React, {useState} from "react";
import {isEmptyArray} from "formik";

import FilterBar from "../Report/FilterBar/FilterBar";
import OrderProductsTable from "./OrderProductsTable/OrderProductsTable";
import {getOrderProductsReport} from "../../../services/database.service";
import {generatePDF} from "../Report/report.service";
import {IOrderProductsReport} from "../../../interfaces/Reports/IOrderProductsReport";

const OrderProductsReport = () => {
    const initDate = new Date().toLocaleDateString('en-CA');
    const [orderProductsReport, setOrderProductsReport] = useState(null as IOrderProductsReport | null);
    const [beginDate, setBeginDate] = useState(new Date("2020-01-01").toLocaleDateString('en-CA'));
    const [endDate, setEndDate] = useState(initDate);
    const tableName = "orderProductsReportTable";

    React.useEffect(() => {
        getOrderProductsReport(beginDate, endDate)
            .then(report => setOrderProductsReport(report))
            .catch(() => setOrderProductsReport(null));
    }, [beginDate, endDate]);

    const handleGeneratePDF = () => {
        generatePDF(beginDate, endDate, tableName);
    };

    const handleGenerateCSV = () => {
        const csvHeaders = ["Nazwa produktu", "Liczba wypożyczeń"];
        const csvData: (string | number)[][] = [];
        csvData.push(csvHeaders);
        orderProductsReport?.productReportDTOList.forEach(productReport => {
            csvData.push(
                [
                    productReport.productName,
                    productReport.rentalNumbers
                ]
            )
        });
        return csvData;
    };

    const exportable = () => {
        return !isEmptyArray(orderProductsReport?.productReportDTOList);
    };

    if (!orderProductsReport) {
        return null;
    }

    return (
        <>
            <FilterBar
                beginDate={beginDate}
                setBeginDate={setBeginDate}
                endDate={endDate}
                setEndDate={setEndDate}
                generatePDF={handleGeneratePDF}
                generateCSV={handleGenerateCSV}
                exportable={exportable()}
            />
            <OrderProductsTable
                tableName={tableName}
                orderProductsReport={orderProductsReport}
                display={exportable()}
            />
        </>
    );
};

export default OrderProductsReport;
