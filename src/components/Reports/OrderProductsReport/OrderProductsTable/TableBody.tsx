import React from "react";

import TableBodyItem from "./TableBodyItem";
import {IProductReport} from "../../../../interfaces/Reports/IProductReport";

type Props = {
    orderProductReportList: IProductReport[];
};

const TableBody: React.FC<Props> = ({orderProductReportList}) => {
    return (
        <tbody className="bg-white text-center">
        {orderProductReportList.map((productReport, index) => {
            return (
                <TableBodyItem
                    productReport={productReport}
                    key={"productReport_" + index}
                />
            );
        })}
        </tbody>
    );
}

export default TableBody;
