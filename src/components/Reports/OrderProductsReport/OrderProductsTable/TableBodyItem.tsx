import React from "react";

import {IProductReport} from "../../../../interfaces/Reports/IProductReport";

type Props = {
    productReport: IProductReport;
};

const TableBodyItem: React.FC<Props> = ({productReport}) => {
    return (
        <tr className="border rounded-bottom">
            <td className="border-right">
                {productReport.productName}
            </td>
            <td className="border-right">
                {productReport.rentalNumbers}
            </td>
        </tr>
    );
};

export default TableBodyItem;
