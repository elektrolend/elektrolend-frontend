import React from "react";

const TableHeader = () => {
    return (
        <thead className="bg-primary text-center text-white">
        <tr>
            <th
                scope="col"
                className="border-right">
                Nazwa produktu
            </th>
            <th
                scope="col"
                className="border-right">
                Liczba wypożyczeń
            </th>
        </tr>
        </thead>
    );
};

export default TableHeader;
