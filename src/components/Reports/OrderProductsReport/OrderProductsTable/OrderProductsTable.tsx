import React from "react";

import TableBody from "./TableBody";
import TableHeader from "./TableHeader";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import {IOrderProductsReport} from "../../../../interfaces/Reports/IOrderProductsReport";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    tableName: string;
    orderProductsReport: IOrderProductsReport;
    display: boolean;
};

const OrderProductsTable: React.FC<Props> = ({tableName, orderProductsReport, display}) => {
    if (!display) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych wyników!"}
                description={"Zmień parametry wyszukiwania."}
                icon={
                    <EmojiFrownFill
                        size={50}
                        className="col-md-12"
                    />
                }
            />
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table" id={tableName}>
                            <TableHeader/>
                            <TableBody orderProductReportList={orderProductsReport.productReportDTOList}/>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderProductsTable;
