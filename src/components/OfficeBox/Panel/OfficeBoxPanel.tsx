import React, {useState} from "react";

import FilterBar from "./FilterBar/FilterBar";
import OfficeBoxTable from "./OfficeBoxTable/OfficeBoxTable";

import {getOfficeBoxStatuses} from "../../../services/database.service";
import {IOfficeBoxDailyDetail} from "../../../interfaces/Orders/IOfficeBoxDailyDetail";

const OfficeBoxPanel = () => {
    const [statuses, setStatuses] = useState([] as IOfficeBoxDailyDetail[]);

    const [date, setDate] = useState(new Date().toLocaleDateString('en-CA'));

    React.useEffect(() => {
        getOfficeBoxStatuses(date)
            .then(s => setStatuses(s))
            .catch(() => setStatuses([]));
    }, [date]);

    return (
        <>
            <FilterBar
                date={date}
                setDate={setDate}
            />
            <OfficeBoxTable
                statuses={statuses}
            />
        </>
    );
};

export default OfficeBoxPanel;
