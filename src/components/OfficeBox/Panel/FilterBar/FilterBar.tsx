import React from "react";

import DatePicker from "./DatePicker/DatePicker";

type Props = {
    date: string;
    setDate: (newDate: string) => void;
};

const FilterBar: React.FC<Props> = ({date, setDate}) => {
    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-9"></div>
                    <DatePicker
                        date={date}
                        setDate={setDate}
                    />
                </div>
            </div>
        </div>
    );
}

export default FilterBar;
