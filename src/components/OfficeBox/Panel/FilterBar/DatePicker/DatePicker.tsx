import React from "react";

type Props = {
    date: string
    setDate: (date: string) => void;
};

const DatePicker: React.FC<Props> = ({date, setDate}) => {
    const handleChangeDate = (event: any) => {
        const newDate = event.target.value || "";
        if (newDate) {
            setDate(newDate);
        }
    };

    return (
        <div className="col-md-3 justify-content-end align-items-center text-left">
            <h6>Data: </h6>
            <input
                type="date"
                className="form-control"
                value={date}
                onChange={(event) => handleChangeDate(event)}
            >
            </input>
        </div>
    );
};

export default DatePicker;
