import React from "react";
import {generatePath, Link} from "react-router-dom";

import StatusCell from "./StatusCell";
import {IOfficeBoxDailyDetail} from "../../../../../interfaces/Orders/IOfficeBoxDailyDetail";

type Props = {
    status: IOfficeBoxDailyDetail;
};

const BodyItem: React.FC<Props> = ({status}) => {
    const getCustomerName = () => {
        if (status.customerName) {
            return status.customerName;
        }
        return "-";
    };

    const getProductName = () => {
        if (status.productName) {
            return getLink();
        }
        return "-";
    };

    const isOccupied = () => {
        return status.productName !== null;
    };

    const getLink = () => {
        if (status.productName && status.productID) {
            return (
                <Link
                    to={generatePath(`/product/details/:id/:name`,
                        {id: status.productID || "", name: status.productName || ""})}
                    className="text-dark"
                    target="_blank"
                >
                    {status.productName}
                </Link>
            );
        }

        return null;
    }

    return (
        <tr className="text-center rounded-bottom align-content-center h6">
            <td style={{verticalAlign: "middle"}}>
                {status.officeBoxID}
            </td>
            <td style={{verticalAlign: "middle"}}>
                <b>{getCustomerName()}</b>
            </td>
            <td style={{verticalAlign: "middle"}}>
                {getProductName()}
            </td>
            <td style={{verticalAlign: "middle"}}>
                <StatusCell isOccupied={isOccupied()}/>
            </td>
        </tr>
    );
};

export default BodyItem;
