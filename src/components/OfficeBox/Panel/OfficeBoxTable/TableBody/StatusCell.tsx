import React from "react";

type Props = {
    isOccupied: boolean;
};

const StatusCell: React.FC<Props> = ({isOccupied}) => {
    const getStatusText = () => {
        if (isOccupied) {
            return "Zajęta";
        }
        return "Pusta";
    };

    return (
        <div className="text-center">
            {getStatusText()}
        </div>
    );
};

export default StatusCell;
