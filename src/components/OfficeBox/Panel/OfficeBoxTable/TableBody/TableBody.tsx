import React from "react";

import BodyItem from "./BodyItem";
import {IOfficeBoxDailyDetail} from "../../../../../interfaces/Orders/IOfficeBoxDailyDetail";

type Props = {
    statuses: IOfficeBoxDailyDetail[];
};

const TableBody: React.FC<Props> = ({statuses}) => {
    return (
        <tbody className="bg-white">
        {statuses.map((status, index) => {
            return (
                <BodyItem
                    status={status}
                    key={"officeBoxStatus_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default TableBody;
