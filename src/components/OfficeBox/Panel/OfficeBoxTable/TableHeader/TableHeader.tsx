import React from "react";

const TableHeader = () => {
    return (
        <thead className="bg-primary text-center text-white border-bottom-2">
        <tr>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="col-md-1">
                ID
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="col-md-4">
                KLIENT
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="col-md-4">
                PRODUKT
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="col-md-3">
                STATUS
            </th>
        </tr>
        </thead>
    );
};

export default TableHeader;
