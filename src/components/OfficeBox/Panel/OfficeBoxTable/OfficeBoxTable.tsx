import React from "react";
import {isEmptyArray} from "formik";

import TableHeader from "./TableHeader/TableHeader";
import TableBody from "./TableBody/TableBody";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import {IOfficeBoxDailyDetail} from "../../../../interfaces/Orders/IOfficeBoxDailyDetail";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    statuses: IOfficeBoxDailyDetail[];
};

const OfficeBoxTable: React.FC<Props> = ({statuses}) => {
    if (isEmptyArray(statuses)) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych wyników!"}
                description={"Zmień parametry filtrowania."}
                icon={
                    <EmojiFrownFill
                        className="col-md-12 mb-2"
                        size={50}
                    />
                }
            />
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <TableHeader/>
                            <TableBody statuses={statuses}/>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OfficeBoxTable;
