import React, {useState} from "react";

import NavBar from "./NavBar/NavBar";
import ProtectedComponent from "../Auth/ProtectedComponent";
import panelItems from "./Panelitems/panelItems";
import {ITileSubItem} from "../../interfaces/Accounts/ITileSubItem";

const EmployeePanel = () => {
    const defaultPanelView = panelItems[2].subItems[1];
    const [currentPanelView, setCurrentPanelView] = useState(defaultPanelView);

    const handleChangeCurrentPanelView = (panelView: ITileSubItem) => {
        setCurrentPanelView(defaultPanelView);
        setTimeout(() => setCurrentPanelView(panelView), 1);
    };

    return (
        <>
            <NavBar
                currentPanelView={currentPanelView}
                setCurrentPanelView={handleChangeCurrentPanelView}
            />
            <div className="container mt-2 mb-2">
                <ProtectedComponent
                    component={<currentPanelView.component/>}
                    roles={currentPanelView.roles}>
                </ProtectedComponent>
            </div>
        </>
    );
};

export default EmployeePanel;
