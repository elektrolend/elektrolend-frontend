import React from "react";

import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

type Props = {
    item: ITileSubItem;
    currentPanelView: ITileSubItem;
    setCurrentPanelView: (item: ITileSubItem) => void;
};

const TileSubItem: React.FC<Props> = ({item, currentPanelView, setCurrentPanelView}) => {
    const isChecked = () => {
        return item === currentPanelView;
    };

    const isActive = () => {
        if (isChecked()) {
            return "active";
        }
        return "";
    };

    return (
        <div
            className={"dropdown-item " + isActive()}
            onClick={() => setCurrentPanelView(item)}>
            {item.title}
        </div>
    );
};

export default TileSubItem;
