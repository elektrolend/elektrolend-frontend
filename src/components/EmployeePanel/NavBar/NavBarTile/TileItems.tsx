import React from "react";

import TileSubItem from "./TileSubItem";
import ProtectedComponent from "../../../Auth/ProtectedComponent";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

type Props = {
    items: ITileSubItem[];
    currentPanelView: ITileSubItem;
    setCurrentPanelView: (item: ITileSubItem) => void;
};

const TileItems: React.FC<Props> = ({items, currentPanelView, setCurrentPanelView}) => {
    return (
        <div
            className="dropdown-menu"
            aria-labelledby="dropdownMenuButton"
        >
            {items.map((subItem: ITileSubItem, index: number) => {
                return (
                    <ProtectedComponent
                        component={
                            <TileSubItem
                                item={subItem}
                                currentPanelView={currentPanelView}
                                setCurrentPanelView={setCurrentPanelView}
                            />
                        }
                        roles={subItem.roles}
                        key={"route_" + index}
                    />
                );
            })}
        </div>
    );
};

export default TileItems;
