import React from "react";

import TileItems from "./TileItems";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";
import {IPanelItem} from "../../../../interfaces/Accounts/IPanelItem";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type Props = {
    panelItem: IPanelItem;
    currentPanelView: ITileSubItem;
    setCurrentPanelView: (item: ITileSubItem) => void;
    buttonType: string;
};

const NavBarTile: React.FC<Props> = ({panelItem, currentPanelView, setCurrentPanelView, buttonType}) => {
    return (
        <div className="dropdown text-center">
            <li
                className="nav-item"
                id="dropdownMenuButton"
                data-toggle="dropdown"
            >
                <button
                    className={"btn btn-light dropdown-toggle mr-2 " + buttonType}
                    id="dropdownMenuButton"
                    aria-haspopup="true"
                    aria-expanded="false">
                    <FontAwesomeIcon
                        className="icon mr-1"
                        icon={panelItem.icon}/>
                    <b className="">{panelItem.title}</b>
                </button>
            </li>
            <TileItems
                items={panelItem.subItems}
                setCurrentPanelView={setCurrentPanelView}
                currentPanelView={currentPanelView}
            />
        </div>
    );
};

export default NavBarTile;
