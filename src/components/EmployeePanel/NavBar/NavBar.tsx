import React from 'react';

import NavBarTile from "./NavBarTile/NavBarTile";
import panelItems from "../Panelitems/panelItems";
import {ITileSubItem} from "../../../interfaces/Accounts/ITileSubItem";
import {IPanelItem} from "../../../interfaces/Accounts/IPanelItem";


type Props = {
    currentPanelView: ITileSubItem;
    setCurrentPanelView: (item: ITileSubItem) => void;
};

const NavBar: React.FC<Props> = ({currentPanelView, setCurrentPanelView}) => {
    const getButtonType = (item: IPanelItem) => {
        if (item.subItems.lastIndexOf(currentPanelView) !== -1) {
            return "text-primary";
        }

        return "text-secondary";
    }

    return (
        <nav className="navbar navbar-light navbar-expand-lg bg-light border-bottom">
            <ul className="navbar-nav w-100 mr-auto justify-content-center align-items-center">
                {panelItems.map((item, index) => {
                    return (
                        <NavBarTile
                            panelItem={item}
                            currentPanelView={currentPanelView}
                            setCurrentPanelView={setCurrentPanelView}
                            buttonType={getButtonType(item)}
                            key={"navbarTile_" + index}
                        />
                    );
                })}
            </ul>
        </nav>
    );
}

export default NavBar;
