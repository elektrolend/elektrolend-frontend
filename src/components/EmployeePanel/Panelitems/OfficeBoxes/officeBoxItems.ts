import OfficeBoxPanel from "../../../OfficeBox/Panel/OfficeBoxPanel";

import {AccountType} from "../../../../interfaces/Accounts/AccountType";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

const officeBoxItems: ITileSubItem[] = [
    {
        title: "Wszystkie",
        url: "/office-boxes",
        component: OfficeBoxPanel,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default officeBoxItems;
