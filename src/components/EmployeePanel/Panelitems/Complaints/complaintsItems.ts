import ComplaintsManagement from "../../../Complaints/ComplaintsManagement/ComplaintsManagement";
import UnclaimedComplaints from "../../../Complaints/UnclaimedComplaints/UnclaimedComplaints";

import {AccountType} from "../../../../interfaces/Accounts/AccountType";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

const complaintsItems: ITileSubItem[] = [
    {
        title: "Wszystkie",
        url: "/complaints-management",
        component: ComplaintsManagement,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Nieodebrane",
        url: "/unclaimed-complaints",
        component: UnclaimedComplaints,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default complaintsItems;
