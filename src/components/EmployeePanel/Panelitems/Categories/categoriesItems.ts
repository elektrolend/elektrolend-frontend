import AddCategory from "../../../Products/AddCategory/AddCategory";
import {AccountType} from "../../../../interfaces/Accounts/AccountType";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

const categoriesItems: ITileSubItem[] = [
    {
        title: "Nowa",
        url: "/add-category",
        component: AddCategory,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default categoriesItems;
