import AddProduct from "../../../Products/AddProduct/AddProduct";
import UnavailableProducts from "../../../Products/UnavailableProducts/UnavailableProducts";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";
import {AccountType} from "../../../../interfaces/Accounts/AccountType";

const productsItems: ITileSubItem[] = [
    {
        title: "Nowy",
        url: "/add-product",
        component: AddProduct,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Nieaktywne",
        url: "/unavailable-products",
        component: UnavailableProducts,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default productsItems;
