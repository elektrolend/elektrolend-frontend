import productsItems from "./Products/productsItems";
import categoriesItems from "./Categories/categoriesItems";
import ordersItems from "./Orders/ordersItems";
import complaintsItems from "./Complaints/complaintsItems";
import reportsItems from "./Reports/reportsItems";
import userItems from "./Users/userItems";
import officeBoxItems from "./OfficeBoxes/officeBoxItems";
import {IPanelItem} from "../../../interfaces/Accounts/IPanelItem";

import {faTh} from "@fortawesome/free-solid-svg-icons";

const panelItems: IPanelItem[] = [
    {
        title: "Produkty",
        icon: faTh,
        subItems: productsItems
    },
    {
        title: "Kategorie",
        icon: faTh,
        subItems: categoriesItems
    },
    {
        title: "Zamówienia",
        icon: faTh,
        subItems: ordersItems
    },
    {
        title: "Reklamacje",
        icon: faTh,
        subItems: complaintsItems
    },
    {
        title: "Raporty",
        icon: faTh,
        subItems: reportsItems
    },
    {
        title: "Użytkownicy",
        icon: faTh,
        subItems: userItems
    },
    {
        title: "Skrytki",
        icon: faTh,
        subItems: officeBoxItems
    }
]

export default panelItems;
