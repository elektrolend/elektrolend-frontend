import AddEmployee from "../../../Users/AddEmployee/AddEmployee";

import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";
import {AccountType} from "../../../../interfaces/Accounts/AccountType";

const userItems: ITileSubItem[] = [
    {
        title: "Nowy pracownik",
        url: "/add-employee",
        component: AddEmployee,
        roles: [AccountType.admin]
    }
]

export default userItems;
