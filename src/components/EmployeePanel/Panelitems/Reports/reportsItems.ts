import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";
import {AccountType} from "../../../../interfaces/Accounts/AccountType";
import CategoriesReport from "../../../Reports/CategoriesReport/CategoriesReport";
import OrderProductsReport from "../../../Reports/OrderProductsReport/OrderProductsReport";
import UsersReport from "../../../Reports/UsersReport/UsersReport";

const reportsItems: ITileSubItem[] = [
    {
        title: "Finansowy",
        url: "/categories-report",
        component: CategoriesReport,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Wypożyczenia",
        url: "/order-products-report",
        component: OrderProductsReport,
        roles: [AccountType.regular, AccountType.admin]
    }
    ,
    {
        title: "Użytkowników",
        url: "/users-report",
        component: UsersReport,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default reportsItems;
