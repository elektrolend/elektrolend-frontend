import UpcomingOrders from "../../../Orders/UpcomingOrders/UpcomingOrders";
import ChangeStates from "../../../Orders/ChangeStates/ChangeStates";
import DepositRefund from "../../../Orders/DepositRefund/DepositRefund";
import OrdersView from "../../../Orders/OrdersView/OrdersView";
import {AccountType} from "../../../../interfaces/Accounts/AccountType";
import {ITileSubItem} from "../../../../interfaces/Accounts/ITileSubItem";

const ordersItems: ITileSubItem[] = [
    {
        title: "Wszystkie",
        url: "/all-orders",
        component: OrdersView,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Nadchodzące",
        url: "/upcoming-orders",
        component: UpcomingOrders,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Zmiana stanów",
        url: "/change-states",
        component: ChangeStates,
        roles: [AccountType.regular, AccountType.admin]
    },
    {
        title: "Zwrot kaucji",
        url: "/deposit-refund",
        component: DepositRefund,
        roles: [AccountType.regular, AccountType.admin]
    }
]

export default ordersItems;
