import React, {useState} from "react";
import {generatePath, Link} from "react-router-dom";

import LoadingPhoto from "../../Utils/LoadingPhoto";
import {getCategory} from "../../../services/database.service";
import {ICategory} from "../../../interfaces/Products/ICategory";

import "./BestCategories.css";

type Props = {
    categoryID: string;
};

const CategoryItem: React.FC<Props> = ({categoryID}) => {
    const [category, setCategory] = useState(null as ICategory | null);

    React.useEffect(() => {
        getCategory(categoryID)
            .then(c => setCategory(c))
            .catch(() => setCategory(null));
    }, [categoryID]);

    if (!category) {
        return null;
    }

    return (
        <div className="col-md-3 p-2">
            <Link
                to={generatePath(`/category/:id`, {id: category.id})}
                className="text-decoration-none"
            >
                <div
                    className="card cardHover"
                    style={{height: "450px"}}
                >
                    <LoadingPhoto
                        photoUrl={category.imageUrl}
                        photoAlt={category.name}
                    />
                    <div
                        className="card-body text-center"
                        style={{height: "40%"}}
                    >
                        <h3 className="card-title text-dark">
                            {category.name}
                        </h3>
                    </div>
                </div>
            </Link>
        </div>
    );
}

export default CategoryItem;
