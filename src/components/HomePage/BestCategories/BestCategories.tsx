import React from "react";
import {useQuery} from "react-query";

import CategoryItem from "./CategoryItem";
import {getProducts} from "../../../services/database.service";
import {getItemsAppearances} from "../home.service";
import {IProductsResponse} from "../../../interfaces/Products/IProductsResponse";

const BestCategories = () => {
    const {data: products, isLoading: productsLoading, error: productsError} = useQuery<IProductsResponse>(
        'products',
        getProducts
    );

    const getFilterCategories = () => {
        const categoriesArray = products ? products.productDTOList.map(e => e.categoryID) : [];
        return getItemsAppearances(categoriesArray)
            .sort((p, c) => p.count - c.count)
            .reverse()
            .slice(0, 4);
    };

    if (productsLoading || productsError) {
        return (
            <div className="container">
                <h1>Ładowanie...</h1>
            </div>
        );
    }

    return (
        <>
            <div className="container">
                <div className="mb-3">
                    <h2>Najpopularniejsze kategorie</h2> <a
                    className="hover-move text-primary text-sm-center"
                    href="/categories"> zobacz wszystkie</a>
                </div>
            </div>
            <div className="bg-light mb-5 p-4">
                <div className="container">
                    <div className="row justify-content-center">
                        {getFilterCategories().map((category, index) => {
                            return (
                                <CategoryItem
                                    categoryID={category.id}
                                    key={"category_" + index}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
}

export default BestCategories;
