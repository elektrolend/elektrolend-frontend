import MainView from "./MainView/MainView";
import BestSellers from "./BestSellers/BestSellers";
import BestCategories from "./BestCategories/BestCategories";

const HomePage = () => {
    return (
        <>
            <MainView/>
            <BestCategories/>
            <BestSellers/>
        </>
    );
}

export default HomePage;
