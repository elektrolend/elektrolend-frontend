import React from "react";

const MainLogo = () => {
    return (
        <div className="bg-light mt-3 text-center">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 text-xl-left p-2 mt-4">
                        <h1 className="text-primary" style={{fontSize: "xxx-large"}}>ElektroLend</h1>
                        <h5>U nas wypożyczysz <b>szybko</b> oraz <b>bezkontaktowo</b>.</h5>
                    </div>
                    <div className="col-md-6 text-right">
                        <img
                            src="frontpage.png"
                            className="img-fluid"
                            alt="elektrolend">
                        </img>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MainLogo;
