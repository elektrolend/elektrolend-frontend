import AboutUs from "../../../AboutUs/AboutUs";
import Contact from "../../../Contact/Contact";

interface MainBarItem {
    title: string,
    url: string,
    cName: string,
    componentName: any
}

const mainBarItems: MainBarItem[] = [
    {
        title: "O nas",
        url: "/o-nas",
        cName: "btn btn-primary mr-2",
        componentName: AboutUs
    },
    {
        title: "Kontakt",
        url: "/kontakt",
        cName: "btn btn-primary",
        componentName: Contact
    }
]

export default mainBarItems;
