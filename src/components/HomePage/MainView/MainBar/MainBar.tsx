import React from "react";
import {Link} from "react-router-dom";
import mainBarItems from "./mainBarItems";

const MainBar = () => {
    return (
        <div className="mb-5 mt-2 bg-light border">
            <div className="container">
                <div className="col-12 text-right mt-2">
                    {mainBarItems.map((mainBarItem, index) => {
                        return (
                            <Link
                                to={mainBarItem.url}
                                key={"mainBarItem_" + index}
                            >
                                <button className={mainBarItem.cName}>{mainBarItem.title}</button>
                            </Link>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}

export default MainBar;
