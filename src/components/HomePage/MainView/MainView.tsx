import React from "react";
import MainBar from "./MainBar/MainBar";
import MainLogo from "./MainLogo/MainLogo";

const MainView = () => {
    return (
        <>
            <MainLogo/>
            <MainBar/>
        </>
    );
}

export default MainView;
