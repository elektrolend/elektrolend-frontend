export const getItemsAppearances = (array: number[]) => {
    const result = array?.reduce((a, c) => {
        a[c] = (a[c] || 0) + 1;
        return a;
    }, Object.create(null));
    return Object.keys(result).map(e => {
        return (
            {
                id: e,
                count: result[e]
            }
        );
    })
};
