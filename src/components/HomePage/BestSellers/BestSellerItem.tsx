import React, {useState} from "react";
import {generatePath, Link} from "react-router-dom";

import LoadingPhoto from "../../Utils/LoadingPhoto";
import {getProduct} from "../../../services/database.service";
import {IProduct} from "../../../interfaces/Products/IProduct";

type Props = {
    productID: string;
};

const BestSellerItem: React.FC<Props> = ({productID}) => {
    const [product, setProduct] = useState(null as IProduct | null);

    React.useEffect(() => {
        getProduct(productID)
            .then(p => setProduct(p))
            .catch(() => setProduct(null));
    }, [productID]);

    if (!product) {
        return null;
    }

    return (
        <div className="col-md-3 p-2">
            <Link
                to={generatePath(`/product/details/:id/:name`, {id: product.id, name: product.name})}
                className="text-dark text-decoration-none"
            >
                <div
                    className="card cardHover"
                    style={{height: "450px"}}
                >
                    <LoadingPhoto
                        photoUrl={product.imageUrl}
                        photoAlt={product.name}
                    />
                    <div
                        className="card-body text-center"
                        style={{height: "40%"}}
                    >
                        <h3 className="card-title text-primary">
                            {product.price} PLN
                        </h3>
                        <p className="card-text">
                            <strong>
                                {product.name}
                            </strong>
                        </p>
                    </div>
                </div>
            </Link>
        </div>
    );
}

export default BestSellerItem;
