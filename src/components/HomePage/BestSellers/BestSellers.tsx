import React, {useState} from "react";

import BestSellerItem from "./BestSellerItem";
import {getAllOrderProducts} from "../../../services/database.service";
import {getItemsAppearances} from "../home.service";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";

import "./BestSellers.css";

const BestSellers = () => {
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);

    React.useEffect(() => {
        getAllOrderProducts()
            .then(products => setOrderProducts(products))
            .catch(() => setOrderProducts([]));
    }, []);

    const getBestSellers = () => {
        const productsArray = orderProducts.map(e => e.productID);
        return getItemsAppearances(productsArray)
            .sort((p, c) => p.count - c.count)
            .reverse()
            .slice(0, 4);
    };

    return (
        <>
            <div className="container">
                <div className="mb-3">
                    <h2>Najpopularniejsze przedmioty</h2> <a
                    className="hover-move text-primary text-sm-center"
                    href="/products"> zobacz wszystkie</a>
                </div>
            </div>
            <div className="bg-light mb-5 p-4">
                <div className="container">
                    <div className="row justify-content-center">
                        {getBestSellers().map((bestSeller, index) => {
                            return (
                                <BestSellerItem
                                    productID={bestSeller.id}
                                    key={"bestSeller_" + index}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
}

export default BestSellers;
