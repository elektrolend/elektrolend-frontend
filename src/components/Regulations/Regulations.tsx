import { Component } from 'react';

class Regulations extends Component {
    render() {
        return (
            <div className="container mt-2">
                <div className="h2 text-primary"> Regulamin </div>
                <div>
                    <h5>Definicje</h5>
                    <p>Na potrzeby niniejszego regulaminu, przyjmuje się następujące znaczenie poniższych pojęć:
                        <ol type="a">
                            <li>
                                Klient – osoba fizyczna, osoba prawna lub ułomna osoba prawna,
                            </li>
                            <li>
                                Kodeks cywilny – ustawa z dnia 23 kwietnia 1964 r. Kodeks cywilny (Dz.U.1964.16.93),
                            </li>
                            <li>
                                Konsument – Klient będący osobą fizyczną, dokonujący zakupów w Sklepie w zakresie niezwiązanym
                                 bezpośrednio o z jego działalnością gospodarczą lub zawodową (zgodnie z treścią art. 221
                                 Kodeksu cywilnego),
                            </li>
                            <li>
                                Produkt – każdy produkt dostępny w Sklepie,
                            </li>
                            <li>
                                Regulamin – niniejszy regulamin,
                            </li>
                            <li>
                                Ustawa o Prawach Konsumenta – ustawa z dnia 30 maja 2014 r. o prawach konsumenta (Dz. U. 2014 poz. 827).
                            </li>
                        </ol>
                    </p>
                    <h5>Postanowienia wstępne</h5>
                    <p>
                        Za pośrednictwem Sklepu, Sprzedawca prowadzi wypożyczenia Produktów. <br />
                        Za pośrednictwem Sklepu, Klient może dokonać wypożyczenia Produktów uwidocznionych na stronie
                        głównej j i podstronach Sklepu.<br />
                        Regulamin określa zasady i warunki korzystania ze Sklepu, a także prawa i obowiązki Sprzedawcy i Klientów.<br />
                        Przed rozpoczęciem korzystania ze Sklepu, Klient jest zobowiązany zapoznać się z Regulaminem,
                         a rozpoczęcie korzystania ze Sklepu jest równoznaczne z akceptacją wszystkich jego postanowień.<br />
                        Regulamin jest integralną częścią umowy sprzedaży zawieranej z Klientem.<br />
                        Dokonanie zakupu możliwe jest wyłącznie bez zakładania konta. W żądnym jednak przypadku,
                        Klient nie może dokonać zakupu w Sklepie anonimowo, ani pod pseudonimem.<br />
                        Umowy w Sklepie są zawierane w języku polskim.<br />
                        Wszystkie ceny Produktów podane na stronie głównej lub podstronach Sklepu są cenami brutto
                        (zawierają podatek VAT).<br />
                        Wiążąca dla stron transakcji jest cena widniejąca przy Produkcie w momencie złożenia zamówienia
                        przez Klienta zgodnie z procedurą opisaną w § 3 Regulaminu.<br />
                        Do korzystania ze Sklepu, w tym w szczególności do dokonania zakupu w Sklepie, nie jest konieczne
                        spełnienie szczególnych warunków technicznych przez komputer lub inne urządzenie Klienta. Wystarczające są:<br />
                        <ul>
                            <li>
                                dostęp do Internetu,
                            </li>
                            <li>
                                standardowy system operacyjny,
                            </li>
                            <li>
                                standardowa przeglądarka internetowa,
                            </li>
                            <li>
                                posiadanie aktywnego adresu e-mail.
                            </li>
                        </ul>
                    </p>
                    <h5>Składanie zamówienia</h5>
                    <p>
                        1. Zamówienia w Sklepie można składać całą dobę, we wszystkie dni roku.
                    </p>

                    <p>
                        2. Zawartość strony internetowej i podstron Sklepu stanowi zaproszenie do zawarcia umowy w rozumieniu
                        art. 71 Kodeksu cywilnego, a zatem samo złożenie zamówienia przez Klienta nie oznacza natychmiastowego
                        zawarcia umowy.
                    </p>
                    <p>
                        3. W celu złożenia zamówienia, Klient zobowiązany jest podjąć następujące kroki:
                        <ol type="a">
                            <li>
                                wybrać Produkt lub Produkty będące przedmiotem zamówienia poprzez kliknięcie przycisku
                                „Dodaj do koszyka”,
                            </li>
                            <li>
                                w rozwiniętym module „Twój koszyk” kliknąć w przycisk „Przejdź do zamówienia” u dołu strony,
                            </li>
                            <li>
                                wpisać dane odbiorcy zamówienia,
                            </li>
                            <li>
                                w przypadku chęci otrzymania faktury, należy wypełnić pole „Nazwa firmy”, a w polu „Uwagi do
                                 zamówienia” wpisać NIP firmy.
                            </li>
                            <li>
                                wybrać formę płatności za zamówione Produkty,
                            </li>
                            <li>
                                zapoznać się z regulaminem i zaznaczyć checkbox “Przeczytałam/em i akceptuję regulamin*”
                            </li>
                            <li>
                                kliknąć przycisk „Kupuję i płacę”.
                            </li>
                        </ol>
                    </p>
                    <p>
                        4. W formularzu zamówienia Klient jest zobowiązany podać prawdziwe dane osobowe. Klient ponosi
                        odpowiedzialność za podanie nieprawdziwych danych osobowych. Sprzedawca zastrzega sobie prawo do
                        wstrzymania realizacji zamówienia w sytuacji, gdy Klient podał nieprawdziwe dane lub gdy dane te
                        budzą uzasadnione wątpliwości Sprzedawcy co do ich poprawności. W takim przypadku Klient zostanie
                        poinformowany telefonicznie lub poprzez pocztę elektroniczną o wątpliwościach Sprzedawcy. W takiej
                        sytuacji Klient przysługuje prawo wyjaśnienia wszelkich okoliczności związanych z weryfikacją prawdziwości
                        podanych danych. W przypadku braku danych pozwalających Sprzedawcy na podjęcie kontaktu z Klientem,
                         Sprzedawca udzieli wszelkich wyjaśnień po podjęciu kontaktu przez Klienta.
                    </p>
                    <p>
                        5. Klient oświadcza, że wszelkie dane podane przez niego w formularzu zamówienia są prawdziwe,
                        natomiast Sprzedawca nie jest zobowiązany do weryfikowania ich prawdziwości i poprawności,
                        choć posiada takie uprawnienie zgodnie z ust. 4 powyżej.
                    </p>
                    <p>
                        6. Z chwilą kliknięcia przycisku „Kupuję i płacę” zgodnie z ust. 3 lit. g) powyżej,
                        Klient składa Sprzedawcy ofertę zawarcia umowy sprzedaży produktów objętych zamówieniem.
                    </p>
                    <p>
                        7. Po kliknięciu przycisku „Kupuję i płacę” zgodnie z ust. 3 lit. g) powyżej, Klient zostanie
                        przeniesiony na podstronę z podsumowaniem zamówienia.
                    </p>
                    <p>
                        8.  Czas realizacji zamówienia jest liczony od momentu uzyskania pozytywnej autoryzacji płatności.
                    </p>
                    <h5>Dostawa i płatność</h5>
                    <p>
                        1. Dostępne metody płatności za zamówienie opisane są na stronach Sklepu oraz prezentowane są
                        Klientowi na etapie składania zamówienia.
                    </p>
                    <p>
                        2. Dostępne metody dostawy Produktów opisane są na stronach Sklepu oraz prezentowane są
                        Klientowi na etapie składania zamówienia. Koszt dostawy zamówienia ponosi Klient,
                        chyba że Sprzedawca na stronie Sklepu wskaże inaczej.
                    </p>
                    <p>
                        3.Dostępne formy płatności w Sklepie:
                        <ul>
                            <li>
                                – przelew tradycyjny,
                            </li>
                            <li>
                                – płatność za pobraniem,
                            </li>
                            <li>
                                – automatyczne płatności online,
                            </li>
                            <li>
                                – Karty płatnicze:
                            </li>
                            <ul>
                                <li>
                                    * Visa,
                                </li>
                                <li>
                                    * Visa Electron,
                                </li>
                                <li>
                                    * Mastercard,
                                </li>
                                <li>
                                    * MasterCard Electronic,
                                </li>
                                <li>
                                    * Maestro.
                                </li>
                            </ul>
                        </ul>

                    </p>
                    <p>
                        4. Realizacja zamówienia obejmującego produkty fizyczne następuje poprzez wysyłkę zakupionych
                        produktów na adres podany przez Klienta.
                    </p>
                    <p>
                        5. Przesyłka powinna zostać dostarczona do skrytki w określonym dniu.
                    </p>
                    <p>
                        6. W sytuacji gdy Sprzedawca nie jest w stanie zrealizować całego zamówienia, Klient ma prawo wyboru,
                        czy zrealizuje zamówienie tylko w części, czy rezygnuje z zamówienia.
                    </p>
                    <p>
                        7. Podmiotem świadczącym obsługę płatności online jest Blue Media S.A.
                    </p>
                    <p>
                        8. W przypadku wystąpienia konieczności zwrotu środków za transakcję dokonaną przez klienta kartą
                        płatniczą sprzedający dokonana zwrotu na rachunek bankowy przypisany do karty płatniczej Zamawiającego.
                    </p>
                    <p>
                        9. W przypadku wyboru przez Klienta sposobu płatności przelewem, płatności elektroniczne lub kartą
                        płatniczą – od dnia uznania rachunku bankowego Sprzedawcy.
                    </p>
                    <h5>Odstąpienie od umowy Konsumenta</h5>
                    <p>
                        1. Aby odstąpić od umowy, Klient musi poinformować Sprzedawcę o swojej decyzji o odstąpieniu od umowy
                        w drodze jednoznacznego oświadczenia.
                    </p>
                    <p>
                        2. Aby zachować termin do odstąpienia od umowy, wystarczy, aby Klient wysłał informację dotyczącą wykonania
                         przysługującemu Klientowi prawa odstąpienia od umowy przed upływem terminu do odstąpienia od umowy.
                    </p>
                    <p>
                        3. Klient ponosi odpowiedzialność za zmniejszenie wartości produktu będące wynikiem korzystania z produkty
                        w sposób wykraczający poza konieczny do stwierdzenia charakteru, cech i funkcjonowania produktu.
                    </p>
                    <p>
                        4. W przypadku wystąpienia konieczności zwrotu środków za transakcję dokonaną przez klienta kartą płatniczą
                        sprzedający dokonana zwrotu na rachunek bankowy przypisany do karty płatniczej Zamawiającego
                    </p>
                    <h5>Rękojmia Sprzedawcy</h5>
                    <p>
                        1. Sprzedawca ma obowiązek dostarczyć Kupującemu produkt wolny od wad.
                    </p>
                    <p>
                        2. Sprzedawca jest odpowiedzialny względem Klienta, jeżeli sprzedany Produkt ma wadę fizyczną lub
                        prawną (rękojmia).
                    </p>
                    <p>
                        3. W razie stwierdzenia wady Produktu, Klient powinien wysłać zawiadomienie (oświadczenie) o stwierdzonej wadzie.
                        Klient zobowiązany jest złożyć reklamację opis stwierdzonej wady oraz, jeśli to możliwe, załączyć zdjęcie.
                    </p>
                    <p>
                        4. Jeżeli Produkt ma wadę, Klient powinien poinformować o tym Sprzedawcę, określając jednocześnie swoje
                        roszczenie związane ze stwierdzoną wadą lub składając oświadczenie stosownej treści.
                    </p>
                    <p>
                        5. Sprzedawca ustosunkuje się do składanej przez Klienta reklamacji w ciągu 14 dni od dnia doręczenia mu
                        reklamacji za pomocą takiego środka komunikacji, przy wykorzystaniu którego reklamacja została złożona.
                    </p>
                    <h5>Dane osobowe i pliki cookies</h5>
                    <p>
                        1. Sprzedawca przetwarza dane osobowe Klientów w celu realizacji zamówienia.
                    </p>
                    <p>
                        3. Sprzedawca gwarantuje poufność wszelkich udostępnionych mu danych osobowych.
                    </p>
                    <p>
                        4. Dane osobowe są gromadzone z należytą starannością i odpowiednio chronione przed dostępem do nich przez
                        osoby do tego nieupoważnione.
                    </p>
                    <p>
                        5. Klientowi przysługuje prawo wglądu do swoich danych osobowych, żądania uzupełnienia, uaktualnienia,
                        sprostowania danych osobowych, czasowego lub stałego wstrzymania ich przetwarzania lub ich usunięcia,
                        jeżeli są one niekompletne, nieaktualne, nieprawdziwe lub zostały zebrane z naruszeniem przepisów prawa
                        albo są już zbędne do realizacji celu, dla którego zostały zebrane.
                    </p>
                    <p>
                        6. Uprawnienia. RODO przyznaje Ci następujące potencjalne uprawnienia związane z przetwarzaniem Twoich
                        danych osobowych:
                        <ol type="1">
                            <li>
                                prawo dostępu do danych osobowych,
                            </li>
                            <li>
                                prawo do sprostowania danych osobowych,
                            </li>
                            <li>
                                prawo do usunięcia danych osobowych,
                            </li>
                            <li>
                                prawo do ograniczenia przetwarzania danych osobowych,
                            </li>
                            <li>
                                prawo do wniesienia sprzeciwu co do przetwarzania danych osobowych,
                            </li>
                            <li>
                                prawo do przenoszenia danych,
                            </li>
                            <li>
                                prawo do wniesienia skargi do organu nadzorczego,
                            </li>
                            <li>
                                prawo do odwołania zgody na przetwarzanie danych osobowych, jeżeli takową zgodę wyraziłeś.
                            </li>
                        </ol>

                        Zasady związane z realizacją wskazanych uprawnień zostały opisane szczegółowo w art. 16 – 21 RODO.
                        Zachęcamy do zapoznania się z tymi przepisami. Ze swojej strony uważamy za potrzebne wyjaśnić Ci,
                        że wskazane powyżej uprawnienia nie są bezwzględne i nie będą przysługiwać Ci w stosunku do wszystkich
                        czynności przetwarzania Twoich danych osobowych. Dla Twojej wygody dołożyliśmy starań, by w ramach opisu
                        poszczególnych operacji przetwarzania danych osobowych wskazać na przysługujące Ci w ramach tych operacji
                        uprawnienia. <br />

                        Podkreślamy, że jedno z uprawnień wskazanych powyżej przysługuje Ci zawsze – jeżeli uznasz, że przy
                        przetwarzaniu Twoich danych osobowych dopuściliśmy się naruszenia przepisów o ochronie danych osobowych,
                        masz możliwość wniesienia skargi do organu nadzorczego (Prezesa Urzędu Ochrony Danych Osobowych).<br />

                        Zawsze możesz również zwrócić się do nas z żądaniem udostępnienia Ci informacji o tym, jakie dane na Twój
                        temat posiadamy oraz w jakich celach je przetwarzamy. Dołożyliśmy jednak wszelkich starań, by interesujące
                        Cię informacje zostały wyczerpująco przedstawione w niniejszej polityce prywatności.
                    </p>
                    <p>
                        7. Bezpieczeństwo. Gwarantujemy Ci poufność wszelkich przekazanych nam danych osobowych.
                        Zapewniamy podjęcie wszelkich środków bezpieczeństwa i ochrony danych osobowych wymaganych
                        przez przepisy o ochronie danych osobowych. Dane osobowe są gromadzone z należytą starannością
                        i odpowiednio chronione przed dostępem do nich przez osoby do tego nieupoważnione.
                    </p>
                    <p>
                        8. Konto użytkownika. Zakładając konto użytkownika, musisz podać dane niezbędne do założenia konta,
                        takie jak adres e-mail, imię i nazwisko, dane adresowe, numer telefonu. Podanie danych jest dobrowolne,
                        ale niezbędne do założenia konta. W ramach edycji danych konta możesz podać swoje dalej idące dane.<br />

                        Dane przekazane nam w związku z założeniem konta, przetwarzane są w celu założenia i utrzymywania konta
                        na podstawie umowy o świadczenie usług drogą elektroniczną zawieranej poprzez rejestrację konta (art. 6
                        ust. 1 lit. b RODO).<br />

                        Dane zawarte w koncie będą przetwarzane przez czas funkcjonowania konta. Gdy zdecydujesz się usunąć konto,
                        usuniemy również dane w nim zawarte. Pamiętaj jednak, że usunięcie konto nie prowadzi do usunięcia informacji
                        o zamówieniach złożonych przez Ciebie z wykorzystaniem konta.<br />

                        W każdej chwili masz możliwości sprostowania danych zawartych w koncie. W każdej chwili możesz również
                        podjąć decyzję o usunięciu konta. Przysługuje Ci również prawo do przenoszenia danych, o którym mowa
                        w art. 20 RODO.<br />

                        Zamówienia. Składając zamówienie, musisz podać dane niezbędne do realizacji zamówienia, takie jak imię
                        i nazwisko, adres rozliczeniowy, adres e-mail, numer telefonu. Podanie danych jest dobrowolne, ale niezbędne
                        do złożenia zamówienia.<br />

                        Dane przekazane nam w związku z zamówieniem, przetwarzane są w celu realizacji zamówienia (art. 6 ust.
                        1 lit. b RODO), wystawienia faktury (art. 6 ust. 1 lit. c RODO), uwzględnienia faktury w naszej dokumentacji
                        księgowej (art. 6 ust. 1 lit. c RODO) oraz w celach archiwalnych i statystycznych (art. 6 ust. 1 lit. f RODO).<br />

                        Dane o zamówieniach będą przetwarzane przez czas niezbędny do realizacji zamówienia, a następnie do czasu
                        upływu terminu przedawnienia roszczeń z tytułu zawartej umowy. Ponadto, po upływie tego terminu, dane nadal
                        mogą być przez nas przetwarzane w celach statystycznych. Pamiętaj również, że mamy obowiązek przechowywać
                        faktury z Twoimi danymi osobowymi przez okres 5 lat od końca roku podatkowego, w którym powstał obowiązek
                        podatkowy.<br />

                        W przypadku danych o zamówieniach nie masz możliwości sprostowania tych danych po realizacji zamówienia.
                        Nie możesz również sprzeciwić się przetwarzaniu danych oraz domagać się usunięcia danych do czasu upływu
                        terminu przedawnienia roszczeń z tytułu zawartej umowy. Podobnie, nie możesz sprzeciwić się przetwarzaniu
                        danych oraz domagać się usunięcia danych zawartych w fakturach. Po upływie terminu przedawnienia roszczeń
                        z tytułu zawartej umowy możesz jednać sprzeciwić się przetwarzaniu przez nas Twoich danych w celach
                        statystycznych, jak również domagać się usunięcia Twoich danych z naszej bazy.<br />

                        W stosunku do danych o zamówieniach przysługuje Ci również prawo do przenoszenia danych, o którym mowa
                        w art. 20 RODO.<br />
                    </p>
                    <p>
                        9. Reklamacje i odstąpienie od umowy. Jeżeli składasz reklamację lub odstępujesz od umowy, to przekazujesz
                        nam dane osobowe zawarte w treści reklamacji lub oświadczeniu o odstąpieniu od umowy, które obejmuję imię
                        i nazwisko, adres zamieszkania, numer telefonu, adres e-mail, numer rachunku bankowego. Podanie danych jest
                        dobrowolne, ale niezbędne, by złożyć reklamację lub odstąpić od umowy.<br />

                        Dane przekazane nam w związku ze złożeniem reklamacji lub odstąpieniem od umowy wykorzystywane są w celu
                        realizacji procedury reklamacyjnej lub procedury odstąpienia od umowy (art. 6 ust. 1 lit. c RODO).<br />

                        Dane będą przetwarzane przez czas niezbędny do realizacji procedury reklamacyjnej lub procedury odstąpienia.
                        Reklamacje oraz oświadczenia o odstąpieniu od umowy mogą być ponadto archiwizowane w celach statystycznych.<br />

                        W przypadku danych zawartych w reklamacjach oraz oświadczeniach o odstąpieniu od umowy nie masz możliwości
                        sprostowania tych danych. Nie możesz również sprzeciwić się przetwarzaniu danych oraz domagać się usunięcia
                        danych do czasu upływu terminu przedawnienia roszczeń z tytułu zawartej umowy. Po upływie terminu przedawnienia
                        roszczeń z tytułu zawartej umowy możesz jednak sprzeciwić się przetwarzaniu przez nas Twoich danych w celach
                        statystycznych, jak również domagać się usunięcia Twoich danych z naszej bazy.<br />
                    </p>
                    <p>
                        10. Kontakt e-mailowy. Kontaktując się z nami za pośrednictwem poczty elektronicznej, w tym również
                        przesyłając zapytanie poprzez formularz kontaktowy, w sposób naturalny przekazujesz nam swój adres e-mail
                        jako adres nadawcy wiadomości. Ponadto, w treści wiadomości możesz zawrzeć również inne dane osobowe.
                        Podanie danych jest dobrowolne, ale niezbędne, by nawiązać kontakt.<br />

                        Twoje dane są w tym przypadku przetwarzane w celu kontaktu z Tobą, a podstawą przetwarzania jest art.
                        6 ust. 1 lit. a RODO, czyli Twoja zgoda wynikające z zainicjowania z nami kontaktu. Podstawą prawną
                        przetwarzania po zakończeniu kontaktu jest usprawiedliwiony cel w postaci archiwizacji korespondencji
                        na potrzeby wewnętrzne (art. 6 ust. 1 lit. c RODO).<br />

                        Treść korespondencji może podlegać archiwizacji i nie jesteśmy w stanie jednoznacznie określić, kiedy
                        zostanie usunięta. Masz prawo do domagania się przedstawienia historii korespondencji, jaką z nami
                        prowadziłeś (jeżeli podlegała archiwizacji), jak również domagać się jej usunięcia, chyba że jej archiwizacja
                        jest uzasadniona z uwagi na nasze nadrzędne interesy, np. obrona przed potencjalnymi roszczeniami z Twojej strony.<br />
                    </p>


                    <h5>Kodeks dobrych praktyk</h5>
                    <p>
                        1. Sprzedawca nie stosuje kodeksu dobrych praktyk, o którym mowa w ustawie z dnia 23 sierpnia 2007 r.
                        o przeciwdziałaniu nieuczciwym praktykom rynkowym.
                    </p>
                    <h5>Pozasądowe sposoby rozpatrywania reklamacji i dochodzenia roszczeń</h5>
                    <p>
                        1. Sprzedawca wyraża zgodę na poddanie ewentualnych sporów wynikłych w związku ze sprzedażą Produktów na
                        drodze postępowania mediacyjnego. Szczegóły zostaną określone przez strony konfliktu.
                    </p>
                    <p>
                        2. Klient będący Konsumentem ma możliwość skorzystania z pozasądowych sposobów rozpatrywania reklamacji
                        i dochodzenia roszczeń. Między innymi, Klient będący Konsumentem, ma możliwość :
                        <ol type="a">
                            <li>
                                zwrócenia się do stałego polubownego sądu konsumenckiego z wnioskiem o rozstrzygnięcie sporu
                                wynikłego z zawartej Umowy Sprzedaży,
                            </li>
                            <li>
                                zwrócenia się do wojewódzkiego inspektora Inspekcji Handlowej z wnioskiem o wszczęcie postępowania
                                mediacyjnego w sprawie polubownego zakończenia sporu między Klientem, a Sprzedawcą,
                            </li>
                            <li>
                                skorzystania z pomocy powiatowego (miejskiego) rzecznika praw konsumenta lub organizacji społecznej,
                                do której statutowych zadań należy ochrona konsumentów.
                            </li>
                        </ol>
                    </p>
                    <p>
                        3. Bardziej szczegółowych informacji na temat pozasądowych sposobów rozpatrywania reklamacji
                        i dochodzenia roszczeń, Klient, będący Konsumentem, może szukać na stronie internetowej
                        https://www.uokik.gov.pl.
                    </p>
                    <h5>Postanowienia końcowe</h5>
                    <p>
                        1. Regulamin jest częścią zawieranej przez Sprzedawcę oraz Klienta umowy sprzedaży.
                    </p>
                    <p>
                        2. Regulamin wchodzi w życie z dniem publikacji na stronie internetowej Sklepu.
                    </p>
                    <p>
                        3. Sprzedawca zastrzega sobie możliwość zmian w Regulaminie, które wchodzą w życie z dniem ich
                        publikacji na stronie internetowej Sklepu. Do umów zawartych przed zmianą Regulaminu stosuje się
                        wersję Regulaminu obowiązującą w dacie złożenia zamówienia przez Klienta. Nowa treść Regulaminu
                        obowiązywać będzie Klienta po zaakceptowaniu przez niego zmian, o których został powiadomiony.
                    </p>
                </div>
            </div >
        );
    }

}
export default Regulations
