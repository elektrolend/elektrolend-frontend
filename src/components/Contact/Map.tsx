import React, { useState } from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from "./Marker";

const Map = () => {

    const [center] = useState({lat: 3.93, lng: 72.95 });
    const [zoom] = useState(11);

    return (
        <div style={{ height: '50vh', width: '100%' }}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyDdB8VPFtoJrkBINLlio6xUEz327LbL0vQ' }}
                defaultCenter={center}
                defaultZoom={zoom}
            >
                <Marker
                    lat={11.0168}
                    lng={76.9558}
                    name="My Marker"
                    color="blue"
                />
            </GoogleMapReact>
        </div>
    );
}

export default Map;
