import React, {Component} from 'react';
import {init, send} from '@emailjs/browser';

import Map from './Map';
import mailCredentials from "./mailCredentials";

init(mailCredentials.USER_ID);

interface MainProps {
    name: string,
    email: string,
    message: string,
    status: number,
    errorMessage: string
}

class Contact extends Component<MainProps, any> {
    constructor(props: MainProps | Readonly<MainProps>) {
        super(props);
        this.state = {
            name: '',
            email: '',
            message: '',
            status: 0,
            errorMessage: ''
        }
    }

    render() {
        return (
            <div className="container mt-2">
                <div className="h2 text-primary"> Kontakt</div>

                <div className="container mt-5">
                    <div className="row">
                        <div className="col-sm-4">

                            <div className="col-sm">

                                <h5>Adres</h5>
                                <p>
                                    Wypożyczalnia Elektrolend <br/>
                                    ul. Wymyślona 1 <br/>
                                    12-345 Macondo
                                </p>

                                <h5>Kontakt</h5>
                                <p>
                                    <strong>Nr tel.:</strong> +48 609 120 685 <br/>
                                    <strong>E-mail:</strong> kontakt@elektrolend.pl
                                </p>

                                <h5>Dane firmowe</h5>
                                <p>
                                    Wypożyczalnia Elektrolend <br/>
                                    NIP: 000 000 00 00 <br/>
                                    Nr konta: 00 0000 0000 0000 0000
                                </p>
                            </div>

                        </div>

                        <div className="col-sm-8">
                            <Map/>
                        </div>
                    </div>

                    <div className="d-flex justify-content-center">
                        <div className="mt-4 w-75">
                            {this.state.status === 200 ?
                                <div className="alert alert-success">
                                    Wiadomość wysłana.
                                </div> : null
                            }

                            {this.state.errorMessage ?
                                <div className="alert alert-danger">
                                    {this.state.errorMessage}
                                </div> : null
                            }
                            <form onSubmit={this.handleSubmit.bind(this)} method="POST">
                                <h1 className="h3 mb-3 mb-2 font-weight-normal">Formularz kontaktowy</h1>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm p-0 pl-1 pr-1">
                                            <input
                                                name="name"
                                                className="form-control mt-1 bg-light"
                                                type="text"
                                                placeholder="imię"
                                                value={this.state.name}
                                                onChange={this.onNameChange.bind(this)}
                                                required
                                            />
                                        </div>
                                        <div className="col-sm p-0 pl-1 pr-1">
                                            <input
                                                name="email"
                                                className="form-control mt-1 bg-light"
                                                type="email"
                                                placeholder="email"
                                                value={this.state.email}
                                                onChange={this.onEmailChange.bind(this)}
                                                required
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="pr-1 pl-1">
                                    <textarea
                                        name="message"
                                        className="form-control bg-light mt-4"
                                        placeholder="wiadomość"
                                        value={this.state.message}
                                        onChange={this.onMessageChange.bind(this)}
                                        required
                                    />
                                </div>
                                <div className="d-flex justify-content-end pr-1">
                                    <button
                                        className="w-50 btn btn-primary btn-block mt-3"
                                        type="submit"
                                    >
                                        Wyślij
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div className="container mt-5 mb-5 text-center">
                        <div className="row">
                            <div className="col-sm">
                                <svg
                                    id="_x31__x2C_5"
                                    enableBackground="new 0 0 24 24"
                                    height="100"
                                    width="100"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="m18.75 24h-13.5c-1.24 0-2.25-1.009-2.25-2.25v-10.5c0-1.241 1.01-2.25 2.25-2.25h13.5c1.24 0 2.25 1.009 2.25 2.25v10.5c0 1.241-1.01 2.25-2.25 2.25zm-13.5-13.5c-.413 0-.75.336-.75.75v10.5c0 .414.337.75.75.75h13.5c.413 0 .75-.336.75-.75v-10.5c0-.414-.337-.75-.75-.75z"/>
                                    <path
                                        d="m17.25 10.5c-.414 0-.75-.336-.75-.75v-3.75c0-2.481-2.019-4.5-4.5-4.5s-4.5 2.019-4.5 4.5v3.75c0 .414-.336.75-.75.75s-.75-.336-.75-.75v-3.75c0-3.309 2.691-6 6-6s6 2.691 6 6v3.75c0 .414-.336.75-.75.75z"/>
                                    <path
                                        d="m12 17c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm0-2.5c-.275 0-.5.224-.5.5s.225.5.5.5.5-.224.5-.5-.225-.5-.5-.5z"/>
                                    <path
                                        d="m12 20c-.414 0-.75-.336-.75-.75v-2.75c0-.414.336-.75.75-.75s.75.336.75.75v2.75c0 .414-.336.75-.75.75z"/>
                                </svg>
                                <div className="mt-4">
                                    Największy wybór produktów
                                </div>
                            </div>
                            <div className="col-sm">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="100"
                                    height="100"
                                    viewBox="0 0 512 512"
                                >
                                    <g>
                                        <path d="M64,400H40a8,8,0,0,0,0,16H64a8,8,0,0,0,0-16Z"/>
                                        <path d="M96,400H88a8,8,0,0,0,0,16h8a8,8,0,0,0,0-16Z"/>
                                        <path
                                            d="M472,168H456V104a32.042,32.042,0,0,0-32-32H88a32.036,32.036,0,0,0-32,32V248H40A32.036,32.036,0,0,0,8,280V408a32.036,32.036,0,0,0,32,32h80a32.042,32.042,0,0,0,32-32V384h69.75l-4,16H192a8,8,0,0,0-8,8v24a8,8,0,0,0,8,8H472a32.042,32.042,0,0,0,32-32V200A32.042,32.042,0,0,0,472,168ZM136,408a16.021,16.021,0,0,1-16,16H40a16.021,16.021,0,0,1-16-16V280a16.021,16.021,0,0,1,16-16H50.23l6.18,18.53A8,8,0,0,0,64,288H96a8,8,0,0,0,7.59-5.47L109.77,264H120a16.021,16.021,0,0,1,16,16ZM67.1,264H92.9l-2.67,8H69.77ZM152,368V344H296v24Zm144,16v16h-1.75l-4-16Zm-18.25,16h-43.5l4-16h35.5ZM200,424v-8h97.01a31.971,31.971,0,0,0,3.3,8Zm96-224V328H152V280a32.042,32.042,0,0,0-32-32H72V104A16.021,16.021,0,0,1,88,88H424a16.021,16.021,0,0,1,16,16v64H328A32.042,32.042,0,0,0,296,200ZM488,408a16.021,16.021,0,0,1-16,16H328a16.021,16.021,0,0,1-16-16V200a16.021,16.021,0,0,1,16-16H472a16.021,16.021,0,0,1,16,16Z"/>
                                        <path
                                            d="M472,192H328a8,8,0,0,0-8,8V384a8,8,0,0,0,8,8H472a8,8,0,0,0,8-8V200A8,8,0,0,0,472,192Zm-8,184H336V208H464Z"/>
                                        <circle
                                            cx="392"
                                            cy="408"
                                            r="8"
                                        />
                                    </g>
                                </svg>
                                <div className="mt-4">
                                    Bezpieczne wypożyczenia
                                </div>
                            </div>
                            <div className="col-sm">
                                <svg
                                    viewBox="-29 0 487 487.71902"
                                    width="100"
                                    height="100"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="m220.867188 266.175781c-.902344-.195312-1.828126-.230469-2.742188-.09375-9.160156-1.066406-16.070312-8.816406-16.085938-18.035156 0-4.417969-3.582031-8-8-8-4.417968 0-8 3.582031-8 8 .023438 15.394531 10.320313 28.878906 25.164063 32.953125v8c0 4.417969 3.582031 8 8 8s8-3.582031 8-8v-7.515625c17.132813-3.585937 28.777344-19.542969 26.976563-36.953125-1.804688-17.410156-16.472657-30.640625-33.976563-30.644531-10.03125 0-18.164063-8.132813-18.164063-18.164063s8.132813-18.164062 18.164063-18.164062 18.164063 8.132812 18.164063 18.164062c0 4.417969 3.582031 8 8 8 4.417968 0 8-3.582031 8-8-.023438-16.164062-11.347657-30.105468-27.164063-33.441406v-7.28125c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v7.769531c-16.507813 4.507813-27.132813 20.535157-24.859375 37.496094s16.746094 29.621094 33.859375 29.617187c9.898437 0 17.972656 7.925782 18.152344 17.820313.183593 9.894531-7.597657 18.113281-17.488281 18.472656zm0 0"/>
                                    <path
                                        d="m104.195312 222.5c0 64.070312 51.9375 116.007812 116.007813 116.007812s116.007813-51.9375 116.007813-116.007812-51.9375-116.007812-116.007813-116.007812c-64.039063.070312-115.933594 51.96875-116.007813 116.007812zm116.007813-100.007812c55.234375 0 100.007813 44.773437 100.007813 100.007812s-44.773438 100.007812-100.007813 100.007812-100.007813-44.773437-100.007813-100.007812c.0625-55.207031 44.800782-99.945312 100.007813-100.007812zm0 0"/>
                                    <path
                                        d="m375.648438 358.230469-62.667969 29.609375c-8.652344-16.09375-25.25-26.335938-43.515625-26.851563l-57.851563-1.589843c-9.160156-.261719-18.148437-2.582032-26.292969-6.789063l-5.886718-3.050781c-30.140625-15.710938-66.066406-15.671875-96.175782.101562l.367188-13.335937c.121094-4.417969-3.359375-8.097657-7.777344-8.21875l-63.4375-1.746094c-4.417968-.121094-8.09375 3.359375-8.214844 7.777344l-3.832031 139.210937c-.121093 4.417969 3.359375 8.097656 7.777344 8.21875l63.4375 1.746094h.21875c4.335937 0 7.882813-3.449219 8-7.78125l.183594-6.660156 16.480469-8.824219c6.46875-3.480469 14.03125-4.308594 21.097656-2.308594l98.414062 27.621094c.171875.050781.34375.089844.519532.128906 7.113281 1.488281 14.363281 2.234375 21.628906 2.230469 15.390625.007812 30.601562-3.308594 44.589844-9.730469.34375-.15625.675781-.339843.992187-.546875l142.691406-92.296875c3.554688-2.300781 4.703125-6.96875 2.621094-10.65625-10.59375-18.796875-34.089844-25.957031-53.367187-16.257812zm-359.070313 107.5625 3.390625-123.21875 47.441406 1.304687-3.390625 123.222656zm258.925781-2.09375c-17.378906 7.84375-36.789062 10.007812-55.46875 6.191406l-98.148437-27.550781c-11.046875-3.121094-22.871094-1.828125-32.976563 3.605468l-8.421875 4.511719 2.253907-81.925781c26.6875-17.75 60.914062-19.574219 89.335937-4.765625l5.886719 3.050781c10.289062 5.3125 21.636718 8.242188 33.210937 8.578125l57.855469 1.589844c16.25.46875 30.050781 12.039063 33.347656 27.960937l-86.175781-2.378906c-4.417969-.121094-8.09375 3.363282-8.21875 7.777344-.121094 4.417969 3.363281 8.097656 7.777344 8.21875l95.101562 2.617188h.222657c4.332031-.003907 7.875-3.453126 7.992187-7.78125.097656-3.476563-.160156-6.957032-.773437-10.378907l64.277343-30.371093c.0625-.027344.125-.058594.1875-.089844 9.117188-4.613282 20.140625-3.070313 27.640625 3.871094zm0 0"/>
                                    <path
                                        d="m228.203125 84v-76c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v76c0 4.417969 3.582031 8 8 8s8-3.582031 8-8zm0 0"/>
                                    <path
                                        d="m288.203125 84v-36c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v36c0 4.417969 3.582031 8 8 8s8-3.582031 8-8zm0 0"/>
                                    <path
                                        d="m168.203125 84v-36c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v36c0 4.417969 3.582031 8 8 8s8-3.582031 8-8zm0 0"/>
                                </svg>
                                <div className="mt-4">
                                    Korzystne oferty cenowe
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onNameChange(event: any) {
        this.setState({name: event.target.value})
    }

    onEmailChange(event: any) {
        this.setState({email: event.target.value})
    }

    onMessageChange(event: any) {
        this.setState({message: event.target.value})
    }

    onStatusChange(newStatus: number) {
        this.setState({status: newStatus})
    }

    onErrorMessageChange(newErrorMessage: string) {
        this.setState({errorMessage: newErrorMessage})
    }

    handleSubmit(event: any) {
        event.preventDefault();
        this.onErrorMessageChange("");
        this.onStatusChange(0);
        const templateParams = {
            name: this.state.name,
            email: this.state.email,
            message: this.state.message
        };
        send(mailCredentials.SERVICE_ID, mailCredentials.TEMPLATE_ID, templateParams)
            .then(response => {
                this.onStatusChange(response.status);
            })
            .catch((error) => {
                this.onErrorMessageChange(error);
            });
    }
}

export default Contact
