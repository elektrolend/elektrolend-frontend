import {Component} from 'react';
import equipmentImageUrl from './equipment.jpg';

class AboutUs extends Component {

    render() {
        return (
            <div className="container mt-2 mb-4">
                <div className="h2 text-primary"> O nas</div>
                <div className="row container">
                    <div className="col-md-6">
                        <img
                            src={equipmentImageUrl}
                            alt="Sprzęt elektroniczny"
                            width="100%"
                        />
                    </div>
                    <div className="col-md-6 mt-2">
                        <div className="row text-center h5">
                            <div className="col-md-12 text-primary">
                                <p className="h1">
                                    ElektroLend
                                </p>
                                <p className="h4">
                                    Firma wypożyczająca sprzęt elektroniczny osobom prywatnym i firmom
                                </p>
                            </div>
                            <div className="col-md-12 p-4">
                                <p className="text-justify">
                                    Staramy się zapewnić Państwu największe bezpieczeństwo w czasach pandemii poprzez
                                    bezkontaktowy odbiór sprzętu oraz jego późniejszy zwrot. Proces odbioru i zwrotu
                                    odbywa
                                    się przy użyciu skrytek działających na takiej zasadzie jak skrytki inPost.
                                    Zamówienia
                                    składają Państwo poprzez stronę internetową dokonując rezerwacji i opłaty za
                                    zamówienie.
                                    Mają Państwo do dyspozycji dwie możliwości zabezpieczenia się przed ewentualnymi
                                    uszkodzeniami: ubezpieczenie lub kaucja. Serdecznie zachęcamy do skorzystania z
                                    naszych
                                    usług.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default AboutUs
