import React, {useEffect} from "react";
import {useAppSelector} from "../../../app/hooks";
import {useDispatch} from "react-redux";
import {Redirect} from "react-router-dom";
import {Field, Form, Formik} from "formik";

import CodeInput from "./CodeInput";
import LoginFooter from "./LoginFooter";
import ErrorAlert from "./ErrorAlert";
import LoginButton from "./LoginButton";
import {signIn} from "../../../services/auth.service";
import {logOut} from "../AuthSlice";
import {ISignInData} from "../../../interfaces/Accounts/ISignInData";
import LoginFields from "./LoginFields";

import "../Auth.css";

const Login = () => {
    const isLogged = useAppSelector(state => state.auth.isLogged);
    const errorMessage = useAppSelector(state => state.auth.errorMessage);
    const loginStatus = useAppSelector(state => state.auth.loginStatus);
    const dispatch = useDispatch();

    const inactiveAccount = () => {
        return loginStatus === 406 || loginStatus === 409;
    };

    const onSubmit = (signInData: ISignInData) => {
        if (!signInData.registrationCode) {
            signInData.registrationCode = null;
        }
        signIn(dispatch, signInData);
    };

    useEffect(() => {
        if (!isLogged) {
            dispatch(logOut());
        }
    }, [dispatch, isLogged]);

    if (!isLogged) {
        return (
            <Formik
                initialValues={{
                    nickname: '',
                    password: '',
                    registrationCode: ''
                }}
                onSubmit={values => {
                    onSubmit(values);
                }}>
                {() => (
                    <Form
                        className="container mt-4 mb-4"
                        style={{width: "400px"}}
                    >
                        <div className="border">
                            <div className="bg-primary text-white p-1 m-2">
                                <h4 className="mt-2 ml-2">Logowanie</h4>
                            </div>
                            <div className="bg-light p-2 m-2">
                                <div className="row">
                                    {LoginFields.map((loginField, index) => {
                                        return (
                                            <div
                                                className={loginField.cName}
                                                key={"loginField_" + index}
                                            >
                                                <Field
                                                    name={loginField.name}
                                                    className="form-control"
                                                    style={{height: "45px"}}
                                                    disabled={inactiveAccount()}
                                                    placeholder={loginField.placeHolder}
                                                    type={loginField.type}
                                                />
                                            </div>
                                        );
                                    })}
                                    <CodeInput show={inactiveAccount()}/>
                                </div>
                                <ErrorAlert errorMessage={errorMessage}/>
                                <LoginButton/>
                                <LoginFooter/>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        );
    } else {
        return (
            <Redirect to={"/home"}/>
        );
    }
};

export default Login;
