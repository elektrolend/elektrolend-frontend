interface LoginField {
    name: string;
    placeHolder: string;
    type: string;
    cName: string;
}

const LoginFields: LoginField[] = [
    {
        name: "nickname",
        placeHolder: "login",
        type: "text",
        cName: "col-12"
    },
    {
        name: "password",
        placeHolder: "hasło",
        type: "password",
        cName: "col-12 mt-3"
    }
]

export default LoginFields;
