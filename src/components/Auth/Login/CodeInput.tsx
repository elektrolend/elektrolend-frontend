import React from "react";
import {Field} from "formik";

type Props = {
    show: boolean;
};

const CodeInput: React.FC<Props> = ({show}) => {
    if (show) {
        return (
            <div className="col-12 mt-3">
                <div className="form-group">
                    <Field
                        name="registrationCode"
                        className="form-control"
                        style={{height: "45px"}}
                        type="text"
                        placeholder="kod aktywacyjny"
                    />
                </div>
            </div>
        );
    }

    return null;
};

export default CodeInput;
