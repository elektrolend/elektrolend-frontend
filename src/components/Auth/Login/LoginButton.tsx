import React from "react";

const LoginButton = () => {
    return (
        <button
            className="btn btn-lg btn-primary btn-block mt-3 "
            type="submit"
        >
            Zaloguj się
        </button>
    );
};

export default LoginButton;
