import React from "react";

const LoginFooter = () => {
    return (
        <div className="text-lg-center h6">
            <hr></hr>
            <p>
                Nie masz konta? <a
                className="hover-move"
                href="/register">
                Zarejestruj się!
            </a>
            </p>
            <p> Zapomniałeś hasła? <a
                className="hover-move"
                href="/restore-password">
                Odzyskaj je!
            </a>
            </p>
        </div>
    );
};

export default LoginFooter;
