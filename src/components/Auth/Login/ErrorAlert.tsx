import React from "react";

type Props = {
    errorMessage: string;
};

const ErrorAlert: React.FC<Props> = ({errorMessage}) => {
    if (errorMessage) {
        return (
            <div
                className="alert alert-danger mt-2"
                role="alert">
                {errorMessage}
            </div>
        );
    }
    return null;
};

export default ErrorAlert;
