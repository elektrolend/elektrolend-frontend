import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {AccountType} from "../../interfaces/Accounts/AccountType";
import {ILoginErrorData} from "../../interfaces/Accounts/ILoginErrorData";
import {IAuthCredentials} from "../../interfaces/Accounts/IAuthCredentials";

interface UserState {
    userID: number;
    username: string;
    authToken: string;
    role: AccountType;
    isLogged: boolean;
    errorMessage: string;
    loginStatus: number;
    registerStatus: number;
    registerErrorMessage: string;
}

const initialState: UserState = {
    userID: -1,
    username: '',
    authToken: '',
    role: AccountType.none,
    isLogged: false,
    errorMessage: '',
    loginStatus: 0,
    registerStatus: 0,
    registerErrorMessage: ''
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        loginSuccess: (state, action: PayloadAction<IAuthCredentials>) => {
            state.userID = action.payload.id;
            state.username = action.payload.nickname;
            state.authToken = action.payload.authToken;
            state.role = action.payload.accountType;
            state.isLogged = true;
            state.loginStatus = 0;
        },
        loginFail: (state, action: PayloadAction<ILoginErrorData>) => {
            state.errorMessage = action.payload.errorMessage;
            state.loginStatus = action.payload.status;
        },
        logOut: state => {
            state.userID = -1;
            state.username = '';
            state.authToken = '';
            state.role = AccountType.none;
            state.isLogged = false;
            state.errorMessage = '';
            state.loginStatus = 0;
        },
        registerInit: (state) => {
            state.registerErrorMessage = '';
            state.registerStatus = 0;
        },
        registerStart: (state) => {
            state.registerErrorMessage = '';
            state.registerStatus = 100;
        },
        registerSuccess: (state, action: PayloadAction<number>) => {
            state.registerStatus = 201;
        },
        registerFail: (state, action: PayloadAction<string>) => {
            state.registerErrorMessage = action.payload;
            state.registerStatus = 0;
        }
    }
})

export const {
    loginSuccess,
    loginFail,
    logOut,
    registerSuccess,
    registerFail,
    registerInit,
    registerStart
} = authSlice.actions
