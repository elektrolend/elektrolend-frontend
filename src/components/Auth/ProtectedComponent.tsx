import React from "react";
import {useAppSelector} from "../../app/hooks";

import {AccountType} from "../../interfaces/Accounts/AccountType";

type Props = {
    component: JSX.Element;
    roles: AccountType[];
}

const ProtectedComponent: React.FC<Props> = ({component, roles}) => {
    const role = useAppSelector(state => state.auth.role);

    if (roles && roles.indexOf(role) === -1) {
        return null;
    }

    return component;
};

export default ProtectedComponent;
