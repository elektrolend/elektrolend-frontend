import * as Yup from "yup";

const BasicInfoValidationSchema = Yup.object().shape({
    email: Yup.string()
        .email('Nieprawidłowy e-mail!')
        .required('E-mail jest wymagany!'),
    name: Yup.string()
        .required('Imię jest wymagane!')
        .matches(/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]+$/, 'Przykład: Kasia'),
    surname: Yup.string()
        .required('Nazwisko jest wymagane!')
        .matches(/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*(-[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćńółęąś]*)*$/, 'Przykład: Kowalska-Nowak'),
    phone: Yup.string()
        .required('Telefon jest wymagany!')
        .matches(/^[1-9]\d{8}/, 'Telefon składa się z 9 cyfr!')
});

export default BasicInfoValidationSchema;
