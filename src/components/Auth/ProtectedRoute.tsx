import * as React from 'react';
import {Route, RouteProps} from 'react-router-dom';
import {useAppSelector} from "../../app/hooks";

import AuthError from "../Utils/AuthError";
import {AccountType} from "../../interfaces/Accounts/AccountType";

interface PrivateRouteProps extends RouteProps {
    component: any;
    roles: AccountType[];
    componentProps?: any;
}

const ProtectedRoute = (props: PrivateRouteProps) => {
    const {component: Component, roles, componentProps, ...rest} = props;
    const role = useAppSelector(state => state.auth.role);

    const isAuthenticated = roles && roles.indexOf(role) !== -1;

    return (
        <Route
            {...rest}
            render={(routeProps) =>
                isAuthenticated ? (
                    <Component {...routeProps} {...componentProps} />
                ) : (
                    <AuthError/>
                )
            }
        />
    );
};

export default ProtectedRoute;
