import * as Yup from "yup";

const LoginValidationSchema = Yup.object().shape({
    nickname: Yup.string()
        .min(5, 'Login musi mieć conajmniej 5 znaków!')
        .max(15, 'Login może mieć maksymalnie 15 znaków!!')
        .required('Login jest wymagany!')
});

export default LoginValidationSchema;
