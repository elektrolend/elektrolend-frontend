import React from "react";
import {ErrorMessage, Field} from "formik";
import {useQuery} from "react-query";

import {findCountryId, getCountries} from "../../../../services/database.service";
import {ICountry} from "../../../../interfaces/Accounts/ICountry";

type Props = {
    setCountryID: (countryID: number) => void;
    setCountryMarked: (countryMarked: boolean) => void;
    setFieldValue: (field: string, value: any, shouldValidate?: (boolean | undefined)) => void;
    setFieldTouched: (field: string, isTouched?: (boolean | undefined), shouldValidate?: (boolean | undefined)) => void;
};

const CountrySelect: React.FC<Props> = ({setCountryID, setCountryMarked, setFieldValue, setFieldTouched}) => {
    const {data: countries, isLoading: countriesLoading, error: countriesError} = useQuery<ICountry[]>(
        'countries',
        getCountries
    );

    const handleChangeCountryId = (e: any) => {
        const foundCountryId = findCountryId(e.target.value, countries);
        if (foundCountryId !== 200) {
            setCountryID(foundCountryId);
            setCountryMarked(true);
        }
    };

    if (countriesLoading || countriesError) {
        return <div className="container"> Ładowanie ...</div>;
    }

    return (
        <div className="col-md-6 form-group">
            <label className="form-label">Kraj</label>
            <Field
                as="select"
                className="form-control"
                name="country"
                onChange={(e: any) => {
                    handleChangeCountryId(e);
                    setFieldValue("country", e.target.value);
                    setFieldValue("state", '');
                    setFieldTouched("state", false);
                }}>
                <option value=""></option>
                {countries?.map((country) => {
                    return (
                        <option
                            key={country.id}
                            value={country.name}>
                            {country.name}
                        </option>
                    )
                })}
            </Field>
            <ErrorMessage
                render={msg => <div className="alert alert-danger">{msg}</div>}
                name="country"
            />
        </div>
    );
};

export default CountrySelect;
