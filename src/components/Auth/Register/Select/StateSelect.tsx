import React from "react";
import {ErrorMessage, Field} from "formik";
import {useQuery} from "react-query";

import {getStates} from "../../../../services/database.service";
import {IState} from "../../../../interfaces/Accounts/IState";
import RegisterFields from "../RegisterFields";

type Props = {
    countryMarked: boolean;
    countryID: number;
};

const StateSelect: React.FC<Props> = ({countryMarked, countryID}) => {
    const {data: states, isLoading: statesLoading, error: statesError} = useQuery<IState[]>(
        'states',
        getStates
    );

    if (statesLoading || statesError) {
        return <div className="container"> Ładowanie ...</div>;
    }

    if (!countryMarked) {
        return null;
    }

    return (
        <div className="col-md-6 form-group">
            <label className="form-label">Województwo</label>
            <Field
                as="select"
                className="form-control"
                name="state"
            >
                <option></option>
                {states?.filter((state) => state.countryID === countryID)
                    .map((state) => {
                        return (
                            <option
                                key={state.id}
                                value={state.name}>{state.name}
                            </option>
                        )
                    })}
            </Field>
            <ErrorMessage
                key={RegisterFields.length}
                render={msg => <div className="alert alert-danger">{msg}</div>}
                name="state"
            />
        </div>
    );
};

export default StateSelect;
