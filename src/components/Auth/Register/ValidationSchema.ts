import PasswordValidationSchema from "../PasswordValidationSchema";
import LoginValidationSchema from "../LoginValidationSchema";
import ProfileValidationSchema from "../../Profile/EditProfile/ProfileValidationSchema";

const ValidationSchema = PasswordValidationSchema.concat(LoginValidationSchema).concat(ProfileValidationSchema);

export default ValidationSchema;
