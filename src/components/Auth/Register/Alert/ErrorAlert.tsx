import React from "react";
import {useAppSelector} from "../../../../app/hooks";

const ErrorAlert = () => {
    const registerErrorMessage = useAppSelector(state => state.auth.registerErrorMessage);

    if (!registerErrorMessage) {
        return null;
    }

    return (
        <div className="col-md-12">
            <div
                className="alert alert-danger"
                role="alert"
            >
                {registerErrorMessage}
            </div>
        </div>
    );
};

export default ErrorAlert;
