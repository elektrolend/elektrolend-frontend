import React from "react";

const SuccessAlert = () => {
    return (
        <div className="m-5">
            <div
                className="alert alert-success"
                role="alert"
            >
                <h4 className="alert-heading">Sukces!</h4>
                <p>Udało Ci się poprawnie zarejestrować.</p>
                <hr></hr>
                <p className="mb-0">
                    Teraz możesz się <a className="hover-move" href="/login"> zalogować</a>.
                </p>
            </div>
        </div>
    );
};

export default SuccessAlert;
