interface RegisterField {
    name: string;
    label: string;
    type: string;
}

const RegisterFields: RegisterField[] = [
    {
        name: "email",
        label: "E-mail",
        type: "text"
    },
    {
        name: "nickname",
        label: "Login",
        type: "text"
    },
    {
        name: "password",
        label: "Hasło",
        type: "password"
    },
    {
        name: "repeatedPassword",
        label: "Powtórz hasło",
        type: "password"
    },
    {
        name: "name",
        label: "Imię",
        type: "text"
    },
    {
        name: "surname",
        label: "Nazwisko",
        type: "text"
    },
    {
        name: "phone",
        label: "Telefon",
        type: "text"
    },
    {
        name: "street",
        label: "Ulica",
        type: "text"
    },
    {
        name: "homeNumber",
        label: "Numer domu",
        type: "text"
    },
    {
        name: "localNumber",
        label: "Numer lokalu",
        type: "text"
    },
    {
        name: "zipCode",
        label: "Kod pocztowy",
        type: "text"
    },
    {
        name: "city",
        label: "Miasto",
        type: "text"
    }
]

export default RegisterFields;
