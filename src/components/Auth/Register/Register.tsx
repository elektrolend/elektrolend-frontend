import React, {useEffect, useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {useDispatch} from "react-redux";
import {useAppSelector} from "../../../app/hooks";

import StateSelect from "./Select/StateSelect";
import CountrySelect from "./Select/CountrySelect";
import ErrorAlert from "./Alert/ErrorAlert";
import SpinnerButton from "../../Utils/SpinnerButton";
import SuccessAlert from "./Alert/SuccessAlert";
import {registerInit} from "../AuthSlice";
import {register} from "../../../services/auth.service";
import {IUser} from "../../../interfaces/Accounts/IUser";
import ValidationSchema from "./ValidationSchema";
import RegisterFields from "./RegisterFields";

import "../Auth.css";

const Register = () => {
    const [countryID, setCountryID] = useState(0);
    const [countryMarked, setCountryMarked] = useState(false);

    const registerStatus = useAppSelector(state => state.auth.registerStatus);
    const dispatch = useDispatch();

    const onSubmit = (user: IUser) => {
        register(user, dispatch);
    };

    useEffect(() => {
        dispatch(registerInit());
    }, [dispatch]);

    if (registerStatus === 201) {
        return (
            <SuccessAlert/>
        );
    } else {
        return (
            <div>
                <Formik
                    initialValues={{
                        email: '',
                        nickname: '',
                        password: '',
                        repeatedPassword: '',
                        name: '',
                        surname: '',
                        phone: '',
                        street: '',
                        homeNumber: '',
                        localNumber: '',
                        zipCode: '',
                        city: '',
                        country: '',
                        state: ''
                    }}
                    validationSchema={ValidationSchema}
                    onSubmit={values => {
                        onSubmit(values);
                    }}>
                    {({setFieldValue, setFieldTouched}) => (
                        <Form
                            className="container mt-4 mb-4"
                            style={{width: "70%"}}
                        >
                            <div className="border">
                                <div className="bg-primary text-white p-1 m-2">
                                    <h4 className="mt-2 ml-2">Rejestracja</h4>
                                </div>
                                <div className="bg-light p-2 m-2">
                                    <div className="row">
                                        {RegisterFields.map((field, index) => {
                                            return (
                                                <div
                                                    key={index}
                                                    className="col-md-6 form-group"
                                                >
                                                    <label className="form-label">{field.label}</label>
                                                    <Field
                                                        key={index}
                                                        name={field.name}
                                                        type={field.type}
                                                        className="form-control"
                                                    />
                                                    <ErrorMessage
                                                        key={index + RegisterFields.length}
                                                        render={msg => <div className="alert alert-danger">{msg}</div>}
                                                        name={field.name}/>
                                                </div>)
                                        })}
                                        <CountrySelect
                                            setCountryID={setCountryID}
                                            setCountryMarked={setCountryMarked}
                                            setFieldValue={setFieldValue}
                                            setFieldTouched={setFieldTouched}
                                        />
                                        <StateSelect
                                            countryMarked={countryMarked}
                                            countryID={countryID}
                                        />
                                        <ErrorAlert/>
                                        <div className="col-md-12">
                                            <hr></hr>
                                            <SpinnerButton
                                                state={registerStatus}
                                                buttonText={"Zarejestruj się"}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}
export default Register;
