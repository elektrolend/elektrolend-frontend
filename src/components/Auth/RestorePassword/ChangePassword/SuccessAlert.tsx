import {CheckCircleFill} from "react-bootstrap-icons";
import * as React from "react";

const SuccessAlert = () => {
    return (
        <div className="container mt-2 mb-2">
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2 justify-content-center">
                        <div className="p-4 bg-light row m-5 text-center">
                            <CheckCircleFill
                                className="col-md-12 mb-2 text-success"
                                size={50}
                            />
                            <h2 className="col-md-12 text-success">Sukces!</h2>
                            <p className="col-md-12 h6">
                                Udało Ci się poprawnie odzyskać hasło.
                            </p>
                            <p className="col-md-12">
                                <a className="hover-move" href="/login">Zaloguj się ponownie</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SuccessAlert;
