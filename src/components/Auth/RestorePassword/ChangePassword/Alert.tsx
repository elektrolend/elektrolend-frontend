import React from "react";

import {ExclamationTriangleFill} from "react-bootstrap-icons";

const Alert = () => {
    return (
        <div
            className="container text-center"
        >
            <div
                className="alert alert-warning row"
                role="alert"
            >
                <div className="col-1 m-auto">
                    <ExclamationTriangleFill size={30}/>
                </div>
                <div className="col-11 text-left">
                    Na twój adres e-mail został wysłany <strong>kod odzyskiwania</strong> niezbędny do zmiany hasła.
                    Podaj go oraz resztę informacji w formularzu poniżej, aby zmienić hasło.
                </div>
            </div>
        </div>
    );
};

export default Alert;
