import {IFieldInfo} from "../../../../interfaces/Accounts/IFieldInfo";

const ChangePasswordFields: IFieldInfo[] = [
    {
        label: "Podaj nazwę użytkownika lub e-mail",
        name: "emailOrNickname",
        type: "text",
        cName: "form-control"
    },
    {
        label: "Podaj kod odzyskiwania",
        name: "code",
        type: "text",
        cName: "form-control"
    },
    {
        label: "Podaj nowe hasło",
        name: "password",
        type: "password",
        cName: "form-control"
    },
    {
        label: "Powtórz nowe hasło",
        name: "repeatedPassword",
        type: "password",
        cName: "form-control"
    }
]

export default ChangePasswordFields;
