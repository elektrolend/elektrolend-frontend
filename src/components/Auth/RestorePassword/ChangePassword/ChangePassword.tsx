import React, {useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";

import SpinnerButton from "../../../Utils/SpinnerButton";
import ErrorAlert from "../../Login/ErrorAlert";
import Alert from "./Alert";
import SuccessAlert from "./SuccessAlert";
import CancelButton from "../../../Utils/CancelButton";
import {changeCustomerPassword} from "../../../../services/database.service";
import {ICustomerNewPassword} from "../../../../interfaces/Accounts/ICustomerNewPassword";
import ChangePasswordFields from "./ChangePasswordFields";
import PasswordValidationSchema from "../../PasswordValidationSchema";

type Props = {
    setRestoringState: (state: number) => void;
};

const ChangePassword: React.FC<Props> = ({setRestoringState}) => {
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    const onSubmit = (values: any) => {
        setErrorMessage("");
        setState(100);
        const newCustomerPasswordData: ICustomerNewPassword = {
            email: values.emailOrNickname,
            nickname: values.emailOrNickname,
            password: values.password,
            code: values.code
        };
        changeCustomerPassword(newCustomerPasswordData)
            .then(response => {
                if (response.status === 200) {
                    setState(response.status);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setErrorMessage(error.response.data);
                    setState(error.response.status);
                } else {
                    setErrorMessage("Brak połączenia z serwerem");
                    setState(500);
                }
            });
    };

    if (state === 200) {
        return (
            <SuccessAlert/>
        );
    }

    return (
        <Formik
            initialValues={{
                code: '',
                emailOrNickname: '',
                password: '',
                repeatedPassword: ''
            }}
            validationSchema={PasswordValidationSchema}
            onSubmit={values => {
                onSubmit(values);
            }}>
            {() => (
                <Form
                    className="container mt-4 mb-4"
                    style={{width: "800px"}}
                >
                    <div className="border">
                        <div className="bg-primary text-white p-1 m-2">
                            <h4 className="mt-2 ml-2">Ustaw nowe hasło</h4>
                        </div>
                        <div className="bg-light p-2 m-2">
                            <Alert/>
                            <hr></hr>
                            <div className="row">
                                {ChangePasswordFields.map((field, index) => {
                                    return (
                                        <div className="col-md-6 form-group" key={index}>
                                            <label className="form-label">{field.label}</label>
                                            <Field
                                                name={field.name}
                                                type={field.type}
                                                className={field.cName}
                                            />
                                            <ErrorMessage
                                                render={msg => <div className="alert alert-danger">{msg}</div>}
                                                name={field.name}/>
                                        </div>
                                    )
                                })}
                            </div>
                            <ErrorAlert errorMessage={errorMessage}/>
                            <hr></hr>
                            <div className="row">
                                <div className="col-md-6 text-center align-self-center">
                                    <CancelButton
                                        text={"ANULUJ"}
                                        newState={-1}
                                        setState={setRestoringState}
                                    />
                                </div>
                                <div className="col-md-6">
                                    <SpinnerButton
                                        state={state}
                                        buttonText={"ZATWIERDŹ"}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default ChangePassword;
