import React, {useState} from "react";
import {Field, Form, Formik} from "formik";
import {Redirect} from "react-router-dom";

import SpinnerButton from "../../Utils/SpinnerButton";
import ErrorAlert from "../Login/ErrorAlert";
import ChangePassword from "./ChangePassword/ChangePassword";
import CancelButton from "../../Utils/CancelButton";
import {restorePassword} from "../../../services/database.service";

const RestorePassword = () => {
    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    const onSubmit = (email: string) => {
        setErrorMessage("");
        setState(100);
        restorePassword(email)
            .then(response => {
                if (response.status === 200) {
                    setState(response.status);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setErrorMessage(error.response.data);
                    setState(error.response.status);
                } else {
                    setErrorMessage("Brak połączenia z serwerem");
                    setState(500);
                }
            });
    };

    if (state === -1) {
        return (
            <Redirect to={"/login"}/>
        );
    }

    if (state === 200) {
        return (
            <ChangePassword
                setRestoringState={setState}
            />
        );
    }

    return (
        <Formik
            initialValues={{
                email: ''
            }}
            onSubmit={values => {
                onSubmit(values.email);
            }}>
            {() => (
                <Form
                    className="container mt-4 mb-4"
                    style={{width: "600px"}}
                >
                    <div className="border">
                        <div className="bg-primary text-white p-1 m-2">
                            <h4 className="mt-2 ml-2">Zapomniałeś hasła?</h4>
                        </div>
                        <div className="bg-light p-2 m-2">
                            <div className="row">
                                <div
                                    className="col-12"
                                >
                                    <p>
                                        Wystarczy, że podasz swój e-mail, a my pomożemy Ci ustawić nowe hasło.
                                    </p>
                                </div>
                                <div className="col-12">
                                    <Field
                                        name="email"
                                        className="form-control"
                                        style={{height: "45px"}}
                                        placeholder="podaj e-mail"
                                        type="text"
                                    />
                                </div>
                            </div>
                            <ErrorAlert errorMessage={errorMessage}/>
                            <hr></hr>
                            <div className="row">
                                <div className="col-md-6">
                                    <CancelButton
                                        text={"WRÓĆ"}
                                        newState={-1}
                                        setState={setState}
                                    />
                                </div>
                                <div className="col-md-6">
                                    <SpinnerButton
                                        state={state}
                                        buttonText={"DALEJ"}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default RestorePassword;
