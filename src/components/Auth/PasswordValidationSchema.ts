import * as Yup from "yup";

const PasswordValidationSchema = Yup.object().shape({
    password: Yup.string()
        .min(6, 'Hasło musi mieć co najmniej 6 znaków!')
        .max(20, 'Hasło może mieć maksymalnie 20 znaków!')
        .matches(/^(?=.*[A-Z])/, 'Hasło musi mieć dużą literę!')
        .matches(/^(?=.*[!@#$&*])/, 'Hasło musi mieć znak specjalny!')
        .matches(/^(?=.*[a-z])/, 'Hasło musi mieć małą literę!')
        .required('Hasło jest wymagane!'),
    repeatedPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Powtórzone hasło musi być identyczne!')
        .required('To pole jest wymagane!')
});

export default PasswordValidationSchema;
