import React, {useEffect, useState} from "react";
import {generatePath, Link} from "react-router-dom";
import DatePicker, {registerLocale} from "react-datepicker";
import pl from "date-fns/locale/pl";

import ServiceSecurity from "./ServiceSecurity";
import {getDisabledDates} from "../../services/database.service";
import {getImageSrc} from "../UploadFiles/photos.service";
import {DateManipulation} from "../Utils/DateManipulation";
import {ICartItem} from "../../interfaces/Orders/ICartItem";
import {IProductOrderDate} from "../../interfaces/Orders/IProductOrderDate";
import {ServiceSecurityType} from "../../interfaces/Orders/ServiceSecurityType";

import {ExclamationTriangleFill, Trash} from "react-bootstrap-icons";
import "react-datepicker/dist/react-datepicker.css";

registerLocale('pl', pl);

type Props = {
    product: ICartItem;
    removeFromCart: (id: number) => void;
    changeStartDate: (product: ICartItem, date: any) => void;
    changeEndDate: (product: ICartItem, date: any) => void;
    changeServiceSecurity: (product: ICartItem, type: ServiceSecurityType) => void;
};


const CartItem: React.FC<Props> = ({
                                       product,
                                       removeFromCart,
                                       changeStartDate,
                                       changeEndDate,
                                       changeServiceSecurity
                                   }) => {
    const id = product.id;
    const name = product.name.replace(/\s/g, '-');

    const [disabledDates, setDisabledDates] = useState([] as IProductOrderDate[]);

    useEffect(() => {
        getDisabledDates(Number(product.id)).then(d => setDisabledDates(d.dates));
    }, [product.id]);

    const [maxEndDate, setMaxEndDate] = useState(DateManipulation.getMaxDate(product.beginDate, disabledDates) as Date | null);

    useEffect(() => {
        setMaxEndDate(DateManipulation.getMaxDate(product.beginDate, disabledDates));
    }, [disabledDates, product.beginDate]);

    const handleChangeStartDate = (date: any) => {
        if (date) {
            changeStartDate(product, date);
            changeEndDate(product, date);
            setMaxEndDate(DateManipulation.getMaxDate(date, disabledDates));
        }
    };

    const handleChangeEndDate = (date: any) => {
        if (date) {
            changeEndDate(product, date);
        }
    };

    const checkStartDate = (date: Date) => {
        for (const d of disabledDates) {
            if (DateManipulation.isBetween(date, d.beginDate, d.endDate)) {
                return false;
            }
        }
        return true;
    };

    const checkEndDate = (date: Date) => {
        if (maxEndDate === null) {
            return true;
        }
        if (DateManipulation.inDates(date, product.beginDate, maxEndDate)) {
            return true;
        }
        return false;
    };

    const calculateProductPrice = () => {
        return product.price * product.amount;
    };

    const insuranceChecked = () => {
        return product.serviceSecurityType === ServiceSecurityType.insurance;
    };

    const dateRangeCorrect = () => {
        if (!DateManipulation.getDiffInDays(product.beginDate, product.endDate)) {
            return false;
        }
        return true;
    };

    const getServiceSecurityType = () => {
        if (insuranceChecked()) {
            return `ubezpieczenie ${product.insurance}`;
        }
        return `kaucja ${product.deposit}`;
    };

    return (
        <div key={product.id} className="border row m-1 w-20 mt-4 p-1">
            <div className="border-bottom row m-1 w-20 mt-4 p-2">
                <div className="col-md-2 align-self-center float-left">
                    <img
                        src={getImageSrc(product.imageUrl)}
                        className="img-fluid"
                        alt={product.name}>
                    </img>
                </div>
                <div className="col-md-4">
                    <div className="flex-column">
                        <div className="col-md">
                            <h4>
                                <Link to={generatePath(`/product/details/:id/:name`,
                                    {id: id, name: name})}>
                                    <strong>
                                        {product.name}
                                    </strong>
                                </Link>
                            </h4>
                        </div>
                        <div className="col-md">
                            {product.description}
                        </div>
                    </div>
                </div>
                <div className="col-md-4 d-flex align-items-center justify-content-center">
                    <div className="flex-column">
                        <div className="form-group row">
                            <h5>Okres wypożyczenia</h5>
                        </div>
                        <div className="form-group row">
                            <label className="col-1 col-form-label">Od</label>
                            <div className="col-10">
                                <DatePicker minDate={new Date()}
                                            selected={product.beginDate}
                                            filterDate={checkStartDate}
                                            onChange={(date) => handleChangeStartDate(date)}
                                            dateFormat="dd/MM/yyyy"
                                            locale="pl"
                                            required={true}/>
                            </div>
                        </div>
                        <div className="flex-column">
                            <div className="form-group row">
                                <label className="col-1 col-form-label">Do</label>
                                <div className="col-10">
                                    <DatePicker minDate={product.beginDate}
                                                selected={product.endDate}
                                                filterDate={checkEndDate}
                                                onChange={(date) => handleChangeEndDate(date)}
                                                dateFormat="dd/MM/yyyy"
                                                locale="pl"
                                                required={true}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-2 align-self-center">
                    <div className="row">
                        <div className="col-xl-6 d-flex align-items-center justify-content-center">
                            <div className="flex-column">
                                <div className="col-md">
                                    <h3>
                                        {product.price} zł / DZIEŃ
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 d-flex align-items-center justify-content-center">
                            <div className="flex-column">
                                <div className="col-md">
                                    <button onClick={() => removeFromCart(product.id)} type="button"
                                            className="btn btn-danger">
                                        <Trash size={20}/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ServiceSecurity
                    product={product}
                    changeServiceSecurity={changeServiceSecurity}>
                </ServiceSecurity>
            </div>
            {dateRangeCorrect() ? <>
                    <div className="col-md-12 d-flex d-inline align-items-center justify-content-end">
                        <h5 className="text-dark mr-2">
                            Suma za {product.amount} dni:
                        </h5>
                        <h2 className="text-primary">{calculateProductPrice()} zł </h2>
                    </div>
                    <div className="col-md-12 d-flex d-inline align-items-center justify-content-end">
                        <h6 className="text-secondary">
                            + {getServiceSecurityType()} zł
                        </h6>
                    </div>
                </> :
                <div className="col-md-12">
                    <div className="border m-2 mt-4 alert alert-danger d-flex align-items-center" role="alert">
                        <ExclamationTriangleFill/>
                        <div className="ml-1">
                            Pzedmiot musi mieć różne daty początku oraz końca wypożyczenia!
                        </div>
                    </div>
                </div>
            }
        </div>
    )
};

export default CartItem;
