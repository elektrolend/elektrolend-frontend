import React from "react";
import {generatePath, Link} from "react-router-dom";
import DatePicker, {registerLocale} from "react-datepicker";
import pl from 'date-fns/locale/pl';

import {ICartItem} from "../../../interfaces/Orders/ICartItem";
import {ServiceSecurityType} from "../../../interfaces/Orders/ServiceSecurityType";
import {getImageSrc} from "../../UploadFiles/photos.service";

import "react-datepicker/dist/react-datepicker.css";

registerLocale('pl', pl);

type Props = {
    product: ICartItem;
};

const FinishOrderItem: React.FC<Props> = ({product}) => {
    const id = product.id;
    const name = product.name.replace(/\s/g, '-');

    const getServiceSecurityCost = () => {
        return product.serviceSecurityType === ServiceSecurityType.insurance ? product.insurance : product.deposit;
    };

    const calculateProductPrice = () => {
        return product.price * product.amount + getServiceSecurityCost();
    };

    return (
        <div key={product.id} className="border m-1 w-20 mt-4 p-1">
            <div className="border-bottom row m-1 w-20 mt-4 p-4">
                <div className="col-md-2">
                    <img
                        src={getImageSrc(product.imageUrl)}
                        className="img-fluid"
                        alt={product.name}>
                    </img>
                </div>
                <div className="col-md-5 align-self-center">
                    <div className="col-md-12">
                        <h4>
                            <Link to={generatePath(`/product/details/:id/:name`,
                                {id: id, name: name})}>
                                <strong>
                                    {product.name}
                                </strong>
                            </Link>
                        </h4>
                    </div>
                </div>
                <div className="col-md-5 align-self-center mb-3">
                    <div className="row">
                        <div className="col-md-6">
                            <label className="col-md-12"><b>Od</b></label>
                            <DatePicker
                                className="col-md-12"
                                selected={product.beginDate}
                                onChange={() => null}
                                dateFormat="dd/MM/yyyy"
                                locale="pl"
                                disabled={true}/>
                        </div>
                        <div className="col-md-6">
                            <label className="col-md-12"><b>Do</b></label>
                            <DatePicker
                                className="col-md-12"
                                selected={product.endDate}
                                onChange={() => null}
                                dateFormat="dd/MM/yyyy"
                                locale="pl"
                                disabled={true}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-12 d-flex d-inline align-items-center justify-content-end">
                <h5 className="text-dark mr-2">
                    SUMA:
                </h5>
                <h2 className="text-primary">{calculateProductPrice()} zł </h2>
            </div>
        </div>
    )
};

export default FinishOrderItem;
