import React, {useState} from "react";
import {Redirect} from "react-router-dom";
import {useAppSelector} from "../../../app/hooks";

import FinishOrderItem from "./FinishOrderItem";
import {addOrder} from "../../../services/database.service";
import {ICartItem} from "../../../interfaces/Orders/ICartItem";
import {IOrderInfo} from "../../../interfaces/Orders/IOrderInfo";
import {DateManipulation} from "../../Utils/DateManipulation";
import {ServiceSecurityType} from "../../../interfaces/Orders/ServiceSecurityType";
import Payment from "../Payment/Payment";
import {IOrderResponse} from "../../../interfaces/Orders/IOrderResponse";
import {checkPermissions} from "../../../helpers/auth-header";

type Props = {
    cartItems: ICartItem[];
    changeCartState: (state: number) => void;
    clearCart: () => void;
};

const FinishOrder: React.FC<Props> = ({cartItems, changeCartState, clearCart}) => {
    const isLogged = useAppSelector(state => state.auth.isLogged);
    const userId = useAppSelector(state => state.auth.userID);
    const [orderState, setOrderState] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');
    const [orderResponse, setOrderResponse] = useState({} as IOrderResponse);
    const [totalPrice, setTotalPrice] = useState(0);

    const getServiceSecurityPrice = (cartItem: ICartItem) =>
        cartItem.serviceSecurityType === ServiceSecurityType.insurance ? cartItem.insurance : cartItem.deposit

    const calculateTotal = (items: ICartItem[]): number =>
        items.reduce((ack: number, cartItem) =>
            ack + (cartItem.amount * cartItem.price) + getServiceSecurityPrice(cartItem), 0);

    const getOrderInfoList = () => {
        const orderInfoList: IOrderInfo[] = [];
        cartItems.forEach(cartItem => {
            orderInfoList.push(
                {
                    productID: cartItem.id,
                    orderBeginDate: DateManipulation.getDateInString(cartItem.beginDate),
                    orderEndDate: DateManipulation.getDateInString(cartItem.endDate),
                    serviceSecurityType: cartItem.serviceSecurityType,
                }
            )
        })

        return {
            customerID: userId,
            orderInfoList: orderInfoList
        };
    };

    const makeOrder = async () => {
        try {
            const response = await addOrder(getOrderInfoList());
            if (response.ok) {
                setTotalPrice(Number(calculateTotal(cartItems).toFixed(2)));
                clearCart();
                const orderResponseInfo = await response.json();
                setOrderResponse(orderResponseInfo);
                setOrderState(response.status);
            } else {
                setOrderState(response.status);
                setErrorMessage(checkPermissions(response.status, response.statusText));
            }

        } catch (error) {
            setOrderState(500);
            setErrorMessage("Brak połączenia z serwerem.");
        }
    };

    if (!isLogged) {
        return (
            <Redirect to="/login"/>
        );
    }

    if (orderState === 200 && orderResponse) {
        return (
            <Payment
                orderResponse={orderResponse}
                orderAmount={totalPrice}
            />
        );
    }
    if (!orderState) {
        return (
            <div className="container mt-2">
                <div className="h2 text-primary">Podsumowanie zamówienia</div>
                <div className="border">
                    {cartItems?.map(product => (
                        <FinishOrderItem
                            key={product.id}
                            product={product}
                        ></FinishOrderItem>
                    ))}
                    <div className="border row m-2 w-20 mt-4 p-2 bg-light">
                        <div className="col-md-12 text-right">
                            <h5> Do zapłaty:</h5>
                            <h2 className="text-primary">{calculateTotal(cartItems).toFixed(2)} zł</h2>
                        </div>
                        <div className="col-md-6">
                            <div className="text-left">
                                <button
                                    className="btn btn-lg btn-danger"
                                    onClick={() => changeCartState(0)}>
                                    POWRÓT
                                </button>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="text-right">
                                <button
                                    className="btn btn-lg btn-success"
                                    onClick={() => makeOrder()}>
                                    POTWIERDŹ
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return (
        <div className="m-5">
            <div className="alert alert-danger" role="alert">
                <h4 className="alert-heading">Coś poszło nie tak...</h4>
                {errorMessage}
                <hr></hr>
                <p className="mb-0">
                    Spróbuj <a className="hover-move" href="/shopping-cart"> złożyć</a> zamówienie ponownie.
                </p>
            </div>
        </div>
    );
};

export default FinishOrder;
