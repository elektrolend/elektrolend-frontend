import React, {useState} from "react";
import {useAppSelector} from "../../app/hooks";

import CartItem from "./CartItem";
import FinishOrder from "./FinishOrder/FinishOrder";
import {ICartItem} from "../../interfaces/Orders/ICartItem";
import {ServiceSecurityType} from "../../interfaces/Orders/ServiceSecurityType";
import {DateManipulation} from "../Utils/DateManipulation";

import {CartX, ExclamationTriangleFill} from "react-bootstrap-icons";

type Props = {
    cartItems: ICartItem[];
    removeFromCart: (id: number) => void;
    clearCart: () => void;
    changeStartDate: (product: ICartItem, date: any) => void;
    changeEndDate: (product: ICartItem, date: any) => void;
    changeServiceSecurity: (product: ICartItem, type: ServiceSecurityType) => void;
};

const ShoppingCart: React.FC<Props> = ({
                                           cartItems,
                                           removeFromCart,
                                           clearCart,
                                           changeStartDate,
                                           changeEndDate,
                                           changeServiceSecurity
                                       }) => {
    const isLogged = useAppSelector(state => state.auth.isLogged);
    const [cartState, setCartState] = useState(0);

    const getServiceSecurityPrice = (cartItem: ICartItem) =>
        cartItem.serviceSecurityType === ServiceSecurityType.insurance ? cartItem.insurance : cartItem.deposit;

    const calculateTotal = (items: ICartItem[]) =>
        items.reduce((ack: number, cartItem) =>
            ack + (cartItem.amount * cartItem.price) + getServiceSecurityPrice(cartItem), 0);

    const isCartEmpty = () => {
        return cartItems.length === 0;
    };

    const datesCorrect = () => {
        for (let item of cartItems) {
            if (!DateManipulation.getDiffInDays(item.beginDate, item.endDate)) {
                return false;
            }
        }
        return true;
    };

    if (cartState) {
        return (
            <FinishOrder
                cartItems={cartItems}
                changeCartState={setCartState}
                clearCart={clearCart}/>
        );
    }
    return (
        <div className="container mt-2">
            <div className="h2 text-primary"> Koszyk</div>
            <div className="border">
                {isCartEmpty() ?
                    <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center"><CartX
                        className="col-md-12 mb-2" size={50}/> <h3>Brak produktów w koszyku.</h3></div> : null}
                {cartItems?.map(product => (
                    <CartItem
                        key={product.id}
                        product={product}
                        removeFromCart={removeFromCart}
                        changeStartDate={changeStartDate}
                        changeEndDate={changeEndDate}
                        changeServiceSecurity={changeServiceSecurity}
                    ></CartItem>
                ))}
                {!isLogged && !isCartEmpty() ?
                    <div className="border row m-2 mt-4 alert alert-warning d-flex align-items-center" role="alert">
                        <ExclamationTriangleFill/>
                        <div className="ml-1">
                            Musisz się <a className="hover-move" href="/login">zalogować</a>, żeby złożyć zamówienie.
                        </div>
                    </div> : null}
                <div className="border row m-2 w-20 mt-4 p-2 bg-light">
                    <div className="col-md-12 text-right">
                        <h5> Do zapłaty:</h5>
                        <h2 className="text-primary">{calculateTotal(cartItems).toFixed(2)} zł</h2>
                    </div>
                    {isLogged && datesCorrect() ?
                        <div className="col-md-12 text-right">
                            <button
                                className="btn btn-lg btn-primary"
                                onClick={() => setCartState(200)}
                                disabled={isCartEmpty()}>
                                ZREALIZUJ ZAMÓWIENIE
                            </button>
                        </div> : null}
                </div>
            </div>
        </div>
    )
};

export default ShoppingCart;
