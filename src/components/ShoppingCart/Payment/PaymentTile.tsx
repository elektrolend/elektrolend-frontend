import React from "react";

import {IPaymentProps} from "../../../interfaces/Orders/IPaymentProps";

const PaymentTile: React.FC<IPaymentProps> = ({paymentMethod, orderID, orderAmount, handleChangeOrderStates}) => {
    return (
        <div className="col-md-4 mb-3">
            <paymentMethod.component
                paymentMethod={paymentMethod}
                orderID={orderID}
                orderAmount={orderAmount}
                handleChangeOrderStates={handleChangeOrderStates}
            />
        </div>
    );
}

export default PaymentTile;
