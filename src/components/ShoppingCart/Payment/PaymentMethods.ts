import {IPaymentMethod} from "../../../interfaces/Orders/IPaymentMethod";
import PayPal from "./Methods/PayPal";
import Blik from "./Methods/Blik";
import Payu from "./Methods/Payu";
import PrzelewyTwentyFour from "./Methods/PrzelewyTwentyFour";
import Skycash from "./Methods/Skycash";
import PaylinkIdea from "./Methods/PaylinkIdea";

const PaymentMethods: IPaymentMethod[] = [
    {
        component: PayPal,
        title: "PayPal",
        imageSrc: "paypal.png"
    },
    {
        component: Payu,
        title: "PayU",
        imageSrc: "payu.png"
    },
    {
        component: Blik,
        title: "BLIK",
        imageSrc: "blik.png"
    },
    {
        component: PrzelewyTwentyFour,
        title: "Przelewy24",
        imageSrc: "przelewy24.png"
    },
    {
        component: Skycash,
        title: "Skycash",
        imageSrc: "skycash.png"
    },
    {
        component: PaylinkIdea,
        title: "PaylinkIdea",
        imageSrc: "paylinkidea.png"
    }
]

export default PaymentMethods;
