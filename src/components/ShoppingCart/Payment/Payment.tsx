import React, {useState} from "react";

import PaymentTile from "./PaymentTile";
import Summary from "../Summary/Summary";
import {addPayment, changeOrderProductsStates} from "../../../services/database.service";
import PaymentMethods from "./PaymentMethods";
import {OrderProductState} from "../../../interfaces/Orders/OrderProductState";
import {IPayment} from "../../../interfaces/Orders/IPayment";
import {IOrderResponse} from "../../../interfaces/Orders/IOrderResponse";

import "./Payment.css";

type Props = {
    orderResponse: IOrderResponse;
    orderAmount: number;
};

const Payment: React.FC<Props> = ({orderResponse, orderAmount}) => {
    const [paymentStatus, setPaymentStatus] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    const makeOrderChangeStatus = () => {
        return {
            productList: orderResponse.orderProductInfoList.map(orderProductInfo => {
                return {
                    orderProductID: orderProductInfo.orderProductID,
                    orderProductState: OrderProductState.paid
                }
            })
        }
    };

    const handleChangeOrderStates = (paymentAccount: string) => {
        setErrorMessage("");
        changeOrderProductsStates(makeOrderChangeStatus())
            .then((response) => {
                if (response.ok) {
                    handleAddPayment(paymentAccount);
                } else {
                    setPaymentStatus(response.status);
                    setErrorMessage(response.statusText);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setPaymentStatus(error.response.status);
                    setErrorMessage(error.response.data);
                }
            });
    };

    const handleAddPayment = (paymentAccount: string) => {
        const newPayment: IPayment = {
            value: orderAmount,
            date: new Date(),
            orderID: orderResponse.orderID,
            paymentAccount: paymentAccount
        };

        addPayment(newPayment)
            .then((response) => {
                if (response.status === 201) {
                    setPaymentStatus(response.status);
                } else {
                    setPaymentStatus(response.status);
                    setErrorMessage(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setPaymentStatus(error.response.status);
                    setErrorMessage(error.response.statusText);
                }
            });
    };

    if (paymentStatus === 201 && orderResponse) {
        return (
            <Summary
                orderResponse={orderResponse}
            />
        );
    }

    if (!paymentStatus) {
        return (
            <div className="container mt-2 mb-2">
                <div className="h2 text-primary">Wybierz metodę płatności</div>
                <div className="border">
                    <div className="bg-light m-2">
                        <div className="row p-2 m-2">
                            {PaymentMethods.map((paymentMethod, index) => {
                                return (
                                    <PaymentTile
                                        key={'payment_' + index}
                                        paymentMethod={paymentMethod}
                                        orderID={orderResponse.orderID}
                                        orderAmount={orderAmount}
                                        handleChangeOrderStates={handleChangeOrderStates}
                                    />
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="m-5">
            <div className="alert alert-danger" role="alert">
                <h4 className="alert-heading">Coś poszło nie tak...</h4>
                {errorMessage}
                <hr></hr>
                <p className="mb-0">
                    Spróbuj <a className="hover-move" href="/shopping-cart"> złożyć</a> zamówienie ponownie.
                </p>
            </div>
        </div>
    );
};

export default Payment;
