import React from "react";

import FakePayment from "./FakePayment";
import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";

export const PaylinkIdea: React.FC<IPaymentProps> = () => {
    return (
        <FakePayment
            src={"paylinkidea.png"}
            alt={"PayLinkIdea"}
        />
    );
};


export default PaylinkIdea;
