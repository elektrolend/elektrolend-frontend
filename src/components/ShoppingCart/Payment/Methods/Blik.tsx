import React from "react";
import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";
import FakePayment from "./FakePayment";

export const Blik: React.FC<IPaymentProps> = () => {
    return (
        <FakePayment
            src={"blik.png"}
            alt={"Blik"}
        />
    );
};


export default Blik;
