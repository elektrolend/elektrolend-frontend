import React from "react";
import {PayPalButton} from "react-paypal-button-v2";

import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";

const PayPal: React.FC<IPaymentProps> = ({orderAmount, handleChangeOrderStates}) => {

    return (
        <PayPalButton
            style={{color: "white"}}
            options={{disableFunding: "card,blik,p24"}}
            createOrder={(data: any, actions: any) => {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            currency_code: "PLN",
                            value: orderAmount
                        }
                    }],
                });
            }}
            onSuccess={(orderData: any) => {
                handleChangeOrderStates(orderData.purchase_units[0].payments.captures[0].id);
            }}
        />
    );
};

export default PayPal;
