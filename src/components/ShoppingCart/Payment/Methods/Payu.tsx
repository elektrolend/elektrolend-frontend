import React from "react";

import FakePayment from "./FakePayment";
import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";

export const Payu: React.FC<IPaymentProps> = () => {
    return (
        <FakePayment
            src={"payu.png"}
            alt={"PayU"}
        />
    );
};

export default Payu;
