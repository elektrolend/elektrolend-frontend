import React from "react";

import FakePayment from "./FakePayment";
import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";

export const PrzelewyTwentyFour: React.FC<IPaymentProps> = () => {
    return (
        <FakePayment
            src={"przelewy24.png"}
            alt={"Przelewy24"}
        />
    );
};

export default PrzelewyTwentyFour;
