import React, {useState} from "react";

type Props = {
    src: string;
    alt: string;
};

const FakePayment: React.FC<Props> = ({src, alt}) => {
    const [display, setDisplay] = useState(false);

    const show = () => {
        setDisplay(true);
    };

    const hide = () => {
        setDisplay(false);
    };

    const isDisplayed = (canBeDisplayed: boolean) => {
        if (canBeDisplayed) {
            return "displayed";
        }
        return "notDisplayed";
    };

    return (
        <div
            className="card border-dark align-items-center"
            style={{height: "47px", textAlign: "center"}}
            onMouseEnter={() => show()}
            onMouseLeave={() => hide()}
        >
            <p className={"mt-2 " + isDisplayed(display)}>
                Metoda płatności niedostępna
            </p>
            <img
                src={src}
                alt={alt}
                className={"mt-1 " + isDisplayed(!display)}
                style={{position: "absolute", maxWidth: '80%', maxHeight: '80%'}}
            />
        </div>
    );
};

export default FakePayment;
