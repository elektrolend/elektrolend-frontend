import React from "react";

import FakePayment from "./FakePayment";
import {IPaymentProps} from "../../../../interfaces/Orders/IPaymentProps";

export const Skycash: React.FC<IPaymentProps> = () => {
    return (
        <FakePayment
            src={"skycash.png"}
            alt={"SkyCash"}
        />
    );
};

export default Skycash;
