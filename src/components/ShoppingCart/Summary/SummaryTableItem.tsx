import React, {useState} from "react";

import {getProduct} from "../../../services/database.service";
import {IProduct} from "../../../interfaces/Products/IProduct";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";

import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type Props = {
    orderProduct: IOrderProduct;
};

const SummaryTableItem: React.FC<Props> = ({orderProduct}) => {
    const [showPassword, setShowPassword] = useState(false);
    const [product, setProduct] = useState({} as IProduct);

    React.useEffect(() => {
        getProduct(orderProduct.productID).then(p => setProduct(p));
    }, [orderProduct.productID]);

    return (
        <tr className="bg-white border text-center rounded">
            <td className="border-right">
                <h4>
                    <strong>
                        {product.name}
                    </strong>
                </h4>
            </td>
            <td className="border-right">
                <h4>{orderProduct.officeBoxIDStart}</h4>
            </td>
            <td className="border-right">
                <h4>{orderProduct.officeBoxIDEnd}</h4>
            </td>
            <td>
                <div className="form-group">
                    <div className="input-group mt-2">
                        <input
                            type={showPassword ? "text" : "password"}
                            className="form-control"
                            data-toggle="password"
                            disabled={true}
                            value={orderProduct.boxPassword}>
                        </input>
                        <div className="input-group-append">
                            <div
                                className="input-group-text"
                                onClick={() => setShowPassword(!showPassword)}
                            >
                                <FontAwesomeIcon
                                    className="icon" icon={showPassword ? faEyeSlash : faEye}/>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    );
}

export default SummaryTableItem;
