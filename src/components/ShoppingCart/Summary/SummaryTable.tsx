import React from "react";

import SummaryTableItem from "./SummaryTableItem";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProducts: IOrderProduct[];
};

const SummaryTable: React.FC<Props> = ({orderProducts}) => {
    return (
        <table className="table border">
            <thead className="bg-primary">
            <tr>
                <th scope="col">
                    <h4 className="text-lg-center text-white">
                        Nazwa produktu
                    </h4>
                </th>
                <th scope="col">
                    <h4 className="text-lg-center text-white">
                        Numer skrytki do odbioru
                    </h4>
                </th>
                <th scope="col">
                    <h4 className="text-lg-center text-white">
                        Numer skrytki do zwrotu
                    </h4>
                </th>
                <th scope="col">
                    <h4 className="text-lg-center text-white">
                        Hasło do skrytki
                    </h4>
                </th>
            </tr>
            </thead>
            <tbody>
            {orderProducts.map((orderProduct, index) => {
                return (
                    <SummaryTableItem
                        key={index}
                        orderProduct={orderProduct}
                    />

                );
            })}
            </tbody>
        </table>
    );
}

export default SummaryTable;
