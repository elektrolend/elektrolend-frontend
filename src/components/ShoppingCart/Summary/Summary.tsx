import React, {useState} from "react";
import {Link} from "react-router-dom";

import SummaryTable from "./SummaryTable";
import {getOrderProducts} from "../../../services/database.service";
import {IOrderResponse} from "../../../interfaces/Orders/IOrderResponse";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";

import {ArrowRightCircleFill, CheckLg, XCircleFill} from "react-bootstrap-icons";

type Props = {
    orderResponse: IOrderResponse;
};

const Summary: React.FC<Props> = ({orderResponse}) => {
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);

    const orderProductsEmpty = () => {
        return orderProducts.length === 0;
    };

    React.useEffect(() => {
        getOrderProducts(orderResponse.orderID)
            .then(orderSummary => setOrderProducts(orderSummary.orderProductList))
            .catch(() => setOrderProducts([]));
    }, [orderResponse.orderID]);

    return (
        <div className="container mt-2 mb-2">
            <div className="border row m-2 mt-4 alert alert-success d-flex align-items-center" role="alert">
                <CheckLg/>
                <div className="ml-1">
                    <b>Sukces!</b> Poniżej znajdują się szczegóły zamówienia.
                </div>
            </div>
            <div className="h2 text-primary">Szczegóły zamówienia nr. {orderResponse.orderID}</div>
            <div className="border">
                {orderProductsEmpty() ?
                    <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                        <XCircleFill className="col-md-12 mb-2 te" size={50}/>
                        <h3>Brak szczegółowych informacji o zamówieniu.</h3>
                    </div> :
                    <div className="bg-light m-2">
                        <div className="row p-2 m-2">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <SummaryTable
                                        orderProducts={orderProducts}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
            <div className="border mt-2 mb-2">
                <div className="bg-light m-2">
                    <div className="col-12 text-right">
                        <Link to="/products">
                            <button
                                className="btn btn-lg btn-labeled btn-primary"
                                type="submit"
                                style={{width: "100%"}}
                            >
                                Nowe zamówienie <ArrowRightCircleFill/>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Summary;
