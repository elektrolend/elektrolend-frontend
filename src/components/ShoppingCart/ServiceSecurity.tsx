import {ICartItem} from "../../interfaces/Orders/ICartItem";
import {ServiceSecurityType} from "../../interfaces/Orders/ServiceSecurityType";
import React from "react";

type Props = {
    product: ICartItem;
    changeServiceSecurity: (product: ICartItem, type: ServiceSecurityType) => void;
};


const ServiceSecurity: React.FC<Props> = ({product, changeServiceSecurity}) => {
    const insuranceChecked = () => {
        return product.serviceSecurityType === ServiceSecurityType.insurance;
    };

    const changeServiceSecurityType = (type: ServiceSecurityType) => {
        changeServiceSecurity(product, type);
    };

    return (
        <div className="col-md-12 text-right">
            <div className="form-check form-check-inline">
                <input className="form-check-input"
                       type="radio"
                       checked={insuranceChecked()}
                       onChange={() => changeServiceSecurityType(ServiceSecurityType.insurance)}
                >
                </input>
                <label className="form-check-label">
                    Ubezpieczenie <b>({product.insurance} zł)</b>
                </label>
            </div>
            <div className="form-check form-check-inline">
                <input className="form-check-input"
                       type="radio"
                       checked={!insuranceChecked()}
                       onChange={() => changeServiceSecurityType(ServiceSecurityType.deposit)}
                >
                </input>
                <label className="form-check-label">
                    Kaucja <b>({product.deposit} zł)</b>
                </label>
            </div>
        </div>
    );
}

export default ServiceSecurity;