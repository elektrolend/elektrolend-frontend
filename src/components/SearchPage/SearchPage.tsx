import React, {useEffect, useState} from "react";

import ProductList from "./ProductList/ProductList";
import FilterBar from "./FilterBar/FilterBar";
import PageNumberBar from "../Pagination/PageNumberBar";
import {getSearchProducts} from "../../services/database.service";
import {ISearchParams} from "../../interfaces/Products/ISearchParams";
import {IProductsResponse} from "../../interfaces/Products/IProductsResponse";
import {IProduct} from "../../interfaces/Products/IProduct";

type Props = {
    addToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    isInCart: (id: number) => boolean;
    searchParams: ISearchParams;
    setSearchParams: (newSearchParams: (prevState: ISearchParams) => any) => void;
};

const SearchPage: React.FC<Props> = ({addToCart, isInCart, searchParams, setSearchParams}) => {
    const [searchProductsInfo, setSearchProductsInfo] = useState(null as IProductsResponse | null);
    const [middlePageNumber, setMiddlePageNumber] = useState(searchParams.pageNum + 1);
    const [maxPageNumber, setMaxPageNumber] = useState(1);

    useEffect(() => {
        getSearchProducts(searchParams)
            .then((productsResponse) => {
                setSearchProductsInfo(productsResponse);
                setMaxPageNumber(Math.ceil(productsResponse.paginationDTO.totalResults / searchParams.pageSize));
            })
            .catch(() => setSearchProductsInfo(null));
    }, [searchParams]);

    if (!searchProductsInfo) {
        return null;
    }

    const handleChangeCurrentPageSize = (newPageSize: number) => {
        setSearchParams(prevState => ({
            ...prevState, pageSize: newPageSize
        }));
    };

    const handleChangeCurrentPageNumber = (newPageNumber: number) => {
        setSearchParams(prevState => ({
            ...prevState, pageNum: newPageNumber
        }));
    };

    const handleChangeCurrentCategoryID = (newCategoryID: string) => {
        setSearchParams(prevState => ({
            ...prevState, categoryId: newCategoryID
        }));
    };

    const handleChangePriceFrom = (newPriceFrom: number | null) => {
        setSearchParams(prevState => ({
            ...prevState, priceFrom: newPriceFrom
        }));
    };

    const handleChangePriceTo = (newPriceTo: number | null) => {
        setSearchParams(prevState => ({
            ...prevState, priceTo: newPriceTo
        }));
    };

    const handleChangeSortingMode = (newSortingMode: string) => {
        setSearchParams(prevState => ({
            ...prevState, sortDescending: newSortingMode
        }));
    };

    return (
        <div className="container mt-2">
            <div className="h2 text-primary"> Produkty</div>
            <FilterBar
                currentPageSize={searchParams.pageSize}
                setPageSize={handleChangeCurrentPageSize}
                setCurrentPageNumber={handleChangeCurrentPageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                setCurrentCategoryID={handleChangeCurrentCategoryID}
                setSortingMode={handleChangeSortingMode}
                priceFrom={searchParams.priceFrom}
                setPriceFrom={handleChangePriceFrom}
                priceTo={searchParams.priceTo}
                setPriceTo={handleChangePriceTo}
            />
            <ProductList
                productList={searchProductsInfo.productDTOList}
                handleAddToCart={addToCart}
                isInCart={isInCart}
            />
            <PageNumberBar
                currentPageNumber={searchParams.pageNum}
                setCurrentPageNumber={handleChangeCurrentPageNumber}
                middlePageNumber={middlePageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                maxPageNumber={maxPageNumber}
            />
        </div>
    );
};

export default SearchPage;
