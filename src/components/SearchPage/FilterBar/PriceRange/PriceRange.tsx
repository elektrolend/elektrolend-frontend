import React from "react";
import NumericInput from "react-numeric-input";

import {DashLg} from "react-bootstrap-icons";

type Props = {
    priceFrom: number | null;
    setPriceFrom: (priceFrom: number | null) => void;
    priceTo: number | null;
    setPriceTo: (priceTo: number | null) => void;
    resetPagination: () => void;
};

const PriceRange: React.FC<Props> = ({priceFrom, setPriceFrom, priceTo, setPriceTo, resetPagination}) => {
    const handleChangePriceFrom = (newPriceFrom: number | null) => {
        if (newPriceFrom && priceTo) {
            setPriceFrom(Math.min(newPriceFrom, priceTo));
        } else {
            setPriceFrom(newPriceFrom);
        }
        resetPagination();
    };

    const handleChangePriceTo = (newPriceTo: number | null) => {
        if (newPriceTo && priceFrom) {
            setPriceTo(Math.max(newPriceTo, priceFrom));
        } else {
            setPriceTo(newPriceTo);
        }
        resetPagination();
    };

    const getPriceValue = (priceValue: number | null) => {
        if (!priceValue) {
            return undefined;
        }

        return priceValue;
    };

    return (
        <div className="row">
            <div className="col-5">
                <NumericInput
                    mobile
                    className="form-control"
                    value={getPriceValue(priceFrom)}
                    min={0}
                    max={getPriceValue(priceTo)}
                    placeholder={"od"}
                    onChange={(value) => handleChangePriceFrom(value)}
                />
            </div>
            <div className="col-2 align-self-center text-center">
                <DashLg size={14}/>
            </div>
            <div className="col-5">
                <NumericInput
                    mobile
                    className="form-control"
                    value={getPriceValue(priceTo)}
                    min={getPriceValue(priceFrom)}
                    placeholder={"do"}
                    onChange={(value) => handleChangePriceTo(value)}
                />
            </div>
        </div>
    );
};

export default PriceRange;
