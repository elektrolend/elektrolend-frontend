import React from "react";

import PageSizeBar from "../../Pagination/PageSizeBar";
import CategorySelect from "../../Products/UnavailableProducts/FilterBar/CategorySelect";
import PriceRange from "./PriceRange/PriceRange";
import SortBar from "./SortBar/SortBar";

type Props = {
    currentPageSize: number;
    setPageSize: (pageSize: number) => void;
    setCurrentPageNumber: (pageNumber: number) => void;
    setMiddlePageNumber: (pageNumber: number) => void;
    setCurrentCategoryID: (categoryID: string) => void;
    setSortingMode: (sortingMode: string) => void;
    priceFrom: number | null;
    setPriceFrom: (priceFrom: number | null) => void;
    priceTo: number | null;
    setPriceTo: (priceTo: number | null) => void;
};

const FilterBar: React.FC<Props> = ({
                                        currentPageSize,
                                        setPageSize,
                                        setCurrentPageNumber,
                                        setMiddlePageNumber,
                                        setCurrentCategoryID,
                                        setSortingMode,
                                        priceFrom,
                                        setPriceFrom,
                                        priceTo,
                                        setPriceTo
                                    }) => {

    const resetPagination = () => {
        setCurrentPageNumber(1);
        setMiddlePageNumber(2);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-6 mt-2">
                        <h6>Cena (zł):</h6>
                        <PriceRange
                            priceFrom={priceFrom}
                            setPriceFrom={setPriceFrom}
                            priceTo={priceTo}
                            setPriceTo={setPriceTo}
                            resetPagination={resetPagination}
                        />
                    </div>
                    <div className="col-md-6 mt-2">
                        <h6>Kategoria: </h6>
                        <CategorySelect
                            setCurrentCategoryID={setCurrentCategoryID}
                            resetPagination={resetPagination}
                        />
                    </div>
                    <div className="col-md-6 mt-2">
                        <h6>Sortowanie: </h6>
                        <SortBar
                            setSortingMode={setSortingMode}
                            resetPagination={resetPagination}
                        />
                    </div>
                    <div className="col-md-6 justify-content-end align-items-center text-left mt-2 mb-2">
                        <h6>Rozmiar: </h6>
                        <PageSizeBar
                            currentPageSize={currentPageSize}
                            setPageSize={setPageSize}
                            resetPagination={resetPagination}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FilterBar;
