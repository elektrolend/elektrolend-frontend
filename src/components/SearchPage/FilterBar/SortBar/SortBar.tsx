import React from "react";

import SortingModes from "./SortingModes";

type Props = {
    setSortingMode: (sortingMode: string) => void;
    resetPagination: () => void;
};

const SortBar: React.FC<Props> = ({setSortingMode, resetPagination}) => {

    const handleChangeSortingMode = (newSortingMode: string) => {
        setSortingMode(newSortingMode);
        resetPagination();
    };

    return (
        <select
            onChange={(e) => handleChangeSortingMode(e.target.value)}
            className="custom-select"
        >
            {SortingModes.map((sortingMode, key) => {
                return (
                    <option
                        value={sortingMode.value}
                        key={key}
                    >
                        {sortingMode.modeName}
                    </option>
                )
            })}
        </select>
    );
};

export default SortBar;
