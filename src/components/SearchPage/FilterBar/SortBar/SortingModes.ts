interface SortingMode {
    modeName: string;
    value: string;
}

const SortingModes: SortingMode[] = [
    {
        modeName: "Domyślne",
        value: ""
    },
    {
        modeName: "Cena - od najniższej",
        value: "true"
    },
    {
        modeName: "Cena - od najwyższej",
        value: "false"
    }
]

export default SortingModes;
