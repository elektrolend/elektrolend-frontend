import React from "react";
import {isEmptyArray} from "formik";

import Product from "../../Products/Product";
import {IProduct} from "../../../interfaces/Products/IProduct";

import {BagX} from "react-bootstrap-icons";

type Props = {
    productList: IProduct[];
    handleAddToCart: (clickedProduct: IProduct, beginDate: Date, endDate: Date) => void;
    isInCart: (id: number) => boolean;
};

const ProductList: React.FC<Props> = ({productList, handleAddToCart, isInCart}) => {
    if (isEmptyArray(productList)) {
        return (
            <div className="border mb-3">
                <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                    <BagX
                        className="col-md-12 mb-2"
                        size={50}/>
                    <h3 className="col-md-12">
                        Brak wyników!
                    </h3>
                    <p className="col-md-12">
                        Spróbuj zmienić parametry filtrowania na inne.
                    </p>
                </div>
            </div>
        );
    }

    return (
        <div className="border mb-3">
            {productList.map((product, index) => {
                return (
                    <Product
                        product={product}
                        handleAddToCart={handleAddToCart}
                        isInCart={isInCart}
                        key={"product_" + index}
                    />
                );
            })}
        </div>
    );
};

export default ProductList;
