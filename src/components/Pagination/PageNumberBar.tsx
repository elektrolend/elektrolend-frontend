import React from "react";

type Props = {
    currentPageNumber: number;
    setCurrentPageNumber: (pageNumber: number) => void;
    middlePageNumber: number;
    setMiddlePageNumber: (pageNumber: number) => void;
    maxPageNumber: number;
}

const PageNumberBar: React.FC<Props> = ({
                                            currentPageNumber,
                                            setCurrentPageNumber,
                                            middlePageNumber,
                                            setMiddlePageNumber,
                                            maxPageNumber
                                        }) => {

    const isItemActive = (pageNumber: number) => {
        if (pageNumber === currentPageNumber) {
            return "active";
        }
        return "";
    };

    const isPreviousDisabled = () => {
        return middlePageNumber === 2 ? "disabled" : "";
    };

    const isNextDisabled = () => {
        if (middlePageNumber >= maxPageNumber) {
            return "disabled";
        }
        return middlePageNumber === maxPageNumber - 1 ? "disabled" : "";
    };

    const isPreviousMaxPage = (pageNumber: number) => {
        return pageNumber <= maxPageNumber;
    };

    const changeCurrentPageNumber = (pageNumber: number) => {
        if (pageNumber > 0) {
            setCurrentPageNumber(pageNumber);
            window.scrollTo(0, 0);
        }
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2 justify-content-end">
                    <nav aria-label="Page navigation example">
                        <ul className="pagination">
                            <li className={"page-item " + isPreviousDisabled()}>
                                <button
                                    className="page-link"
                                    onClick={() => setMiddlePageNumber(middlePageNumber - 1)}>
                                    &lt;&lt;
                                </button>
                            </li>
                            <li className={"page-item " + isItemActive(middlePageNumber - 1)}>
                                <button
                                    className="page-link"
                                    onClick={() => changeCurrentPageNumber(middlePageNumber - 1)}>
                                    {middlePageNumber - 1}
                                </button>
                            </li>
                            {isPreviousMaxPage(middlePageNumber) ?
                                <li className={"page-item " + isItemActive(middlePageNumber)}>
                                    <button
                                        className="page-link"
                                        onClick={() => changeCurrentPageNumber(middlePageNumber)}>
                                        {middlePageNumber}
                                    </button>
                                </li> : null}
                            {isPreviousMaxPage(middlePageNumber + 1) ?
                                <li className={"page-item " + isItemActive(middlePageNumber + 1)}>
                                    <button
                                        className="page-link"
                                        onClick={() => changeCurrentPageNumber(middlePageNumber + 1)}>
                                        {middlePageNumber + 1}
                                    </button>
                                </li> : null}
                            <li className={"page-item " + isNextDisabled()}>
                                <button
                                    className="page-link"
                                    onClick={() => setMiddlePageNumber(middlePageNumber + 1)}>
                                    &gt;&gt;
                                </button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
}

export default PageNumberBar;
