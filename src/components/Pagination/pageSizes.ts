interface IPageSizeInfo {
    title: string;
    value: number;
}

const pageSizes: IPageSizeInfo[] = [
    {
        title: "5 pozycji na stronie",
        value: 5
    },
    {
        title: "10 pozycji na stronie",
        value: 10
    },
    {
        title: "20 pozycji na stronie",
        value: 20
    },
    {
        title: "50 pozycji na stronie",
        value: 50
    }
]

export default pageSizes;
