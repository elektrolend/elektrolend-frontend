import React from "react";

import pageSizes from "./pageSizes";

type Props = {
    currentPageSize: number;
    setPageSize: (pageSize: number) => void;
    resetPagination: () => void;
}

const PageSizeBar: React.FC<Props> = ({currentPageSize, setPageSize, resetPagination}) => {
    const handleChangePageSize = (newPageSize: number) => {
        resetPagination();
        setPageSize(newPageSize);
    };

    return (
        <select
            className="custom-select"
            id="inputGroupSelect02"
            value={currentPageSize}
            onChange={(e) => handleChangePageSize(Number(e.target.value))}>
            {pageSizes.map((pageSize, index) => {
                return (
                    <option
                        key={"pageSize_" + index}
                        value={pageSize.value}>
                        {pageSize.title}
                    </option>
                )
            })}
        </select>
    );
};

export default PageSizeBar;
