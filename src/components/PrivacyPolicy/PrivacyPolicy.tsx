import {Component} from 'react';

class PrivacyPolicy extends Component {
    render() {
        return (
            <div className="container mt-2">
                <div className="h2 text-primary"> Polityka prywatności</div>
                <div>
                    <p>
                        Polityka cookies sporządzona została zgodnie z obowiązkami wynikającymi z nowelizacji Prawa
                        Telekomunikacyjnego, które weszły w życie 22 marca 2013 roku.
                        Obowiązki te podobnie są realizowane w całej Europie wskutek implementowania dyrektywy
                        europejskiej.
                    </p>
                    <p>
                        Niniejsza polityka cookies dotyczy usług oferowanych w ramach tej strony (Serwis).
                    </p>
                    <p>
                        1. Serwis nie zbiera w sposób automatyczny żadnych informacji,
                        z wyjątkiem informacji zawartych w plikach cookies.
                    </p>
                    <p>
                        2. Pliki cookies (tzw. ciasteczka) stanowią dane informatyczne, w szczególności pliki tekstowe,
                        które przechowywane są w urządzeniu końcowym Użytkownika Serwisu i przeznaczone są do
                        korzystania ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony
                        internetowej, z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz
                        unikalny numer.
                    </p>
                    <p>
                        3. Podmiotem zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies
                        oraz uzyskującym do nich dostęp jest operator tej strony.
                    </p>
                    <p>
                        4. Pliki cookies wykorzystywane są w celu:
                    </p>
                    <ul>
                        <li>
                            dostosowania zawartości stron internetowych do preferencji Użytkownika oraz optymalizacji
                            korzystania ze stron internetowych; w szczególności pliki te pozwalają rozpoznać urządzenie
                            Użytkownika Serwisu i odpowiednio wyświetlić stronę internetową, dostosowaną do jego
                            indywidualnych potrzeb;
                        </li>
                        <li>
                            tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają
                            ze stron internetowych, co umożliwia ulepszanie ich struktury i zawartości;
                        </li>
                    </ul>
                    <p>
                        5. W ramach Serwisu stosowane są dwa zasadnicze typy plików cookies: sesyjne (session cookies)
                        oraz stałe (persistent cookies). Cookies sesyjne są plikami tymczasowymi, które przechowywane są
                        w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej lub
                        wyłączenia oprogramowania (przeglądarki internetowej). Stałe pliki cookies przechowywane są w
                        urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu
                        ich usunięcia przez Użytkownika.
                    </p>
                    <p>
                        6. W ramach Serwisu stosowane są następujące rodzaje plików cookies:
                    </p>
                    <ul>
                        <li>
                            niezbędne pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach Serwisu, np.
                            uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania w
                            ramach Serwisu;
                        </li>
                        <li>
                            pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania
                            nadużyć w zakresie uwierzytelniania w ramach Serwisu;
                        </li>
                        <li>
                            wydajnościowe pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze
                            stron internetowych Serwisu;
                        </li>
                        <li>
                            funkcjonalne pliki cookies, umożliwiające zapamiętanie wybranych przez Użytkownika ustawień
                            i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z
                            którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.;
                        </li>
                        <li>
                            reklamowe pliki cookies, umożliwiające dostarczanie Użytkownikom treści reklamowych bardziej
                            dostosowanych do ich zainteresowań.
                        </li>
                    </ul>
                    <p>
                        7. W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka
                        internetowa) domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym
                        Użytkownika. Użytkownicy Serwisu mogą dokonać w każdym czasie zmiany ustawień dotyczących plików
                        cookies.Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować
                        automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej bądź informować o
                        ich każdorazowym zamieszczeniu w urządzeniu Użytkownika Serwisu. Szczegółowe informacje o
                        możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania
                        (przeglądarki internetowej).
                    </p>
                    <p>
                        8. Operator Serwisu informuje, że ograniczenia stosowania plików cookies mogą wpłynąć na
                        niektóre funkcjonalności dostępne na stronach internetowych Serwisu.
                    </p>
                    <p>
                        9. Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane mogą
                        być również przez współpracujących z operatorem Serwisu reklamodawców oraz partnerów.
                    </p>
                    <p>
                        10. Więcej informacji na temat plików cookies dostępnych jest pod adresem
                        www.wszystkoociasteczkach.pl lub w sekcji Pomoc w menu przeglądarki internetowej.
                    </p>

                    <h5>Dane osobowe</h5>
                    <p>
                        1. Operatorem Serwisu www.elektrolend.pl, jest Elektrolend Jan Kowalski 12-123 Reimerswaal,
                        ul. Słoneczna 1
                    </p>
                    <p>
                        2. Serwis realizuje funkcje pozyskiwania informacji o użytkownikach i ich zachowaniu w
                        następujący sposób:
                    </p>
                    <ol type="a">
                        <li>
                            Poprzez dobrowolnie wprowadzone w formularzach informacje.
                        </li>
                        <li>
                            Poprzez zapisywanie w urządzeniach końcowych pliki cookie (tzw. “ciasteczka”).
                        </li>
                        <li>
                            Poprzez gromadzenie logów serwera www przez operatora hostingowego NAJLEPSZY-HOSTING S.A.,
                            funkcjonującego pod adresem https://hosting-najlepszy.pl
                        </li>
                    </ol>
                    <p>
                        3. Informacje w formularzach.
                    </p>
                    <ol type="a">
                        <li>
                            Serwis zbiera informacje podane dobrowolnie przez użytkownika.
                        </li>
                        <li>
                            Serwis może zapisać ponadto informacje o parametrach połączenia (oznaczenie czasu, adres IP)
                        </li>
                        <li>
                            Dane w formularzu nie są udostępniane podmiotom trzecim inaczej, niż za zgodą użytkownika.
                        </li>
                        <li>
                            Dane podane w formularzu są przetwarzane w celu wynikającym z funkcji konkretnego
                            formularza, np. kontaktu handlowego.
                        </li>
                    </ol>
                    <p>
                        4. Informacja o plikach cookies.
                    </p>
                    <ol type="a">
                        <li>
                            Serwis korzysta z plików cookies.
                        </li>
                        <li>
                            Pliki cookies (tzw. “ciasteczka”) stanowią dane informatyczne, w szczególności pliki
                            tekstowe, które przechowywane są w urządzeniu końcowym Użytkownika Serwisu i przeznaczone są
                            do korzystania ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony
                            internetowej, z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz
                            unikalny numer.
                        </li>
                        <li>
                            Podmiotem zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies oraz
                            uzyskującym do nich dostęp jest operator Serwisu
                        </li>
                        <li>
                            Pliki cookies wykorzystywane są w następujących celach:
                        </li>
                        <ol type="i">
                            <li>
                                tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu
                                korzystają ze stron internetowych, co umożliwia ulepszanie ich struktury i
                                zawartości;
                            </li>
                            <li>
                                utrzymanie sesji Użytkownika Serwisu (po zalogowaniu), dzięki której Użytkownik
                                nie musi na każdej podstronie Serwisu ponownie wpisywać loginu i hasła;
                            </li>
                            <li>
                                określania profilu użytkownika w celu wyświetlania mu dopasowanych materiałów .
                            </li>
                        </ol>
                    </ol>
                    <p>
                        5. W ramach Serwisu stosowane są dwa zasadnicze rodzaje plików cookies: “sesyjne” (session
                        cookies) oraz “stałe” (persistent cookies). Cookies “sesyjne” są plikami tymczasowymi, które
                        przechowywane są w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony
                        internetowej lub wyłączenia oprogramowania (przeglądarki internetowej). “Stałe” pliki
                        cookies przechowywane są w urządzeniu końcowym Użytkownika przez czas określony w parametrach
                        plików cookies lub do czasu ich usunięcia przez Użytkownika.
                    </p>
                    <p>
                        6. Oprogramowanie do przeglądania stron internetowych (przeglądarka internetowa) zazwyczaj
                        domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym Użytkownika.
                        Użytkownicy Serwisu mogą dokonać zmiany ustawień w tym zakresie. Przeglądarka internetowa
                        umożliwia usunięcie plików cookies. Możliwe jest także automatyczne blokowanie plików cookies
                        Szczegółowe informacje na ten temat zawiera pomoc lub dokumentacja przeglądarki internetowej.
                    </p>
                    <p>
                        7. Ograniczenia stosowania plików cookies mogą wpłynąć na niektóre funkcjonalności dostępne
                        na stronach internetowych Serwisu.
                    </p>
                    <p>
                        8. Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane
                        mogą być również przez współpracujących z operatorem Serwisu reklamodawców oraz partnerów.
                    </p>
                    <p>
                        5. Udostępnienie danych
                    </p>
                    <ol type="a">
                        <li>
                            Dane podlegają udostępnieniu podmiotom zewnętrznym wyłącznie w granicach prawnie
                            dozwolonych.
                        </li>
                        <li>
                            Dane umożliwiające identyfikację osoby fizycznej są udostępniane wyłączenie za zgodą tej
                            osoby.
                        </li>
                        <li>
                            Operator może mieć obowiązek udzielania informacji zebranych przez Serwis upoważnionym
                            organom na podstawie zgodnych z prawem żądań w zakresie wynikającym z żądania.
                        </li>
                    </ol>
                    <p>
                        6. Zarządzanie plikami cookies – jak w praktyce wyrażać i cofać zgodę?
                    </p>
                    <ol type="a">
                        <li>
                            Jeśli użytkownik nie chce otrzymywać plików cookies, może zmienić ustawienia przeglądarki.
                            Zastrzegamy, że wyłączenie obsługi plików cookies niezbędnych dla procesów uwierzytelniania,
                            bezpieczeństwa, utrzymania preferencji użytkownika może utrudnić, a w skrajnych przypadkach
                            może uniemożliwić korzystanie ze stron www.
                        </li>
                    </ol>
                </div>
            </div>
        );
    }

}

export default PrivacyPolicy
