import {AccountType} from "../../../interfaces/Accounts/AccountType";

interface IEmployeeInfo {
    title: string;
    accountType: AccountType;
}

const employeeTypes: IEmployeeInfo[] = [
    {
        title: "Pracownik",
        accountType: AccountType.regular
    },
    {
        title: "Administrator",
        accountType: AccountType.admin
    }
];

export default employeeTypes;
