interface IEmployeeDataField {
    label: string,
    name: string,
    type: string,
}

const employeeDataFields: IEmployeeDataField[] = [
    {
        label: "Nazwa użytkownika",
        name: "nickname",
        type: "text",
    },
    {
        label: "E-mail",
        name: "email",
        type: "text",

    },
    {
        label: "Hasło",
        name: "password",
        type: "password",
    },
    {
        label: "Powtórz hasło",
        name: "repeatedPassword",
        type: "password",
    },
    {
        label: "Imię",
        name: "name",
        type: "text",
    },
    {
        label: "Nazwisko",
        name: "surname",
        type: "text",
    },
    {
        label: "Telefon",
        name: "phone",
        type: "number",
    }
]

export default employeeDataFields;
