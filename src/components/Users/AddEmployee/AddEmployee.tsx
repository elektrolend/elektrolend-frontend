import React, {useState} from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";

import SpinnerButton from "../../Utils/SpinnerButton";
import {addEmployee} from "../../../services/database.service";
import employeeTypes from "./employeeTypes";
import employeeDataFields from "./employeeDataFields";
import EmployeeValidationSchema from "./EmployeeValidationSchema";

const AddEmployee = () => {
    const [errorMessage, setErrorMessage] = useState("");
    const [state, setState] = useState(0);

    const onSubmit = (values: any) => {
        setErrorMessage("");
        setState(100);
        addEmployee(values)
            .then((response) => {
                if (response.status === 201) {
                    setState(response.status);
                }
            })
            .catch((error) => {
                if (error.response.status < 500) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                } else {
                    setState(500);
                    setErrorMessage("Brak połączenia z serwerem.");
                }
            });
    };

    if (state === 201) {
        return (
            <div className="m-5">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading">Sukces!</h4>
                    <p>Udało Ci się poprawnie dodać nowego pracownika.</p>
                    <hr></hr>
                    <p className="mb-0">
                        Wróc do panelu <a className="hover-move" href="/employee-panel">Administratora</a>.
                    </p>
                </div>
            </div>
        );
    }

    return (
        <div className="container mt-2 mb-2">
            <div className="border">
                <Formik
                    initialValues={{
                        nickname: "",
                        password: "",
                        repeatedPassword: "",
                        phone: "",
                        email: "",
                        name: "",
                        surname: "",
                        employeeType: ""
                    }}
                    validationSchema={EmployeeValidationSchema}
                    onSubmit={values => {
                        onSubmit(values)
                    }}>
                    {() => (
                        <Form>
                            <div className="bg-primary text-white p-2 m-2">
                                <h4>Nowy pracownik</h4>
                            </div>
                            <div className="bg-light p-2 m-2">
                                <div className="row">
                                    {employeeDataFields.map((field, index) => {
                                        return (
                                            <div className="col-md-6 form-group" key={index}>
                                                <label className="form-label">{field.label}</label>
                                                <Field
                                                    name={field.name}
                                                    type={field.type}
                                                    className="form-control"
                                                    key={"employeeDataField_" + index}
                                                />
                                                <ErrorMessage render={msg => <div
                                                    className="alert alert-danger">{msg}</div>}
                                                              name={field.name}/>
                                            </div>
                                        )
                                    })}
                                    <div className="col-md-6 form-group">
                                        <label className="form-label">Typ konta</label>
                                        <Field
                                            as="select"
                                            className="form-control"
                                            name="employeeType"
                                        >
                                            <option value="" defaultChecked>
                                                Wybierz typ konta
                                            </option>
                                            {employeeTypes.map((employeeType, index) => {
                                                return (
                                                    <option value={employeeType.accountType}>
                                                        {employeeType.title}
                                                    </option>
                                                );
                                            })}
                                        </Field>
                                        <ErrorMessage render={msg => <div
                                            className="alert alert-danger">{msg}</div>}
                                                      name="employeeType"/>
                                    </div>
                                    {errorMessage ?
                                        <div className="col-md-12">
                                            <div className="alert alert-danger" role="alert">
                                                {errorMessage}
                                            </div>
                                        </div>
                                        : null}
                                    <div className="col-12">
                                        <SpinnerButton
                                            state={state}
                                            buttonText={"Dodaj nowego pracownika"}
                                        />
                                    </div>

                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};
export default AddEmployee;
