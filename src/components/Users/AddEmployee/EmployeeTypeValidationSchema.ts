import * as Yup from "yup";

import {AccountType} from "../../../interfaces/Accounts/AccountType";

const EmployeeTypeValidationSchema = Yup.object().shape({
    employeeType: Yup.string()
        .required("Typ pracownika jest wymagany!")
        .oneOf([AccountType.regular, AccountType.admin], "Dozwolone typy konta to pracownik oraz administrator!")
});

export default EmployeeTypeValidationSchema;
