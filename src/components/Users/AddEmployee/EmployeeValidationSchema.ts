import EmployeeTypeValidationSchema from "./EmployeeTypeValidationSchema";
import BasicInfoValidationSchema from "../../Auth/BasicInfoValidationSchema";
import PasswordValidationSchema from "../../Auth/PasswordValidationSchema";
import LoginValidationSchema from "../../Auth/LoginValidationSchema";

const EmployeeValidationSchema = PasswordValidationSchema
    .concat(LoginValidationSchema)
    .concat(BasicInfoValidationSchema)
    .concat(EmployeeTypeValidationSchema);


export default EmployeeValidationSchema;
