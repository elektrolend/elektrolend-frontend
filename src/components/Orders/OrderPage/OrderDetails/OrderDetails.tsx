import React from "react";

import {IOrder} from "../../../../interfaces/Orders/IOrder";

import {CalendarDay, CardList, Wallet2} from "react-bootstrap-icons";

type Props = {
    order: IOrder;
    summaryPrice: number;
};

const OrderDetails: React.FC<Props> = ({order, summaryPrice}) => {
    const getDate = () => {
        return new Date(order.orderDate).toLocaleDateString();
    };

    return (
        <div className="border mb-3">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Szczegóły zamówienia</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-6">
                        <h5>
                            <CardList className="mr-1 mb-1"/>
                            <strong>ID</strong>
                        </h5>
                        <p>
                            {order.id}
                        </p>
                    </div>
                    <div className="col-md-6">
                        <h5>
                            <CalendarDay className="mr-1 mb-1"/>
                            <strong>Data</strong>
                        </h5>
                        <p>{getDate()}</p>
                    </div>
                    <div className="col-md-6">
                        <h5>
                            <Wallet2 className="mr-1 mb-1"/>
                            <strong>Wartość</strong>
                        </h5>
                        <p>{summaryPrice} PLN</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderDetails;
