import React, {useEffect, useState} from "react";

import {getUser} from "../../../../services/database.service";
import {IUserProfile} from "../../../../interfaces/Accounts/IUserProfile";

import {Envelope, PersonCircle, Telephone} from "react-bootstrap-icons";

type Props = {
    customerID: number;
};

const CustomerDetails: React.FC<Props> = ({customerID}) => {
    const [customer, setCustomer] = useState(null as IUserProfile | null);

    useEffect(() => {
        getUser(customerID)
            .then(user => setCustomer(user))
            .catch(() => setCustomer(null));
    }, [customerID]);

    if (!customer) {
        return null;
    }

    return (
        <div className="border mb-3">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Dane klienta</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <div className="row">
                    <div className="col-md-4">
                        <h5>
                            <PersonCircle className="mr-1 mb-1"/>
                            <strong>Imię</strong>
                        </h5>
                        <p>
                            {customer.name}
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h5>
                            <PersonCircle className="mr-1 mb-1"/>
                            <strong>Nazwisko</strong>
                        </h5>
                        <p>
                            {customer.surname}
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h5>
                            <Telephone className="mr-1 mb-1"/>
                            <strong>Telefon</strong>
                        </h5>
                        <p>
                            {customer.phone}
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h5>
                            <PersonCircle className="mr-1 mb-1"/>
                            <strong>Pseudonim</strong>
                        </h5>
                        <p>
                            {customer.nickname}
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h5>
                            <Envelope className="mr-1 mb-1"/>
                            <strong>E-mail</strong>
                        </h5>
                        <p>
                            {customer.email}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CustomerDetails;
