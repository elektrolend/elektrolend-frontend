import React from "react";
import {isEmptyArray} from "formik";

import Header from "./Header/Header";
import Body from "./Body/Body";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";

import {ExclamationTriangleFill} from "react-bootstrap-icons";

type Props = {
    orderProducts: IOrderProduct[];
};

const Table: React.FC<Props> = ({orderProducts}) => {
    if (isEmptyArray(orderProducts)) {
        return (
            <div className="row p-2 m-2 justify-content-center">
                <div className="p-4 bg-light row m-5 text-center">
                    <ExclamationTriangleFill
                        className="col-md-12 mb-2"
                        size={50}/>
                    <h3 className="col-md-12">Coś poszło nie tak...</h3>
                    <p className="col-md-12">
                        Nie znaleziono powiązanych z tym zamówieniem produktów.
                    </p>
                </div>
            </div>
        );
    }

    return (
        <div className="row p-2 m-2">
            <div className="table-responsive">
                <table className="table">
                    <Header/>
                    <Body orderProducts={orderProducts}/>
                </table>
            </div>
        </div>
    );
};

export default Table;
