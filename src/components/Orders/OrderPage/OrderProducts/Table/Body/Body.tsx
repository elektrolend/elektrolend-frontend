import React from "react";

import OrderProductItem from "./OrderProductItem";
import {IOrderProduct} from "../../../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProducts: IOrderProduct[];
};

const Body: React.FC<Props> = ({orderProducts}) => {
    return (
        <tbody className="bg-white">
        {orderProducts.map((orderProduct, index) => {
            return (
                <OrderProductItem
                    orderProduct={orderProduct}
                    key={"orderProduct_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default Body;
