import React, {useEffect, useState} from "react";
import {generatePath, Link} from "react-router-dom";

import StateButton from "../../../../ChangeStates/OrderProductsTable/TableBody/TableBodyItem/StateButton";
import {getProduct} from "../../../../../../services/database.service";
import serviceSecurityTypes
    from "../../../../UserOrders/UserOrdersTable/OrderInfo/OrderProducts/OrderItem/serviceSecurityTypes";
import {IProduct} from "../../../../../../interfaces/Products/IProduct";
import {IOrderProduct} from "../../../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProduct: IOrderProduct;
};

const OrderProductItem: React.FC<Props> = ({orderProduct}) => {
    const [product, setProduct] = useState(null as IProduct | null);

    useEffect(() => {
        getProduct(orderProduct.productID)
            .then(p => setProduct(p))
            .catch(() => setProduct(null));
    }, [orderProduct.productID]);

    if (!product) {
        return null;
    }

    const getDate = (date: string) => {
        return new Date(date).toLocaleDateString();
    };

    return (
        <tr className="border text-center rounded-bottom">
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {orderProduct.id}
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                <Link
                    to={generatePath(`/product/details/:id/:name`,
                        {id: orderProduct.productID, name: product.name})}
                    className="text-dark"
                    target="_blank"
                >
                    {product.name}
                </Link>
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {getDate(orderProduct.orderBeginDate)} - {getDate(orderProduct.orderEndDate)}
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {serviceSecurityTypes.get(orderProduct.serviceSecurityType)?.title}
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {orderProduct.price} PLN
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}>
                <StateButton orderProductState={orderProduct.orderProductState}/>
            </td>
        </tr>
    );
};

export default OrderProductItem;
