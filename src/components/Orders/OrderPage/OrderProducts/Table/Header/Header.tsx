const Header = () => {
    return (
        <thead
            className="text-center text-white border-bottom-2"
            style={{backgroundColor: "#48ABFF"}}>
        <tr>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="border-right col-md-1">
                ID
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="border-right col-md-3">
                PRODUKT
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="border-right col-md-3">
                OKRES
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="border-right col-md-1">
                ZABEZPIECZENIE
            </th>
            <th
                scope="col"
                style={{verticalAlign: "middle"}}
                className="border-right col-md-2">
                CENA
            </th>
            <th
                scope="col"
                className="col-md-2"
                style={{verticalAlign: "middle"}}>
                STAN
            </th>
        </tr>
        </thead>
    );
};

export default Header;
