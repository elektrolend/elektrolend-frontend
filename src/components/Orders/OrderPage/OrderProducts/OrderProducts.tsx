import React from "react";

import Table from "./Table/Table";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProductList: IOrderProduct[];

};

const OrderProducts: React.FC<Props> = ({orderProductList}) => {
    return (
        <div className="border mb-3">
            <div className="bg-primary text-white p-2 m-2">
                <h4>Zamówione produkty</h4>
            </div>
            <div className="bg-light p-2 m-2">
                <Table orderProducts={orderProductList}/>
            </div>
        </div>
    );
};

export default OrderProducts;
