import React, {useEffect, useState} from "react";

import OrderDetails from "./OrderDetails/OrderDetails";
import OrderProducts from "./OrderProducts/OrderProducts";
import CustomerDetails from "./CustomerDetails/CustomerDetails";
import {getOrderProducts} from "../../../services/database.service";
import {IOrderSummary} from "../../../interfaces/Orders/IOrderSummary";

import {ArrowLeftCircleFill} from "react-bootstrap-icons";

type Props = {
    orderID: number;
    setCurrentOrderID: (id: number | null) => void;
};

const OrderPage: React.FC<Props> = ({orderID, setCurrentOrderID}) => {
    const [orderSummary, setOrderSummary] = useState(null as IOrderSummary | null);

    useEffect(() => {
        getOrderProducts(orderID)
            .then(order => setOrderSummary(order))
            .catch(() => setOrderSummary(null));
    }, [orderID]);

    if (!orderID || !orderSummary) {
        return null;
    }


    const getOrderSummaryPrice = () => {
        return orderSummary.orderProductList
            .reduce((summaryPrice, orderProduct) => {
                summaryPrice += orderProduct.price;
                return summaryPrice;
            }, 0);
    };

    return (
        <>
            <div>
                <h2 className="text-primary">Zamówienie nr. {orderID}</h2>
                <h6 className="mb-10" onClick={() => setCurrentOrderID(null)}>
                    <ArrowLeftCircleFill className="mb-1 mr-1"/>
                    POWRÓT DO LISTY
                </h6>
            </div>
            <div className="row">
                <div className="col-5">
                    <OrderDetails
                        order={orderSummary.order}
                        summaryPrice={getOrderSummaryPrice()}
                    />
                </div>
                <div className="col-7">
                    <CustomerDetails
                        customerID={orderSummary.order.customerID}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <OrderProducts orderProductList={orderSummary.orderProductList}/>
                </div>
            </div>
        </>
    );
};

export default OrderPage;
