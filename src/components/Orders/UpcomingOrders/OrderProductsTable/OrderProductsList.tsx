import React from "react";

import OrderProductItem from "./OrderProductItem/OrderProductItem";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProductsList: IOrderProduct[];
    currentFilterMode: OrderProductState;
    addToList: (orderProduct: IOrderProduct) => void;
    removeFromList: (id: number) => void;
    isInList: (orderProductID: number) => boolean;
};

const OrderProductsList: React.FC<Props> = ({
                                                orderProductsList,
                                                currentFilterMode,
                                                addToList,
                                                removeFromList,
                                                isInList,
                                            }) => {
    return (
        <tbody className="bg-white">
        {orderProductsList.map((o, index) => {
            return (
                <OrderProductItem
                    orderProductInfo={o}
                    currentFilterMode={currentFilterMode}
                    addToList={addToList}
                    removeFromList={removeFromList}
                    isInList={isInList}
                    key={"item_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default OrderProductsList;
