import React, {useState} from "react";

import PasswordBox from "./PasswordBox";
import {getProduct} from "../../../../../services/database.service";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";
import {IProduct} from "../../../../../interfaces/Products/IProduct";
import {OrderProductState} from "../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProductInfo: IOrderProduct;
    currentFilterMode: OrderProductState;
    addToList: (orderProduct: IOrderProduct) => void;
    removeFromList: (id: number) => void;
    isInList: (orderProductID: number) => boolean;
};

const OrderProductItem: React.FC<Props> = ({
                                               orderProductInfo,
                                               currentFilterMode,
                                               addToList,
                                               removeFromList,
                                               isInList,
                                           }) => {
    const [product, setProduct] = useState({} as IProduct);

    React.useEffect(() => {
        getProduct(orderProductInfo.productID).then(p => setProduct(p));
    }, [orderProductInfo.productID]);

    const handleChange = (e: any, orderProduct: IOrderProduct) => {
        if (!e.target.checked) {
            removeFromList(orderProduct.id);
        } else {
            addToList(orderProduct);
        }
    };

    const isPaid = () => {
        return currentFilterMode === OrderProductState.paid;
    };


    const getDate = () => {
        return isPaid() ? orderProductInfo.orderBeginDate : orderProductInfo.orderEndDate;
    };

    const getOfficeBoxID = () => {
        return isPaid() ? orderProductInfo.officeBoxIDStart : orderProductInfo.officeBoxIDEnd;
    };

    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">
                <p>
                    <input
                        type="checkbox"
                        onChange={(e) => handleChange(e, orderProductInfo)}
                        checked={isInList(orderProductInfo.id)}>
                    </input>
                </p>
            </td>
            <td className="border-right">
                <p>{product.name}</p>
            </td>
            <td className="border-right">
                <p>{getDate()}</p>
            </td>
            <td className="border-right">
                <p>{getOfficeBoxID()}</p>
            </td>
            <td>
                <PasswordBox boxPassword={orderProductInfo.boxPassword}/>
            </td>
        </tr>

    );
};

export default OrderProductItem;
