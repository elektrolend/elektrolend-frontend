import React, {useState} from "react";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";

type Props = {
    boxPassword: string;
};

const PasswordBox: React.FC<Props> = ({boxPassword}) => {
    const [showPassword, setShowPassword] = useState(false);

    return (
        <div className="form-group text-lg">
            <div className="input-group">
                <input
                    type={showPassword ? "text" : "password"}
                    className="form-control"
                    data-toggle="password"
                    disabled={true}
                    value={boxPassword}>
                </input>
                <div className="input-group-append">
                    <div
                        className="input-group-text"
                        onClick={() => setShowPassword(!showPassword)}
                    >
                        <FontAwesomeIcon
                            className="icon"
                            icon={showPassword ? faEyeSlash : faEye}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PasswordBox;
