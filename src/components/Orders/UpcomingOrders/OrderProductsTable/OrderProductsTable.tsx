import React from "react";
import {isEmptyArray} from "formik";

import OrderProductsList from "./OrderProductsList";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

import {BagCheckFill} from "react-bootstrap-icons";

type Props = {
    filteredOrderProducts: IOrderProduct[];
    currentFilterMode: OrderProductState;
    addToList: (orderProduct: IOrderProduct) => void;
    removeFromList: (id: number) => void;
    isInList: (orderProductID: number) => boolean;
};

const OrderProductsTable: React.FC<Props> = ({
                                                 filteredOrderProducts,
                                                 currentFilterMode,
                                                 addToList,
                                                 removeFromList,
                                                 isInList,
                                             }) => {
    const handleChange = (e: any) => {
        if (!e.target.checked) {
            filteredOrderProducts.forEach(o => removeFromList(o.id));
        } else {
            filteredOrderProducts.forEach(o => addToList(o));
        }
    };

    const isChecked = () => {
        for (const orderProduct of filteredOrderProducts) {
            if (!isInList(orderProduct.id)) {
                return false;
            }
        }
        return true;
    };

    const isPaid = () => {
        if (currentFilterMode === OrderProductState.paid) {
            return "od";
        }
        return "do";
    };

    if (isEmptyArray(filteredOrderProducts)) {
        return (
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2">
                        <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                            <BagCheckFill
                                className="col-md-12 mb-2"
                                size={50}/>
                            <h3 className="col-md-12">To wszystko na dzisiaj! Czas na przerwę...</h3>
                            <p className="col-md-12">
                                Wszystkie produkty zostały już rozmieszczone w skrytkach.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <thead
                                className="bg-primary text-center text-white">
                            <tr>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>
                                        <input
                                            type="checkbox"
                                            onChange={(e) => handleChange(e)}
                                            checked={isChecked()}
                                        >
                                        </input>
                                    </h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>Nazwa produktu</h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>Wypożyczone {isPaid()}</h5>
                                </th>
                                <th
                                    scope="col"
                                    className="border-right">
                                    <h5>Numer skrytki</h5>
                                </th>
                                <th scope="col">
                                    <h5>Hasło do skrytki</h5>
                                </th>
                            </tr>
                            </thead>
                            <OrderProductsList
                                orderProductsList={filteredOrderProducts}
                                currentFilterMode={currentFilterMode}
                                addToList={addToList}
                                removeFromList={removeFromList}
                                isInList={isInList}
                            />
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderProductsTable;
