import React, {useState} from "react";

import FilterBar from "./FilterBar/FilterBar";
import Alert from "./Alert/Alert";
import ConfirmationBar from "./ConfirmationBar/ConfirmationBar";
import OrderProductsTable from "./OrderProductsTable/OrderProductsTable";
import {changeOrderProductsStates, getAllOrderProducts} from "../../../services/database.service";
import {IOrderProductState} from "../../../interfaces/Orders/IOrderProductState";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";
import {OrderProductState} from "../../../interfaces/Orders/OrderProductState";
import {DateManipulation} from "../../Utils/DateManipulation";

const UpcomingOrders = () => {
    const [orderProductsStates, setOrderProductsStates] = useState([] as IOrderProductState[]);
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);
    const [currentFilterMode, setCurrentFilterMode] = useState(OrderProductState.paid);
    const [currentDateMode, setCurrentDateMode] = useState(String(new Date()));

    const [status, setStatus] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        getAllOrderProducts()
            .then(items => setOrderProducts(items))
            .catch(() => []);
    }, [status, currentDateMode]);

    const getNewOrderProductState = () => {
        return currentFilterMode === OrderProductState.paid ? OrderProductState.in_office_box
            : OrderProductState.back_to_office;
    };

    const handleAddToList = (orderProduct: IOrderProduct) => {
        setOrderProductsStates(prev => {
            const isItemInCart = prev.find(i => i.orderProductID === orderProduct.id)
            if (!isItemInCart) {
                return [...prev, {
                    orderProductID: orderProduct.id,
                    orderProductState: getNewOrderProductState()
                }];
            }
            return [...prev]
        })
    };

    const handleRemoveFromList = (id: number) => {
        setOrderProductsStates(prev =>
            prev.reduce((ack, item) => {
                if (item.orderProductID === id) {
                    return ack;
                } else {
                    return [...ack, item];
                }
            }, [] as IOrderProductState[])
        );
    };

    const handleIsInList = (orderProductID: number): boolean => {
        return orderProductsStates.filter(o => o.orderProductID === orderProductID).length > 0;
    };

    const handleClearList = () => {
        setOrderProductsStates([] as IOrderProductState[]);
    };

    const handleChangeStates = (states: IOrderProductState[]) => {
        setStatus(0);
        setErrorMessage("");
        changeOrderProductsStates({productList: states})
            .then((response) => {
                if (response.ok) {
                    setStatus(response.status);
                    handleClearList();
                } else {
                    setStatus(response.status);
                    setErrorMessage(response.statusText);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setStatus(error.response.status);
                    setErrorMessage(error.response.data);
                }
            });
    };

    const getDate = (i: IOrderProduct) => {
        return OrderProductState.paid === currentFilterMode ? i.orderBeginDate : i.orderEndDate;
    };

    const isToBeDone = (i: IOrderProduct) => {
        return i.orderProductState === currentFilterMode
            && DateManipulation.inDates(new Date(getDate(i)), new Date(), new Date(currentDateMode));
    };

    const getFilteredOrderProducts = () => {
        return orderProducts.filter(i => isToBeDone(i));
    };

    const handleChangeCurrentFilterMode = (orderProductState: OrderProductState) => {
        if (orderProductState !== currentFilterMode) {
            handleClearList();
            setCurrentFilterMode(orderProductState)
        }
    };

    const handleChangeCurrentDateMode = (newDateMode: string) => {
        if (newDateMode !== currentDateMode) {
            handleClearList();
            setCurrentDateMode(newDateMode);
        }
    };

    return (
        <>
            <Alert
                status={status}
                errorMessage={errorMessage}
                successMessage={"Poprawnie zmieniono statusy."}
            />
            <FilterBar
                currentFilterMode={currentFilterMode}
                setCurrentFilterMode={handleChangeCurrentFilterMode}
                currentDateMode={currentDateMode}
                setCurrentDateMode={handleChangeCurrentDateMode}
            />
            <OrderProductsTable
                filteredOrderProducts={getFilteredOrderProducts()}
                currentFilterMode={currentFilterMode}
                addToList={handleAddToList}
                removeFromList={handleRemoveFromList}
                isInList={handleIsInList}
            />
            <ConfirmationBar
                orderProductsStates={orderProductsStates}
                changeStates={handleChangeStates}
                clearList={handleClearList}
            />
        </>
    );
};

export default UpcomingOrders;
