import React from "react";

import DateBar from "./DateBar";
import filterModes from "./filterModes";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";

type Props = {
    currentFilterMode: OrderProductState;
    setCurrentFilterMode: (orderProductState: OrderProductState) => void;
    currentDateMode: string;
    setCurrentDateMode: (date: string) => void;
};

export const FilterBar: React.FC<Props> = ({
                                               currentFilterMode,
                                               setCurrentFilterMode,
                                               currentDateMode,
                                               setCurrentDateMode
                                           }) => {
    const isChecked = (orderState: OrderProductState) => {
        return orderState === currentFilterMode;
    };

    const handleChangeFilterMode = (newFilterMode: OrderProductState) => {
        setCurrentFilterMode(newFilterMode);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-9">
                        <h6>Przedmioty: </h6>
                        <nav className="nav nav-pills nav-fill">
                            {filterModes.map((filterMode, index) => {
                                return (
                                    <div
                                        key={"orderState_" + index}
                                        className={"nav-item nav-link " + (isChecked(filterMode.state) ? "active" : "")}
                                        onClick={() => handleChangeFilterMode(filterMode.state)}>
                                        {filterMode.title}
                                    </div>
                                );
                            })}
                        </nav>
                    </div>
                    <div className="col-md-3 justify-content-end allign-items-center text-right">
                        <DateBar
                            currentDateMode={currentDateMode}
                            setCurrentDateMode={setCurrentDateMode}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FilterBar;
