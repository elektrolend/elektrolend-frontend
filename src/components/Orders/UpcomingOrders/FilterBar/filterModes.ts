import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";

interface IFilterModeInfo {
    title: string;
    state: OrderProductState;
}

const filterModes: IFilterModeInfo[] = [
    {
        title: "do umieszczenia w skrytkach",
        state: OrderProductState.paid
    },
    {
        title: "do wyjęcia ze skrytek",
        state: OrderProductState.back_to_office_box
    }
]

export default filterModes;
