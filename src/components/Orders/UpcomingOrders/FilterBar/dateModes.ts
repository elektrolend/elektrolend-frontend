import {DateManipulation} from "../../../Utils/DateManipulation";

interface IDateInfo {
    title: string;
    date: string;
}

const dateModes: IDateInfo[] = [
    {
        title: "Na dzisiaj",
        date: String(new Date())
    },
    {
        title: "Do jutra",
        date: String(DateManipulation.getDateAfterDays(new Date(), 2))
    },
    {
        title: "W ciągu tygodnia",
        date: String(DateManipulation.getDateAfterDays(new Date(), 8))
    },
    {
        title: "W ciągu miesiąca",
        date: String(DateManipulation.getDateAfterDays(new Date(), 31))
    },
    {
        title: "W ciągu roku",
        date: String(DateManipulation.getDateAfterDays(new Date(), 366))
    }
]

export default dateModes;
