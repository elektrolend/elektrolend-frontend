import React from "react";

import dateModes from "./dateModes";

type Props = {
    currentDateMode: string;
    setCurrentDateMode: (date: string) => void;
};

const DateBar: React.FC<Props> = ({currentDateMode, setCurrentDateMode}) => {
    const handleChangeDateMode = (newDateMode: string) => {

        setCurrentDateMode(newDateMode);
    };

    return (
        <div className="row">
            <h6>Termin wykonania: </h6>
            <select
                className="custom-select"
                id="inputGroupSelect02"
                value={currentDateMode}
                onChange={(e) => handleChangeDateMode(String(e.target.value))}>
                {dateModes.map((dateMode, index) => {
                    return (
                        <option
                            key={"pageSize_" + index}
                            value={String(dateMode.date)}>
                            {dateMode.title}
                        </option>
                    )
                })}
            </select>
        </div>
    );
};

export default DateBar;
