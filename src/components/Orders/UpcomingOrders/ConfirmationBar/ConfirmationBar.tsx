import React from "react";

import {IOrderProductState} from "../../../../interfaces/Orders/IOrderProductState";

import {CheckLg, XLg} from "react-bootstrap-icons";

type Props = {
    orderProductsStates: IOrderProductState[];
    changeStates: (states: IOrderProductState[]) => void;
    clearList: () => void;
};

const ConfirmationBar: React.FC<Props> = ({orderProductsStates, changeStates, clearList}) => {
    if (orderProductsStates.length === 0) {
        return null;
    }
    return (
        <div className="border mt-2 mb-2">
            <div className="bg-light m-2">
                <div className="row">
                    <div className="col-6 text-left">
                        <h6 className="ml-4 mt-2">Zaznaczono {orderProductsStates.length} produkty</h6>
                    </div>
                    <div className="col-6 text-right">
                        <button
                            className="btn btn-labeled btn-danger pt-0 pb-0 mb-10 mr-2"
                            onClick={() => clearList()}
                        >
                            <span className="button-label"><XLg/></span>Odznacz
                        </button>
                        <button
                            className="btn btn-labeled btn-success pt-0 pb-0 mb-10 ml-2 mr-2"
                            onClick={() => changeStates(orderProductsStates)}
                            type="submit"
                        >
                            <span className="button-label"><CheckLg/></span>Zatwierdź
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ConfirmationBar;
