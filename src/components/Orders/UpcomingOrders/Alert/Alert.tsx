import React from "react";

import {CheckLg, XLg} from "react-bootstrap-icons";

type Props = {
    status: number;
    errorMessage: string;
    successMessage: string;
};

const Alert: React.FC<Props> = ({status, errorMessage, successMessage}) => {
    if (errorMessage) {
        return (
            <div className="border row m-2 mt-4 alert alert-danger d-flex align-items-center" role="alert">
                <XLg/>
                <div className="ml-1">
                    <b>Coś poszło nie tak...</b> {errorMessage}
                </div>
            </div>
        );
    }

    if (status === 200) {
        return (
            <div className="border row m-2 mt-4 alert alert-success d-flex align-items-center" role="alert">
                <CheckLg/>
                <div className="ml-1">
                    <b>Sukces!</b> {successMessage}
                </div>
            </div>
        );
    }

    return null;
};

export default Alert;
