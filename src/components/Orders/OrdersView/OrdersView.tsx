import {useEffect, useState} from "react";

import OrdersTable from "./OrdersTable/OrdersTable";
import OrderPage from "../OrderPage/OrderPage";
import {getOrders} from "../../../services/database.service";
import {IOrderSummary} from "../../../interfaces/Orders/IOrderSummary";

const OrdersView = () => {
    const [orderList, setOrderList] = useState([] as IOrderSummary[]);
    const [currentOrderID, setCurrentOrderID] = useState(null as number | null);

    useEffect(() => {
        getOrders()
            .then(orders => setOrderList(orders))
            .catch(() => setOrderList([]));
    }, []);

    if (currentOrderID !== null) {
        return (
            <OrderPage
                orderID={currentOrderID}
                setCurrentOrderID={setCurrentOrderID}
            />
        );
    }

    return (
        <>
            <OrdersTable
                orderList={orderList}
                setCurrentOrderID={setCurrentOrderID}
            />
        </>
    );
};

export default OrdersView;
