const TableHeader = () => {
    return (
        <thead className="bg-primary text-center text-white">
        <tr>
            <th
                scope="col"
                className="border-right">
                ID
            </th>
            <th
                scope="col"
                className="border-right">
                KLIENT
            </th>
            <th
                scope="col"
                className="border-right">
                DATA
            </th>
            <th scope="col">
                OPCJE
            </th>
        </tr>
        </thead>
    );
};

export default TableHeader;
