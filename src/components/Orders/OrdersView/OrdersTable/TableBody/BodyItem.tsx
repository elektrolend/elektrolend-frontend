import React, {useEffect, useState} from "react";

import PreviewButton from "./PreviewButton";
import {getUser} from "../../../../../services/database.service";
import {IOrder} from "../../../../../interfaces/Orders/IOrder";
import {IUserProfile} from "../../../../../interfaces/Accounts/IUserProfile";

type Props = {
    orderSummary: IOrder;
    setCurrentOrderID: (id: number | null) => void;
};

const BodyItem: React.FC<Props> = ({orderSummary, setCurrentOrderID}) => {
    const [customer, setCustomer] = useState(null as IUserProfile | null);

    useEffect(() => {
        getUser(orderSummary.customerID)
            .then(user => setCustomer(user))
            .catch(() => setCustomer(null));
    }, [orderSummary.customerID]);

    if (!customer) {
        return null;
    }

    const getCustomerFullName = () => {
        return customer.name
            .concat(' ')
            .concat(customer.surname);
    };

    const getOrderDate = () => {
        return new Date(orderSummary.orderDate).toLocaleDateString();
    };

    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">{orderSummary.id}</td>
            <td className="border-right">{getCustomerFullName()}</td>
            <td className="border-right">{getOrderDate()}</td>
            <td>
                <PreviewButton
                    orderID={orderSummary.id}
                    setCurrentOrderID={setCurrentOrderID}
                />
            </td>
        </tr>
    );
};

export default BodyItem;
