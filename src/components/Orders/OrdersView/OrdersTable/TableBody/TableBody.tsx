import React from "react";

import BodyItem from "./BodyItem";
import {IOrderSummary} from "../../../../../interfaces/Orders/IOrderSummary";

type Props = {
    orderList: IOrderSummary[];
    setCurrentOrderID: (id: number | null) => void;
};

const TableBody: React.FC<Props> = ({orderList, setCurrentOrderID}) => {
    return (
        <tbody className="bg-white">
        {orderList.map((orderSummary, index) => {
                return (
                    <BodyItem
                        orderSummary={orderSummary.order}
                        setCurrentOrderID={setCurrentOrderID}
                        key={"order_" + index}
                    />
                );
            }
        )}

        </tbody>
    );
};

export default TableBody;
