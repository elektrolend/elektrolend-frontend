import React from "react";

import {BinocularsFill} from "react-bootstrap-icons";

type Props = {
    orderID: number;
    setCurrentOrderID: (id: number | null) => void;
};

const PreviewButton: React.FC<Props> = ({orderID, setCurrentOrderID}) => {
    return (
        <button
            type="button"
            className="btn btn-secondary"
            data-content="Zgłoś reklamację"
            onClick={() => setCurrentOrderID(orderID)}
        >
            <BinocularsFill className="mb-1"/> <strong>PODGLĄD</strong>
        </button>
    );
};

export default PreviewButton;
