import React from "react";
import {isEmptyArray} from "formik";

import TableBody from "./TableBody/TableBody";
import TableHeader from "./TableHeader/TableHeader";
import {IOrderSummary} from "../../../../interfaces/Orders/IOrderSummary";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    orderList: IOrderSummary[];
    setCurrentOrderID: (id: number | null) => void;
};

const OrdersTable: React.FC<Props> = ({orderList, setCurrentOrderID}) => {
    if (isEmptyArray(orderList)) {
        return (
            <div className="border mb-3">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2 justify-content-center">
                        <div className="p-4 bg-light row m-5 text-center">
                            <EmojiFrownFill
                                className="col-md-12 mb-2"
                                size={50}/>
                            <h3 className="col-md-12">To jeszcze nie ten moment...</h3>
                            <p className="col-md-12">
                                Nie znaleziono żadnych zamówień.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <TableHeader/>
                            <TableBody
                                orderList={orderList}
                                setCurrentOrderID={setCurrentOrderID}
                            />
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrdersTable;
