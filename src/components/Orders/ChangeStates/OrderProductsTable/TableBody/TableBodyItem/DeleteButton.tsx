import React from "react";

import {TrashFill} from "react-bootstrap-icons";

type Props = {
    edit: boolean;
    onCancel: () => void;
};

const DeleteButton: React.FC<Props> = ({edit, onCancel}) => {
    if (edit) {
        return null;
    }
    return (
        <button
            className="btn btn-outline-danger border-danger rounded-circle shadow-sm"
            onClick={() => onCancel()}
        >
            <TrashFill/>
        </button>
    );
};

export default DeleteButton;
