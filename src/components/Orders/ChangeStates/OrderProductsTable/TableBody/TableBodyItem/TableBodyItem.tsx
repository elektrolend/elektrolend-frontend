import React, {useState} from "react";

import StateButton from "./StateButton";
import StateSelect from "./StateSelect";
import EditButton from "./EditButton";
import DeleteButton from "./DeleteButton";
import {IOrderProductStatus} from "../../../../../../interfaces/Orders/IOrderProductStatus";
import {OrderProductState} from "../../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProduct: IOrderProductStatus;
    addToList: (orderProduct: IOrderProductStatus, newOrderProductState: string) => void;
    removeFromList: (orderProductID: number) => void;
    isInList: (id: number) => string | null;
};

const TableBodyItem: React.FC<Props> = ({orderProduct, addToList, removeFromList, isInList}) => {
    const [edit, setEdit] = useState(false);
    const [selectedState, setSelectedState] = useState("" as string);

    const getSelectedState = () => {
        const currState = isInList(orderProduct.id);

        if (currState) {
            return currState;
        }
        return selectedState;
    };


    const handleSubmit = () => {
        if (selectedState) {
            setEdit(false);
            addToList(orderProduct, selectedState);
        }
    };

    const handleCancel = () => {
        setEdit(false);
        removeFromList(orderProduct.id);
        setSelectedState("");
    };

    const stateChosen = () => {
        if (isInList(orderProduct.id) !== null) {
            return true;
        }
        return false;
    };

    const editable = () => {
        return orderProduct.orderProductState !== OrderProductState.finished;
    };

    const getButton = () => {
        if (stateChosen()) {
            return (
                <DeleteButton
                    edit={edit}
                    onCancel={handleCancel}
                />
            );
        }

        if (editable()) {
            return (
                <EditButton
                    edit={edit}
                    setEdit={setEdit}
                    onSubmit={handleSubmit}
                />
            );
        }

        return null;
    };


    return (
        <tr className="border-bottom text-center">
            <td className="border-right">
                {orderProduct.id}
            </td>
            <td className="border-right">
                {orderProduct.productName}
            </td>
            <td className="border-right">
                {orderProduct.customerName}
            </td>
            <td className="border-right">
                {edit || stateChosen() ?
                    <StateSelect
                        orderProductState={orderProduct.orderProductState}
                        selectedState={getSelectedState()}
                        edit={edit}
                        setSelectedState={setSelectedState}
                    />
                    : <StateButton orderProductState={orderProduct.orderProductState}/>}
            </td>
            <td>
                {getButton()}
            </td>
        </tr>
    );
};

export default TableBodyItem;
