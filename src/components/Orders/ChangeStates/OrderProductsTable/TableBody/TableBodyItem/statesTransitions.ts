import {OrderProductState} from "../../../../../../interfaces/Orders/OrderProductState";

const statesTransitions = new Map<OrderProductState, OrderProductState[]>([
        [OrderProductState.to_realization,
            [OrderProductState.paid, OrderProductState.canceled]],
        [OrderProductState.canceled,
            [OrderProductState.finished]],
        [OrderProductState.paid,
            [OrderProductState.in_office_box]],
        [OrderProductState.in_office_box,
            [OrderProductState.received, OrderProductState.finished]],
        [OrderProductState.received,
            [OrderProductState.not_damaged, OrderProductState.damaged]],
        [OrderProductState.damaged,
            [OrderProductState.in_office_box_complaint]],
        [OrderProductState.not_damaged,
            [OrderProductState.back_to_office_box]],
        [OrderProductState.in_office_box_complaint,
            [OrderProductState.complaint_for_consideration]],
        [OrderProductState.complaint_for_consideration,
            [OrderProductState.complaint_accepted, OrderProductState.complaint_not_accepted]
        ],
        [OrderProductState.complaint_accepted,
            [OrderProductState.refund]],
        [OrderProductState.complaint_not_accepted,
            [OrderProductState.finished]],
        [OrderProductState.back_to_office_box,
            [OrderProductState.back_to_office]],
        [OrderProductState.back_to_office,
            [OrderProductState.refund]],
        [OrderProductState.refund,
            [OrderProductState.finished]],
        [OrderProductState.finished, []]
    ]
);

export default statesTransitions;
