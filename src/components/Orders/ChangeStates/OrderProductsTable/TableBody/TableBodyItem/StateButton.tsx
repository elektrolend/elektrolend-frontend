import React from "react";

import orderProductStates
    from "../../../../UserOrders/UserOrdersTable/OrderInfo/OrderProducts/OrderItem/orderProductStates";
import {OrderProductState} from "../../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProductState: OrderProductState;
};

const StateButton: React.FC<Props> = ({orderProductState}) => {
    return (
        <button
            type="button"
            className="btn text-white shadow-sm"
            style={{backgroundColor: orderProductStates.get(orderProductState)?.color, borderRadius: "35px"}}>
            {orderProductStates.get(orderProductState)?.title}
        </button>
    );
};

export default StateButton;
