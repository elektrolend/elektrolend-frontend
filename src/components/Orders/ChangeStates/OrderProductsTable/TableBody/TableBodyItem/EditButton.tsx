import React from "react";

import {CheckLg, PencilFill, XLg} from "react-bootstrap-icons";

type Props = {
    edit: boolean;
    setEdit: (edit: boolean) => void;
    onSubmit: () => void;
};

const EditButton: React.FC<Props> = ({edit, setEdit, onSubmit}) => {
    if (edit) {
        return (
            <div>
                <button
                    className="btn btn-outline-success border-success rounded-circle shadow-sm mr-2"
                    onClick={() => onSubmit()}
                >
                    <CheckLg></CheckLg>
                </button>
                <button
                    className="btn btn-outline-danger border-danger rounded-circle shadow-sm"
                    onClick={() => setEdit(false)}
                >
                    <XLg></XLg>
                </button>
            </div>
        );
    }
    return (
        <button
            className="btn btn-outline-secondary border-secondary rounded-circle shadow-sm mr-1"
            onClick={() => setEdit(true)}
        >
            <PencilFill/>
        </button>
    );
};

export default EditButton;
