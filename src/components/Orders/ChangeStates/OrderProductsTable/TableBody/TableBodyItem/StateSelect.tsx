import React from "react";

import statesTransitions from "./statesTransitions";
import orderProductStates
    from "../../../../UserOrders/UserOrdersTable/OrderInfo/OrderProducts/OrderItem/orderProductStates";
import {OrderProductState} from "../../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProductState: OrderProductState;
    selectedState: string;
    edit: boolean;
    setSelectedState: (state: string) => void;
};

const StateSelect: React.FC<Props> = ({orderProductState, selectedState, edit, setSelectedState}) => {
    return (
        <select
            className="form-control"
            value={selectedState}
            disabled={!edit}
            onChange={(e) => setSelectedState(e.target.value)}
        >
            <option value="">Wybierz nowy stan</option>
            {statesTransitions.get(orderProductState)?.map((transition, index) => {
                return (
                    <option
                        value={transition}
                        key={"transition_" + index}
                    >
                        {orderProductStates.get(transition)?.title}
                    </option>
                );
            })}
        </select>
    );
};

export default StateSelect;
