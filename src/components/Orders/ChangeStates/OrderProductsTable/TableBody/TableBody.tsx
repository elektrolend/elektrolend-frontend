import React from "react";

import TableBodyItem from "./TableBodyItem/TableBodyItem";
import {IOrderProductStatus} from "../../../../../interfaces/Orders/IOrderProductStatus";

type Props = {
    orderProducts: IOrderProductStatus[];
    addToList: (orderProduct: IOrderProductStatus, newOrderProductState: string) => void;
    removeFromList: (orderProductID: number) => void;
    isInList: (id: number) => string | null;
};

const TableBody: React.FC<Props> = ({orderProducts, addToList, removeFromList, isInList}) => {
    return (
        <tbody className="bg-white">
        {orderProducts.map((orderProduct) => {
            return (
                <TableBodyItem
                    orderProduct={orderProduct}
                    addToList={addToList}
                    removeFromList={removeFromList}
                    isInList={isInList}
                    key={"orderProduct_" + orderProduct.id}
                />
            );
        })}
        </tbody>
    );
}

export default TableBody;
