interface ITableHeader {
    title: string;
    cName: string;
}

const tableHeaderItems: ITableHeader[] = [
    {
        title: "ID",
        cName: "col-md-1 border-right"
    },
    {
        title: "PRODUKT",
        cName: "col-md-3 border-right"
    },
    {
        title: "KLIENT",
        cName: "col-md-3 border-right"
    },
    {
        title: "STAN",
        cName: "col-md-3 border-right"
    },
    {
        title: "OPCJE",
        cName: "col-md-2"
    }
];

export default tableHeaderItems;
