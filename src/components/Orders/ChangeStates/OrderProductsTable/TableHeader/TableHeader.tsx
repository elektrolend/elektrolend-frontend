import TableHeaderItem from "./TableHeaderItem";
import tableHeaderItems from "./tableHeaderItems";

const TableHeader = () => {
    return (
        <thead
            className="bg-primary text-center text-white border-bottom-2"
            style={{borderColor: "#003a9e"}}
        >
        <tr>
            {tableHeaderItems.map((tableHeader, index) => {
                return (
                    <TableHeaderItem
                        title={tableHeader.title}
                        cName={tableHeader.cName}
                        key={"tableHeaderItem_" + index}
                    />
                );
            })}
        </tr>
        </thead>
    );
};

export default TableHeader;
