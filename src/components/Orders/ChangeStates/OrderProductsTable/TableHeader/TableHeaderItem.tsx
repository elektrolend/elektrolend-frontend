import React from "react";

type Props = {
    title: string;
    cName: string;
};

const TableHeaderItem: React.FC<Props> = ({title, cName}) => {
    return (
        <th
            className={cName}
            scope="col"
        >
            {title}
        </th>
    );
};

export default TableHeaderItem;
