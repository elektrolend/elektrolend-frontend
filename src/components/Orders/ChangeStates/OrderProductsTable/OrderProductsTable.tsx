import React from "react";
import {isEmptyArray} from "formik";

import TableHeader from "./TableHeader/TableHeader";
import TableBody from "./TableBody/TableBody";
import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import {IOrderProductStatus} from "../../../../interfaces/Orders/IOrderProductStatus";

import {EmojiFrownFill} from "react-bootstrap-icons";

type Props = {
    orderProducts: IOrderProductStatus[];
    addToList: (orderProduct: IOrderProductStatus, newOrderProductState: string) => void;
    removeFromList: (orderProductID: number) => void;
    isInList: (id: number) => string | null;
};

const OrderProductsTable: React.FC<Props> = ({orderProducts, addToList, removeFromList, isInList}) => {

    if (isEmptyArray(orderProducts)) {
        return (
            <EmptyArrayAlert
                title={"Nie znaleziono żadnych zamówionych produktów!"}
                description={"Zmień parametry filtrowania."}
                icon={<EmojiFrownFill size={50}/>}
            />
        );
    }

    return (
        <div className="border mb-3" id="orderProductTab">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <TableHeader/>
                            <TableBody
                                orderProducts={orderProducts}
                                addToList={addToList}
                                removeFromList={removeFromList}
                                isInList={isInList}
                            />
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderProductsTable;
