import React, {useState} from "react";
import {isEmptyArray} from "formik";

import FilterBar from "./FilterBar/FilterBar";
import OrderProductsTable from "./OrderProductsTable/OrderProductsTable";
import WarningAlert from "../../Utils/WarningAlert";
import ConfirmationBar from "./ConfirmationBar/ConfirmationBar";
import SuccessAlert from "../../Utils/SuccessAlert";
import DangerAlert from "../../Utils/DangerAlert";
import {changeOrderProductsStates, getOrderProductStatuses} from "../../../services/database.service";
import filterModes from "./FilterBar/filterModes";
import {IOrderProductState} from "../../../interfaces/Orders/IOrderProductState";
import {IOrderProductStatus} from "../../../interfaces/Orders/IOrderProductStatus";

const ChangeStates = () => {
    const [orderProducts, setOrderProducts] = useState([] as IOrderProductStatus[]);
    const [currFilterMode, setCurrFilterMode] = useState(filterModes[0]);
    const [orderProductsStates, setOrderProductsStates] = useState([] as IOrderProductState[]);

    const [state, setState] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        getOrderProductStatuses()
            .then(statuses => setOrderProducts(statuses.orderProducts))
            .catch(() => setOrderProducts([]));
    }, [currFilterMode, state]);

    const getFilteredOrderProducts = () => {
        return orderProducts?.filter(op => currFilterMode.checkState(op.orderProductState));
    };

    const handleAddToList = (orderProductStatus: IOrderProductStatus, newOrderProductState: string) => {
        const isItemInCart = orderProductsStates.findIndex(i => i.orderProductID === orderProductStatus.id);
        if (isItemInCart === -1) {
            setOrderProductsStates(prev => {
                return [...prev, {
                    orderProductID: orderProductStatus.id,
                    orderProductState: newOrderProductState
                }];
            });
        } else {
            const currOrderProducts = orderProductsStates;
            currOrderProducts[isItemInCart].orderProductState = newOrderProductState;
            setOrderProductsStates(currOrderProducts);
        }
    };

    const handleRemoveFromList = (id: number) => {
        setOrderProductsStates(prev =>
            prev.reduce((ack, item) => {
                if (item.orderProductID === id) {
                    return ack;
                } else {
                    return [...ack, item];
                }
            }, [] as IOrderProductState[])
        );
    };

    const handleClearList = () => {
        setOrderProductsStates([] as IOrderProductState[]);
    };

    const isInList = (id: number) => {
        const isItemInCart = orderProductsStates.findIndex(i => i.orderProductID === id);
        if (isItemInCart !== -1) {
            return orderProductsStates[isItemInCart].orderProductState;
        }
        return null;
    };

    const unconfirmedChanges = () => {
        return orderProductsStates.length > 0;
    };

    const handleOnSubmit = () => {
        setState(100);
        setErrorMessage("");
        changeOrderProductsStates({productList: orderProductsStates})
            .then((response) => {
                if (response.ok) {
                    setState(response.status);
                    handleClearList();
                } else {
                    setState(response.status);
                    setErrorMessage(response.statusText);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setState(error.response.status);
                    setErrorMessage(error.response.data);
                }
            });
    };

    const isLoading = () => {
        return state === 100;
    };

    if (!orderProducts || isEmptyArray(orderProducts)) {
        return null;
    }

    return (
        <>
            <SuccessAlert
                state={state}
                message={"Udało się zmienić stany zamówionych przedmiotów."}
            />
            <WarningAlert
                show={unconfirmedChanges()}
                message={"Masz niezapisane zmiany!"}
            />
            <FilterBar
                currFilterMode={currFilterMode}
                setCurrFilterMode={setCurrFilterMode}
            />
            <OrderProductsTable
                orderProducts={getFilteredOrderProducts()}
                addToList={handleAddToList}
                removeFromList={handleRemoveFromList}
                isInList={isInList}
            />
            <DangerAlert errorMessage={errorMessage}/>
            <ConfirmationBar
                orderProductStates={orderProductsStates}
                loading={isLoading()}
                clearList={handleClearList}
                onSubmit={handleOnSubmit}
            />
        </>
    );
};

export default ChangeStates;
