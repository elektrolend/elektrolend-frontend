import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";
import {IFilterModeInfo} from "../../../../interfaces/Orders/IFilterModeInfo";
import {FilterState} from "../../../../interfaces/Orders/FilterState";

const filterModes: IFilterModeInfo[] = [
    {
        title: "Wszystkie",
        state: FilterState.all,
        checkState: () => true
    },
    {
        title: "W trakcie",
        state: FilterState.in_realization,
        checkState: (orderProductState => orderProductState !== OrderProductState.finished)
    },
    {
        title: "Zakończone",
        state: FilterState.finished,
        checkState: (orderProductState => orderProductState === OrderProductState.finished)
    }
]

export default filterModes;
