import React from "react";

import filterModes from "./filterModes";
import {IFilterModeInfo} from "../../../../interfaces/Orders/IFilterModeInfo";

type Props = {
    currFilterMode: IFilterModeInfo;
    setCurrFilterMode: (newFilterMode: IFilterModeInfo) => void;
};

const FilterBar: React.FC<Props> = ({currFilterMode, setCurrFilterMode}) => {

    const isChecked = (mode: IFilterModeInfo) => {
        return mode === currFilterMode;
    };


    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-9">
                        <nav className="nav nav-pills nav-fill">
                            {filterModes.map((filterMode, index) => {
                                return (
                                    <div
                                        key={"orderState_" + index}
                                        className={"nav-item nav-link " + (isChecked(filterMode) ? "active" : "")}
                                        onClick={() => setCurrFilterMode(filterMode)}>
                                        {filterMode.title}
                                    </div>
                                );
                            })}
                        </nav>
                    </div>
                    <div className="col-md-3 justify-content-end allign-items-center text-right">

                    </div>
                </div>
            </div>
        </div>
    );
};

export default FilterBar;
