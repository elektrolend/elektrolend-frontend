import React from "react";

import {IOrderProductState} from "../../../../interfaces/Orders/IOrderProductState";

import {CheckLg, XLg} from "react-bootstrap-icons";

type Props = {
    orderProductStates: IOrderProductState[];
    loading: boolean;
    clearList: () => void;
    onSubmit: () => void;
};

const ConfirmationBar: React.FC<Props> = ({orderProductStates, loading, clearList, onSubmit}) => {
    if (orderProductStates.length === 0) {
        return null;
    }

    return (
        <div className="container fixed-bottom">
            <div className="border mt-2 mb-2 bg-white">
                <div className="bg-light m-2">
                    <div className="row">
                        <div className="col-6 text-left">
                            <h6 className="ml-4 mt-2">
                                Zmieniono stany dla {orderProductStates.length} zamówionych produktów
                            </h6>
                        </div>
                        <div className="col-6 text-right">
                            <button
                                className="btn btn-labeled btn-danger pt-0 pb-0 mb-10 mr-2"
                                onClick={() => clearList()}
                                disabled={loading}
                            >
                                <span className="button-label"><XLg/></span>Anuluj
                            </button>
                            <button
                                className="btn btn-labeled btn-success pt-0 pb-0 mb-10 ml-2 mr-2"
                                onClick={() => onSubmit()}
                                type="submit"
                                disabled={loading}
                            >
                                <span className="button-label"><CheckLg/></span>Zatwierdź
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ConfirmationBar;
