import React from "react";
import {isEmptyArray} from "formik";

import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import Body from "./Body/Body";
import Header from "./Header/Header";
import {IOrderProduct} from "../../../../interfaces/Orders/IOrderProduct";

import {EmojiSmileFill} from "react-bootstrap-icons";

type Props = {
    orderProducts: IOrderProduct[];
};

const Table: React.FC<Props> = ({orderProducts}) => {
    if (isEmptyArray(orderProducts)) {
        return (
            <div className="container mt-2 mb-2">
                <EmptyArrayAlert
                    title={"Dobra robota!"}
                    description={"Wszyscy klienci, którzy zwrócili przedmioty otrzymali zwrot kaucji."}
                    icon={
                        <EmojiSmileFill
                            className="col-md-12 mb-2"
                            size={50}
                        />
                    }
                />
            </div>
        );
    }

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <Header/>
                            <Body
                                orderProducts={orderProducts}
                            />
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Table;
