import React from "react";

import Item from "./Item/Item";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderProducts: IOrderProduct[];
};

const Body: React.FC<Props> = ({orderProducts}) => {
    return (
        <tbody className="bg-white">
        {orderProducts.map((orderProduct, index) => {
            return (
                <Item
                    orderProduct={orderProduct}
                    key={"orderProductStatus_" + index}
                />
            );
        })}
        </tbody>
    );
};

export default Body;
