import React, {useState} from "react";

import RefundButton from "./RefundButton";
import {getOrderProductStatuses, getProduct} from "../../../../../../services/database.service";
import {IOrderProductStatus} from "../../../../../../interfaces/Orders/IOrderProductStatus";
import {IOrderProduct} from "../../../../../../interfaces/Orders/IOrderProduct";
import {IProduct} from "../../../../../../interfaces/Products/IProduct";

type Props = {
    orderProduct: IOrderProduct;
};

const Item: React.FC<Props> = ({orderProduct}) => {
    const [product, setProduct] = useState({} as IProduct);
    const [productStatus, setProductStatus] = useState({} as IOrderProductStatus);

    React.useEffect(() => {
        getProduct(orderProduct.productID).then(p => setProduct(p));
        getOrderProductStatuses()
            .then(statuses => setProductStatus(statuses.orderProducts.filter(s => s.id === orderProduct.id)[0]));
    }, [orderProduct.id, orderProduct.productID]);

    if (!product || !orderProduct) {
        return null;
    }

    return (
        <tr className="border text-center rounded-bottom">
            <td className="border-right">{orderProduct.id}</td>
            <td className="border-right">{productStatus.customerName}</td>
            <td className="border-right">{productStatus.productName}</td>
            <td className="border-right">{product.deposit} PLN</td>
            <td>
                <RefundButton
                    orderProductID={orderProduct.id}
                    refundAmount={product.deposit}
                />
            </td>
        </tr>
    );
};

export default Item;
