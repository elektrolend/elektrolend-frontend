import React, {useState} from "react";
import {Redirect} from "react-router-dom";

import {WalletFill} from "react-bootstrap-icons";

type Props = {
    orderProductID: number;
    refundAmount: number;
};

const RefundButton: React.FC<Props> = ({orderProductID, refundAmount}) => {
    const [clicked, setClicked] = useState(false);

    if (!orderProductID || !refundAmount) {
        return null;
    }

    if (clicked) {
        return (
            <Redirect to={{
                pathname: "/deposit-refund",
                state: {
                    orderProductID: orderProductID,
                    refundAmount: refundAmount
                }
            }}/>
        );
    }

    return (
        <button
            type="button"
            className="btn btn-outline-success"
            onClick={() => setClicked(true)}
        >
            <WalletFill className="mb-1"/> <strong>ZWROT</strong>
        </button>
    );
};

export default RefundButton;
