const Header = () => {
    return (
        <thead className="bg-primary text-center text-white">
        <tr>
            <th
                scope="col"
                className="border-right">
                ID
            </th>
            <th
                scope="col"
                className="border-right">
                KLIENT
            </th>
            <th
                scope="col"
                className="border-right">
                PRODUKT
            </th>
            <th
                scope="col"
                className="border-right">
                KAUCJA
            </th>
            <th
                scope="col"
                className="border-right">
                OPCJE
            </th>
        </tr>
        </thead>
    );
};

export default Header;
