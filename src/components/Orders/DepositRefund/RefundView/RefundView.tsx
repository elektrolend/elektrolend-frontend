import React, {useState} from "react";
import {Redirect} from "react-router-dom";

import EmptyArrayAlert from "../../../Utils/EmptyArrayAlert";
import RefundTile from "../../../Refund/RefundTile/RefundTile";
import ErrorAlert from "../../../Utils/ErrorAlert";
import {changeOrderProductsStates, getPaymentAccount} from "../../../../services/database.service";
import refundMethods from "../../../Refund/refundMethods";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";

import {XLg} from "react-bootstrap-icons";

type Props = {
    orderProductID: number;
    refundAmount: number;
};

const RefundView = ({...props}) => {
    const state: Props = props.history.location.state;
    const orderProductID = state ? state.orderProductID : null;
    const refundAmount = state ? state.refundAmount : null;

    const [paymentAccount, setPaymentAccount] = useState(null as string | null);
    const [status, setStatus] = useState(0);
    const [errorMessage, setErrorMessage] = useState("");

    React.useEffect(() => {
        if (orderProductID) {
            getPaymentAccount(orderProductID)
                .then(p => setPaymentAccount(p.paymentAccount))
                .catch(() => setPaymentAccount(null));
        }
    }, [orderProductID]);

    if (!orderProductID || !refundAmount) {
        return (
            <Redirect to={"/employee-panel"}/>
        );
    }

    const onSubmit = () => {
        setStatus(100);
        setErrorMessage("");
        changeOrderProductsStates({
            productList: [
                {
                    orderProductID: orderProductID,
                    orderProductState: OrderProductState.refund
                }
            ]
        })
            .then((response) => {
                if (response.ok) {
                    setStatus(response.status);
                } else {
                    setStatus(response.status);
                    setErrorMessage(response.statusText);
                }
            })
            .catch((error) => {
                if (error.response) {
                    setStatus(error.response.status);
                    setErrorMessage(error.response.data);
                }
            });
    };

    if (!paymentAccount) {
        return (
            <div className="container mt-2 mb-2">
                <EmptyArrayAlert
                    title={"Coś poszło nie tak..."}
                    description={"Nie znaleziono danych niezbędnych do wykonania zwrotu."}
                    icon={
                        <XLg
                            className="col-md-12 mb-2"
                            size={50}
                        />
                    }
                />
            </div>
        );
    }

    if (status === 200) {
        return (
            <div className="m-5 ">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading ">Sukces!</h4>
                    <p>Udało Ci się poprawnie zwrócić kaucję.</p>
                    <hr></hr>
                    <p>Teraz możesz wrócić do <a className="hover-move" href="/employee-panel"> panelu pracownika</a>.
                    </p>
                </div>
            </div>
        );
    }

    return (
        <div className="container mt-2 mb-2">
            <ErrorAlert
                message={errorMessage}
            />
            <div className="h2 text-primary">Wybierz metodę zwrotu kaucji</div>
            <div className="border">
                <div className="bg-light m-2">
                    <div className="row p-2 m-2">
                        {refundMethods.map((refundMethod, index) => {
                            return (
                                <RefundTile
                                    key={'refund_' + index}
                                    refundMethod={refundMethod}
                                    paymentAccount={paymentAccount}
                                    refundAmount={refundAmount}
                                    onSubmit={onSubmit}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RefundView;
