import React, {useEffect, useState} from "react";

import Table from "./Table/Table";
import {getAllOrderProducts} from "../../../services/database.service";
import {OrderProductState} from "../../../interfaces/Orders/OrderProductState";
import {IOrderProduct} from "../../../interfaces/Orders/IOrderProduct";
import {ServiceSecurityType} from "../../../interfaces/Orders/ServiceSecurityType";

const DepositRefund = () => {
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);

    useEffect(() => {
        getAllOrderProducts()
            .then(o => setOrderProducts(o))
            .catch(() => setOrderProducts([]));
    }, []);

    const isInOffice = (product: IOrderProduct) => {
        return product.orderProductState === OrderProductState.back_to_office;
    };

    const depositSelected = (p: IOrderProduct) => {
        return p.serviceSecurityType === ServiceSecurityType.deposit;
    };

    const filterOrderProducts = (products: IOrderProduct[]) => {
        return products.filter(p => isInOffice(p) && depositSelected(p));
    };

    return (
        <>
            <Table
                orderProducts={filterOrderProducts(orderProducts)}
            />
        </>
    );
};

export default DepositRefund;
