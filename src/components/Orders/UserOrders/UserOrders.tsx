import React, {useState} from "react";
import {useAppSelector} from "../../../app/hooks";
import {Redirect} from "react-router-dom";

import UserOrdersTable from "./UserOrdersTable/UserOrdersTable";
import FilterBar from "./FilterBar/FilterBar";
import PageNumberBar from "../../Pagination/PageNumberBar";
import {getUserOrders} from "../../../services/database.service";
import {IUserOrder} from "../../../interfaces/Orders/IUserOrder";
import {OrderProductState} from "../../../interfaces/Orders/OrderProductState";

const UserOrders = () => {
    const [userOrders, setUserOrders] = useState([] as IUserOrder[]);
    const [currentPageNumber, setCurrentPageNumber] = useState(1);
    const [middlePageNumber, setMiddlePageNumber] = useState(currentPageNumber + 1);
    const [currentPageSize, setCurrentPageSize] = useState(5);
    const [maxPageNumber, setMaxPageNumber] = useState(1);
    const [currentFilterMode, setCurrentFilterMode] = useState(OrderProductState.none);
    const [orderIDToPay, setOrderIDToPay] = useState(-1)
    const userID = useAppSelector(state => state.auth.userID);
    const isLogged = useAppSelector(state => state.auth.isLogged);

    React.useEffect(() => {
        const requestData = {
            userID: userID,
            pageSize: currentPageSize,
            pageNumber: currentPageNumber,
            orderProductState: currentFilterMode
        };
        getUserOrders(requestData).then(orders => {
            setUserOrders(orders.userOrderDTOList);
            setMaxPageNumber(Math.ceil(orders.paginationDTO.totalResults / currentPageSize));
        }).catch(() => setUserOrders([]));
    }, [userID, currentPageSize, currentPageNumber, currentFilterMode]);

    if (!isLogged) {
        return (
            <Redirect to="/login"/>
        );
    }

    if (orderIDToPay !== -1) {
        return (
            <Redirect to={{
                pathname: "/order-payment",
                state: {
                    orderIDToPay: orderIDToPay,
                    userOrders: userOrders
                }
            }}/>
        );
    }

    return (
        <div className="container mt-2 mb-2">
            <FilterBar
                currentPageSize={currentPageSize}
                setPageSize={setCurrentPageSize}
                currentFilterMode={currentFilterMode}
                setCurrentFilterMode={setCurrentFilterMode}
                setCurrentPageNumber={setCurrentPageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
            />
            <UserOrdersTable
                userOrders={userOrders}
                setOrderIDToPay={setOrderIDToPay}
            />
            <PageNumberBar
                currentPageNumber={currentPageNumber}
                setCurrentPageNumber={setCurrentPageNumber}
                middlePageNumber={middlePageNumber}
                setMiddlePageNumber={setMiddlePageNumber}
                maxPageNumber={maxPageNumber}
            />
        </div>
    );
}

export default UserOrders;
