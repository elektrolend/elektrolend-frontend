import React from "react";
import {isEmptyArray} from "formik";

import OrderInfo from "./OrderInfo/OrderInfo";
import {IUserOrder} from "../../../../interfaces/Orders/IUserOrder";

import {BagX} from "react-bootstrap-icons";

type Props = {
    userOrders: IUserOrder[];
    setOrderIDToPay: (orderID: number) => void;
};

const UserOrdersTable: React.FC<Props> = ({userOrders, setOrderIDToPay}) => {
    if (isEmptyArray(userOrders)) {
        return (
            <div className="border mb-3">
                <div className="p-4 bg-light row m-5 align-items-center justify-content-center text-center">
                    <BagX
                        className="col-md-12 mb-2"
                        size={50}/>
                    <h3 className="col-md-12">Nie znaleziono zamówień o podanych parametrach!</h3>
                    <p className="col-md-12">
                        Spróbuj zmienić filtry lub <a
                        className="hover-move"
                        href="/products">
                        złożyć
                    </a> nowe zamówienie.
                    </p>
                </div>
            </div>
        );
    }
    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="table-responsive">
                        <table className="table">
                            <thead
                                className="bg-primary text-center text-white">
                            <tr>
                                <th scope="col">
                                    <h4>Numer zamówienia</h4>
                                </th>
                                <th scope="col">
                                    <h4>Data zamówienia</h4>
                                </th>
                                <th scope="col">
                                    <h4>Rozwiń szczegóły</h4>
                                </th>
                            </tr>
                            </thead>
                            <tbody className="bg-white">
                            {userOrders.map((userOrder, index) => {
                                return (
                                    <OrderInfo
                                        userOrder={userOrder}
                                        userOrderIndex={index}
                                        setOrderIDToPay={setOrderIDToPay}
                                        key={"orderInfo_" + index}
                                    />
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default UserOrdersTable;
