import React, {useState} from "react";
import {isEmptyArray} from "formik";

import Payment from "../../../../ShoppingCart/Payment/Payment";
import {getOrderProducts} from "../../../../../services/database.service";
import {IUserOrder} from "../../../../../interfaces/Orders/IUserOrder";
import {IOrderProduct} from "../../../../../interfaces/Orders/IOrderProduct";

type Props = {
    orderIDToPay: number;
    userOrders: IUserOrder[];
};

const OrderPayment = ({...props}) => {
    const state: Props = props.history.location.state;
    const [orderProducts, setOrderProducts] = useState([] as IOrderProduct[]);

    React.useEffect(() => {
        getOrderProducts(state.orderIDToPay)
            .then(orderSummary => setOrderProducts(orderSummary.orderProductList))
            .catch(() => setOrderProducts([]));
    }, [state.orderIDToPay]);

    const findOrderProducts = () => {
        return state.userOrders.filter(userOrder => userOrder.orderID === state.orderIDToPay)[0].orderInfoList;
    };

    const getTotalAmount = () => {
        return findOrderProducts()
            .reduce((ack, orderProduct) => ack + orderProduct.orderProductPrice, 0);
    };

    const getOrderResponse = () => {
        return {
            orderID: state.orderIDToPay,
            orderProductInfoList: orderProducts.map(orderProduct => {
                return {
                    orderProductID: orderProduct.id,
                    officeBoxIDStart: orderProduct.officeBoxIDStart,
                    officeBoxIDEnd: orderProduct.officeBoxIDEnd,
                    officeBoxPassword: orderProduct.boxPassword
                }
            })
        }
    };

    if (isEmptyArray(findOrderProducts())) {
        return (
            <div className="m-5">
                <div className="alert alert-danger" role="alert">
                    <h4 className="alert-heading">Coś poszło nie tak...</h4>
                    <hr></hr>
                    <p className="mb-0">
                        Spróbuj <a className="hover-move" href="/shopping-cart"> złożyć</a> zamówienie ponownie.
                    </p>
                </div>
            </div>
        );
    }

    return (
        <Payment
            orderAmount={getTotalAmount()}
            orderResponse={getOrderResponse()}
        />
    );
};

export default OrderPayment;
