import React, {useState} from "react";

import OrderProducts from "./OrderProducts/OrderProducts";
import {DateManipulation} from "../../../../Utils/DateManipulation";
import {IUserOrder} from "../../../../../interfaces/Orders/IUserOrder";

import {ChevronDown, ChevronUp} from "react-bootstrap-icons";

type Props = {
    userOrder: IUserOrder;
    setOrderIDToPay: (orderID: number) => void;
    userOrderIndex: number;
};

const OrderInfo: React.FC<Props> = ({userOrder, setOrderIDToPay, userOrderIndex}) => {
    const [clicked, setClicked] = useState(false);
    const getOrderDate = () => {
        return DateManipulation.makeDate(userOrder.orderDate).toLocaleDateString();
    };

    return (
        <>
            <tr className="border text-center rounded-bottom">
                <td>
                    <h4>{userOrder.orderID}</h4>
                </td>
                <td>
                    <h4>{getOrderDate()}</h4>
                </td>
                <td>
                    <h4>
                        {clicked ?
                            <ChevronUp
                                data-toggle="collapse"
                                href={"#collapse_" + userOrderIndex}
                                role="button"
                                aria-expanded="false"
                                aria-controls={"collapse_" + userOrderIndex}
                                onClick={() => setClicked(!clicked)}
                            /> :
                            <ChevronDown
                                data-toggle="collapse"
                                href={"#collapse_" + userOrderIndex}
                                role="button"
                                aria-expanded="false"
                                aria-controls={"collapse_" + userOrderIndex}
                                onClick={() => setClicked(!clicked)}
                            />}
                    </h4>
                </td>
            </tr>
            <tr>
                <OrderProducts
                    orderID={userOrder.orderID}
                    orderInfoList={userOrder.orderInfoList}
                    setOrderIDToPay={setOrderIDToPay}
                    userOrderIndex={userOrderIndex}
                />
            </tr>
        </>
    );
}

export default OrderInfo;
