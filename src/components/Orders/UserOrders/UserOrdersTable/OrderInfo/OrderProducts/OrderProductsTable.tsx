import React from "react";

import OrderItem from "./OrderItem/OrderItem";
import {IUserOrderProduct} from "../../../../../../interfaces/Orders/IUserOrderProduct";

type Props = {
    orderInfoList: IUserOrderProduct[];
}

const OrderProductsTable: React.FC<Props> = ({orderInfoList}) => {
    return (
        <div className="table-responsive">
            <table className="table border">
                <thead
                    className="text-center text-white border-bottom-2"
                    style={{backgroundColor: "#48ABFF"}}>
                <tr>
                    <th
                        className="col-md-3 border-right"
                        style={{verticalAlign: "middle"}}
                        scope="col">
                        Nazwa produktu
                    </th>
                    <th
                        className="col-md-3 border-right"
                        style={{verticalAlign: "middle"}}
                        scope="col">
                        Okres wypożyczenia
                    </th>
                    <th
                        className="col-md-2 border-right"
                        style={{verticalAlign: "middle"}}
                        scope="col">
                        Zabezpieczenie
                    </th>
                    <th
                        className="col-md-3 border-right"
                        style={{verticalAlign: "middle"}}
                        scope="col">
                        Stan zamówienia
                    </th>
                    <th
                        className="col-md-2"
                        style={{verticalAlign: "middle"}}
                        scope="col">
                        Opcje
                    </th>
                </tr>
                </thead>
                <tbody className="bg-white">
                {orderInfoList.map((orderProduct, index) => {
                    return (
                        <OrderItem
                            orderProduct={orderProduct}
                            key={"orderItem_" + index}
                        />
                    )
                })}
                </tbody>
            </table>
        </div>
    );
}

export default OrderProductsTable;
