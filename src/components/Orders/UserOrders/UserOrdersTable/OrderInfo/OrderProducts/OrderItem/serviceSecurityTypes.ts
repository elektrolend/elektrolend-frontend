import {ServiceSecurityType} from "../../../../../../../interfaces/Orders/ServiceSecurityType";

interface ISecurityTypeInfo {
    title: string;
    color: string;
}

const serviceSecurityTypes = new Map<ServiceSecurityType | null, ISecurityTypeInfo>([
        [ServiceSecurityType.deposit, {title: "KAUCJA ZWRTONA", color: "orange"}],
        [ServiceSecurityType.insurance, {title: "UBEZPIECZENIE", color: "pink"}],
        [null, {title: "BRAK", color: "red"}]
    ]
);

export default serviceSecurityTypes;
