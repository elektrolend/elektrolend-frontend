import React, {useState} from "react";
import {generatePath, Link} from "react-router-dom";

import ComplaintButton from "../../../../../../Complaints/UserComplaint/ComplaintButton";
import OfficeBoxModal from "./OfficeBoxModal";
import OfficeBoxButton from "./OfficeBoxButton";
import {getProduct} from "../../../../../../../services/database.service";
import orderProductStates from "./orderProductStates";
import serviceSecurityTypes from "./serviceSecurityTypes";
import {IUserOrderProduct} from "../../../../../../../interfaces/Orders/IUserOrderProduct";
import {DateManipulation} from "../../../../../../Utils/DateManipulation";
import {OrderProductState} from "../../../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderProduct: IUserOrderProduct
};

const OrderItem: React.FC<Props> = ({orderProduct}) => {
    const [productName, setProductName] = useState(null as string | null);
    const [isModalOpen, setIsModalOpen] = React.useState(false);

    React.useEffect(() => {
        getProduct(orderProduct.productID).then(p => setProductName(p.name))
            .catch(() => setProductName(null));
    }, [orderProduct.productID]);

    const getOrderProductDate = (date: number[]) => {
        return DateManipulation.makeDate(date).toLocaleDateString();
    };

    const toComplaint = () => {
        return orderProduct.orderProductState === OrderProductState.received;
    };

    if (!productName) {
        return null
    }

    return (
        <tr className="bg-white text-center border rounded-bottom align-content-center h6">
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                <Link
                    to={generatePath(`/product/details/:id/:name`,
                        {id: orderProduct.productID, name: productName})}
                    className="text-dark"
                    target="_blank"
                >
                    {productName}
                </Link>
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {getOrderProductDate(orderProduct.orderBeginDate)} - {getOrderProductDate(orderProduct.orderEndDate)}
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >
                {serviceSecurityTypes.get(orderProduct.serviceSecurityType)?.title}
            </td>
            <td
                className="border-right"
                style={{verticalAlign: "middle"}}
            >

                <button
                    type="button"
                    className="btn border text-white"
                    style={{backgroundColor: orderProductStates.get(orderProduct.orderProductState)?.color}}>
                    {orderProductStates.get(orderProduct.orderProductState)?.title}
                </button>
            </td>
            <td style={{verticalAlign: "middle"}}>
                <div className="span2">
                    <OfficeBoxButton setIsOpen={setIsModalOpen}/>
                    {toComplaint() ?
                        <ComplaintButton
                            orderProductID={orderProduct.orderProductID}
                        /> : null}
                </div>
            </td>
            {isModalOpen ?
                <OfficeBoxModal
                    isOpen={isModalOpen}
                    setIsOpen={setIsModalOpen}
                    boxPassword={orderProduct.boxPassword}
                    officeBoxStart={orderProduct.officeBoxStart}
                    officeBoxEnd={orderProduct.officeBoxEnd}
                /> : null
            }
        </tr>
    );
}

export default OrderItem;
