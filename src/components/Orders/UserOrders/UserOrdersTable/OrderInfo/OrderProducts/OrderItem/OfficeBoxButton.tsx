import React from "react";

import {BoxSeam} from "react-bootstrap-icons";

type Props = {
    setIsOpen: (isOpen: boolean) => void;
};

const OfficeBoxButton: React.FC<Props> = ({setIsOpen}) => {
    return (
        <button
            type="button"
            className="btn btn-outline-info btn-block btn-group"
            onClick={() => setIsOpen(true)}
        >
            <h6 className="text-justify m-auto">SKRYTKA </h6>
            <BoxSeam className="mt-auto mb-auto mr-auto ml-1"/>
        </button>
    );
};

export default OfficeBoxButton;
