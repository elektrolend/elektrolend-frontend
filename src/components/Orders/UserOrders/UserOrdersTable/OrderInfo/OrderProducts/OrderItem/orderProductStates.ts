import {OrderProductState} from "../../../../../../../interfaces/Orders/OrderProductState";

interface IOrderProductStateInfo {
    title: string;
    color: string;
}

const orderProductStates = new Map<OrderProductState | null, IOrderProductStateInfo>([
        [OrderProductState.to_realization, {title: "DO REALIZACJI", color: "burlywood"}],
        [OrderProductState.paid, {title: "ZAPŁACONE", color: "magenta"}],
        [OrderProductState.canceled, {title: "ANULOWANE", color: "black"}],
        [OrderProductState.in_office_box, {title: "W SKRYTCE", color: "sienna"}],
        [OrderProductState.received, {title: "ODEBRANE", color: "green"}],
        [OrderProductState.damaged, {title: "USZKODZONE", color: "slateblue"}],
        [OrderProductState.not_damaged, {title: "NIEUSZKODZONE", color: "teal"}],
        [OrderProductState.in_office_box_complaint, {title: "W SKRYTCE PO REKLAMACJI", color: "darkslategray"}],
        [OrderProductState.complaint_for_consideration, {title: "REKLAMACJA DO ROZPATRZENIA", color: "tomato"}],
        [OrderProductState.complaint_accepted, {title: "REKLAMACJA ZAAKCEPTOWANA", color: "lime"}],
        [OrderProductState.complaint_not_accepted, {title: "REKLAMACJA NIEZAAKCEPTOWANA", color: "maroon"}],
        [OrderProductState.back_to_office_box, {title: "ZWROT DO SKRYTKI", color: "blue"}],
        [OrderProductState.back_to_office, {title: "ZWROT DO BIURA", color: "olive"}],
        [OrderProductState.refund, {title: "ZWROT", color: "orange"}],
        [OrderProductState.finished, {title: "ZAKOŃCZONE", color: "pink"}],
        [null, {title: "BRAK", color: "red"}]
    ]
);

export default orderProductStates;
