import {IOfficeBox} from "../../../../../../../interfaces/Orders/IOfficeBox";
import React from "react";

import PasswordBox from "../../../../../UpcomingOrders/OrderProductsTable/OrderProductItem/PasswordBox";

import {Modal} from "react-bootstrap";

type Props = {
    isOpen: boolean;
    setIsOpen: (isOpen: boolean) => void;
    boxPassword: string;
    officeBoxStart: IOfficeBox;
    officeBoxEnd: IOfficeBox;
};

const OfficeBoxModal: React.FC<Props> = ({
                                             isOpen,
                                             setIsOpen,
                                             boxPassword,
                                             officeBoxStart,
                                             officeBoxEnd
                                         }) => {

    const hideModal = () => {
        setIsOpen(false);
    };

    return (
        <Modal
            show={isOpen}
            onHide={hideModal}>
            <Modal.Header>
                <Modal.Title>Informacje o skrytce</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="container">
                    <div className="row bg-light shadow-sm border mb-3">
                        <div className="col-md-12 bg-primary text-white p-1 mb-2">
                            <h5 className="ml-3 mt-1">Skrytka do odbioru</h5>
                        </div>
                        <div className="col-md-6 form-group">
                            Nazwa
                            <input
                                type="text"
                                value={officeBoxStart.code}
                                className="form-control"
                                disabled={true}
                            >
                            </input>
                        </div>
                        <div className="col-md-6 form-group">
                            Numer
                            <input
                                type="text"
                                value={officeBoxStart.id}
                                className="form-control"
                                disabled={true}
                            >
                            </input>
                        </div>
                    </div>
                    <div className="row bg-light shadow-sm border mb-3">
                        <div className="col-md-12 bg-primary text-white p-1 mb-2">
                            <h5 className="ml-3 mt-1">Skrytka do zwrotu</h5>
                        </div>
                        <div className="col-md-6 form-group">
                            Nazwa
                            <input
                                type="text"
                                value={officeBoxEnd.code}
                                className="form-control"
                                disabled={true}
                            >
                            </input>
                        </div>
                        <div className="col-md-6 form-group">
                            Numer
                            <input
                                type="text"
                                value={officeBoxEnd.id}
                                className="form-control"
                                disabled={true}
                            >
                            </input>
                        </div>
                    </div>
                    <div className="row bg-light shadow-sm border">
                        <div className="col-md-12 bg-primary text-white p-1 mb-2">
                            <h5 className="ml-3 mt-1">Hasło do skrytki</h5>
                        </div>
                        <div className="col-md-12 form-group">
                            <PasswordBox boxPassword={boxPassword}/>
                        </div>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button
                    className="btn btn-danger"
                    type="submit"
                    onClick={hideModal}>
                    Zamknij
                </button>
            </Modal.Footer>
        </Modal>
    );
};

export default OfficeBoxModal;
