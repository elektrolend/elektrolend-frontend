import React from "react";

import OrderProductsTable from "./OrderProductsTable";
import {IUserOrderProduct} from "../../../../../../interfaces/Orders/IUserOrderProduct";

import {WalletFill, XCircleFill} from "react-bootstrap-icons";
import {OrderProductState} from "../../../../../../interfaces/Orders/OrderProductState";

type Props = {
    orderID: number;
    orderInfoList: IUserOrderProduct[];
    setOrderIDToPay: (orderID: number) => void;
    userOrderIndex: number;
};

const OrderProducts: React.FC<Props> = ({orderID, orderInfoList, setOrderIDToPay, userOrderIndex}) => {
    const orderProductsEmpty = () => {
        return orderInfoList.length === 0;
    };

    const checkOrderPayment = () => {
        for (const orderProduct of orderInfoList) {
            if (orderProduct.orderProductState !== OrderProductState.to_realization) {
                return false;
            }
        }
        return true;
    };

    return (
        <td
            id={"collapse_" + userOrderIndex}
            className="collapse border-0"
            colSpan={3}>
            <div className="bg-light">
                {orderProductsEmpty() ?
                    <div className="p-4 row align-items-center justify-content-center text-center">
                        <XCircleFill
                            className="col-md-12 mb-2"
                            size={50}/>
                        <h3>Brak informacji o zamówieniu</h3>
                    </div> :
                    <div className="row p-2 m-2">
                        <div className="col-md-12">
                            <OrderProductsTable
                                orderInfoList={orderInfoList}
                            />
                        </div>
                    </div>
                }
                {checkOrderPayment() ?
                    <div className="row p-2 m-2">
                        <div className="col-md-12 text-right">
                            <button
                                type="button"
                                className="btn btn-lg border btn-success text-white"
                                onClick={() => setOrderIDToPay(orderID)}
                            >
                                <WalletFill size={20}/> OPŁAĆ ZAMÓWIENIE
                            </button>
                        </div>
                    </div> : null}
            </div>
        </td>
    );
}
export default OrderProducts;
