import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";

interface IFilterModeInfo {
    title: string;
    state: OrderProductState;
}

const filterModes: IFilterModeInfo[] = [
    {
        title: "Wszystkie",
        state: OrderProductState.none
    },
    {
        title: "Do zapłaty",
        state: OrderProductState.to_realization
    },
    {
        title: "Zapłacone",
        state: OrderProductState.paid
    },
    {
        title: "Zakończone",
        state: OrderProductState.finished
    }
]

export default filterModes;
