import React from "react";

import PageSizeBar from "../../../Pagination/PageSizeBar";
import {OrderProductState} from "../../../../interfaces/Orders/OrderProductState";
import filterModes from "./filterModes";

type Props = {
    currentPageSize: number;
    setPageSize: (pageSize: number) => void;
    currentFilterMode: OrderProductState;
    setCurrentFilterMode: (orderProductState: OrderProductState) => void;
    setCurrentPageNumber: (pageNumber: number) => void;
    setMiddlePageNumber: (pageNumber: number) => void;
}

export const FilterBar: React.FC<Props> = ({
                                               currentPageSize,
                                               setPageSize,
                                               currentFilterMode,
                                               setCurrentFilterMode,
                                               setCurrentPageNumber,
                                               setMiddlePageNumber
                                           }) => {
    const isChecked = (orderState: OrderProductState) => {
        return orderState === currentFilterMode;
    };

    const resetPagination = () => {
        setCurrentPageNumber(1);
        setMiddlePageNumber(2);
    };

    const handleChangeFilterMode = (newFilterMode: OrderProductState) => {
        resetPagination();
        setCurrentFilterMode(newFilterMode);
    };

    return (
        <div className="border mb-3">
            <div className="bg-light m-2">
                <div className="row p-2 m-2">
                    <div className="col-md-9">
                        <nav className="nav nav-pills nav-fill">
                            {filterModes.map((filterMode, index) => {
                                return (
                                    <div
                                        key={"orderState_" + index}
                                        className={"nav-item nav-link " + (isChecked(filterMode.state) ? "active" : "")}
                                        onClick={() => handleChangeFilterMode(filterMode.state)}>
                                        {filterMode.title}
                                    </div>
                                );
                            })}
                        </nav>
                    </div>
                    <div className="col-md-3 justify-content-end allign-items-center text-right">
                        <PageSizeBar
                            currentPageSize={currentPageSize}
                            setPageSize={setPageSize}
                            resetPagination={resetPagination}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FilterBar;
