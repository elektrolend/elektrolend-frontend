import React from "react";
import ReactDOM from "react-dom";
import {QueryClient, QueryClientProvider} from "react-query";
import reportWebVitals from "./reportWebVitals";
import {Provider} from "react-redux";
import SimpleReactLightbox from "simple-react-lightbox";
import {Helmet} from "react-helmet";

import App from "./App";
import {store} from "./app/store";

const client = new QueryClient();
const TITLE = "ElektroLend";

ReactDOM.render(
    <QueryClientProvider client={client}>
        <Provider store={store}>
            <SimpleReactLightbox>
                <Helmet>
                    <title>{TITLE}</title>
                </Helmet>
                <App/>
            </SimpleReactLightbox>
        </Provider>
    </QueryClientProvider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
