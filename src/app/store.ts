import {configureStore} from '@reduxjs/toolkit'
import {authSlice} from "../components/Auth/AuthSlice";

const persistedState = localStorage.getItem('reduxState')
    ? JSON.parse(localStorage.getItem('reduxState') as string)
    : {}

export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        persistedState
    },
    preloadedState: persistedState
})

store.subscribe(() => {
    localStorage.setItem('reduxState', JSON.stringify(store.getState()))
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
