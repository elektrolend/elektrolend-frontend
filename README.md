# ElektroLend Frontend

## Demo link:
The application is available online at [ElektroLend](https://elektrolend.pl).

## Table of Content:

- [About The App](#about-the-app)
- [Features](#features)
- [Technologies](#technologies)
- [Setup](#setup)
- [Status](#status)

## About The App
Elektrolend is a web application that supports the rental of electronic equipment. The app allows the user to rent equipment for a specified period and rent it in a contact-free manner, using lockers. It also has an interface for an employee or administrator, which provides options for manipulating the assortment, tracking orders, or checking statistics. There is a universal API for connecting payment methods. The possibility of payment via the PayPal platform is implemented.

## Features

<b>NOTE</b>: You must click on the picture to watch the video showing the feature.

### Placing an order by the customer
The customer ordering process is as follows. This consists of selecting items and their rental periods, adding them to the cart, confirming the order, paying with PayPal and receiving access data to lockers.

[![Placing an order](http://img.youtube.com/vi/BNKeBsCEhT8/0.jpg)](http://www.youtube.com/watch?v=BNKeBsCEhT8 "Order")

### Order handling by an employee
A special section has been prepared for the employee where he can see items that should be placed in the lockers. In addition, each item in the list has a number and password. After putting an item in the locker, he can change the status of the item using the checkbox.

[![Order handling by an employee](http://img.youtube.com/vi/vgH3c7VL-vk/0.jpg)](http://www.youtube.com/watch?v=vgH3c7VL-vk "Order handling")

### User complaint
Each customer can make a complaint about an item if it is damaged. In the <b> user orders </b> section, for each ordered product, when it has been picked up from the locker, there is a COMPLAINT button that allows customer to fill out a form with a description and photos of the damaged equipment. Photos are uploaded to firebase storage.

[![User complaint](http://img.youtube.com/vi/iSlQns-H2nA/0.jpg)](http://www.youtube.com/watch?v=iSlQns-H2nA "User complaint")

### Consideration of the complaint by the employee
Two sections have been created for the employee related to the consideration of complaints. One is used to indicate that the item was picked up from the box after the customer's return. The second one allows you to see the details of the complaint and consider positively or negatively the complaint. In the event of a positive approval, the employee has the option of refunding the money directly to the paypal account of the user who made the order.

[![Consideration of the complaint by the employee](http://img.youtube.com/vi/ePqfsArPDDw/0.jpg)](http://www.youtube.com/watch?v=ePqfsArPDDw "Consideration of the complaint by the employee")

### Generating reports
The employee can generate loan, user and financial reports. Data export to pdf and csv formats is available.

[![Generating reports](http://img.youtube.com/vi/lwvp1TTc22s/0.jpg)](http://www.youtube.com/watch?v=lwvp1TTc22s "Generating reports")

## Technologies
![TypeScript](https://img.shields.io/badge/-TypeScript-000?&logo=TypeScript&logoColor=ddc508)
![React](https://img.shields.io/badge/-React-000?&logo=React)
![Redux](https://img.shields.io/badge/-redux-000?&logo=redux)
![Axios](https://img.shields.io/badge/-axios-000?&logo=axios)
![Formik](https://img.shields.io/badge/-formik-000?&logo=formik)
![Yup](https://img.shields.io/badge/-yup-000?&logo=yup)
![Reactrouter](https://img.shields.io/badge/-Reactrouter-000?&logo=Reactrouter)
![Bootstrap](https://img.shields.io/badge/-bootstrap-000?&logo=bootstrap)
![Firebase](https://img.shields.io/badge/-firebase-000?&logo=firebase)
![PayPal](https://img.shields.io/badge/-paypal-000?&logo=paypal)

## Setup

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  

To Start Server:

`npm start`  

To Visit App:

`localhost:3000`  


## Status
[ElektroLend] is done. All tasks have been implemented.
